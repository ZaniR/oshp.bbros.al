<?php
    include 'db.php';
    header('Content-Type: text/html; charset=utf-8');
    include_once 'simple_html_dom.php';

    $handle = curl_init();
        curl_setopt($handle, CURLOPT_URL, "https://oshp.rks-gov.net/sq/Reports/ComplaintByBlackList");
        curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
        $str = curl_exec($handle);
        curl_close($handle);

    $html = str_get_html($str);
    global $conn;
 
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }

    $pageNumber = 0;

    if(!empty($html->find('li.PagedList-skipToLast'))){
        $pageNumber = $html->find('li.PagedList-skipToLast',0);
        $pageNumber = explode('page=',$pageNumber->find('a',0)->href);
        $pageNumber = $pageNumber[1]{0};
    }else{
        $pageNumber = $html->find('li.PagedList-skipToNext',0);
        $pageNumber = explode('page=',$pageNumber->find('a',0)->href);
        $pageNumber = $pageNumber[1]{0};
    }

    $selection = $conn->prepare("SELECT emri_lendes,autoriteti_kontraktues,kerkesa_kunder_oe FROM liste_e_zeze");
    $selection->execute();
    $result = $selection->get_result();
    $blackList = array();
    while($row = $result->fetch_assoc()){
        $blackList[] = $row;
    }   

    $insertion = $conn->prepare("INSERT INTO liste_e_zeze (emri_lendes,autoriteti_kontraktues,kerkesa_kunder_oe,created_on,updated_on) values (?,?,?,?,?)");

    for($i=1;$i<=$pageNumber;$i++){
        $handle = curl_init();
            curl_setopt($handle, CURLOPT_URL, "https://oshp.rks-gov.net/sq/Reports/ComplaintByBlackList?page=".$i."&ReasonId=00000000-0000-0000-0000-000000000000&BlackListStatusId=00000000-0000-0000-0000-000000000000");
            curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
            $str = curl_exec($handle);
            curl_close($handle);

        $html = str_get_html($str); 
        $table = $html->find('div.table-responsive-vertical',0);
        $rows = $table->find('tr');
    
        foreach(array_slice($rows,1) as $row){
            $compArr  = array();
            $aktivitetiProkurimit = html_entity_decode($row->find('td',1)->plaintext);
            $compArr['emri_lendes'] = $aktivitetiProkurimit;
            $autoritetiKontraktues = html_entity_decode($row->find('td[data-label="Autoriteti kontraktues"]',0)->plaintext);
            $compArr['autoriteti_kontraktues'] =  $autoritetiKontraktues;
            $operatoriEkonomik = html_entity_decode($row->find('td[data-label="Operatori Ekonomik"]',0)->plaintext);
            $compArr['kerkesa_kunder_oe'] =  $operatoriEkonomik;
            $currentTime = date('Y/m/d H:i:s', time());

            if(in_array($compArr,$blackList)){
                continue;
            }

            $insertion->bind_param('sssss',$aktivitetiProkurimit,$autoritetiKontraktues,$operatoriEkonomik,$currentTime,$currentTime);
            $insertion->execute();
        }   
    }
    
    echo 'Work done!';
    $conn->close();
