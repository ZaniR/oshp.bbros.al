<?php
    include 'db.php';
    header('Content-Type: text/html; charset=utf-8');
    include_once 'simple_html_dom.php';

    $handle = curl_init();
        curl_setopt($handle, CURLOPT_URL, "https://oshp.rks-gov.net/sq/ProcurementActivities/Search");
        curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
        $str = curl_exec($handle);
        curl_close($handle);

    $html = str_get_html($str);
    global $conn;

    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }

    $selection = $conn->prepare("SELECT nr_protokollit FROM vendimet");
    $selection->execute();
    $result = $selection->get_result();
    $vendimet = array();
    while($row = $result->fetch_assoc()){
        $vendimet[] = $row['nr_protokollit'];
    }

    $insertion = $conn->prepare(
        "INSERT INTO vendimet(nr_protokollit,vlera_parashikuar,lloji_kontrates,autoriteti_kontraktues,oe_ankues,eksperti_shqyrtues,eksperti_teknik,created_on,updated_on) VALUES (?,?,?,?,?,?,?,?,?)"
    );

    $results = $html->find('p.resultatet');
    $pageNumberArr = explode(" ",trim($results[0]->plaintext));
    $pageNumber = $pageNumberArr[6] / 10;
    
    if(is_float($pageNumber)){
        $pageNumber = round($pageNumber) +1;
    }

    for($j=67;$j<=$pageNumber;$j++){

        $handle = curl_init();
        curl_setopt($handle, CURLOPT_URL, "https://oshp.rks-gov.net/sq/ProcurementActivities/Search?ContractingAuthority=System.Collections.Generic.List%601%5BOSHP.web.Models.ListItemOther%5D&RecommendedEO=System.Collections.Generic.List%601%5BOSHP.web.Models.ListItemOther%5D&TenderCategory=System.Collections.Generic.List%601%5BOSHP.web.Models.ListItemOther%5D&TenderValue=System.Collections.Generic.List%601%5BOSHP.web.Models.ListItemOther%5D&EconomicOperatorId=0&RecommendedEOId=0&ContractingAuthorityId=0&TenderCategoryId=0&TenderValueId=0&TotalComplaints=0&ComplaintStatusId=0&ContratValueFrom=0&ContratValueTo=0&OfferValueFrom=0&OfferValueTo=0&ProcurementActivities=System.Collections.Generic.List%601%5BOSHP.Web.Models.GetProcurementActivitieViewModel%5D&SelectedTags=System.Collections.Generic.List%601%5BSystem.String%5D&SelectedArticles=System.Collections.Generic.List%601%5BSystem.String%5D&page=".$j."&pageSize=10&pageNumber=0&");
        curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
        $str = curl_exec($handle);
        curl_close($handle);

        $html = str_get_html($str);
        $insertArray = array();
        
        $tables = $html->find('table.search-results-table');
        
        foreach($tables as $table){
            $tableContent = $table->find('div.pills-content',0);
            echo $tableContent;
            exit();
            $rows = $tableContent->find('tr');

            $nrIProkurimit = $rows[0]->find('td',1)->plaintext;
            if(in_array($nrIProkurimit,$vendimet)){
                continue;
            }

            $autoritetiKontraktues = html_entity_decode($rows[2]->find('td',1)->innertext);
            $vleraParashikuar = str_replace(',','.',$rows[4]->find('td',1)->plaintext);
            $dataEPranimit = $rows[10]->find('td',1)->plaintext;
            $oeAnkues = html_entity_decode($table->find('td[data-label="OE Ankues"]',0)->plaintext);
            $ekspertiShqyrtues = html_entity_decode($rows[13]->find('td',1)->plaintext);
            $ekspertiTeknik = html_entity_decode($rows[15]->find('td',1)->plaintext);
            $currentTime = date('Y/m/d H:i:s', time());

            $llojiIKontrates = "";
            if($vleraParashikuar < 10000){
                $llojiIKontrates = "Vlerë e vogel";
            }elseif($vleraParashikuar < 200000){
                $llojiIKontrates = "Vlerë e mesme";
            }else{
                $llojiIKontrates = "Vlerë e madhe";

            }
            
            $insertion->bind_param('sssssssss',$nrIProkurimit,$vleraParashikuar,$llojiIKontrates,$autoritetiKontraktues,$oeAnkues,$ekspertiShqyrtues,$ekspertiTeknik,$currentTime,$currentTime);
            $insertion->execute();
        }
    }

    $conn->close();
	echo "Work done!";


    






