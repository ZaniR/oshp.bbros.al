<?php 
class My404 extends MY_Controller 
{
 public function __construct() 
 {
    parent::__construct(); 
 } 

 public function index() 
 { 
    $this->view('errors/err404');
 } 
}