<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class HeqjePezullimi {

	public $id;
	public $nr_protokollit;
	public $emri_lendes;
	public $vlera_parashikuar;
	public $lloji_kontrates;
	public $autoriteti_kontraktuar;
	public $oe_rekomanduar;
	public $heqje_pezullimi;
	public $vendimi;
	public $neni;
	public $komente;
	public $kryetar;
	public $referent;
	public $anetari_1;
	public $anetari_2;
	public $anetari_3;
	public $nr_panelisteve;
	public $seanca;
	public $data_kerkeses;
	public $data_vendimit;
	public $data_publikimit;
	public $komente_gjenerale;

	public function __construct() {
		$vendimi = new StdClass;
		$vendimi->titulli = null;
		$vendimi->url = null;
		$this->vendimi = array('0' => $vendimi);
		return $this;
	}
}