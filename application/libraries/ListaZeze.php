<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class ListaZeze {

	public $id;
	public $nr_protokollit;
	public $emri_lendes;
	public $vlera_parashikuar;
	public $lloji_kontrates;
	public $autoriteti_kontraktues;
	public $kerkesa_kunder_oe;
	public $pretendimet_ak;
	public $vendimi;
	public $diskualifikimi_kohezgjatja;
	public $neni;
	public $komente_paneli;
	public $vendimi_liste_zeze;
	public $kryetar;
	public $referent;
	public $anetari_1;
	public $anetari_2;
	public $anetari_3;
	public $nr_panelisteve;
	public $rekomandimi_ekspertit;
	public $aprovohet_kerkesa_e;
	public $refuzohet_kerkesa_e;
	public $komente_eksperti;
	public $eksperti_shqyrtues;
	public $eksperti_teknik;
	public $seanca;
	public $data_kerkeses;
	public $data_vendimit;
	public $data_publikimit;
	public $komente_gjenerale;

	public function __construct() {
		$vendimi_liste_zeze = new StdClass;
		$vendimi_liste_zeze->titulli = null;
		$vendimi_liste_zeze->url = null;
		$this->vendimi_liste_zeze = array('0' => $vendimi_liste_zeze);
		return $this;
	}
}