<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Vendimi {

	public $id;
	public $nr_protokollit;
	public $emri_lendes;
	public $vlera_parashikuar;
	public $lloji_kontrates;
	public $autoriteti_kontraktues;
	
	public $oe_ankues;
	public $ankesa_kunder;

	public $vendimi;
	public $terheqje_verejtje;
	public $neni;
	public $komente_paneli;
	public $nr_vendimit;
	public $vendimet_meparshme;
	public $vendimi_jokonsistent;
	public $konfiskim_tarifes;

	public $kryetar;
	public $referent;
	public $anetari_1;
	public $anetari_2;
	public $anetari_3;
	public $nr_panelisteve;

	public $rekomandimi_ekspertit;
	public $komente_eksperti;

	public $eksperti_shqyrtues;
	public $eksperti_teknik;

	public $seanca;

	public $data_ankeses;
	public $data_vendimit;
	public $data_publikimit;

	public $komente_gjenerale;
	public $ankesa;
	public $raporti_ekspertit;

	public $tags;

	public function __construct() {
		$vendimi = new StdClass;
		$vendimi->titulli = null;
		$vendimi->url = null;
		$this->nr_vendimit = array('0' => $vendimi);
		$this->vendimet_meparshme = array('0' => $vendimi);
		$this->vendimi_jokonsistent = array('0' => $vendimi);
		$this->ankesa = array('0' => $vendimi);
		$this->raporti_ekspertit = array('0' => $vendimi);

		$this->tags = array();
		return $this;
	}
}