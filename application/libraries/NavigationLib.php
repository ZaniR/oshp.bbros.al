<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class NavigationLib {

	public $id;
	public $name;
	public $description;
	public $url;
	public $parent;
	public $active;
	public $hidden;
	public $page_title;
	public $page_keyword;
	public $authorization;
	public $index;

	public function __construct() {
		$this->groups = array();
		return $this;
	}
}