<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Vendimet_model extends CI_Model {
	public function __construct() 
	{
		$this->load->library(array('ion_auth','Vendimi'));
	}
	private function _populate_object($data){
		$vendimet = new Vendimi();
		if(!$data){
			return $vendimet;
		}
		$vendimet->id 							= $data->id;

		$vendimet->nr_protokollit 				= $data->nr_protokollit;
		$vendimet->emri_lendes 					= $data->emri_lendes;
		$vendimet->vlera_parashikuar 			= $data->vlera_parashikuar;
		$vendimet->lloji_kontrates				= $data->lloji_kontrates;
		$vendimet->autoriteti_kontraktues 		= $data->autoriteti_kontraktues;

		$vendimet->oe_ankues 					= $data->oe_ankues;
		$vendimet->ankesa_kunder 				= $data->ankesa_kunder;

		$vendimet->vendimi 						= $data->vendimi;
		$vendimet->terheqje_verejtje 			= $data->terheqje_verejtje;
		$vendimet->neni 						= $data->neni;
		$vendimet->komente_paneli 				= $data->komente_paneli;
		$vendimet->nr_vendimit 					= $data->nr_vendimit ? json_decode($data->nr_vendimit) : $vendimet->nr_vendimit;
		$vendimet->vendimet_meparshme 			= $data->vendimet_meparshme ? json_decode($data->vendimet_meparshme) : $vendimet->vendimet_meparshme;
		$vendimet->vendimi_jokonsistent 		= $data->vendimi_jokonsistent ? json_decode($data->vendimi_jokonsistent) : $vendimet->vendimi_jokonsistent;
		$vendimet->konfiskim_tarifes 			= $data->konfiskim_tarifes;

		$vendimet->kryetar 						= $data->kryetar;
		$vendimet->referent 					= $data->referent;
		$vendimet->anetari_1 					= $data->anetari_1;
		$vendimet->anetari_2 					= $data->anetari_2;
		$vendimet->anetari_3 					= $data->anetari_3;
		$vendimet->nr_panelisteve 				= $data->nr_panelisteve;

		$vendimet->rekomandimi_ekspertit		= $data->rekomandimi_ekspertit;
		$vendimet->komente_eksperti				= $data->komente_eksperti;

		$vendimet->eksperti_shqyrtues			= $data->eksperti_shqyrtues;
		$vendimet->eksperti_teknik				= $data->eksperti_teknik;

		$vendimet->seanca 						= $data->seanca;

		$vendimet->data_ankeses 				= $data->data_ankeses;
		$vendimet->data_vendimit 				= $data->data_vendimit;
		$vendimet->data_publikimit 				= $data->data_publikimit;

		$vendimet->komente_gjenerale 			= $data->komente_gjenerale;
		$vendimet->pergjigja_ak					= $data->pergjigja_ak;
		$vendimet->pergjigja_oe					= $data->pergjigja_oe;
		$vendimet->kategoria_kontrates 			= $data->kategoria_kontrates;
		$vendimet->oferta_oe 					= $data->oferta_oe;
		$vendimet->depozita_ankeses 			= $data->depozita_ankeses;
		$vendimet->statusi_ankeses 				= $data->statusi_ankeses;
		$vendimet->njoftimi_ak 					= $data->njoftimi_ak;
		$vendimet->ankesa				 		= $data->ankesa ? json_decode($data->ankesa) : $vendimet->ankesa;
		$vendimet->teksti_kryesor				= $data->teksti_kryesor;
		$vendimet->eksperti_profesional			= $data->eksperti_profesional;
		$vendimet->njoftimi_ekspertizes			= $data->njoftimi_ekspertizes;
		$vendimet->raporti_ekspertit			= $data->raporti_ekspertit ? json_decode($data->raporti_ekspertit) : $vendimet->raporti_ekspertit;
		$vendimet->oferta_oe_rekomanduar		= $data->oferta_oe_rekomanduar;
		$vendimet->data_seances					= $data->data_seances;

		$vendimet->tags 						= $this->_get_tags($data->id);

		return $vendimet;
	}

	private function _create_db_array($vendimet){
		return array(
				'nr_protokollit' 				=> $vendimet->nr_protokollit,
				'emri_lendes' 					=> $vendimet->emri_lendes,
				'vlera_parashikuar' 			=> $vendimet->vlera_parashikuar,
				'lloji_kontrates'				=> $vendimet->lloji_kontrates,
				'autoriteti_kontraktues' 		=> $vendimet->autoriteti_kontraktues,
				'oe_ankues'						=> $vendimet->oe_ankues,
				'ankesa_kunder'					=> $vendimet->ankesa_kunder,
				'vendimi'						=> $vendimet->vendimi,
				'terheqje_verejtje'				=> $vendimet->terheqje_verejtje,
				'neni'							=> $vendimet->neni,
				'komente_paneli'				=> $vendimet->komente_paneli,
				'nr_vendimit'					=> $vendimet->nr_vendimit,
				'vendimet_meparshme'			=> $vendimet->vendimet_meparshme,
				'vendimi_jokonsistent'			=> $vendimet->vendimi_jokonsistent,
				'konfiskim_tarifes'				=> $vendimet->konfiskim_tarifes,
				'kryetar'						=> $vendimet->kryetar,
				'referent'						=> $vendimet->referent,
				'anetari_1'						=> $vendimet->anetari_1,
				'anetari_2'						=> $vendimet->anetari_2,
				'anetari_3'						=> $vendimet->anetari_3,
				'nr_panelisteve'				=> $vendimet->nr_panelisteve,
				'rekomandimi_ekspertit'			=> $vendimet->rekomandimi_ekspertit,
				'komente_eksperti'				=> $vendimet->komente_eksperti,
				'eksperti_shqyrtues'			=> $vendimet->eksperti_shqyrtues,
				'eksperti_teknik'				=> $vendimet->eksperti_teknik,
				'seanca'						=> $vendimet->seanca,
				'data_ankeses'					=> $vendimet->data_ankeses,
				'data_vendimit'					=> $vendimet->data_vendimit,
				'data_publikimit'				=> $vendimet->data_publikimit,
				'komente_gjenerale'				=> $vendimet->komente_gjenerale
				);
	}

	public function save($vendimet){
		$data = $this->_create_db_array($vendimet);
		$data['updated_by'] = $this->ion_auth->get_user_id();
		$data['updated_on'] = date('Y-m-d H:i:s');		
		if($vendimet->id){
			$data['nr_vendimit'] 			= $vendimet->nr_vendimit ? $vendimet->nr_vendimit : NULL;
			$data['vendimet_meparshme'] 	= $vendimet->vendimet_meparshme ? $vendimet->vendimet_meparshme : NULL;
			$data['vendimi_jokonsistent'] 	= $vendimet->vendimi_jokonsistent ? $vendimet->vendimi_jokonsistent : NULL;
			$this->db->where('id', $vendimet->id);
			$this->db->update('vendimet', $data);
		}else{
			$data['created_by'] = $this->ion_auth->get_user_id();
			$data['created_on'] = date('Y-m-d H:i:s');
			$data['nr_vendimit'] 			= $vendimet->nr_vendimit ? $vendimet->nr_vendimit : NULL;
			$data['vendimet_meparshme'] 	= $vendimet->vendimet_meparshme ? $vendimet->vendimet_meparshme : NULL;
			$data['vendimi_jokonsistent'] 	= $vendimet->vendimi_jokonsistent ? $vendimet->vendimi_jokonsistent : NULL;
			$this->db->insert('vendimet', $data);
			$vendimet->id = $this->db->insert_id();
		}
		$this->_save_tags($vendimet);
		return $vendimet;
	}

	private function _save_tags($vendimet){
		$this->db->where('vendimi_id', $vendimet->id)->delete('vendimet_tags');
		$data = array();
		foreach($vendimet->tags as $tag) {
			$data[] = array(
				'tag_id' 		=> $tag,
				'vendimi_id'	=> $vendimet->id,
				'created_by'	=> $this->ion_auth->get_user_id(),
				'created_on' 	=> date('Y-m-d H:i:s'),
				'updated_by' 	=> $this->ion_auth->get_user_id(), 
				'updated_on' 	=> date('Y-m-d H:i:s')
			);
		}
		if($data)
			$this->db->insert_batch('vendimet_tags', $data);
	}

	public function save_tags($vendimi_id, $tags){
		$this->db->where('vendimi_id', $vendimi_id)->delete('vendimet_tags');
		$data = array();
		foreach($tags as $tag) {
			$data[] = array(
				'tag_id' 		=> $tag,
				'vendimi_id'	=> $vendimi_id,
				'created_by'	=> $this->ion_auth->get_user_id(),
				'created_on' 	=> date('Y-m-d H:i:s'),
				'updated_by' 	=> $this->ion_auth->get_user_id(), 
				'updated_on' 	=> date('Y-m-d H:i:s')
			);
		}
		if($data){
			if($this->db->insert_batch('vendimet_tags', $data)){
				return true;
			}		
		}
	}

	public function object_from_post(){
		if($this->input->post('id')){
			$vendimet = $this->get_vendimet($this->input->post('id'));
		}else{
			$vendimet = new Vendimi();
		}

		$vendimet->nr_protokollit 				= $this->input->post('nr_protokollit') ? $this->input->post('nr_protokollit') : NULL;
		$vendimet->emri_lendes    				= $this->input->post('emri_lendes') ? $this->input->post('emri_lendes') : NULL;
		$vendimet->vlera_parashikuar			= $this->input->post('vlera_parashikuar') ? $this->input->post('vlera_parashikuar') : 'N/A';
		$vendimet->lloji_kontrates				= $this->input->post('lloji_kontrates') ? $this->input->post('lloji_kontrates') : NULL;
		$vendimet->autoriteti_kontraktues		= $this->input->post('autoriteti_kontraktues') ? $this->input->post('autoriteti_kontraktues') : NULL;
		$vendimet->oe_ankues					= $this->input->post('oe_ankues') ? $this->input->post('oe_ankues') : NULL;
		$vendimet->ankesa_kunder				= $this->input->post('ankesa_kunder') ? $this->input->post('ankesa_kunder') : NULL;
		$vendimet->vendimi 						= $this->input->post('vendimi') ? $this->input->post('vendimi') : NULL;
		$vendimet->terheqje_verejtje			= $this->input->post('terheqje_verejtje') ? $this->input->post('terheqje_verejtje') : NULL;
		$vendimet->neni 						= $this->input->post('neni') ? $this->input->post('neni') : NULL;
		$vendimet->komente_paneli				= $this->input->post('komente_paneli') ? $this->input->post('komente_paneli') : NULL;
		$nr_vendimit_array 						= array('titulli' => $this->input->post('titulli'), 'url' => $this->input->post('url'));
		$vendimet->nr_vendimit 					= json_encode($nr_vendimit_array ? array($nr_vendimit_array) : NULL);
		$vendimi_meparshem_array				= array('titulli' => $this->input->post('titulli_meparshem'), 'url' => $this->input->post('url_meparshem'));
		$vendimet->vendimet_meparshme			= json_encode($vendimi_meparshem_array ? array($vendimi_meparshem_array) : NULL);
		$vendimi_jokonsistent_array				= array('titulli' => $this->input->post('titulli_konsistent'), 'url' => $this->input->post('url_konsistent'));
		$vendimet->vendimi_jokonsistent			= json_encode($vendimi_jokonsistent_array ? array($vendimi_jokonsistent_array) : NULL);
		$vendimet->konfiskim_tarifes			= $this->input->post('konfiskim_tarifes') ? $this->input->post('konfiskim_tarifes') : NULL;
		$vendimet->kryetar						= $this->input->post('kryetar') ? $this->input->post('kryetar') : NULL;
		$vendimet->referent						= $this->input->post('referent') ? $this->input->post('referent') : NULL;
		$vendimet->anetari_1					= $this->input->post('anetari_1') ? $this->input->post('anetari_1') : NULL;
		$vendimet->anetari_2					= $this->input->post('anetari_2') ? $this->input->post('anetari_2') : NULL;
		$vendimet->anetari_3					= $this->input->post('anetari_3') ? $this->input->post('anetari_3') : NULL;
		$vendimet->nr_panelisteve				= $this->input->post('nr_panelisteve') ? $this->input->post('nr_panelisteve') : NULL;
		$vendimet->rekomandimi_ekspertit		= $this->input->post('rekomandimi_ekspertit') ? trim($this->input->post('rekomandimi_ekspertit')) : NULL;
		$vendimet->komente_eksperti				= $this->input->post('komente_eksperti') ? $this->input->post('komente_eksperti') : NULL;
		$vendimet->eksperti_shqyrtues			= $this->input->post('eksperti_shqyrtues') ? $this->input->post('eksperti_shqyrtues') : NULL;
		$vendimet->eksperti_teknik				= $this->input->post('eksperti_teknik') ? $this->input->post('eksperti_teknik') : NULL;
		$vendimet->seanca 						= $this->input->post('seanca') ? $this->input->post('seanca') : NULL;
		$vendimet->data_ankeses					= $this->input->post('data_ankeses') ? $this->input->post('data_ankeses') : NULL;
		$vendimet->data_vendimit				= $this->input->post('data_vendimit') ? $this->input->post('data_vendimit') : NULL;
		$vendimet->data_publikimit				= $this->input->post('data_publikimit') ? $this->input->post('data_publikimit') : NULL;
		$vendimet->komente_gjenerale			= $this->input->post('komente_gjenerale') ? $this->input->post('komente_gjenerale') : NULL;

		$vendimet->tags 						= $this->input->post('tags') ? $this->input->post('tags') : array();

		return $vendimet; 		

	}
	public function get_vendimet($id){
		if(!$id) return new Vendimi();
		$vendimet = $this->db->select('*')->from('vendimet')->where('id', $id)->get()->row();
		$vendimet = $this->_populate_object($vendimet);
		return $vendimet;
	}
	public function get_vendimet_data($fieldset = null, $order_by = null, $limit = null, $filters = null, $count_only = false){
    	if($fieldset && (is_array($fieldset) || is_object($fieldset))) {
			foreach($fieldset as $label => $value) {
				if($label && $this->db->field_exists($label, 'vendimet')) {
					if(is_array($value)) {
						if($value) {
							$this->db->or_like($label, $value);
						} else {
							$this->db->where('1=0');
						}
					} else {
						if(in_array($label, array('nr_vendimit', 'vendimet_meparshme', 'vendimi_jokonsistent'))){
							$value = str_replace("/", "\/", $value);
						}						
						$this->db->or_like($label, $value);
					}
				}
			}
		}
		if($filters){
			foreach($filters as $column => $value) {
				if(in_array($column, array('nr_vendimit', 'vendimet_meparshme', 'vendimi_jokonsistent'))){
					$value = str_replace("/", "\/", $value);
				}
				if($column == 'advanced_filters'){
					$tag_arr = isset($value['tags']) ? $value['tags'] : null;
					$datat_arr = isset($value['datat']) ? $value['datat'] : null;
					$vlerat_arr = isset($value['vlerat']) ? $value['vlerat'] : null;
					$ak_arr = isset($value['ak'][0]) ? reset($value['ak'][0]) : null;
					$oeankues_arr = isset($value['oeankues'][0]) ? reset($value['oeankues'][0]) : null;					
					$panelistet_arr = isset($value['panelistet'][0]) ? reset($value['panelistet'][0]) : null;
					$rekomandimet_arr = isset($value['rekomandimet'][0]) ? reset($value['rekomandimet'][0]) : null;
					$nenet_arr = isset($value['nenet'][0]) ? reset($value['nenet'][0]) : null;
					$vendimet_arr = isset($value['vendimet'][0]) ? reset($value['vendimet'][0]) : null;
					if(!empty($tag_arr)){
						$this->db->join('vendimet_tags vt', 'vt.vendimi_id = v.id', 'left');
						$this->db->where_in('vt.tag_id', $tag_arr);
						$this->db->group_by('v.id');
					}
					if(!empty($datat_arr)){
						if(!empty($datat_arr[0]['ngaData']) && !empty($datat_arr[1]['deriData'])){
							$this->db->where('v.data_publikimit >=', $datat_arr[0]['ngaData']);
							$this->db->where('v.data_publikimit <=', $datat_arr[1]['deriData']);
						}
					}
					if(!empty($vlerat_arr)){
						if(!empty($vlerat_arr[0]['ngaVlera']) && !empty($vlerat_arr[1]['deriVlera'])){
							$this->db->where('cast(v.vlera_parashikuar as DECIMAL(18,2)) >=', $vlerat_arr[0]['ngaVlera']);
							$this->db->where('cast(v.vlera_parashikuar as DECIMAL(18,2)) <=', $vlerat_arr[1]['deriVlera']);
						}
					}
					if(!empty($ak_arr)){
						$ak_where = '(';
					 	foreach($ak_arr as $key => $ak){
					 		if($ak){
					 			if($key != 0){
					 				$ak_where .= ' OR ';
					 			}
								$ak_where .= 'v.autoriteti_kontraktues = "'.$ak.'"';
							}
						}
						$ak_where .= ')';
						$this->db->where($ak_where);
					}
					if(!empty($oeankues_arr)){
						$oeankues_where = '(';
					 	foreach($oeankues_arr as $key => $oeankues){
					 		if($oeankues){
					 			if($key != 0){
					 				$oeankues_where .= ' OR ';
					 			}
								$oeankues_where .= 'v.oe_ankues = "'.$oeankues.'"';
							}
						}
						$oeankues_where .= ')';
						$this->db->where($oeankues_where);
					}										
					if(!empty($panelistet_arr)){
						$pan_where = '(';
						$paneli_column_arr = array('kryetar', 'referent', 'anetari_1', 'anetari_2', 'anetari_3');
						foreach ($paneli_column_arr as $key => $pan_column) {						
					 		foreach($panelistet_arr as $key2 => $panelist){
					 			if(($key == 0) && ($key2 == 0)){
					 				// do nothing
					 			}else{
					 				$pan_where .= ' OR ';
					 			}
					 			$pan_where .= 'v.'.$pan_column.' = "'.$panelist.'"';
					 		}
						}
						$pan_where .= ')';
						$this->db->where($pan_where);
					}
					if(!empty($rekomandimet_arr)){
						$rek_where = '(';
					 	foreach($rekomandimet_arr as $key => $rekomandimi){
					 		if($rekomandimi){
					 			if($key != 0){
					 				$rek_where .= ' OR ';
					 			}
								$rek_where .= 'v.rekomandimi_ekspertit = "'.$rekomandimi.'"';
							}
						}
						$rek_where .= ')';
						$this->db->where($rek_where);
					}
					if(!empty($nenet_arr)){
						$nen_where = '(';
					 	foreach($nenet_arr as $key => $neni){
					 		if($neni){
					 			if($key != 0){
					 				$nen_where .= ' OR ';
					 			}
					 			$nen_where .= 'FIND_IN_SET("'.$neni.'", v.neni)';
					 			//$nen_where .= 'v.neni LIKE "%'.$neni.'%"';
							}
						}
						$nen_where .= ')';
						$this->db->where($nen_where);
					}
					if(!empty($vendimet_arr)){
						$ven_where = '(';
					 	foreach($vendimet_arr as $key => $vendimi){
					 		if($vendimi){
					 			if($key != 0){
					 				$ven_where .= ' OR ';
					 			}
								$ven_where .= 'v.vendimi = "'.$vendimi.'"';
							}
						}
						$ven_where .= ')';
						$this->db->where($ven_where);
					}
				}else{
					$this->db->like($column, $value);
				}
			}
		}
		if($order_by) {
			if($this->db->field_exists($order_by['column'], 'vendimet')){
				$this->db->order_by('v.'.$order_by['column'], $order_by['direction']);
			}else{
				$this->db->order_by('v.id', 'DESC');
			}
		}else{
			$this->db->order_by('v.id', 'DESC');
		}
		if($limit['length'] != -1){
			$this->db->limit($limit['length'], $limit['start']);
		}
		$result = array();
		if($count_only){
    		$vendimet = $this->db->select('COUNT(v.id) as total')->from('vendimet v')->get()->row();
    		$vendimet_total = 0;
    		if(isset($vendimet->total)){
    			$vendimet_total = $vendimet->total;
    		}
    		return $vendimet_total;
    	}else{
    		$vendimet = $this->db->select('*')->from('vendimet v')->get()->result();
    	}
    	foreach ($vendimet as $row) {
    		$result[] = $this->_populate_object($row);
    	}
    	return $result;
    }
    public function get_total_vendimet(){
    	$result = $this->db->select('COUNT(id) as total')->from('vendimet')->get()->row();
    	return $result->total;
    }
    public function delete_vendimet($id){
    	$vendimet = $this->get_vendimet($id);
		if($this->db->where('id', $id)->delete('vendimet')){
			$this->db->where('vendimi_id', $id)->delete('vendimet_tags');
			return true;
		}
    }
    public function get_last_updated(){
    	$result = $this->db->select('MAX(updated_on) as last_updated')->from('vendimet')->get()->row();
    	return $result->last_updated;
    }

    public function get_all_unique_used_tags(){
    	$result = $this->db->select('t.id, t.emri')->from('vendimet v')->join('vendimet_tags vt', 'v.id = vt.vendimi_id')->join('tags t', 't.id = vt.tag_id')->group_by('t.id')->get()->result();
    	return $result;
    }

    // MAIN PAGE ONLY FUNCTIONS
    public function get_all_years(){
    	$year_arr[] = $this->db->select('MIN(YEAR(data_publikimit)) as min')->from('vendimet')->get()->row()->min;
    	$year_arr[] = $this->db->select('MAX(YEAR(data_publikimit)) as max')->from('vendimet')->get()->row()->max;

    	$year_arr[] = $this->db->select('MIN(YEAR(data_publikimit)) as min')->from('liste_e_zeze')->get()->row()->min;
    	$year_arr[] = $this->db->select('MAX(YEAR(data_publikimit)) as max')->from('liste_e_zeze')->get()->row()->max;

    	$year_arr[] = $this->db->select('MIN(YEAR(data_publikimit)) as min')->from('gjobe')->get()->row()->min;
    	$year_arr[] = $this->db->select('MAX(YEAR(data_publikimit)) as max')->from('gjobe')->get()->row()->max;

    	$year_arr[] = $this->db->select('MIN(YEAR(data_publikimit)) as min')->from('heqje_pezullimi')->get()->row()->min;
    	$year_arr[]= $this->db->select('MAX(YEAR(data_publikimit)) as max')->from('heqje_pezullimi')->get()->row()->max;
    	$years = array_unique(array_filter($year_arr));
    	rsort($years);
    	return $years;
    }

    public function get_total_vendime_by_filter($table = 'vendimet', $year = null){
    	if($year){
    		if($table == 'vendimet'){
    			$column = 'data_publikimit';
    		}else{
    			$column = 'data_publikimit';
    		}
    		$this->db->where('YEAR('.$column.')', $year);
    	}

 		$total = $this->db->select('COUNT("id") as total')->from($table)->get()->row()->total;
 		return $total;
    }

    public function get_autocomplete_data($table, $column, $value){
    	$value = strtolower($value);
		$i_patterns = array();
		$i_patterns[0] = '/e/';
		$i_patterns[1] = '/c/';
		$i_replacements = array();
		$i_replacements[0] = 'ë';
		$i_replacements[1] = 'ç';
		
		$replace_value = preg_replace($i_patterns, $i_replacements, $value);
 		
 		if(in_array($column, array('vendimi', 'vendimi_liste_zeze', 'nr_vendimit', 'vendimet_meparshme', 'vendimi_jokonsistent'))){
 			if(($column == 'vendimi') && ($table == 'liste_e_zeze' || $table == 'vendimet')){
 				// do nothing
 			}else{
 				$value = str_replace("/", "\/", $value);
 				$replace_value = str_replace("/", "\/", $replace_value);
 			}
 		}
    	$result = $this->db->select('DISTINCT '.$column, false)->from($table)->or_like($column, $value)->or_like($column, $replace_value)->get()->result();
    	$return = array();
    	foreach($result as $row){
			if(in_array($column, array('vendimi', 'vendimi_liste_zeze', 'nr_vendimit', 'vendimet_meparshme', 'vendimi_jokonsistent'))){
				if(($column == 'vendimi') && ($table == 'liste_e_zeze' || $table == 'vendimet')){
					$end_value = preg_replace('/ç/', 'c', preg_replace('/ë/', 'e', $row->$column));
				}else{
					$column_arr = json_decode($row->$column);
					$end_value = $column_arr[0]->titulli;
				}
			}else{
			    $end_value = preg_replace('/ç/', 'c', preg_replace('/ë/', 'e', $row->$column));
			}
    		$return[] = $end_value;
    	}
    	return $return;
    }
    private function _get_tags($vendimi_id) {
        $tags = $this->db->select('t.id, t.emri')->from('vendimet_tags vt')->join('tags t', 't.id = vt.tag_id')->where('vt.vendimi_id', $vendimi_id)->get()->result();
        return $tags;
    }

    public function get_tags(){
        $tags = $this->db->select('t.id, t.emri')
                        ->from('tags t')
                        ->get()->result();
        return $tags;
    }

    public function get_unique_fields_by_column($table = null, $column = null){
    	$result = array();
    	if(!$table || !$column) return $result;
    	if($this->db->table_exists($table) && $this->db->field_exists($column, $table)) {
    		if($column == 'neni'){
    			$fields = $this->db->select($column)->from($table)->where($column . '!=', NULL)->where($column . '!=', '')->get()->result();
    			$nenet = array();
	    		foreach ($fields as $field) {
	    			$nen_arr = explode(',', $field->$column);
	    			foreach ($nen_arr as $neni) {
	    				$nenet[] = trim($neni);
	    			}
	    		}

	    		$result = array_unique(array_filter($nenet));
	    		sort($result);
    		}else{
	    		$fields = $this->db->select('DISTINCT '.$column, false)->from($table)->where($column . '!=', NULL)->where($column . '!=', '')->order_by($column, 'ASC')->get()->result();
	    		foreach ($fields as $field) {
	    			$result[] = $field->$column;
	    		}
    		}
    	}elseif($this->db->table_exists($table) && ($column == 'paneli')) {
    		$panelistet = array();
    		$kryetar = $this->db->select('DISTINCT kryetar', false)->from($table)->where('kryetar !=', NULL)->where('kryetar !=', '')->get()->result();
    		foreach($kryetar as $k){
    			$panelistet[] = $k->kryetar;
    		}
    		$referent = $this->db->select('DISTINCT referent', false)->from($table)->where('referent !=', NULL)->where('referent !=', '')->get()->result();
    		foreach($referent as $r){
    			$panelistet[] = $r->referent;
    		}
    		$anetari_1 = $this->db->select('DISTINCT anetari_1', false)->from($table)->where('anetari_1 !=', NULL)->where('anetari_1 !=', '')->get()->result();
    		foreach($anetari_1 as $a1){
    			$panelistet[] = $a1->anetari_1;
    		}
    		$anetari_2 = $this->db->select('DISTINCT anetari_2', false)->from($table)->where('anetari_2 !=', NULL)->where('anetari_2 !=', '')->get()->result();
    		foreach($anetari_2 as $a2){
    			$panelistet[] = $a2->anetari_2;
    		}
    		$anetari_3 = $this->db->select('DISTINCT anetari_3', false)->from($table)->where('anetari_3 !=', NULL)->where('anetari_3 !=', '')->get()->result();
    		foreach($anetari_3 as $a3){
    			$panelistet[] = $a3->anetari_3;
    		}
    		$result = array_unique($panelistet);
    		sort($result);
    	}
    	return $result;
    }
}