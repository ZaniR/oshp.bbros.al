<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Navigation_model extends CI_Model
{
	public function __construct() 
	{
        $this->load->library('NavigationLib');
	}
	private function _populate_object($data){
		$navigation = new NavigationLib();
		if(!$data){
			return $navigation;
		}
		$navigation->id 		= $data->id;
		$navigation->name 		= $data->name;
		$navigation->description = $data->description;
		$navigation->url 		= $data->url;
		$navigation->parent 	= $this->_get_parent($data->parent);
		$navigation->active 	= $data->active;
		$navigation->hidden 	= $data->hidden;
		$navigation->page_title = $data->page_title;
		$navigation->page_keyword = $data->page_keyword;
		$navigation->authorization = $data->authorization;
		$navigation->index 		= $data->index;

		$navigation->groups 	= $this->_get_groups($data->id);
		return $navigation;
	}
	private function _get_parent($parent_id){
		$navigation = new NavigationLib();
		$parent = $this->db->select('*')->from('navigation')->where('id', $parent_id)->get()->row();
		if(!isset($parent->id)){
			return $navigation;
		}else{
			$navigation = $this->_populate_object($parent);
		}
		return $navigation;
	}
	private function _get_groups($id){
		$groups = $this->db->select('*')->from('groups_navigation')->where('navigation_id', $id)->get()->result();
		return $groups;
	}
	private function _save_groups($navigation){
		$this->db->where('navigation_id', $navigation->id)->delete('groups_navigation');
		$data = array();
		foreach($navigation->groups as $nav_group) {
			$data[] = array(
				'navigation_id' => $navigation->id,
				'group_id'	 => $nav_group,
				'_created_by' => $this->ion_auth->get_user_id(),
				'_created_on' => date('Y-m-d H:i:s'),
				'_updated_by' => $this->ion_auth->get_user_id(), 
				'_updated_on' => date('Y-m-d H:i:s')
			);
		}
		if($data)
			$this->db->insert_batch('groups_navigation', $data);
	}
	private function _create_db_array($navigation){
		return array(
			'name'			=> $navigation->name,
			'description'	=> $navigation->description,
			'url'			=> $navigation->url,
			'parent'		=> $navigation->parent,
			'active'		=> $navigation->active ? $navigation->active : 0,
			'hidden'		=> $navigation->hidden ? $navigation->hidden : 0,
			'page_title'	=> $navigation->page_title,
			'page_keyword'	=> $navigation->page_keyword,
			'authorization' => $navigation->authorization ? $navigation->authorization : 0,
			'index'			=> $navigation->index
		);
	}

	public function object_from_post(){
		if($this->input->post('id')){
			$navigation = $this->get_navigation($this->input->post('id'));
		}else{
			$navigation = new NavigationLib();
		}

		$navigation->name = $this->input->post('name') ? $this->input->post('name') : NULL;
		$navigation->description = $this->input->post('description') ? $this->input->post('description') : NULL;
		$navigation->url = $this->input->post('url') ? $this->input->post('url') : NULL;
		$navigation->parent = $this->input->post('parent') ? $this->input->post('parent') : NULL;
		$navigation->active = $this->input->post('active') ? $this->input->post('active') : NULL;
		$navigation->hidden = $this->input->post('hidden') ? $this->input->post('hidden') : NULL;
		$navigation->page_title = $this->input->post('page_title') ? $this->input->post('page_title') : NULL;
		$navigation->page_keyword = $this->input->post('page_keyword') ? $this->input->post('page_keyword') : NULL;
		$navigation->authorization = $this->input->post('authorization') ? $this->input->post('authorization') : NULL;
		$navigation->index = $this->input->post('index') ? $this->input->post('index') : NULL;

		$navigation->groups = $this->input->post('groups') ? $this->input->post('groups') : array();

		return $navigation;
	}	

	public function save($navigation){
		$data = $this->_create_db_array($navigation);
		$data['_updated_by'] = $this->ion_auth->get_user_id();
		$data['_updated_on'] = date('Y-m-d H:i:s');		
		if($navigation->id){
			$this->db->where('id', $navigation->id);
			$this->db->update('navigation', $data);
		}else{
			$data['_created_by'] = $this->ion_auth->get_user_id();
			$data['_created_on'] = date('Y-m-d H:i:s');
			$this->db->insert('navigation', $data);
			$navigation->id = $this->db->insert_id();
		}
		$this->_save_groups($navigation);
		return $navigation;
	}	
    public function get_navigation($id){
    	if(!$id) return new NavigationLib();
    	$navigation = $this->db->select('*')->from('navigation')->where('id', $id)->get()->row();
    	$navigation = $this->_populate_object($navigation);
    	return $navigation;
    }	
	public function get_navigations($fieldset = null, $order_by = null, $limit = null, $filters = null){
    	if($fieldset && (is_array($fieldset) || is_object($fieldset))) {
			foreach($fieldset as $label => $value) {
				if($label && $this->db->field_exists($label, 'navigation')) {
					if(is_array($value)) {
						if($value) {
							$this->db->or_like($label, $value);
						} else {
							$this->db->where('1=0');
						}
					} else {
						$this->db->or_like($label, $value);
					}
				}
			}
		}
		if($filters){
			foreach($filters as $column => $value) {
				$this->db->like($column, $value);
			}
		}		
		if($order_by) {
			$this->db->order_by($order_by['column'], $order_by['direction']);
		}
		if($limit['length'] != -1){
			$this->db->limit($limit['length'], $limit['start']);
		}
		$result = array();
    	$navigation = $this->db->select('*')->from('navigation')->get()->result();
    	foreach ($navigation as $row) {
    		$result[] = $this->_populate_object($row);
    	}
    	return $result;
    }
    public function get_total_navigations(){
    	$result = $this->db->select('COUNT(id) as total')->from('navigation')->get()->row();
    	return $result->total;
    }
    public function get_parent_navigations($id = null){
    	if($id){
    		$this->db->where('id !=', $id);
    	}
    	$result = $this->db->select('*')->from('navigation')->get()->result();
    	return $result;
    }
    public function delete($id){
		if($this->db->where('id', $id)->delete('navigation')){
			$this->db->set('parent', NULL);
			$this->db->where('parent', $id);
			$this->db->update('navigation');
			return true;
		}
    }
}