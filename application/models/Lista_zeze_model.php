<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Lista_zeze_model extends CI_Model {
	public function __construct() 
	{
		$this->load->library(array('ion_auth','ListaZeze'));
	}
	private function _populate_object($data){
		$listazeze = new ListaZeze();
		if(!$data){
			return $listazeze;
		}
		$listazeze->id 							= $data->id;

		$listazeze->nr_protokollit 				= $data->nr_protokollit;
		$listazeze->emri_lendes 				= $data->emri_lendes;
		$listazeze->vlera_parashikuar 			= $data->vlera_parashikuar;
		$listazeze->lloji_kontrates				= $data->lloji_kontrates;
		$listazeze->autoriteti_kontraktues 		= $data->autoriteti_kontraktues;

		$listazeze->kerkesa_kunder_oe 			= $data->kerkesa_kunder_oe;
		$listazeze->pretendimet_ak 				= $data->pretendimet_ak;

		$listazeze->vendimi 					= $data->vendimi;
		$listazeze->diskualifikimi_kohezgjatja 	= $data->diskualifikimi_kohezgjatja;
		$listazeze->neni 						= $data->neni;
		$listazeze->komente_paneli 				= $data->komente_paneli;
		$listazeze->vendimi_liste_zeze 		 	= $data->vendimi_liste_zeze ? json_decode($data->vendimi_liste_zeze) : $listazeze->vendimi_liste_zeze;

		$listazeze->kryetar 					= $data->kryetar;
		$listazeze->referent 					= $data->referent;
		$listazeze->anetari_1 					= $data->anetari_1;
		$listazeze->anetari_2 					= $data->anetari_2;
		$listazeze->anetari_3 					= $data->anetari_3;
		$listazeze->nr_panelisteve 				= $data->nr_panelisteve;

		$listazeze->rekomandimi_ekspertit		= $data->rekomandimi_ekspertit;
		
		$listazeze->komente_eksperti			= $data->komente_eksperti;
		$listazeze->eksperti_shqyrtues			= $data->eksperti_shqyrtues;
		$listazeze->eksperti_teknik				= $data->eksperti_teknik;

		$listazeze->seanca 						= $data->seanca;

		$listazeze->data_kerkeses 				= $data->data_kerkeses;
		$listazeze->data_vendimit 				= $data->data_vendimit;
		$listazeze->data_publikimit 			= $data->data_publikimit;
		
		$listazeze->komente_gjenerale 			= $data->komente_gjenerale;

		return $listazeze;
	}

	private function _create_db_array($listazeze){
		return array(
				'nr_protokollit' 			=> $listazeze->nr_protokollit,
				'emri_lendes' 				=> $listazeze->emri_lendes,
				'vlera_parashikuar' 		=> $listazeze->vlera_parashikuar,
				'lloji_kontrates'			=> $listazeze->lloji_kontrates,
				'autoriteti_kontraktues' 	=> $listazeze->autoriteti_kontraktues,
				'kerkesa_kunder_oe' 		=> $listazeze->kerkesa_kunder_oe,
				'pretendimet_ak' 			=> $listazeze->pretendimet_ak,
				'vendimi' 					=> $listazeze->vendimi,
				'diskualifikimi_kohezgjatja'=> $listazeze->diskualifikimi_kohezgjatja,
				'neni' 						=> $listazeze->neni,
				'komente_paneli' 			=> $listazeze->komente_paneli,
				'vendimi_liste_zeze'		=> $listazeze->vendimi_liste_zeze,
				'kryetar' 					=> $listazeze->kryetar,
				'referent' 					=> $listazeze->referent,
				'anetari_1' 				=> $listazeze->anetari_1,
				'anetari_2' 				=> $listazeze->anetari_2,
				'anetari_3' 				=> $listazeze->anetari_3,
				'nr_panelisteve' 			=> $listazeze->nr_panelisteve,
				'rekomandimi_ekspertit'		=> $listazeze->rekomandimi_ekspertit,
				'komente_eksperti' 			=> $listazeze->komente_eksperti,
				'eksperti_shqyrtues'		=> $listazeze->eksperti_shqyrtues,
				'eksperti_teknik'			=> $listazeze->eksperti_teknik,
				'seanca' 					=> $listazeze->seanca,
				'data_kerkeses' 			=> $listazeze->data_kerkeses,
				'data_vendimit' 			=> $listazeze->data_vendimit,
				'data_publikimit' 			=> $listazeze->data_publikimit,
				'komente_gjenerale' 		=> $listazeze->komente_gjenerale
				);
	}
	public function save($listazeze){
		$data = $this->_create_db_array($listazeze);
		$data['updated_by'] = $this->ion_auth->get_user_id();
		$data['updated_on'] = date('Y-m-d H:i:s');		
		if($listazeze->id){
			$data['vendimi_liste_zeze'] 	= $listazeze->vendimi_liste_zeze ? $listazeze->vendimi_liste_zeze : NULL;
			$this->db->where('id', $listazeze->id);
			$this->db->update('liste_e_zeze', $data);
		}else{
			$data['created_by'] = $this->ion_auth->get_user_id();
			$data['created_on'] = date('Y-m-d H:i:s');
			$data['vendimi_liste_zeze'] 	= $listazeze->vendimi_liste_zeze ? $listazeze->vendimi_liste_zeze : NULL;
	
			$this->db->insert('liste_e_zeze', $data);
			$listazeze->id = $this->db->insert_id();
		}
		return $listazeze;
	}

	public function object_from_post(){
		if($this->input->post('id')){
			$listazeze = $this->get_listazeze($this->input->post('id'));
		}else{
			$listazeze = new listaZeze();
		}

		$listazeze->nr_protokollit 			= $this->input->post('nr_protokollit') ? $this->input->post('nr_protokollit') : NULL;
		$listazeze->emri_lendes    			= $this->input->post('emri_lendes') ? $this->input->post('emri_lendes') : NULL;
		$listazeze->vlera_parashikuar		= $this->input->post('vlera_parashikuar') ? $this->input->post('vlera_parashikuar') : 'N/A';
		$listazeze->lloji_kontrates			= $this->input->post('lloji_kontrates') ? $this->input->post('lloji_kontrates') : NULL;
		$listazeze->autoriteti_kontraktuar	= $this->input->post('autoriteti_kontraktuar') ? $this->input->post('autoriteti_kontraktuar') : NULL;
		$listazeze->kerkesa_kunder_oe		= $this->input->post('kerkesa_kunder_oe') ? $this->input->post('kerkesa_kunder_oe') : NULL;
		$listazeze->pretendimet_ak			= $this->input->post('pretendimet_ak') ? $this->input->post('pretendimet_ak') : NULL;
		$listazeze->vendimi 				= $this->input->post('vendimi') ? $this->input->post('vendimi') : NULL;
		$listazeze->diskualifikimi_kohezgjatja	= $this->input->post('diskualifikimi_kohezgjatja') ? $this->input->post('diskualifikimi_kohezgjatja') : NULL;
		$listazeze->neni 					= $this->input->post('neni') ? $this->input->post('neni') : NULL;
		$listazeze->komente_paneli 			= $this->input->post('komente_paneli') ? $this->input->post('komente_paneli') : NULL;		
		$vendimi_liste_zeze_array 			= array('titulli' => $this->input->post('titulli'), 'url' => $this->input->post('url'));
		$listazeze->vendimi_liste_zeze   	= json_encode($vendimi_liste_zeze_array ? array($vendimi_liste_zeze_array) : NULL);
		$listazeze->kryetar 				= $this->input->post('kryetar') ? $this->input->post('kryetar') : NULL;
		$listazeze->referent 				= $this->input->post('referent') ? $this->input->post('referent') : NULL;
		$listazeze->anetari_1				= $this->input->post('anetari_1') ? $this->input->post('anetari_1') : NULL;
		$listazeze->anetari_2 				= $this->input->post('anetari_2') ? $this->input->post('anetari_2') : NULL;
		$listazeze->anetari_3 				= $this->input->post('anetari_3') ? $this->input->post('anetari_3') : NULL;
		$listazeze->nr_panelisteve 			= $this->input->post('nr_panelisteve') ? $this->input->post('nr_panelisteve') : NULL;
		$listazeze->rekomandimi_ekspertit	= $this->input->post('rekomandimi_ekspertit') ? $this->input->post('rekomandimi_ekspertit') : NULL;
		$listazeze->komente_eksperti 		= $this->input->post('komente_eksperti') ? $this->input->post('komente_eksperti') : NULL;
		$listazeze->eksperti_shqyrtues		= $this->input->post('eksperti_shqyrtues') ? $this->input->post('eksperti_shqyrtues') : NULL;
		$listazeze->eksperti_teknik			= $this->input->post('eksperti_teknik') ? $this->input->post('eksperti_teknik') : NULL;
		$listazeze->seanca 					= $this->input->post('seanca') ? $this->input->post('seanca') : NULL;
		$listazeze->data_kerkeses 			= $this->input->post('data_kerkeses') ? $this->input->post('data_kerkeses') : NULL;
		$listazeze->data_vendimit 			= $this->input->post('data_vendimit') ? $this->input->post('data_vendimit') : NULL;
		$listazeze->data_publikimit 		= $this->input->post('data_publikimit') ? $this->input->post('data_publikimit') : NULL;
		$listazeze->komente_gjenerale 		= $this->input->post('komente_gjenerale') ? $this->input->post('komente_gjenerale') : NULL;

		return $listazeze; 		

	}
	public function get_listazeze($id){
		if(!$id) return new listaZeze();
		$listazeze = $this->db->select('*')->from('liste_e_zeze')->where('id', $id)->get()->row();
		$listazeze = $this->_populate_object($listazeze);
		return $listazeze;
	}
	public function get_listazeze_data($fieldset = null, $order_by = null, $limit = null, $filters = null, $count_only = false){
    	if($fieldset && (is_array($fieldset) || is_object($fieldset))) {
			foreach($fieldset as $label => $value) {
				if($label && $this->db->field_exists($label, 'liste_e_zeze')) {
					if(is_array($value)) {
						if($value) {
							$this->db->or_like($label, $value);
						} else {
							$this->db->where('1=0');
						}
					} else {
						if($label == 'vendimi_liste_zeze'){
							$value = str_replace("/", "\/", $value);
						}
						$this->db->or_like($label, $value);
					}
				}
			}
		}
		if($filters){
			foreach($filters as $column => $value) {
				if($column == 'vendimi_liste_zeze'){
					$value = str_replace("/", "\/", $value);
				}
				$this->db->like($column, $value);
			}
		}
		if($order_by) {
			if($this->db->field_exists($order_by['column'], 'liste_e_zeze')){
				$this->db->order_by($order_by['column'], $order_by['direction']);
			}else{
				$this->db->order_by('id', 'DESC');
			}
		}else{
			$this->db->order_by('id', 'DESC');
		}
		if($limit['length'] != -1){
			$this->db->limit($limit['length'], $limit['start']);
		}
		$result = array();
		if($count_only){		
    		$listazeze = $this->db->select('COUNT(id) as total')->from('liste_e_zeze')->get()->row();
    		$listazeze_total = 0;
    		if(isset($listazeze->total)){
    			$listazeze_total = $listazeze->total;
    		}    		
    		return $listazeze_total;
    	}else{
    		$listazeze = $this->db->select('*')->from('liste_e_zeze')->get()->result();
    	}
    	foreach ($listazeze as $row) {
    		$result[] = $this->_populate_object($row);
    	}
    	return $result;
    }
    public function get_total_listazeze(){
    	$result = $this->db->select('COUNT(id) as total')->from('liste_e_zeze')->get()->row();
    	return $result->total;
    }
    public function delete_listazeze($id){
		if($this->db->where('id', $id)->delete('liste_e_zeze')){
			return true;
		}
    }
    public function get_last_updated(){
    	$result = $this->db->select('MAX(updated_on) as last_updated')->from('liste_e_zeze')->get()->row();
    	return $result->last_updated;
    }
}