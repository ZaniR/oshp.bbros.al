<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Heqje_pezullimi_model extends CI_Model {
	public function __construct() 
	{
		$this->load->library(array('ion_auth','HeqjePezullimi'));
	}
	private function _populate_object($data){
		$pezullimi = new HeqjePezullimi();
		if(!$data){
			return $pezullimi;
		}

		$pezullimi->id 						= $data->id;
		$pezullimi->nr_protokollit			= $data->nr_protokollit;
		$pezullimi->emri_lendes				= $data->emri_lendes;
		$pezullimi->vlera_parashikuar		= $data->vlera_parashikuar;
		$pezullimi->lloji_kontrates			= $data->lloji_kontrates;
		$pezullimi->autoriteti_kontraktuar	= $data->autoriteti_kontraktuar;
		$pezullimi->oe_rekomanduar			= $data->oe_rekomanduar;
		$pezullimi->heqje_pezullimi 		= $data->heqje_pezullimi;
		$pezullimi->vendimi 				= $data->vendimi ? json_decode($data->vendimi) : $pezullimi->vendimi;
		$pezullimi->neni 					= $data->neni;
		$pezullimi->komente					= $data->komente;
		$pezullimi->kryetar					= $data->kryetar;
		$pezullimi->referent 				= $data->referent;
		$pezullimi->anetari_1 				= $data->anetari_1;
		$pezullimi->anetari_2 				= $data->anetari_2;
		$pezullimi->anetari_3				= $data->anetari_3;
		$pezullimi->nr_panelisteve 			= $data->nr_panelisteve;
		$pezullimi->seanca 					= $data->seanca;
		$pezullimi->data_kerkeses			= $data->data_kerkeses;
		$pezullimi->data_vendimit			= $data->data_vendimit;
		$pezullimi->data_publikimit 		= $data->data_publikimit;
		$pezullimi->komente_gjenerale 		= $data->komente_gjenerale;

		return $pezullimi;
	}

	private function _create_db_array($pezullimi){
		return array(
				'nr_protokollit'			=> $pezullimi->nr_protokollit,
				'emri_lendes'				=> $pezullimi->emri_lendes,
				'vlera_parashikuar'			=> $pezullimi->vlera_parashikuar,
				'lloji_kontrates'			=> $pezullimi->lloji_kontrates,
				'autoriteti_kontraktuar'	=> $pezullimi->autoriteti_kontraktuar,
				'oe_rekomanduar'			=> $pezullimi->oe_rekomanduar,
				'heqje_pezullimi'			=> $pezullimi->heqje_pezullimi,
				'vendimi'					=> $pezullimi->vendimi,
				'neni'						=> $pezullimi->neni,
				'komente'					=> $pezullimi->komente,
				'kryetar'					=> $pezullimi->kryetar,
				'referent'					=> $pezullimi->referent,
				'anetari_1'					=> $pezullimi->anetari_1,
				'anetari_2'					=> $pezullimi->anetari_2,
				'anetari_3'					=> $pezullimi->anetari_3,
				'nr_panelisteve'			=> $pezullimi->nr_panelisteve,
				'seanca'					=> $pezullimi->seanca,
				'data_kerkeses'				=> $pezullimi->data_kerkeses,
				'data_vendimit'				=> $pezullimi->data_vendimit,
				'data_publikimit'			=> $pezullimi->data_publikimit,
				'komente_gjenerale'			=> $pezullimi->komente_gjenerale
				);
	}
	public function save($pezullimi){
		$data = $this->_create_db_array($pezullimi);
		$data['updated_by'] = $this->ion_auth->get_user_id();
		$data['updated_on'] = date('Y-m-d H:i:s');		
		if($pezullimi->id){
			$data['vendimi'] 	= $pezullimi->vendimi ? $pezullimi->vendimi : NULL;
			$this->db->where('id', $pezullimi->id);
			$this->db->update('heqje_pezullimi', $data);
		}else{
			$data['created_by'] = $this->ion_auth->get_user_id();
			$data['created_on'] = date('Y-m-d H:i:s');
			$data['vendimi'] 	= $pezullimi->vendimi ? $pezullimi->vendimi : NULL;
			$this->db->insert('heqje_pezullimi', $data);
			$pezullimi->id = $this->db->insert_id();
		}
		return $pezullimi;
	}

	public function object_from_post(){
		if($this->input->post('id')){
			$pezullimi = $this->get_pezullimi($this->input->post('id'));
		}else{
			$pezullimi = new HeqjePezullimi();
		}

		$pezullimi->nr_protokollit 			= $this->input->post('nr_protokollit') ? $this->input->post('nr_protokollit') : NULL;
		$pezullimi->emri_lendes    			= $this->input->post('emri_lendes') ? $this->input->post('emri_lendes') : NULL;
		$pezullimi->vlera_parashikuar		= $this->input->post('vlera_parashikuar') ? $this->input->post('vlera_parashikuar') : NULL;
		$pezullimi->lloji_kontrates			= $this->input->post('lloji_kontrates') ? $this->input->post('lloji_kontrates') : NULL;
		$pezullimi->autoriteti_kontraktuar	= $this->input->post('autoriteti_kontraktuar') ? $this->input->post('autoriteti_kontraktuar') : NULL;
		$pezullimi->oe_rekomanduar 			= $this->input->post('oe_rekomanduar') ? $this->input->post('oe_rekomanduar') : NULL;
		$pezullimi->heqje_pezullimi 		= $this->input->post('heqje_pezullimi') ? $this->input->post('heqje_pezullimi') : NULL; 
		$vendimi_array = array('titulli' => $this->input->post('titulli'), 'url' => $this->input->post('url'));
		$pezullimi->vendimi 				= json_encode($vendimi_array ? array($vendimi_array) : NULL);
		$pezullimi->neni 					= $this->input->post('neni') ? $this->input->post('neni') : NULL;
		$pezullimi->komente 				= $this->input->post('komente') ? $this->input->post('komente') : NULL;
		$pezullimi->kryetar 				= $this->input->post('kryetar') ? $this->input->post('kryetar') : NULL;
		$pezullimi->referent 				= $this->input->post('referent') ? $this->input->post('referent') : NULL;
		$pezullimi->anetari_1				= $this->input->post('anetari_1') ? $this->input->post('anetari_1') : NULL;
		$pezullimi->anetari_2 				= $this->input->post('anetari_2') ? $this->input->post('anetari_2') : NULL;
		$pezullimi->anetari_3 				= $this->input->post('anetari_3') ? $this->input->post('anetari_3') : NULL;
		$pezullimi->nr_panelisteve 			= $this->input->post('nr_panelisteve') ? $this->input->post('nr_panelisteve') : NULL;
		$pezullimi->seanca 					= $this->input->post('seanca') ? $this->input->post('seanca') : NULL;
		$pezullimi->data_kerkeses 			= $this->input->post('data_kerkeses') ? $this->input->post('data_kerkeses') : NULL;
		$pezullimi->data_vendimit 			= $this->input->post('data_vendimit') ? $this->input->post('data_vendimit') : NULL;
		$pezullimi->data_publikimit 		= $this->input->post('data_publikimit') ? $this->input->post('data_publikimit') : NULL;
		$pezullimi->komente_gjenerale 		= $this->input->post('komente_gjenerale') ? $this->input->post('komente_gjenerale') : NULL;

		return $pezullimi; 		

	}
	public function get_pezullimi($id){
		if(!$id) return new HeqjePezullimi();
		$pezullimi = $this->db->select('*')->from('heqje_pezullimi')->where('id', $id)->get()->row();
		$pezullimi = $this->_populate_object($pezullimi);
		return $pezullimi;
	}
	public function get_heqje_pezullimi_data($fieldset = null, $order_by = null, $limit = null, $filters = null, $count_only = false){
    	if($fieldset && (is_array($fieldset) || is_object($fieldset))) {
			foreach($fieldset as $label => $value) {
				if($label && $this->db->field_exists($label, 'heqje_pezullimi')) {
					if(is_array($value)) {
						if($value) {
							$this->db->or_like($label, $value);
						} else {
							$this->db->where('1=0');
						}
					} else {
						if($label == 'vendimi'){
							$value = str_replace("/", "\/", $value);
						}
						$this->db->or_like($label, $value);
					}
				}
			}
		}
		if($filters){
			foreach($filters as $column => $value) {
				if($column == 'vendimi'){
					$value = str_replace("/", "\/", $value);
				}
				$this->db->like($column, $value);
			}
		}
		if($order_by) {
			if($this->db->field_exists($order_by['column'], 'heqje_pezullimi')){
				$this->db->order_by($order_by['column'], $order_by['direction']);
			}else{
				$this->db->order_by('id', 'DESC');
			}
		}else{
			$this->db->order_by('id', 'DESC');
		}
		if($limit['length'] != -1){
			$this->db->limit($limit['length'], $limit['start']);
		}
		$result = array();
		if($count_only){
    		$pezullimet = $this->db->select('COUNT(id) as total')->from('heqje_pezullimi')->get()->row();
    		$pezullimet_total = 0;
    		if(isset($pezullimet->total)){
    			$pezullimet_total = $pezullimet->total;
    		}
    		return $pezullimet_total;
    	}else{
    		$pezullimet = $this->db->select('*')->from('heqje_pezullimi')->get()->result();
    	}
    	foreach ($pezullimet as $row) {
    		$result[] = $this->_populate_object($row);
    	}
    	return $result;
    }
    public function get_total_heqje_pezullimi(){
    	$result = $this->db->select('COUNT(id) as total')->from('heqje_pezullimi')->get()->row();
    	return $result->total;
    }
    public function delete_pezullimi($id){
		if($this->db->where('id', $id)->delete('heqje_pezullimi')){
			return true;
		}
    }
    public function get_last_updated(){
    	$result = $this->db->select('MAX(updated_on) as last_updated')->from('heqje_pezullimi')->get()->row();
    	return $result->last_updated;
    }
}