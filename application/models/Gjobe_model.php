<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Gjobe_model extends CI_Model {
	public function __construct() 
	{
		$this->load->library(array('ion_auth','Gjobe'));
	}
	private function _populate_object($data){
		$gjobe = new Gjobe();
		if(!$data){
			return $gjobe;
		}

		$gjobe->id 					= $data->id;
		$gjobe->nr_protokollit 		= $data->nr_protokollit;
		$gjobe->emri_lendes			= $data->emri_lendes;
		$gjobe->vlera_parashikuar	= $data->vlera_parashikuar;
		$gjobe->lloji_kontrates		= $data->lloji_kontrates;
		$gjobe->operatori_ekonomik	= $data->operatori_ekonomik;
		$gjobe->gjoba_kunder_ak		= $data->gjoba_kunder_ak;
		$gjobe->shuma 				= $data->shuma;
		$gjobe->terheqje_verejtje	= $data->terheqje_verejtje;
		$gjobe->heqje_licence		= $data->heqje_licence;
		$gjobe->vendimi				= $data->vendimi ? json_decode($data->vendimi) : $gjobe->vendimi;
		$gjobe->neni 				= $data->neni;
		$gjobe->komente_paneli		= $data->komente_paneli;
		$gjobe->kryetar 			= $data->kryetar;
		$gjobe->referent			= $data->referent;
		$gjobe->anetari_1			= $data->anetari_1;
		$gjobe->anetari_2			= $data->anetari_2;
		$gjobe->anetari_3			= $data->anetari_3;
		$gjobe->nr_panelisteve		= $data->nr_panelisteve;
		$gjobe->seanca 				= $data->seanca;
		$gjobe->data_kerkeses		= $data->data_kerkeses;
		$gjobe->data_vendimit		= $data->data_vendimit;
		$gjobe->data_publikimit		= $data->data_publikimit;
		$gjobe->komente_gjenerale	= $data->komente_gjenerale;

		return $gjobe;
	}

	private function _create_db_array($gjobe){
		return array(
				'nr_protokollit' 		=> $gjobe->nr_protokollit,
				'emri_lendes'			=> $gjobe->emri_lendes,
				'vlera_parashikuar'		=> $gjobe->vlera_parashikuar,
				'lloji_kontrates'		=> $gjobe->lloji_kontrates,
				'operatori_ekonomik'	=> $gjobe->operatori_ekonomik,
				'gjoba_kunder_ak'		=> $gjobe->gjoba_kunder_ak,
				'shuma'					=> $gjobe->shuma,
				'terheqje_verejtje'		=> $gjobe->terheqje_verejtje,
				'heqje_licence'			=> $gjobe->heqje_licence,
				'vendimi'				=> $gjobe->vendimi,
				'neni'					=> $gjobe->neni,
				'komente_paneli'		=> $gjobe->komente_paneli,
				'kryetar'				=> $gjobe->kryetar,
				'referent'				=> $gjobe->referent,
				'anetari_1'				=> $gjobe->anetari_1,
				'anetari_2'				=> $gjobe->anetari_2,
				'anetari_3'				=> $gjobe->anetari_3,
				'nr_panelisteve'		=> $gjobe->nr_panelisteve,
				'seanca'				=> $gjobe->seanca,
				'data_kerkeses'			=> $gjobe->data_kerkeses,
				'data_vendimit'			=> $gjobe->data_vendimit,
				'data_publikimit'		=> $gjobe->data_publikimit,
				'komente_gjenerale' 	=> $gjobe->komente_gjenerale
				);
	}
	public function save($gjobe){
		$data = $this->_create_db_array($gjobe);
		$data['updated_by'] = $this->ion_auth->get_user_id();
		$data['updated_on'] = date('Y-m-d H:i:s');		
		if($gjobe->id){
			$data['vendimi'] 	= $gjobe->vendimi ? $gjobe->vendimi : NULL;
			$this->db->where('id', $gjobe->id);
			$this->db->update('gjobe', $data);
		}else{
			$data['created_by'] = $this->ion_auth->get_user_id();
			$data['created_on'] = date('Y-m-d H:i:s');
			$data['vendimi'] 	= $gjobe->vendimi ? $gjobe->vendimi : NULL;
			$this->db->insert('gjobe', $data);
			$gjobe->id = $this->db->insert_id();
		}
		return $gjobe;
	}
	public function object_from_post(){
		if($this->input->post('id')){
			$gjobe = $this->get_gjobe($this->input->post('id'));
		}else{
			$gjobe = new Gjobe();
		}

		$gjobe->nr_protokollit 		= $this->input->post('nr_protokollit') ? $this->input->post('nr_protokollit') : NULL;
		$gjobe->emri_lendes			= $this->input->post('emri_lendes') ? $this->input->post('emri_lendes') : NULL;
		$gjobe->vlera_parashikuar	= $this->input->post('vlera_parashikuar') ? $this->input->post('vlera_parashikuar') : 'N/A';
		$gjobe->lloji_kontrates		= $this->input->post('lloji_kontrates') ? $this->input->post('lloji_kontrates') : NULL;
		$gjobe->operatori_ekonomik	= $this->input->post('operatori_ekonomik') ? $this->input->post('operatori_ekonomik') : NULL;
		$gjobe->gjoba_kunder_ak		= $this->input->post('gjoba_kunder_ak') ? $this->input->post('gjoba_kunder_ak') : NULL;
		$gjobe->shuma 				= $this->input->post('shuma') ? $this->input->post('shuma') : NULL;
		$gjobe->terheqje_verejtje 	= $this->input->post('terheqje_verejtje') ? $this->input->post('terheqje_verejtje') : NULL;
		$gjobe->heqje_licence		= $this->input->post('heqje_licence') ? $this->input->post('heqje_licence') : NULL;
		$vendimi_array 				= array('titulli' => $this->input->post('titulli'), 'url' => $this->input->post('url'));
		$gjobe->vendimi 			= json_encode($vendimi_array ? array($vendimi_array) : NULL);
		$gjobe->neni 				= $this->input->post('neni') ? $this->input->post('neni') : NULL;
		$gjobe->komente_paneli 		= $this->input->post('komente_paneli') ? $this->input->post('komente_paneli') : NULL;
		$gjobe->kryetar				= $this->input->post('kryetar') ? $this->input->post('kryetar') : NULL;
		$gjobe->referent			= $this->input->post('referent') ? $this->input->post('referent') : NULL;
		$gjobe->anetari_1			= $this->input->post('anetari_1') ? $this->input->post('anetari_1') : NULL;
		$gjobe->anetari_2			= $this->input->post('anetari_2') ? $this->input->post('anetari_2') : NULL;
		$gjobe->anetari_3			= $this->input->post('anetari_3') ? $this->input->post('anetari_3') : NULL;
		$gjobe->nr_panelisteve		= $this->input->post('nr_panelisteve') ? $this->input->post('nr_panelisteve') : NULL;
		$gjobe->seanca 				= $this->input->post('seanca') ? $this->input->post('seanca') : NULL;
		$gjobe->data_kerkeses		= $this->input->post('data_kerkeses') ? $this->input->post('data_kerkeses') : NULL;
		$gjobe->data_vendimit		= $this->input->post('data_vendimit') ? $this->input->post('data_vendimit') : NULL;
		$gjobe->data_publikimit		= $this->input->post('data_publikimit') ? $this->input->post('data_publikimit') : NULLL;
		$gjobe->komente_gjenerale 	= $this->input->post('komente_gjenerale') ? $this->input->post('komente_gjenerale') : NULL;

		return $gjobe;
 	}
	public function get_gjobe_data($fieldset = null, $order_by = null, $limit = null, $filters = null, $count_only = false){
    	if($fieldset && (is_array($fieldset) || is_object($fieldset))) {
			foreach($fieldset as $label => $value) {
				if($label && $this->db->field_exists($label, 'gjobe')) {
					if(is_array($value)) {
						if($value) {
							$this->db->or_like($label, $value);
						} else {
							$this->db->where('1=0');
						}
					} else {
						if($label == 'vendimi'){
							$value = str_replace("/", "\/", $value);
						}
						$this->db->or_like($label, $value);
					}
				}
			}
		}
		if($filters){
			foreach($filters as $column => $value) {
				if($column == 'vendimi'){
					$value = str_replace("/", "\/", $value);
				}				
				$this->db->like($column, $value);
			}
		}		
		if($order_by) {
			if($this->db->field_exists($order_by['column'], 'heqje_pezullimi')){
				$this->db->order_by($order_by['column'], $order_by['direction']);
			}else{
				$this->db->order_by('id', 'DESC');
			}
		}else{
			$this->db->order_by('id', 'DESC');
		}
		if($limit['length'] != -1){
			$this->db->limit($limit['length'], $limit['start']);
		}

		$result = array();
		if($count_only){
    		$gjobat = $this->db->select('COUNT(id) as total')->from('gjobe')->get()->row();
    		$gjobat_total = 0;
    		if(isset($gjobat->total)){
    			$gjobat_total = $gjobat->total;
    		}    		
    		return $gjobat_total;
    	}else{
    		$gjobat = $this->db->select('*')->from('gjobe')->get()->result();
    	}
    	foreach ($gjobat as $gjobe) {
    		$result[] = $this->_populate_object($gjobe);
    	}
    	return $result;
    }
    public function get_total_gjobe_data(){
    	$result = $this->db->select('COUNT(id) as total')->from('gjobe')->get()->row();
    	return $result->total;
    }
    public function get_gjobe($id){
    	if(!$id) return new Gjobe();
    	$gjobe = $this->db->select('*')->from('gjobe')->where('id', $id)->get()->row();
    	$gjobe = $this->_populate_object($gjobe);
    	return $gjobe;
    }
    public function delete_gjobe($id){
		if($this->db->where('id', $id)->delete('gjobe')){
			return true;
		}
    }
    public function get_last_updated(){
    	$result = $this->db->select('MAX(updated_on) as last_updated')->from('gjobe')->get()->row();
    	return $result->last_updated;
    }
}