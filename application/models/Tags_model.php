<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Tags_model extends CI_Model {

	public function __construct() 
	{
        parent::__construct();
        $this->load->library('TagsLib');
    }

	private function _populate_object($data){
		$tags = new TagsLib();
		if(!$data){
			return $tags;
		}
		$tags->id 	= $data->id;
		$tags->emri = $data->emri;

		return $tags;
	}

	private function _create_db_array($tags){
		return array(
				'emri'	=> $tags->emri
				);
	}

	public function save($tags){
		$data = $this->_create_db_array($tags);
		$data['updated_by'] = $this->ion_auth->get_user_id();
		$data['updated_on'] = date('Y-m-d H:i:s');		
		if($tags->id){
			$this->db->where('id', $tags->id);
			$this->db->update('tags', $data);
		}else{
			$data['created_by'] = $this->ion_auth->get_user_id();
			$data['created_on'] = date('Y-m-d H:i:s');
	
			$this->db->insert('tags', $data);
			$tags->id = $this->db->insert_id();
		}
		return $tags;
	}

	public function object_from_post(){
		if($this->input->post('id')){
			$tags = $this->get_tag($this->input->post('id'));
		}else{
			$tags = new TagsLib();
		}

		$tags->emri = $this->input->post('emri') ? $this->input->post('emri') : NULL;

		return $tags;
	}

	public function get_tag($id){
		if(!$id) return new TagsLib();
		$tag = $this->db->select('*')->from('tags')->where('id', $id)->get()->row();
		$tag = $this->_populate_object($tag);
		return $tag;
	}
	public function get_tags($fieldset = null, $order_by = null, $limit = null, $filters = null){
    	if($fieldset && (is_array($fieldset) || is_object($fieldset))) {
			foreach($fieldset as $label => $value) {
				if($label && $this->db->field_exists($label, 'tags')) {
					if(is_array($value)) {
						if($value) {
							$this->db->or_like($label, $value);
						} else {
							$this->db->where('1=0');
						}
					} else {
						$this->db->or_like($label, $value);
					}
				}
			}
		}
		if($filters){
			foreach($filters as $column => $value) {
				$this->db->like($column, $value);
			}
		}
		if($order_by) {
			if($this->db->field_exists($order_by['column'], 'tags')){
				$this->db->order_by($order_by['column'], $order_by['direction']);
			}else{
				$this->db->order_by('id', 'DESC');
			}
		}else{
			$this->db->order_by('id', 'DESC');
		}
		if($limit['length'] != -1){
			$this->db->limit($limit['length'], $limit['start']);
		}
		$result = array();
    	$tags = $this->db->select('*')->from('tags')->get()->result();
    	foreach ($tags as $tag) {
    		$result[] = $this->_populate_object($tag);
    	}
    	return $result;
    }
    
    public function get_last_updated(){
    	$result = $this->db->select('MAX(updated_on) as last_updated')->from('tags')->get()->row();
    	return $result->last_updated;
    }

    public function get_total_tags(){
    	$result = $this->db->select('COUNT(id) as total')->from('tags')->get()->row();
    	return $result->total;
    }

     public function delete_tag($id){
		if($this->db->where('id', $id)->delete('tags')){
			$this->db->where('tag_id', $id)->delete('vendimet_tags');
			return true;
		}
    }

}