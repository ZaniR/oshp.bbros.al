<section id="contact-section">
  <div class="container">
    <div class="row">
      <div class="col-md-4">
      </div>
      <div class="col-md-4">
      <p>A jeni të sigurt që dëshironi ta de-aktivizoni përdoruesin '<?php echo $user->username; ?>'</p>
      <form action="<?php echo base_url().'auth/deactivate/'.$user->id; ?>" method="post" accept-charset="utf-8">
        <table class="table">
        	<tr>
        		<td>PO<input type="radio" name="confirm" value="yes" checked="checked" /></td>
        	</tr>
        	<tr>
          		<td>JO<input type="radio" name="confirm" value="no" /></td>
      		</tr>
        </table>
 		<?php echo form_hidden($csrf); ?>
  		<?php echo form_hidden(array('id'=>$user->id)); ?>           
        <input type="submit" name="submit" class="btn" value="Dërgo">
      </form>
      </div>
      <div class="col-md-4">
      </div>
    </div>
  </div>  
</section>