<section id="contact-section">
  <div class="container">
    <div class="row">
      <div class="col-md-4"></div>
      <div class="col-md-4">
      <p>Ju lutem, vendosni Email-in qe të mund ta riktheni fjalëkalimin.</p>

      <div id="infoMessage"><?php echo $message;?></div>

      <div class="contact-form">
        <form method="post" action="<?php echo base_url(); ?>auth/forgot_password"" accept-charset="utf-8" role="form">
            <div class="form-group wow fadeInDown" data-wow-duration="500ms" data-wow-delay=".2s">
                <input type="email" placeholder="Email" class="form-control" name="identity" id="identity">
            </div>
               
            <div id="submit" class="wow fadeInDown" data-wow-duration="500ms" data-wow-delay=".4s">
                <input type="submit" name="submit" class="btn btn-default btn-send" value="Dërgo">
            </div>                      
        </form>
      </div>
      </div>
      <div class="col-md-4"></div>
    </div>
  </div>  
</section>