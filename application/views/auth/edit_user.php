<section id="contact-section">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div id="infoMessage" class="text-success"><?php echo $message;?></div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-4">
      </div>
      <div class="col-md-4">
      <form action="<?php echo base_url().uri_string(); ?>" method="post" accept-charset="utf-8">
        <div class="custom-well">
        <div class="form-group">
          <label for="Emri">Emri</label>
          <input type="<?php echo $first_name['type']; ?>" placeholder="Emri" class="form-control" name="<?php echo $first_name['name']; ?>" id="<?php echo $first_name['id']; ?>" value="<?php echo $first_name['value']; ?>">
        </div>
        <div class="form-group">
          <label for="Mbiemri">Mbiemri</label>
          <input type="<?php echo $last_name['type']; ?>" placeholder="Mbiemri" class="form-control" name="<?php echo $last_name['name']; ?>" id="<?php echo $last_name['id']; ?>" value="<?php echo $last_name['value']; ?>">
        </div>
        <div class="form-group">
          <label for="Kompania">Kompania</label>
          <input type="<?php echo $company['type']; ?>" placeholder="Kompania" class="form-control" name="<?php echo $company['name']; ?>" id="<?php echo $company['id']; ?>" value="<?php echo $company['value']; ?>">
        </div>
        <div class="form-group">
          <label for="Telefoni">Telefoni</label>
          <input type="<?php echo $phone['type']; ?>" placeholder="Telefoni" class="form-control" name="<?php echo $phone['name']; ?>" id="<?php echo $phone['id']; ?>" value="<?php echo $phone['value']; ?>">
        </div>
        <div class="form-group">
          <label for="Fjalëkalimi">Fjalëkalimi (nëse dëshironi)</label>
          <input type="<?php echo $password['type']; ?>" placeholder="Fjalëkalimi" class="form-control" name="<?php echo $password['name']; ?>" id="<?php echo $password['id']; ?>">
        </div>
        <div class="form-group">
          <label for="Konfirmo fjalëkalimin">Konfirmo fjalëkalimin (nëse dëshironi)</label>
          <input type="<?php echo $password_confirm['type']; ?>" placeholder="Konfirmo fjalëkalimin" class="form-control" name="<?php echo $password_confirm['name']; ?>" id="<?php echo $password_confirm['id']; ?>">
        </div>        
            <?php if ($this->ion_auth->is_admin() || ($this->ion_auth->in_group('moderators') && !($this->ion_auth->user()->row()->id == $user->id)) ): ?>

                <h4>Anëtar i grupeve</h4>
                <table class="table">
                <?php foreach ($groups as $group):?>
                  <tr>
                    <?php
                        $gID=$group['id'];
                        $checked = null;
                        $item = null;
                        foreach($currentGroups as $grp) {
                            if ($gID == $grp->id) {
                                $checked= ' checked="checked"';
                            break;
                            }
                        }
                    ?>
                    <td><input type="checkbox" name="groups[]" value="<?php echo $group['id'];?>"<?php echo $checked;?>></td>
                    <td><strong><?php echo htmlspecialchars($group['name'],ENT_QUOTES,'UTF-8');?></strong><br><small>(<?php echo $group['description']; ?>)</small></td>
                  </tr>
                <?php endforeach; ?>
                </table>

            <?php endif ?>

            <?php echo form_hidden('id', $user->id);?>
            <?php echo form_hidden($csrf); ?>
      </div>
      <input type="submit" name="submit" class="btn" value="Ruaj përdoruesin">
      </form>
      </div>
      <div class="col-md-4">
      </div>
    </div>
  </div>  
</section>
