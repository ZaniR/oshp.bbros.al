<section id="contact-section">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div id="infoMessage" class="text-success"><?php echo $message;?></div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-4">
      </div>
      <div class="col-md-4">
      <form action="<?php echo base_url().'auth/reset_password/'.$code; ?>" method="post" accept-charset="utf-8">
        <div class="form-group">
          <label for="Fjalëkalimi">Fjalëkalimi (së paku 8 karaktere)</label>
          <input type="<?php echo $new_password['type']; ?>" placeholder="Fjalëkalimi" class="form-control" name="<?php echo $new_password['name']; ?>" id="<?php echo $new_password['id']; ?>" pattern="<?php echo $new_password['pattern']; ?>">
        </div>
        <div class="form-group">
          <label for="Konfirmo_fjalëkalimin">Konfirmo fjalëkalimin</label>
          <input type="<?php echo $new_password_confirm['type']; ?>" placeholder="Konfirmo fjalëkalimin" class="form-control" name="<?php echo $new_password_confirm['name']; ?>" id="<?php echo $new_password_confirm['id']; ?>"  pattern="<?php echo $new_password_confirm['pattern']; ?>">
        </div> 
		<?php echo form_input($user_id);?>
		<?php echo form_hidden($csrf); ?>            
        <input type="submit" name="submit" class="btn" value="Ndrysho">
      </form>
      </div>
      <div class="col-md-4">
      </div>
    </div>
  </div>  
</section>