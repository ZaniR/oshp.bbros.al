<section id="contact-section">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div id="infoMessage" class="text-success"><?php echo $message;?></div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-4">
      </div>
      <div class="col-md-4">
      <form action="<?php echo base_url().'auth/create_user'; ?>" method="post" accept-charset="utf-8">
      <div class="custom-well">
        <div class="form-group">
          <label for="Emri">Emri</label>
          <input type="<?php echo $first_name['type']; ?>" placeholder="Emri" class="form-control" name="<?php echo $first_name['name']; ?>" id="<?php echo $first_name['id']; ?>">
        </div>
        <div class="form-group">
          <label for="Mbiemri">Mbiemri</label>
          <input type="<?php echo $last_name['type']; ?>" placeholder="Mbiemri" class="form-control" name="<?php echo $last_name['name']; ?>" id="<?php echo $last_name['id']; ?>">
        </div>
        <?php if($identity_column!=='email'): ?>
          <label for="Username">Username</label>
        <?php echo form_error('identity'); ?>
          <input type="<?php echo $identity['type']; ?>" placeholder="Username" class="form-control" name="<?php echo $identity['name']; ?>" id="<?php echo $identity['id']; ?>">
        <?php endif; ?>

        <div class="form-group">
          <label for="Kompania">Kompania</label>
          <input type="<?php echo $company['type']; ?>" placeholder="Kompania" class="form-control" name="<?php echo $company['name']; ?>" id="<?php echo $company['id']; ?>">
        </div>
        <div class="form-group">
          <label for="Email">Email-i</label>
          <input type="<?php echo $email['type']; ?>" placeholder="Email" class="form-control" name="<?php echo $email['name']; ?>" id="<?php echo $email['id']; ?>">
        </div>
        <div class="form-group">
          <label for="Telefoni">Telefoni</label>
          <input type="<?php echo $phone['type']; ?>" placeholder="Telefoni" class="form-control" name="<?php echo $phone['name']; ?>" id="<?php echo $phone['id']; ?>">
        </div>
        <div class="form-group">
          <label for="Fjalëkalimi">Fjalëkalimi</label>
          <input type="<?php echo $password['type']; ?>" placeholder="Fjalëkalimi" class="form-control" name="<?php echo $password['name']; ?>" id="<?php echo $password['id']; ?>">
        </div>
        <div class="form-group">
          <label for="Konfirmo fjalëkalimin">Konfirmo fjalëkalimin</label>
          <input type="<?php echo $password_confirm['type']; ?>" placeholder="Konfirmo fjalëkalimin" class="form-control" name="<?php echo $password_confirm['name']; ?>" id="<?php echo $password_confirm['id']; ?>">
        </div>        
        <?php echo form_hidden($csrf); ?>
      </div>
        <input type="submit" name="submit" class="btn" value="Krijo përdoruesin">
      </form>
      </div>
      <div class="col-md-4">
      </div>
    </div>
  </div>  
</section>