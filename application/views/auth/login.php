<section id="contact-section">
  <div class="container">
    <div class="row">
      <div class="col-md-4"></div>
      <div class="col-md-4">
      <div id="infoMessage"><?php echo $message;?></div>
      <div class="contact-form">
        <form method="post" action="<?php echo base_url(); ?>auth/login" accept-charset="utf-8" role="form">

            <div class="form-group wow fadeInDown" data-wow-duration="500ms" data-wow-delay=".2s">
                <label>Përdoruesi/Email</label>
                <input type="email" placeholder="Username/Email" class="form-control" name="identity" id="identity">
            </div>
            
            <div class="form-group wow fadeInDown" data-wow-duration="500ms" data-wow-delay=".4s">
                <label>Fjalëkalimi</label>
                <input type="password" placeholder="Fjalëkalimi" class="form-control" name="password" id="password" >
            </div>
               
            <div id="submit" class="wow fadeInDown" data-wow-duration="500ms" data-wow-delay=".6s">
                <input type="submit" name="submit" class="btn btn-default btn-send" value="Kyçu">
            </div>                      
        </form>
        <br>
        <p class="wow fadeInDown" data-wow-duration="500ms" data-wow-delay=".8s"><a href="<?php echo base_url(); ?>auth/forgot_password">Rikthimi i fjalëkalimit</a></p>
      </div>
      </div>
      <div class="col-md-4"></div>
    </div>
  </div>  
</section>
