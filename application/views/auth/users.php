<script>
$(document).ready(function() {
	var encodedLogo = '<?php echo $this->config->item("encodedLogo"); ?>';
    var users_table = $('#users').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'pageLength',
            {
                extend: 'excel',
                exportOptions: {
                    columns: [':visible :not(.not-export-col)']
                },
                title: 'Përdoruesit',                            
            },
            {
                extend: 'pdfHtml5',
                orientation: 'portrait',
                pageSize: 'LEGAL',
                exportOptions: {
                    columns: [':visible :not(.not-export-col)']             
                },
                title: 'Përdoruesit',
                customize: function ( doc ) {
                    doc.content.splice( 1, 0, {
                        margin: [ 0, 0, 0, 12 ],
                        alignment: 'center',
                        image: encodedLogo,
                        width: 170
                    } );
                }                          
            },
            {
                extend: 'print',
                exportOptions: {
                    columns: [':visible :not(.not-export-col)']            
                },
                title: 'Përdoruesit',                            
            },
            {
                extend: 'csvHtml5',
                exportOptions: {
                    columns: [':visible :not(.not-export-col)']             
                },
                title: 'Përdoruesit',                            
            }
        ],    	
        "language": {
                "processing": "Hang on. Loading Data...", //add a loading image,simply putting <img src="loader.gif" /> tag.
                buttons: {
                    pageLength: {
                    _: "Shfaq %d rreshta",
                    '-1': "Të gjithë rreshtat"
                    }
                },
                "info": "Shfaqur _START_ deri _END_ prej _TOTAL_ të dhënave",
                "paginate": {
                            "previous": "Faqja paraprake",
                            "next": "Faqja tjetër",
                            "first": "Faqja parë",
                            "last": "Faqja fundit"
                        },
                "search": ""
            },          
        "autoWidth": false,
        "lengthMenu": [ [15, 25, 50, -1], [15, 25, 50, "Të gjitha"] ]    	
    });
});
</script>
<section id="contact-section">
  <div class="container">
  	<div class="tags-section" style="padding: 5px 5px 5px 0px;">
  	<div class="row">
  		<div class="col-md-6">
  			<div id="infoMessage" class="text-success"><?php echo $message;?></div>
  		</div>
  		<div class="col-md-6">
  			<div class="pull-right">
				<a href="<?php echo base_url(); ?>auth/create_user" class="btn btn-default"><i class="fa fa-user"></i> Krijo përdorues</a>
				<?php if($this->ion_auth->is_admin()): ?>
				<a href="<?php echo base_url(); ?>auth/create_group" class="btn btn-default"><i class="fa fa-group"></i> Krijo grup</a>
				<?php endif; ?>
			</div>
		</div>
  	</div>
  	</div>
    <div class="row">
      <div class="col-md-12">
		<div class="table-responsive">
		<table class="table row-border compact" id="users" style="width:100%">
			<thead>
			<tr>
				<th>Emri</th>
				<th>Mbiemri</th>
				<th>Email-i</th>
				<th>Grupet</th>
				<th>Statusi</th>
				<th class="not-export-col"><?php echo lang('index_action_th');?></th>
			</tr>
			</thead>
			<tbody>
			<?php foreach ($users as $user):?>
				<tr>
		            <td><?php echo htmlspecialchars($user->first_name,ENT_QUOTES,'UTF-8');?></td>
		            <td><?php echo htmlspecialchars($user->last_name,ENT_QUOTES,'UTF-8');?></td>
		            <td><?php echo htmlspecialchars($user->email,ENT_QUOTES,'UTF-8');?></td>
					<td>
						<?php foreach ($user->groups as $group):?>
							<?php echo htmlspecialchars($group->name,ENT_QUOTES,'UTF-8') ;?><br />
		                <?php endforeach?>
					</td>
					<td><?php echo ($user->active) ? anchor("auth/deactivate/".$user->id, lang('index_active_link')) : anchor("auth/activate/". $user->id, lang('index_inactive_link'));?></td>
					<td class="not-export-col"><?php echo anchor("auth/edit_user/".$user->id, 'Edit') ;?></td>
				</tr>
			<?php endforeach;?>
			</tbody>
		</table>
		</div>
      </div>
    </div>
  </div>  
</section>