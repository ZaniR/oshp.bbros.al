<section id="contact-section">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div id="infoMessage" class="text-success"><?php echo $message;?></div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-4">
      </div>
      <div class="col-md-4">
      <form action="<?php echo base_url().'auth/create_group'; ?>" method="post" accept-charset="utf-8">
        <div class="custom-well">
        <div class="form-group">
          <label for="Emri_i_grupit">Emri i grupit</label>
          <input type="<?php echo $group_name['type']; ?>" placeholder="Emri i grupi" class="form-control" name="<?php echo $group_name['name']; ?>" id="<?php echo $group_name['id']; ?>">
        </div>
        <div class="form-group">
          <label for="Përshkrimi">Përshkrimi</label>
          <input type="<?php echo $description['type']; ?>" placeholder="Përshkrimi" class="form-control" name="<?php echo $description['name']; ?>" id="<?php echo $description['id']; ?>">
        </div>
        </div>    
        <input type="submit" name="submit" class="btn" value="Krijo grup">
      </form>
      </div>
      <div class="col-md-4">
      </div>
    </div>
  </div>  
</section>