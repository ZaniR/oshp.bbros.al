<section id="general-section" class="wrapper_404">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="text-center">
                    <h1 class="wow fadeInUp animated cd-headline slide" data-wow-delay=".4s" >404</h1>
                    <h2 class="wow fadeInUp animated" data-wow-delay=".6s">Opps! You have some problems</h2>
                    <p class="wow fadeInUp animated" data-wow-delay=".9s">The page you are looking for was moved, removed, renamed or might never existed.</p>
                </div>
            </div>
        </div>
    </div>
</section>