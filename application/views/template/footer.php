            <div class="black_background">
              <div class="container">
                <div class="row">
                  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <p><?php echo $this->lang->line('footer_text')?></p>
                  </div>
                </div>
              </div>
            </div>
            <!--
            ==================================================
            Footer Section Start
            ================================================== -->
            <footer id="footer">
                <div class="container">
                    <div class="row">
                        <div class="col-md-9">
                          <div class="copyright-left">
                            <p class="copyright">
                              <a href="http://www.dplus-ks.org/" target="_blank"><img src="<?php echo base_url(); ?>theme/images/logo.png" alt="Demokraci Plus" style="height:50px;" /></a> Copyright © <span><script>document.write(new Date().getFullYear())</script></span>
                            </p>
                          </div>
                        </div>
                        <div class="col-md-3">
                            <div class="copyright-right">
                                <p class="copyright"><?php echo $this->lang->line('perkrahur_nga')?><img src="<?php echo base_url(); ?>theme/images/usaid.png" style="height:50px;" /></p>
                            </div>
                        </div>
                    </div>
                </div>
            </footer> <!-- /#footer -->

	<!-- Template Javascript Files
	================================================== -->
	<!-- jquery -->
<!-- 	<script src="<?php echo base_url(); ?>theme/plugins/jQurey/jquery.min.js"></script>
 -->	<!-- Form Validation -->
    <script src="<?php echo base_url(); ?>theme/plugins/form-validation/jquery.form.js"></script> 
    <script src="<?php echo base_url(); ?>theme/plugins/form-validation/jquery.validate.min.js"></script>
	<!-- owl carouserl js -->
	<script src="<?php echo base_url(); ?>theme/plugins/owl-carousel/owl.carousel.min.js"></script>
	<!-- bootstrap js -->
	<script src="<?php echo base_url(); ?>theme/plugins/bootstrap/bootstrap.min.js"></script>
	<!-- wow js -->
	<script src="<?php echo base_url(); ?>theme/plugins/wow-js/wow.min.js"></script>
	<!-- slider js -->
	<script src="<?php echo base_url(); ?>theme/plugins/slider/slider.js"></script>
	<!-- Fancybox -->
	<script src="<?php echo base_url(); ?>theme/plugins/facncybox/jquery.fancybox.js"></script>
	<!-- template main js -->
	<script src="<?php echo base_url(); ?>theme/js/main.js"></script>
	
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-68434294-10"></script>
	<script>
  		window.dataLayer = window.dataLayer || [];
  		function gtag(){dataLayer.push(arguments);}
  		gtag('js', new Date());

  		gtag('config', 'UA-68434294-10');
	</script>

 	</body>
</html>