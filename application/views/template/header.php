<!DOCTYPE html>
<html class="no-js">
    <head>
        <!-- Basic Page Needs
        ================================================== -->
        <meta charset="utf-8">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <link rel="icon" href="<?php echo base_url(); ?>theme/images/favicon.ico">
        <title><?php echo isset($template->page_title) ? ((isset($template->url) && $template->url) ? $template->page_title.' | ' : '') : ''; ?>OSHP DPLUS</title>
        <meta name="description" content="<?php echo isset($template->description) ? $template->description : ''; ?>">
        <meta name="keywords" content="<?php echo isset($template->page_keyword) ? $template->page_keyword : ''; ?>">
        <meta name="author" content="">
        <!-- Mobile Specific Metas
        ================================================== -->
        <meta name="format-detection" content="telephone=no">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        
        <!-- Template CSS Files
        ================================================== -->
        <!-- Twitter Bootstrs CSS -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>theme/plugins/bootstrap/bootstrap.min.css">
        <!-- Ionicons Fonts Css -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>theme/plugins/ionicons/ionicons.min.css">
        <!-- animate css -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>theme/plugins/animate-css/animate.css">
        <!-- Hero area slider css-->
        <link rel="stylesheet" href="<?php echo base_url(); ?>theme/plugins/slider/slider.css">
        <!-- owl craousel css -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>theme/plugins/owl-carousel/owl.carousel.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>theme/plugins/owl-carousel/owl.theme.css">
        <!-- Fancybox -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>theme/plugins/facncybox/jquery.fancybox.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/font-awesome.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/jquery.dataTables.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/jquery-ui.css">

        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/buttons.dataTables.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap-datetimepicker.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>theme/plugins/chosen/chosen.css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/dataTables.searchHighlight.css" />
        <!-- template main css file -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>theme/css/style.css?v=<?php echo uniqid();?>">
        <style>
            .nav>li>a {
                padding-left: 11px;
                padding-right: 11px;
            }
        </style>

    </head>
    <body>
    <script src="<?php echo base_url(); ?>assets/js/jquery-1.11.1.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/jquery.dataTables.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/buttons.flash.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/jszip.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/pdfmake.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/vfs_fonts.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/buttons.html5.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/buttons.print.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/buttons.colVis.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/moment.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/dataTables.fixedColumns.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/dataTables.colReorder.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/dataTables.searchHighlight.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/jquery.highlight.js"></script>
    <script src="<?php echo base_url(); ?>theme/plugins/chosen/chosen.jquery.js"></script>

    <!-- Datepikcer js -->
    <script src="<?php echo base_url(); ?>assets/js/bootstrap-datetimepicker.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

        <!--
        ==================================================
        Header Section Start
        ================================================== -->
        <header id="top-bar" class="navbar-fixed-top animated-header">
            <div class="container">
                <div class="navbar-header">
                    <!-- responsive nav button -->
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    </button>
                    <!-- /responsive nav button -->
                    
                    <!-- logo -->
<!--                <div class="navbar-brand"> -->
                        <ul class="navbar-links nav navbar-nav">
                            <li><a href="<?php echo base_url(); ?>"><i class="fa fa-home"></i></a></li>
                            <li><a href="<?php echo base_url(); ?>about"><?php echo $this->lang->line('per_ne')?></a></li>
                            <li><a href="<?php echo base_url(); ?>main/howTo"><?php echo $this->lang->line('si_teperdoret')?></a></li>
                        </ul>
<!--                         <a href="<?php echo base_url(); ?>" >
                            <img src="<?php echo base_url(); ?>theme/images/logo.png" alt="" class="logo">
                        </a>
                    </div> -->
                    <!-- /logo -->
                </div>
                <!-- main menu -->
                <nav class="collapse navbar-collapse navbar-right" role="navigation">
                    <div class="main-menu">
                        <ul class="nav navbar-nav navbar-right">
                        <?php foreach($navigation as $nav): ?>
                            <?php if($this->ion_auth->is_admin() || $this->ion_auth->in_group('moderators') || $this->ion_auth->in_group('contributors')): ?>
                                <?php if($nav->url == 'admin'): ?>
                                    <?php if(!empty($nav->children)): ?>
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php echo $nav->name; ?> <span class="caret"></span></a>
                                        <div class="dropdown-menu">
                                            <ul>
                                                <?php foreach($nav->children as $child): ?>
                                                <li><a href="<?php echo base_url().$child->url; ?>"><?php echo $child->name; ?></a></li>
                                                <?php endforeach; ?>
                                            </ul>
                                        </div>
                                    </li> 
                                    <?php endif; ?> 
                                <?php endif; ?>
                            <?php else: ?> 
                                <?php if(!empty($nav->children)): ?>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php echo $nav->name; ?> <span class="caret"></span></a>
                                    <div class="dropdown-menu">
                                        <ul>
                                            <?php foreach($nav->children as $child): ?>
                                            <li><a href="<?php echo base_url().$child->url; ?>"><?php echo $child->name; ?>Zani</a></li>
                                            <?php endforeach; ?>
                                        </ul>
                                    </div>
                                </li> 
                                <?php else: ?>                              
                                <li><a href="<?php echo base_url().$nav->url; ?>"><?php echo $this->lang->line($nav->name); ?></a></li>
                                <?php endif; ?>                                
                            <?php endif; ?>    
                        <?php endforeach; ?>
                        <?php if($this->ion_auth->logged_in()): ?>
                            <li><a href="<?php echo base_url().'auth/logout'; ?>" title="Ç'KYÇU">Dalje <i class="fa fa-sign-out"></i></a></li>
                        <?php endif; ?>

                        <?php if($this->lang->line('language') == 'albanian'):?>
                            <li style="border: none;">
                                <a href="<?php echo base_url().'admin/languageswitcher/switchLang/english'; ?>">En</a>
                            </li>
                            <li>
                                <a href="<?php echo base_url().'admin/languageswitcher/switchLang/serbian'; ?>">Sr</a>
                            </li>
                        <?php endif; ?>
                        <?php if($this->lang->line('language') == 'serbian'): ?>
                            <li style="border: none;">
                                <a href="<?php echo base_url().'admin/languageswitcher/switchLang/albanian'; ?>">Al</a>
                            </li>
                            <li>
                                <a href="<?php echo base_url().'admin/languageswitcher/switchLang/english'; ?>">En</a>
                            </li>
                        <?php endif;?>
                        <?php if($this->lang->line('language') == 'english'):?>
                            <li style="border: none;">
                                <a href="<?php echo base_url().'admin/languageswitcher/switchLang/albanian'; ?>">Al</a>
                            </li>
                            <li>
                                <a href="<?php echo base_url().'admin/languageswitcher/switchLang/serbian'; ?>">Sr</a>
                            </li>
                        <?php endif;?>
                        </ul>
                    </div>
                </nav>
                <!-- /main nav -->
            </div>
        </header>


        <!-- 
        ================================================== 
            Global Page Section Start
        ================================================== -->
        <section class="global-page-header">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="block">
                            <h2><?php echo (isset($template->description)) ? $this->lang->line($template->description) : ''; ?></h2>
                            <ol class="breadcrumb">
<!--                                 <li>
                                    <a href="<?php echo base_url(); ?>">
                                        <i class="ion-ios-home"></i>
                                        Ballina
                                    </a>
                                </li>
                                <?php if(isset($template->url) && $template->url): ?>
                                <li class="active"><?php echo isset($template->name) ? $template->name: ''; ?></li>
                                <?php endif; ?> -->
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </section>