<style>
    .well{
        border-radius: 0px;
        margin-top:15px;
        box-shadow: none;
    }
</style>
<div class="row" id="legjenda">
    <div class="col-md-12">
    <ul class="well">
        <p>Legjendë:</p>
	<?php foreach($legjenda as $legjende): ?>
        <li id="<?php echo $legjende['item_id']; ?>">              
            <i class="<?php echo $legjende['icon']; ?>" style="<?php echo $legjende['style']; ?>"></i> - <?php echo $this->lang->line($legjende['description']); ?>
        </li>
	<?php endforeach; ?>
    </ul>
	</div>
</div>