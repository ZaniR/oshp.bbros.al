<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Vendimet extends MY_Controller {
	
	public function __construct() 
	{
        parent::__construct();
        $this->load->model(array('Vendimet_model', 'Heqje_pezullimi_model', 'Lista_zeze_model', 'Gjobe_model'));
    }

	public function index($type = null)
	{
		if($type == 'json'){
			$requestData = $_REQUEST;
			$columns = array(
				0	=> null,
				1	=> null,
				2	=> 'nr_protokollit',
				3	=> 'emri_lendes',
				4	=> 'vlera_parashikuar',
				5  	=> 'lloji_kontrates',
				6	=> 'autoriteti_kontraktues',
				7 	=> 'oe_ankues',
				8	=> 'rekomandimi_ekspertit',				
				9	=> 'ankesa_kunder',
				10	=> 'vendimi',
				11	=> 'terheqje_verejtje',
				12	=> 'neni',
				13	=> 'nr_vendimit',
				14	=> 'vendimet_meparshme',
				15	=> 'vendimi_jokonsistent',
				16	=> 'konfiskim_tarifes',
				17	=> 'kryetar',
				18	=> 'referent',
				19	=> 'anetari_1',
				20	=> 'anetari_2',
				21	=> 'anetari_3',
				22	=> 'nr_panelisteve',
				23	=> 'eksperti_shqyrtues',
				24	=> 'eksperti_teknik',
				25	=> 'seanca',
				26	=> 'data_ankeses',
				27	=> 'data_vendimit',
				28	=> 'data_publikimit',
				29  => 'pergjigja_ak',
				30 	=> 'pergjigja_oe',
				31 	=> 'kategoria_kontrates',
				32 	=> 'oferta_oe',
				33	=> 'depozita_ankeses',
				34	=> 'statusi_ankeses',
				35	=> 'njoftimi_ak',
				36	=> 'ankesa',
				37	=> 'teksti_kryesor',
				38	=> 'eksperti_profesional',
				39 	=> 'njoftimi_ekspertizes',
				40	=> 'raporti_ekspertit',
				41 	=> 'oferta_oe_rekomanduar',
				42  => 'data_seances'
			);
			$totalData = $this->Vendimet_model->get_total_vendimet();
			$totalFiltered = $totalData;
			$fieldset = null;
			if(!empty($requestData['search']['value'])){
				$fieldset = array(
					'nr_protokollit'		=> $requestData['search']['value'],
					'emri_lendes'			=> $requestData['search']['value'],
					'vlera_parashikuar'		=> $requestData['search']['value'],
					'lloji_kontrates'		=> $requestData['search']['value'],
					'autoriteti_kontraktues'=> $requestData['search']['value'],
					'oe_ankues'				=> $requestData['search']['value'],
					'rekomandimi_ekspertit'	=> $requestData['search']['value'],					
					'ankesa_kunder'			=> $requestData['search']['value'],
					'vendimi'				=> $requestData['search']['value'],
					'terheqje_verejtje'		=> $requestData['search']['value'],
					'neni'					=> $requestData['search']['value'],
					'nr_vendimit'			=> $requestData['search']['value'],
					'vendimet_meparshme'	=> $requestData['search']['value'],
					'vendimi_jokonsistent'	=> $requestData['search']['value'],
					'konfiskim_tarifes'		=> $requestData['search']['value'],
					'kryetar'				=> $requestData['search']['value'],
					'referent'				=> $requestData['search']['value'],
					'anetari_1'				=> $requestData['search']['value'],
					'anetari_2'				=> $requestData['search']['value'],
					'anetari_3'				=> $requestData['search']['value'],
					'nr_panelisteve'		=> $requestData['search']['value'],
					'eksperti_shqyrtues'	=> $requestData['search']['value'],
					'eksperti_teknik'		=> $requestData['search']['value'],
					'seanca'				=> $requestData['search']['value'],
					'data_ankeses'			=> $requestData['search']['value'],
					'data_vendimit'			=> $requestData['search']['value'],
					'data_publikimit'		=> $requestData['search']['value'],
					'pergjigja_ak'			=> $requestData['search']['value'],
					'pergjigja_oe'			=> $requestData['search']['value'],
					'kategoria_kontrates'	=> $requestData['search']['value'],
					'oferta_oe'				=> $requestData['search']['value'],
					'depozita_ankeses'		=> $requestData['search']['value'],
					'statusi_ankeses'		=> $requestData['search']['value'],
					'njoftimi_ak'			=> $requestData['search']['value'],
					'ankesa'				=> $requestData['search']['value'],
					'teksti_kryesor'		=> $requestData['search']['value'],
					'eksperti_profesional'	=> $requestData['search']['value'],
					'njoftimi_ekspertizes'	=> $requestData['search']['value'],
					'raporti_ekspertit'		=> $requestData['search']['value'],
					'oferta_oe_rekomanduar' => $requestData['search']['value'],
					'data_seances'			=> $requestData['search']['value']
				);
			}
			$filters = null;
			if(!empty($requestData['columns'][2]['search']['value']) || 
				!empty($requestData['columns'][3]['search']['value']) || 
				!empty($requestData['columns'][4]['search']['value']) || 
				!empty($requestData['columns'][5]['search']['value']) ||
				!empty($requestData['columns'][6]['search']['value']) ||
				!empty($requestData['columns'][7]['search']['value']) ||
				!empty($requestData['columns'][8]['search']['value']) ||
				!empty($requestData['columns'][9]['search']['value']) ||
				!empty($requestData['columns'][10]['search']['value']) ||
				!empty($requestData['columns'][11]['search']['value']) ||
				!empty($requestData['columns'][12]['search']['value']) ||
				!empty($requestData['columns'][13]['search']['value']) ||
				!empty($requestData['columns'][14]['search']['value']) ||
				!empty($requestData['columns'][15]['search']['value']) ||
				!empty($requestData['columns'][16]['search']['value']) ||
				!empty($requestData['columns'][17]['search']['value']) ||
				!empty($requestData['columns'][18]['search']['value']) ||
				!empty($requestData['columns'][19]['search']['value']) ||
				!empty($requestData['columns'][20]['search']['value']) ||
				!empty($requestData['columns'][21]['search']['value']) ||
				!empty($requestData['columns'][22]['search']['value']) ||
				!empty($requestData['columns'][23]['search']['value']) ||
				!empty($requestData['columns'][24]['search']['value']) ||
				!empty($requestData['columns'][25]['search']['value']) ||
				!empty($requestData['columns'][26]['search']['value']) ||
				!empty($requestData['columns'][27]['search']['value']) ||
				!empty($requestData['columns'][28]['search']['value']) ||
				!empty($requestData['columns'][29]['search']['value']) ||
				!empty($requestData['advFilters'])
			)
			{
				if(!empty($requestData['columns'][2]['search']['value'])){
					$filters['nr_protokollit'] = $requestData['columns'][2]['search']['value'];
				}
				if(!empty($requestData['columns'][3]['search']['value'])){
					$filters['emri_lendes'] = $requestData['columns'][3]['search']['value'];
				}
				if(!empty($requestData['columns'][4]['search']['value'])){
					$filters['vlera_parashikuar'] = $requestData['columns'][4]['search']['value'];
				}
				if(!empty($requestData['columns'][5]['search']['value'])){
					$filters['lloji_kontrates'] = $requestData['columns'][5]['search']['value'];
				}				
				if(!empty($requestData['columns'][6]['search']['value'])){
					$filters['autoriteti_kontraktues'] = $requestData['columns'][6]['search']['value'];
				}
				if(!empty($requestData['columns'][7]['search']['value'])){
					$filters['oe_ankues'] = $requestData['columns'][7]['search']['value'];
				}
				if(!empty($requestData['columns'][8]['search']['value'])){
					$filters['rekomandimi_ekspertit'] = $requestData['columns'][8]['search']['value'];
				}				
				if(!empty($requestData['columns'][9]['search']['value'])){
					$filters['ankesa_kunder'] = $requestData['columns'][9]['search']['value'];
				}
				if(!empty($requestData['columns'][10]['search']['value'])){
					$filters['vendimi'] = $requestData['columns'][10]['search']['value'];
				}
				if(!empty($requestData['columns'][11]['search']['value'])){
					$filters['terheqje_verejtje'] = $requestData['columns'][11]['search']['value'];
				}
				if(!empty($requestData['columns'][12]['search']['value'])){
					$filters['neni'] = $requestData['columns'][12]['search']['value'];
				}
				if(!empty($requestData['columns'][13]['search']['value'])){
					$filters['nr_vendimit'] = $requestData['columns'][13]['search']['value'];
				}
				if(!empty($requestData['columns'][14]['search']['value'])){
					$filters['vendimet_meparshme'] = $requestData['columns'][14]['search']['value'];
				}
				if(!empty($requestData['columns'][15]['search']['value'])){
					$filters['vendimi_jokonsistent'] = $requestData['columns'][15]['search']['value'];
				}
				if(!empty($requestData['columns'][16]['search']['value'])){
					$filters['konfiskim_tarifes'] = $requestData['columns'][16]['search']['value'];
				}
				if(!empty($requestData['columns'][17]['search']['value'])){
					$filters['kryetar'] = $requestData['columns'][17]['search']['value'];
				}
				if(!empty($requestData['columns'][18]['search']['value'])){
					$filters['referent'] = $requestData['columns'][18]['search']['value'];
				}
				if(!empty($requestData['columns'][19]['search']['value'])){
					$filters['anetari_1'] = $requestData['columns'][19]['search']['value'];
				}
				if(!empty($requestData['columns'][20]['search']['value'])){
					$filters['anetari_2'] = $requestData['columns'][20]['search']['value'];
				}
				if(!empty($requestData['columns'][21]['search']['value'])){
					$filters['anetari_3'] = $requestData['columns'][21]['search']['value'];
				}
				if(!empty($requestData['columns'][22]['search']['value'])){
					$filters['nr_panelisteve'] = $requestData['columns'][22]['search']['value'];
				}
				if(!empty($requestData['columns'][23]['search']['value'])){
					$filters['eksperti_shqyrtues'] = $requestData['columns'][23]['search']['value'];
				}
				if(!empty($requestData['columns'][24]['search']['value'])){
					$filters['eksperti_teknik'] = $requestData['columns'][24]['search']['value'];
				}
				if(!empty($requestData['columns'][25]['search']['value'])){
					$filters['seanca'] = $requestData['columns'][25]['search']['value'];
				}
				if(!empty($requestData['columns'][26]['search']['value'])){
					$filters['data_ankeses'] = $requestData['columns'][26]['search']['value'];
				}
				if(!empty($requestData['columns'][27]['search']['value'])){
					$filters['data_vendimit'] = $requestData['columns'][27]['search']['value'];
				}
				if(!empty($requestData['columns'][28]['search']['value'])){
					$filters['data_publikimit'] = $requestData['columns'][28]['search']['value'];
				}
				if(!empty($requestData['columns'][29]['search']['value'])){
					$filters['pergjigja_ak'] = $requestData['columns'][29]['search']['value'];
				}
				if(!empty($requestData['columns'][30]['search']['value'])){
					$filters['pergjigja_oe'] = $requestData['columns'][30]['search']['value'];
				}
				if(!empty($requestData['columns'][31]['search']['value'])){
					$filters['kategoria_kontrates'] = $requestData['columns'][31]['search']['value'];
				}
				if(!empty($requestData['columns'][32]['search']['value'])){
					$filters['oferta_oe'] = $requestData['columns'][32]['search']['value'];
				}
				if(!empty($requestData['columns'][33]['search']['value'])){
					$filters['depozita_ankeses'] = $requestData['columns'][33]['search']['value'];
				}
				if(!empty($requestData['columns'][34]['search']['value'])){
					$filters['statusi_ankeses'] = $requestData['columns'][34]['search']['value'];
				}
				if(!empty($requestData['columns'][35]['search']['value'])){
					$filters['njoftimi_ak'] = $requestData['columns'][35]['search']['value'];
				}
				if(!empty($requestData['columns'][36]['search']['value'])){
					$filters['ankesa'] = $requestData['columns'][36]['search']['value'];
				}
				if(!empty($requestData['columns'][37]['search']['value'])){
					$filters['teksti_kryesor'] = $requestData['columns'][37]['search']['value'];
				}
				if(!empty($requestData['columns'][38]['search']['value'])){
					$filters['eksperti_profesional'] = $requestData['columns'][38]['search']['value'];
				}
				if(!empty($requestData['columns'][39]['search']['value'])){
					$filters['njoftimi_ekspertizes'] = $requestData['columns'][39]['search']['value'];
				}
				if(!empty($requestData['columns'][40]['search']['value'])){
					$filters['raporti_ekspertit'] = $requestData['columns'][40]['search']['value'];
				}
				if(!empty($requestData['columns'][41]['search']['value'])){
					$filters['oferta_oe_rekomanduar'] = $requestData['columns'][41]['search']['value'];
				}
				if(!empty($requestData['columns'][42]['search']['value'])){
					$filters['data_seances'] = $requestData['columns'][42]['search']['value'];
				}
				if(!empty($requestData['advFilters'])){
					$filters['advanced_filters'] = $requestData['advFilters'];
				}
			}
			$order_by = array('column' => $columns[$requestData['order'][0]['column']], 'direction' => $requestData['order'][0]['dir']);
			$limit = array('start' => $requestData['start'], 'length' => $requestData['length']);
			$vendimet = $this->Vendimet_model->get_vendimet_data($fieldset, $order_by, $limit, $filters);
			if(!empty($requestData['search']['value']) || 
				!empty($requestData['columns'][2]['search']['value']) || 
				!empty($requestData['columns'][3]['search']['value']) || 
				!empty($requestData['columns'][4]['search']['value']) || 
				!empty($requestData['columns'][5]['search']['value']) ||
				!empty($requestData['columns'][6]['search']['value']) ||
				!empty($requestData['columns'][7]['search']['value']) ||
				!empty($requestData['columns'][8]['search']['value']) ||
				!empty($requestData['columns'][9]['search']['value']) ||
				!empty($requestData['columns'][10]['search']['value']) ||
				!empty($requestData['columns'][11]['search']['value']) ||
				!empty($requestData['columns'][12]['search']['value']) ||
				!empty($requestData['columns'][13]['search']['value']) ||
				!empty($requestData['columns'][14]['search']['value']) ||
				!empty($requestData['columns'][15]['search']['value']) ||
				!empty($requestData['columns'][16]['search']['value']) ||
				!empty($requestData['columns'][17]['search']['value']) ||
				!empty($requestData['columns'][18]['search']['value']) ||
				!empty($requestData['columns'][19]['search']['value']) ||
				!empty($requestData['columns'][20]['search']['value']) ||
				!empty($requestData['columns'][21]['search']['value']) ||
				!empty($requestData['columns'][22]['search']['value']) ||
				!empty($requestData['columns'][23]['search']['value']) ||
				!empty($requestData['columns'][24]['search']['value']) ||
				!empty($requestData['columns'][25]['search']['value']) ||
				!empty($requestData['columns'][26]['search']['value']) ||
				!empty($requestData['columns'][27]['search']['value']) ||
				!empty($requestData['columns'][28]['search']['value']) ||
				!empty($requestData['columns'][29]['search']['value']) ||
				!empty($requestData['advFilters']))
			{ 
				$totalFiltered = $this->Vendimet_model->get_vendimet_data($fieldset, $order_by, null, $filters, TRUE);
			}
			$data = array();
			foreach($vendimet as $v){
				$count_tags = count($v->tags);
				$tags = '';
				foreach ($v->tags as $key => $tag) {
					$delimiter = ' ';
					if($key != $count_tags-1){
						$delimiter = ', ';
					}
					$tags .= $tag->emri.$delimiter;
				}
				$label = '';
				if($v->vendimet_meparshme[0]->url){
					$label .= '<a href="#'.$this->config->item('legjenda')['vendimet_meparshme']['item_id'].'"><i class="'.$this->config->item('legjenda')['vendimet_meparshme']['icon'].'" style="'.$this->config->item('legjenda')['vendimet_meparshme']['style'].'"></i></a> ';
				}
				if($this->date_diff($v->data_ankeses,$v->data_publikimit)->days > $this->config->item('afati_pergjigjjes')){
					$label .= '<a href="#'.$this->config->item('legjenda')['afati_pergjigjjes']['item_id'].'"><i class="'.$this->config->item('legjenda')['afati_pergjigjjes']['icon'].'" style="'.$this->config->item('legjenda')['afati_pergjigjjes']['style'].'"></i></a>';
				}
				$nestedData = array();
				$nestedData[] = '';
				$nestedData[] = $label;

				$nestedData[] = $v->nr_protokollit; // 2
				$nestedData[] = $v->emri_lendes; // 3
				$nestedData[] = ($v->vlera_parashikuar != 0) ? '€'.number_format($v->vlera_parashikuar, 2) : 'N/A'; // 4
				$nestedData[] = $v->lloji_kontrates; // 5
				$nestedData[] = $v->autoriteti_kontraktues; // 6

				$nestedData[] = $v->oe_ankues; // 7

				$nestedData[] = $v->rekomandimi_ekspertit; // 8

				$nestedData[] = $v->ankesa_kunder; // 9

				$nestedData[] = $v->vendimi; // 10
				$nestedData[] = $v->terheqje_verejtje; // 11
				$nestedData[] = $v->neni; // 12			
				$nestedData[] = '<a href="'.$v->nr_vendimit[0]->url.'" target="_blank">'.$v->nr_vendimit[0]->titulli.'</a>'; // 13
				$nestedData[] = '<a href="'.$v->vendimet_meparshme[0]->url.'" target="_blank">'.$v->vendimet_meparshme[0]->titulli.'</a>'; // 14
				$nestedData[] = '<a href="'.$v->vendimi_jokonsistent[0]->url.'" target="_blank">'.$v->vendimi_jokonsistent[0]->titulli.'</a>'; // 15
				$nestedData[] = $v->konfiskim_tarifes; // 16

				$nestedData[] = $v->kryetar; // 17
				$nestedData[] = $v->referent; // 18
				$nestedData[] = $v->anetari_1; // 19
				$nestedData[] = $v->anetari_2; // 20
				$nestedData[] = $v->anetari_3; // 21
				$nestedData[] = $v->nr_panelisteve; // 22

				$nestedData[] = $v->eksperti_shqyrtues; // 23
				$nestedData[] = $v->eksperti_teknik; // 24

				$nestedData[] = $v->seanca; // 25

				$nestedData[] = $v->data_ankeses; // 26
				$nestedData[] = $v->data_vendimit; // 27
				$nestedData[] = $v->data_publikimit; // 28
				$nestedData[] = $v->pergjigja_ak; // 29
				$nestedData[] = $this->date_diff($v->data_ankeses,$v->data_vendimit)->days.' ditë'; // 30
				$nestedData[] = $this->date_diff($v->data_vendimit,$v->data_publikimit)->days.' ditë'; // 31
				$nestedData[] = $this->date_diff($v->data_ankeses,$v->data_publikimit)->days.' ditë'; // 32

				$nestedData[] = $tags; // 33

				//New fields added by request, didn't add with order cause old code might break
				$nestedData[] = $v->pergjigja_oe; // 34
				$nestedData[] = $v->kategoria_kontrates; // 35
				$nestedData[] = $v->oferta_oe; // 36
				$nestedData[] = $v->depozita_ankeses; // 37
				$nestedData[] = $v->statusi_ankeses; // 38
				$nestedData[] = $v->njoftimi_ak; // 39
				$nestedData[] = '<a href="'.$v->ankesa[0]->url.'" target="_blank">'.$v->ankesa[0]->titulli.'</a>'; // 40
				$nestedData[] = $v->teksti_kryesor; //41
				$nestedData[] = $v->eksperti_profesional; // 42
				$nestedData[] = $v->njoftimi_ekspertizes; // 43
				$nestedData[] = '<a href="'.$v->raporti_ekspertit[0]->url.'" target="_blank">'.$v->raporti_ekspertit[0]->titulli.'</a>'; // 44
				$nestedData[] = $v->oferta_oe_rekomanduar; // 45
				$nestedData[] = $v->data_seances; //46

				$data[] = $nestedData;
			}
			$json_data = array(
				"draw"		=> intval($requestData["draw"]),
				"recordsTotal"    => intval( $totalData ),
				"recordsFiltered" => intval( $totalFiltered ),
				"data"		=> $data
				);
			echo json_encode($json_data);
			return;
		}else{
			$data['vendimet'] = array();
		}
		$data['used_tags'] = $this->Vendimet_model->get_all_unique_used_tags();
		$data['last_updated'] = $this->Vendimet_model->get_last_updated();
		$data['panelistet'] = $this->Vendimet_model->get_unique_fields_by_column('vendimet', 'paneli');
		$data['vendimet'] = $this->Vendimet_model->get_unique_fields_by_column('vendimet', 'vendimi');
		$data['rekomandimet'] = $this->Vendimet_model->get_unique_fields_by_column('vendimet', 'rekomandimi_ekspertit');
		$data['nenet'] = $this->Vendimet_model->get_unique_fields_by_column('vendimet', 'neni');
		$data['oe_ankues'] = $this->Vendimet_model->get_unique_fields_by_column('vendimet', 'oe_ankues');
		$data['ak'] = $this->Vendimet_model->get_unique_fields_by_column('vendimet', 'autoriteti_kontraktues');
		$this->view('vendimet', $data);	
	}

	public function lista_e_zeze($type = null){
		if($type == 'json'){
			$requestData = $_REQUEST;
			$columns = array(
				0	=> null,
				1	=> null,
				2	=> 'kerkesa_kunder_oe',
				3	=> 'nr_protokollit',
				4	=> 'emri_lendes',
				5	=> 'vlera_parashikuar',
				6  	=> 'lloji_kontrates',
				7	=> 'autoriteti_kontraktues',
				8	=> 'pretendimet_ak',
				9	=> 'vendimi',
				10	=> 'diskualifikimi_kohezgjatja',
				11	=> 'neni',
				12	=> 'vendimi_liste_zeze',
				13	=> 'kryetar',
				14	=> 'referent',
				15	=> 'anetari_1',
				16	=> 'anetari_2',
				17	=> 'anetari_3',
				18	=> 'nr_panelisteve',
				19	=> 'rekomandimi_ekspertit',
				20	=> 'eksperti_shqyrtues',
				21	=> 'eksperti_teknik',
				22	=> 'seanca',
				23	=> 'data_kerkeses',
				24	=> 'data_vendimit',
				25	=> 'data_publikimit',
				);

			$totalData = $this->Lista_zeze_model->get_total_listazeze();
			$totalFiltered = $totalData;
			$fieldset = null;
			if(!empty($requestData['search']['value'])){
				$fieldset = array(
					'nr_protokollit'			=> $requestData['search']['value'],
					'emri_lendes'				=> $requestData['search']['value'],
					'vlera_parashikuar'			=> $requestData['search']['value'],
					'lloji_kontrates'			=> $requestData['search']['value'],
					'autoriteti_kontraktues'	=> $requestData['search']['value'],
					'kerkesa_kunder_oe'			=> $requestData['search']['value'],
					'pretendimet_ak'			=> $requestData['search']['value'],
					'vendimi'					=> $requestData['search']['value'],
					'diskualifikimi_kohezgjatja'=> $requestData['search']['value'],
					'neni'						=> $requestData['search']['value'],
					'vendimi_liste_zeze'		=> $requestData['search']['value'],
					'kryetar'					=> $requestData['search']['value'],
					'referent'					=> $requestData['search']['value'],
					'anetari_1'					=> $requestData['search']['value'],
					'anetari_2'					=> $requestData['search']['value'],
					'anetari_3'					=> $requestData['search']['value'],
					'nr_panelisteve'			=> $requestData['search']['value'],
					'rekomandimi_ekspertit'		=> $requestData['search']['value'],
					'eksperti_shqyrtues'		=> $requestData['search']['value'],
					'eksperti_teknik'			=> $requestData['search']['value'],
					'seanca'					=> $requestData['search']['value'],
					'data_kerkeses'				=> $requestData['search']['value'],
					'data_vendimit'				=> $requestData['search']['value'],
					'data_publikimit'			=> $requestData['search']['value'],
				);
			}
			$filters = null;
			if(!empty($requestData['columns'][2]['search']['value']) || 
				!empty($requestData['columns'][3]['search']['value']) || 
				!empty($requestData['columns'][4]['search']['value']) || 
				!empty($requestData['columns'][5]['search']['value']) ||
				!empty($requestData['columns'][6]['search']['value']) ||
				!empty($requestData['columns'][7]['search']['value']) ||
				!empty($requestData['columns'][8]['search']['value']) ||
				!empty($requestData['columns'][9]['search']['value']) ||
				!empty($requestData['columns'][10]['search']['value']) ||
				!empty($requestData['columns'][11]['search']['value']) ||
				!empty($requestData['columns'][12]['search']['value']) ||
				!empty($requestData['columns'][13]['search']['value']) ||
				!empty($requestData['columns'][14]['search']['value']) ||
				!empty($requestData['columns'][15]['search']['value']) ||
				!empty($requestData['columns'][16]['search']['value']) ||
				!empty($requestData['columns'][17]['search']['value']) ||
				!empty($requestData['columns'][18]['search']['value']) ||
				!empty($requestData['columns'][19]['search']['value']) ||
				!empty($requestData['columns'][20]['search']['value']) ||
				!empty($requestData['columns'][21]['search']['value']) ||
				!empty($requestData['columns'][22]['search']['value']) ||
				!empty($requestData['columns'][23]['search']['value']) ||
				!empty($requestData['columns'][24]['search']['value']) ||
				!empty($requestData['columns'][25]['search']['value'])
			)
			{
				if(!empty($requestData['columns'][2]['search']['value'])){
					$filters['kerkesa_kunder_oe'] = $requestData['columns'][2]['search']['value'];
				}
				if(!empty($requestData['columns'][3]['search']['value'])){
					$filters['nr_protokollit'] = $requestData['columns'][3]['search']['value'];
				}
				if(!empty($requestData['columns'][4]['search']['value'])){
					$filters['emri_lendes'] = $requestData['columns'][4]['search']['value'];
				}
				if(!empty($requestData['columns'][5]['search']['value'])){
					$filters['vlera_parashikuar'] = $requestData['columns'][5]['search']['value'];
				}
				if(!empty($requestData['columns'][6]['search']['value'])){
					$filters['lloji_kontrates'] = $requestData['columns'][6]['search']['value'];
				}
				if(!empty($requestData['columns'][7]['search']['value'])){
					$filters['autoriteti_kontraktues'] = $requestData['columns'][7]['search']['value'];
				}
				if(!empty($requestData['columns'][8]['search']['value'])){
					$filters['pretendimet_ak'] = $requestData['columns'][8]['search']['value'];
				}
				if(!empty($requestData['columns'][9]['search']['value'])){
					$filters['vendimi'] = $requestData['columns'][9]['search']['value'];
				}
				if(!empty($requestData['columns'][10]['search']['value'])){
					$filters['diskualifikimi_kohezgjatja'] = $requestData['columns'][10]['search']['value'];
				}
				if(!empty($requestData['columns'][11]['search']['value'])){
					$filters['neni'] = $requestData['columns'][11]['search']['value'];
				}
				if(!empty($requestData['columns'][12]['search']['value'])){
					$filters['vendimi_liste_zeze'] = $requestData['columns'][12]['search']['value'];
				}
				if(!empty($requestData['columns'][13]['search']['value'])){
					$filters['kryetar'] = $requestData['columns'][13]['search']['value'];
				}
				if(!empty($requestData['columns'][14]['search']['value'])){
					$filters['referent'] = $requestData['columns'][14]['search']['value'];
				}
				if(!empty($requestData['columns'][15]['search']['value'])){
					$filters['anetari_1'] = $requestData['columns'][15]['search']['value'];
				}
				if(!empty($requestData['columns'][16]['search']['value'])){
					$filters['anetari_2'] = $requestData['columns'][16]['search']['value'];
				}
				if(!empty($requestData['columns'][17]['search']['value'])){
					$filters['anetari_3'] = $requestData['columns'][17]['search']['value'];
				}
				if(!empty($requestData['columns'][18]['search']['value'])){
					$filters['nr_panelisteve'] = $requestData['columns'][18]['search']['value'];
				}
				if(!empty($requestData['columns'][19]['search']['value'])){
					$filters['rekomandimi_ekspertit'] = $requestData['columns'][19]['search']['value'];
				}
				if(!empty($requestData['columns'][20]['search']['value'])){
					$filters['eksperti_shqyrtues'] = $requestData['columns'][20]['search']['value'];
				}
				if(!empty($requestData['columns'][21]['search']['value'])){
					$filters['eksperti_teknik'] = $requestData['columns'][21]['search']['value'];
				}
				if(!empty($requestData['columns'][22]['search']['value'])){
					$filters['seanca'] = $requestData['columns'][22]['search']['value'];
				}
				if(!empty($requestData['columns'][23]['search']['value'])){
					$filters['data_kerkeses'] = $requestData['columns'][23]['search']['value'];
				}
				if(!empty($requestData['columns'][24]['search']['value'])){
					$filters['data_vendimit'] = $requestData['columns'][24]['search']['value'];
				}
				if(!empty($requestData['columns'][25]['search']['value'])){
					$filters['data_publikimit'] = $requestData['columns'][25]['search']['value'];
				}
			}	
			$order_by = array('column' => $columns[$requestData['order'][0]['column']], 'direction' => $requestData['order'][0]['dir']);
			$limit = array('start' => $requestData['start'], 'length' => $requestData['length']);
			$listazeze = $this->Lista_zeze_model->get_listazeze_data($fieldset, $order_by, $limit, $filters);
			if(!empty($requestData['search']['value']) || 
				!empty($requestData['columns'][2]['search']['value']) ||
				!empty($requestData['columns'][3]['search']['value']) || 
				!empty($requestData['columns'][4]['search']['value']) || 
				!empty($requestData['columns'][5]['search']['value']) ||
				!empty($requestData['columns'][6]['search']['value']) ||
				!empty($requestData['columns'][7]['search']['value']) ||
				!empty($requestData['columns'][8]['search']['value']) ||
				!empty($requestData['columns'][9]['search']['value']) ||
				!empty($requestData['columns'][10]['search']['value']) ||
				!empty($requestData['columns'][11]['search']['value']) ||
				!empty($requestData['columns'][12]['search']['value']) ||
				!empty($requestData['columns'][13]['search']['value']) ||
				!empty($requestData['columns'][14]['search']['value']) ||
				!empty($requestData['columns'][15]['search']['value']) ||
				!empty($requestData['columns'][16]['search']['value']) ||
				!empty($requestData['columns'][17]['search']['value']) ||
				!empty($requestData['columns'][18]['search']['value']) ||
				!empty($requestData['columns'][19]['search']['value']) ||
				!empty($requestData['columns'][20]['search']['value']) ||
				!empty($requestData['columns'][21]['search']['value']) ||
				!empty($requestData['columns'][22]['search']['value']) ||
				!empty($requestData['columns'][23]['search']['value']) ||
				!empty($requestData['columns'][24]['search']['value']) ||
				!empty($requestData['columns'][25]['search']['value'])
			)
			{
				$totalFiltered = $this->Lista_zeze_model->get_listazeze_data($fieldset, $order_by, null, $filters, TRUE);
			}
			$data = array();
			foreach($listazeze as $lz){
				$label = '';
				if($this->date_diff($lz->data_kerkeses,$lz->data_publikimit)->days > $this->config->item('afati_pergjigjjes')){
					$label .= '<a href="#'.$this->config->item('legjenda')['afati_pergjigjjes']['item_id'].'"><i class="'.$this->config->item('legjenda')['afati_pergjigjjes']['icon'].'" style="'.$this->config->item('legjenda')['afati_pergjigjjes']['style'].'"></i></a>';
				}

				$nestedData = array();
				$nestedData[] = '';
				$nestedData[] = $label;

				$nestedData[] = $lz->kerkesa_kunder_oe; // 2
				$nestedData[] = $lz->nr_protokollit; // 3
				$nestedData[] = $lz->emri_lendes; // 4
				$nestedData[] = ($lz->vlera_parashikuar != 0) ? '€'.number_format($lz->vlera_parashikuar, 2) : 'N/A'; // 5
				$nestedData[] = $lz->lloji_kontrates; // 6			
				$nestedData[] = $lz->autoriteti_kontraktues; // 7

				$nestedData[] = $lz->pretendimet_ak; // 8

				$nestedData[] = $lz->vendimi; // 9
				$nestedData[] = $lz->diskualifikimi_kohezgjatja; // 10
				$nestedData[] = $lz->neni; // 11
				$nestedData[] = '<a href="'.$lz->vendimi_liste_zeze[0]->url.'" target="_blank">'.$lz->vendimi_liste_zeze[0]->titulli.'</a>'; // 12

				$nestedData[] = $lz->kryetar; // 13
				$nestedData[] = $lz->referent; // 14
				$nestedData[] = $lz->anetari_1; // 15
				$nestedData[] = $lz->anetari_2; // 16
				$nestedData[] = $lz->anetari_3; // 17
				$nestedData[] = $lz->nr_panelisteve; // 18

				$nestedData[] = $lz->rekomandimi_ekspertit; // 19

				$nestedData[] = $lz->eksperti_shqyrtues; // 20
				$nestedData[] = $lz->eksperti_teknik; // 21

				$nestedData[] = $lz->seanca; // 22

				$nestedData[] = $lz->data_kerkeses; // 23
				$nestedData[] = $lz->data_vendimit; // 24
				$nestedData[] = $lz->data_publikimit; // 25
				$nestedData[] = $this->date_diff($lz->data_kerkeses,$lz->data_vendimit)->days.' ditë'; // 26
				$nestedData[] = $this->date_diff($lz->data_vendimit,$lz->data_publikimit)->days.' ditë'; // 27
				$nestedData[] = $this->date_diff($lz->data_kerkeses,$lz->data_publikimit)->days.' ditë'; // 28

				$data[] = $nestedData;
			}
			$json_data = array(
				"draw"		=> intval($requestData["draw"]),
				"recordsTotal"    => intval( $totalData ),
				"recordsFiltered" => intval( $totalFiltered ),
				"data"		=> $data
				);
			echo json_encode($json_data);
			return;
		}else{
			$data['listazeze'] = array();
		}
		$data['last_updated'] = $this->Lista_zeze_model->get_last_updated();
		$this->view('lista_e_zeze', $data);
	}

	public function gjobe($type = null){
		if($type == 'json'){
			$requestData = $_REQUEST;
			$columns = array(
				0	=> null,
				1	=> null,
				2	=> 'nr_protokollit',
				3	=> 'emri_lendes',
				4	=> 'vlera_parashikuar',
				5  	=> 'lloji_kontrates',
				6	=> 'operatori_ekonomik',
				7	=> 'gjoba_kunder_ak',
				8 	=> 'shuma',
				9	=> 'terheqje_verejtje',
				10	=> 'heqje_licence',
				11	=> 'vendimi',
				12	=> 'neni',
				13	=> 'kryetar',
				14	=> 'referent',
				15	=> 'anetari_1',
				16	=> 'anetari_2',
				17 	=> 'anetari_3',
				18	=> 'nr_panelisteve',
				19	=> 'seanca',
				20	=> 'data_kerkeses',
				21	=> 'data_vendimit',
				22	=> 'data_publikimit'
				);

			$totalData = $this->Gjobe_model->get_total_gjobe_data();
			$totalFiltered = $totalData;
			$fieldset = null;
			if(!empty($requestData['search']['value'])){
				$fieldset = array(
					'nr_protokollit'	=> $requestData['search']['value'],
					'emri_lendes'		=> $requestData['search']['value'],
					'vlera_parashikuar'	=> $requestData['search']['value'],
					'lloji_kontrates'	=> $requestData['search']['value'],
					'operatori_ekonomik'=> $requestData['search']['value'],
					'gjoba_kunder_ak'	=> $requestData['search']['value'],
					'shuma'				=> $requestData['search']['value'],
					'terheqje_verejtje'	=> $requestData['search']['value'],
					'heqje_licence'		=> $requestData['search']['value'],
					'vendimi'			=> $requestData['search']['value'],
					'neni'				=> $requestData['search']['value'],
					'kryetar'			=> $requestData['search']['value'],
					'referent'			=> $requestData['search']['value'],
					'anetari_1'			=> $requestData['search']['value'],
					'anetari_2'			=> $requestData['search']['value'],
					'anetari_3'			=> $requestData['search']['value'],
					'nr_panelisteve'	=> $requestData['search']['value'],
					'seanca'			=> $requestData['search']['value'],
					'data_kerkeses'		=> $requestData['search']['value'],
					'data_vendimit'		=> $requestData['search']['value'],
					'data_publikimit'	=> $requestData['search']['value']
				);
			}
			$filters = null;
			if(!empty($requestData['columns'][2]['search']['value']) || 
				!empty($requestData['columns'][3]['search']['value']) || 
				!empty($requestData['columns'][4]['search']['value']) || 
				!empty($requestData['columns'][5]['search']['value']) ||
				!empty($requestData['columns'][6]['search']['value']) ||
				!empty($requestData['columns'][7]['search']['value']) ||
				!empty($requestData['columns'][8]['search']['value']) ||
				!empty($requestData['columns'][9]['search']['value']) ||
				!empty($requestData['columns'][10]['search']['value']) ||
				!empty($requestData['columns'][11]['search']['value']) ||
				!empty($requestData['columns'][12]['search']['value']) ||
				!empty($requestData['columns'][13]['search']['value']) ||
				!empty($requestData['columns'][14]['search']['value']) ||
				!empty($requestData['columns'][15]['search']['value']) ||
				!empty($requestData['columns'][16]['search']['value']) ||
				!empty($requestData['columns'][17]['search']['value']) ||
				!empty($requestData['columns'][18]['search']['value']) ||
				!empty($requestData['columns'][19]['search']['value']) ||
				!empty($requestData['columns'][20]['search']['value']) ||
				!empty($requestData['columns'][21]['search']['value']) ||
				!empty($requestData['columns'][22]['search']['value'])
			)
			{
				if(!empty($requestData['columns'][2]['search']['value'])){
					$filters['nr_protokollit'] = $requestData['columns'][2]['search']['value'];
				}
				if(!empty($requestData['columns'][3]['search']['value'])){
					$filters['emri_lendes'] = $requestData['columns'][3]['search']['value'];
				}
				if(!empty($requestData['columns'][4]['search']['value'])){
					$filters['vlera_parashikuar'] = $requestData['columns'][4]['search']['value'];
				}
				if(!empty($requestData['columns'][5]['search']['value'])){
					$filters['lloji_kontrates'] = $requestData['columns'][5]['search']['value'];
				}
				if(!empty($requestData['columns'][6]['search']['value'])){
					$filters['operatori_ekonomik'] = $requestData['columns'][6]['search']['value'];
				}
				if(!empty($requestData['columns'][7]['search']['value'])){
					$filters['gjoba_kunder_ak'] = $requestData['columns'][7]['search']['value'];
				}
				if(!empty($requestData['columns'][8]['search']['value'])){
					$filters['shuma'] = $requestData['columns'][8]['search']['value'];
				}
				if(!empty($requestData['columns'][9]['search']['value'])){
					$filters['terheqje_verejtje'] = $requestData['columns'][9]['search']['value'];
				}
				if(!empty($requestData['columns'][10]['search']['value'])){
					$filters['heqje_licence'] = $requestData['columns'][10]['search']['value'];
				}
				if(!empty($requestData['columns'][11]['search']['value'])){
					$filters['vendimi'] = $requestData['columns'][11]['search']['value'];
				}
				if(!empty($requestData['columns'][12]['search']['value'])){
					$filters['neni'] = $requestData['columns'][12]['search']['value'];
				}
				if(!empty($requestData['columns'][13]['search']['value'])){
					$filters['kryetar'] = $requestData['columns'][13]['search']['value'];
				}
				if(!empty($requestData['columns'][14]['search']['value'])){
					$filters['referent'] = $requestData['columns'][14]['search']['value'];
				}
				if(!empty($requestData['columns'][15]['search']['value'])){
					$filters['anetari_1'] = $requestData['columns'][15]['search']['value'];
				}
				if(!empty($requestData['columns'][16]['search']['value'])){
					$filters['anetari_2'] = $requestData['columns'][16]['search']['value'];
				}
				if(!empty($requestData['columns'][17]['search']['value'])){
					$filters['anetari_3'] = $requestData['columns'][17]['search']['value'];
				}
				if(!empty($requestData['columns'][18]['search']['value'])){
					$filters['nr_panelisteve'] = $requestData['columns'][18]['search']['value'];
				}
				if(!empty($requestData['columns'][19]['search']['value'])){
					$filters['seanca'] = $requestData['columns'][19]['search']['value'];
				}
				if(!empty($requestData['columns'][20]['search']['value'])){
					$filters['data_kerkeses'] = $requestData['columns'][20]['search']['value'];
				}
				if(!empty($requestData['columns'][21]['search']['value'])){
					$filters['data_vendimit'] = $requestData['columns'][21]['search']['value'];
				}
				if(!empty($requestData['columns'][22]['search']['value'])){
					$filters['data_publikimit'] = $requestData['columns'][22]['search']['value'];
				}
			}			
			$order_by = array('column' => $columns[$requestData['order'][0]['column']], 'direction' => $requestData['order'][0]['dir']);
			$limit = array('start' => $requestData['start'], 'length' => $requestData['length']);
			$gjobe = $this->Gjobe_model->get_gjobe_data($fieldset, $order_by, $limit, $filters);
			if(!empty($requestData['search']['value']) || 
				!empty($requestData['columns'][2]['search']['value']) || 
				!empty($requestData['columns'][3]['search']['value']) || 
				!empty($requestData['columns'][4]['search']['value']) || 
				!empty($requestData['columns'][5]['search']['value']) ||
				!empty($requestData['columns'][6]['search']['value']) ||
				!empty($requestData['columns'][7]['search']['value']) ||
				!empty($requestData['columns'][8]['search']['value']) ||
				!empty($requestData['columns'][9]['search']['value']) ||
				!empty($requestData['columns'][10]['search']['value']) ||
				!empty($requestData['columns'][11]['search']['value']) ||
				!empty($requestData['columns'][12]['search']['value']) ||
				!empty($requestData['columns'][13]['search']['value']) ||
				!empty($requestData['columns'][14]['search']['value']) ||
				!empty($requestData['columns'][15]['search']['value']) ||
				!empty($requestData['columns'][16]['search']['value']) ||
				!empty($requestData['columns'][17]['search']['value']) ||
				!empty($requestData['columns'][18]['search']['value']) ||
				!empty($requestData['columns'][19]['search']['value']) ||
				!empty($requestData['columns'][20]['search']['value']) ||
				!empty($requestData['columns'][21]['search']['value']) ||
				!empty($requestData['columns'][22]['search']['value'])
			)
			{ 
				$totalFiltered = $this->Gjobe_model->get_gjobe_data($fieldset, $order_by, null, $filters, TRUE);
			}
			$data = array();
			foreach($gjobe as $gj){
				$label = '';
				if($this->date_diff($gj->data_kerkeses,$gj->data_publikimit)->days > $this->config->item('afati_pergjigjjes')){
					$label .= '<a href="#'.$this->config->item('legjenda')['afati_pergjigjjes']['item_id'].'"><i class="'.$this->config->item('legjenda')['afati_pergjigjjes']['icon'].'" style="'.$this->config->item('legjenda')['afati_pergjigjjes']['style'].'"></i></a>';
				}
				$nestedData = array();
				$nestedData[] = '';
				$nestedData[] = $label;

				$nestedData[] = $gj->nr_protokollit; // 2
				$nestedData[] = $gj->emri_lendes; // 3
				$nestedData[] = ($gj->vlera_parashikuar != 0) ? '€'.number_format($gj->vlera_parashikuar, 2) : 'N/A'; // 4
				$nestedData[] = $gj->lloji_kontrates; // 5 				
				$nestedData[] = $gj->operatori_ekonomik; // 6

				$nestedData[] = $gj->gjoba_kunder_ak; // 7
				$nestedData[] = ($gj->shuma != 0) ? '€'.number_format($gj->shuma, 2) : 'N/A'; // 8
				$nestedData[] = $gj->terheqje_verejtje; // 9

				$nestedData[] = $gj->heqje_licence; // 10
				$nestedData[] = '<a href="'.$gj->vendimi[0]->url.'" target="_blank">'.$gj->vendimi[0]->titulli.'</a>'; // 11
				$nestedData[] = $gj->neni; // 12

				$nestedData[] = $gj->kryetar; // 13
				$nestedData[] = $gj->referent; // 14
				$nestedData[] = $gj->anetari_1; // 15
				$nestedData[] = $gj->anetari_2; // 16
				$nestedData[] = $gj->anetari_3; // 17
				$nestedData[] = $gj->nr_panelisteve; // 18

				$nestedData[] = $gj->seanca; // 19

				$nestedData[] = $gj->data_kerkeses; // 20
				$nestedData[] = $gj->data_vendimit; // 21
				$nestedData[] = $gj->data_publikimit; // 22
				$nestedData[] = $this->date_diff($gj->data_kerkeses,$gj->data_vendimit)->days.' ditë'; // 23
				$nestedData[] = $this->date_diff($gj->data_vendimit,$gj->data_publikimit)->days.' ditë'; // 24
				$nestedData[] = $this->date_diff($gj->data_kerkeses,$gj->data_publikimit)->days.' ditë'; // 25

				$data[] = $nestedData;
			}
			$json_data = array(
				"draw"		=> intval($requestData["draw"]),
				"recordsTotal"    => intval( $totalData ),
				"recordsFiltered" => intval( $totalFiltered ),
				"data"		=> $data
				);
			echo json_encode($json_data);
			return;
		}else{
			$data['gjobe'] = array();
		}
		$data['last_updated'] = $this->Gjobe_model->get_last_updated();
		$this->view('gjobe', $data);
	}

	public function heqje_pezullimi($type = null){
		if($type == 'json'){
			$requestData = $_REQUEST;
			$columns = array(
				0	=> null,
				1	=> null,
				2	=> 'nr_protokollit',
				3	=> 'emri_lendes',
				4	=> 'vlera_parashikuar',
				5   => 'lloji_kontrates',
				6  	=> 'autoriteti_kontraktuar',
				7	=> 'oe_rekomanduar',
				8 	=> 'heqje_pezullimi',
				9	=> 'vendimi',
				10	=> 'neni',
				11	=> 'kryetar',
				12	=> 'referent',
				13	=> 'anetari_1',
				14	=> 'anetari_2',
				15	=> 'anetari_3',
				16	=> 'nr_panelisteve',
				17 	=> 'seanca',
				18	=> 'data_kerkeses',
				19	=> 'data_vendimit',
				20	=> 'data_publikimit'
			);

			$totalData = $this->Heqje_pezullimi_model->get_total_heqje_pezullimi();
			$totalFiltered = $totalData;
			$fieldset = null;
			if(!empty($requestData['search']['value'])){
				$fieldset = array(
					'nr_protokollit'		=> $requestData['search']['value'],
					'emri_lendes'			=> $requestData['search']['value'],
					'vlera_parashikuar'		=> $requestData['search']['value'],
					'lloji_kontrates'		=> $requestData['search']['value'],
					'autoriteti_kontraktuar'=> $requestData['search']['value'],
					'oe_rekomanduar'		=> $requestData['search']['value'],
					'heqje_pezullimi'		=> $requestData['search']['value'],
					'vendimi'				=> $requestData['search']['value'],
					'neni'					=> $requestData['search']['value'],
					'kryetar'				=> $requestData['search']['value'],
					'referent'				=> $requestData['search']['value'],
					'anetari_1'				=> $requestData['search']['value'],
					'anetari_2'				=> $requestData['search']['value'],
					'anetari_3'				=> $requestData['search']['value'],
					'nr_panelisteve'		=> $requestData['search']['value'],
					'seanca'				=> $requestData['search']['value'],
					'data_kerkeses'			=> $requestData['search']['value'],
					'data_vendimit'			=> $requestData['search']['value'],
					'data_publikimit'		=> $requestData['search']['value']
				);
			}
			$filters = null;
			if(!empty($requestData['columns'][2]['search']['value']) || 
				!empty($requestData['columns'][3]['search']['value']) || 
				!empty($requestData['columns'][4]['search']['value']) || 
				!empty($requestData['columns'][5]['search']['value']) ||
				!empty($requestData['columns'][6]['search']['value']) ||
				!empty($requestData['columns'][7]['search']['value']) ||
				!empty($requestData['columns'][8]['search']['value']) ||
				!empty($requestData['columns'][9]['search']['value']) ||
				!empty($requestData['columns'][10]['search']['value']) ||
				!empty($requestData['columns'][11]['search']['value']) ||
				!empty($requestData['columns'][12]['search']['value']) ||
				!empty($requestData['columns'][13]['search']['value']) ||
				!empty($requestData['columns'][14]['search']['value']) ||
				!empty($requestData['columns'][15]['search']['value']) ||
				!empty($requestData['columns'][16]['search']['value']) ||
				!empty($requestData['columns'][17]['search']['value']) ||
				!empty($requestData['columns'][18]['search']['value']) ||
				!empty($requestData['columns'][19]['search']['value']) ||
				!empty($requestData['columns'][20]['search']['value'])
			)
			{
				if(!empty($requestData['columns'][2]['search']['value'])){
					$filters['nr_protokollit'] = $requestData['columns'][2]['search']['value'];
				}
				if(!empty($requestData['columns'][3]['search']['value'])){
					$filters['emri_lendes'] = $requestData['columns'][3]['search']['value'];
				}
				if(!empty($requestData['columns'][4]['search']['value'])){
					$filters['vlera_parashikuar'] = $requestData['columns'][4]['search']['value'];
				}
				if(!empty($requestData['columns'][5]['search']['value'])){
					$filters['lloji_kontrates'] = $requestData['columns'][5]['search']['value'];
				}
				if(!empty($requestData['columns'][6]['search']['value'])){
					$filters['autoriteti_kontraktuar'] = $requestData['columns'][6]['search']['value'];
				}
				if(!empty($requestData['columns'][7]['search']['value'])){
					$filters['oe_rekomanduar'] = $requestData['columns'][7]['search']['value'];
				}
				if(!empty($requestData['columns'][8]['search']['value'])){
					$filters['heqje_pezullimi'] = $requestData['columns'][8]['search']['value'];
				}
				if(!empty($requestData['columns'][9]['search']['value'])){
					$filters['vendimi'] = $requestData['columns'][9]['search']['value'];
				}
				if(!empty($requestData['columns'][10]['search']['value'])){
					$filters['neni'] = $requestData['columns'][10]['search']['value'];
				}
				if(!empty($requestData['columns'][11]['search']['value'])){
					$filters['kryetar'] = $requestData['columns'][11]['search']['value'];
				}
				if(!empty($requestData['columns'][12]['search']['value'])){
					$filters['referent'] = $requestData['columns'][12]['search']['value'];
				}
				if(!empty($requestData['columns'][13]['search']['value'])){
					$filters['anetari_1'] = $requestData['columns'][13]['search']['value'];
				}
				if(!empty($requestData['columns'][14]['search']['value'])){
					$filters['anetari_2'] = $requestData['columns'][14]['search']['value'];
				}
				if(!empty($requestData['columns'][15]['search']['value'])){
					$filters['anetari_3'] = $requestData['columns'][15]['search']['value'];
				}
				if(!empty($requestData['columns'][16]['search']['value'])){
					$filters['nr_panelisteve'] = $requestData['columns'][16]['search']['value'];
				}
				if(!empty($requestData['columns'][17]['search']['value'])){
					$filters['seanca'] = $requestData['columns'][17]['search']['value'];
				}
				if(!empty($requestData['columns'][18]['search']['value'])){
					$filters['data_kerkeses'] = $requestData['columns'][18]['search']['value'];
				}
				if(!empty($requestData['columns'][19]['search']['value'])){
					$filters['data_vendimit'] = $requestData['columns'][19]['search']['value'];
				}
				if(!empty($requestData['columns'][20]['search']['value'])){
					$filters['data_publikimit'] = $requestData['columns'][20]['search']['value'];
				}			
			}				
			$order_by = array('column' => $columns[$requestData['order'][0]['column']], 'direction' => $requestData['order'][0]['dir']);
			$limit = array('start' => $requestData['start'], 'length' => $requestData['length']);
			$heqje_pezullimi = $this->Heqje_pezullimi_model->get_heqje_pezullimi_data($fieldset, $order_by, $limit, $filters);
			if(!empty($requestData['search']['value']) || 
				!empty($requestData['columns'][2]['search']['value']) || 
				!empty($requestData['columns'][3]['search']['value']) || 
				!empty($requestData['columns'][4]['search']['value']) || 
				!empty($requestData['columns'][5]['search']['value']) ||
				!empty($requestData['columns'][6]['search']['value']) ||
				!empty($requestData['columns'][7]['search']['value']) ||
				!empty($requestData['columns'][8]['search']['value']) ||
				!empty($requestData['columns'][9]['search']['value']) ||
				!empty($requestData['columns'][10]['search']['value']) ||
				!empty($requestData['columns'][11]['search']['value']) ||
				!empty($requestData['columns'][12]['search']['value']) ||
				!empty($requestData['columns'][13]['search']['value']) ||
				!empty($requestData['columns'][14]['search']['value']) ||
				!empty($requestData['columns'][15]['search']['value']) ||
				!empty($requestData['columns'][16]['search']['value']) ||
				!empty($requestData['columns'][17]['search']['value']) ||
				!empty($requestData['columns'][18]['search']['value']) ||
				!empty($requestData['columns'][19]['search']['value']) ||
				!empty($requestData['columns'][20]['search']['value'])
			)
			{ 
				$totalFiltered = $this->Heqje_pezullimi_model->get_heqje_pezullimi_data($fieldset, $order_by, null, $filters, TRUE);
			}

			$data = array();
			foreach($heqje_pezullimi as $pezullimi){
				$label = '';
				if($this->date_diff($pezullimi->data_kerkeses,$pezullimi->data_publikimit)->days > $this->config->item('afati_pergjigjjes')){
					$label .= '<a href="#'.$this->config->item('legjenda')['afati_pergjigjjes']['item_id'].'"><i class="'.$this->config->item('legjenda')['afati_pergjigjjes']['icon'].'" style="'.$this->config->item('legjenda')['afati_pergjigjjes']['style'].'"></i></a>';
				}
				
				$nestedData = array();
				$nestedData[] = '';
				$nestedData[] = $label;

				$nestedData[] = $pezullimi->nr_protokollit; // 2
				$nestedData[] = $pezullimi->emri_lendes; // 3
				$nestedData[] = ($pezullimi->vlera_parashikuar != 0) ? '€'.number_format($pezullimi->vlera_parashikuar, 2) : 'N/A'; // 4
				$nestedData[] = $pezullimi->lloji_kontrates; // 5
				$nestedData[] = $pezullimi->autoriteti_kontraktuar; //6
				$nestedData[] = $pezullimi->oe_rekomanduar; // 7

				$nestedData[] = $pezullimi->heqje_pezullimi; // 8
				$nestedData[] = '<a href="'.$pezullimi->vendimi[0]->url.'" target="_blank">'.$pezullimi->vendimi[0]->titulli.'</a>'; // 9
				$nestedData[] = $pezullimi->neni; // 10

				$nestedData[] = $pezullimi->kryetar; // 11
				$nestedData[] = $pezullimi->referent; // 12
				$nestedData[] = $pezullimi->anetari_1; // 13
				$nestedData[] = $pezullimi->anetari_2; // 14
				$nestedData[] = $pezullimi->anetari_3; // 15
				$nestedData[] = $pezullimi->nr_panelisteve; // 16

				$nestedData[] = $pezullimi->seanca; // 17

				$nestedData[] = $pezullimi->data_kerkeses; // 18
				$nestedData[] = $pezullimi->data_vendimit; // 19
				$nestedData[] = $pezullimi->data_publikimit; // 20
				$nestedData[] = $this->date_diff($pezullimi->data_kerkeses,$pezullimi->data_vendimit)->days.' ditë'; // 21
				$nestedData[] = $this->date_diff($pezullimi->data_vendimit,$pezullimi->data_publikimit)->days.' ditë'; // 22
				$nestedData[] = $this->date_diff($pezullimi->data_kerkeses,$pezullimi->data_publikimit)->days.' ditë'; // 23

				$data[] = $nestedData;
			}
			$json_data = array(
				"draw"		=> intval($requestData["draw"]),
				"recordsTotal"    => intval( $totalData ),
				"recordsFiltered" => intval( $totalFiltered ),
				"data"		=> $data
				);
			echo json_encode($json_data);
			return;
		}else{
			$data['pezullimet'] = array();
		}
		$data['last_updated'] = $this->Heqje_pezullimi_model->get_last_updated();
		$this->view('heqje_pezullimi', $data);
	}

	private function date_diff($date1, $date2){
    	$date1 = date_create($date1);
		$date2 = date_create($date2);		
		$diff = date_diff($date1,$date2);
		return $diff;
	}

	public function autocomplete_data(){
		$result = array();
		if($this->input->post('table') && $this->input->post('column') && $this->input->post('value')) {
			$table = $this->input->post('table');
			$column = $this->input->post('column');
			$value = $this->input->post('value');

 			$result = $this->Vendimet_model->get_autocomplete_data($table, $column, $value);
 		}	
		echo json_encode($result);	
	}

	public function get_tag($id = null){
		$this->load->model('Tags_model');
		$tag = $this->Tags_model->get_tag($id);
		echo json_encode($tag);
	}
}
