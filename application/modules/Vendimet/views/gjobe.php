<style type="text/css">
    td.details-control {
        background: url('<?php echo base_url(); ?>assets/images/icons8-double-down-20.png') no-repeat center center;
        cursor: pointer;
    }
    tr.shown td.details-control {
        background: url('<?php echo base_url(); ?>assets/images/icons8-double-up-20.png') no-repeat center center;
    }
    table.extra-details {
        table-layout:fixed; 
        width:auto;
    }
    table.extra-details th, table.extra-details tr td {
        width:500px !important;
        word-wrap: break-word !important;
        white-space: normal !important;
    }
    div.dt-button-collection.four-column {
        width:80% !important;
    }
    div.dt-button-collection.fixed {
        left:10%;
        margin:0;
        padding:4px;
    }
    div.dt-button-collection.fixed.four-column {
        margin:0;
    }
    table th{
        text-transform: uppercase;
        font-size: 12px;
    }
    table td{
        font-size: 12px;
    }
    div.dt-button-collection.one-column {
        width:30% !important;
        height:80% !important;
        overflow-y: auto;
    }
    div.dt-button-collection.fixed {
        left:35%;
        margin:0;
        padding:4px;
    }
    div.dt-button-collection.fixed.one-column {
        margin:0;
    } 
    @media only screen and (max-width: 767px) {
        div.dt-button-collection.one-column {
            width:90% !important;
            height:80% !important;
        }
        div.dt-button-collection.fixed {
            left:5%;
        }        
    }
</style>
<section id="general-section">
<?php if($this->input->get('success') == 3): ?>
  <div class="alert alert-success text-center"><strong><?php echo $this->lang->line('onDelete')?>.</strong></div>
<?php endif; ?>
    <div class="container">
        <div class="tags-section">
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-8">
                        </div>
                        <div class="col-md-4">
                            <div class="pull-right">
                                <h5><?php echo $this->lang->line('perditsimi')?>: <strong><?php echo $last_updated; ?></strong></h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
            	<table id="gjobe_table" class="row-border compact" style="width:100%">
                    <thead>
                        <tr>
                            <th class="not-export-col"></th>
                            <th class="not-export-col"></th>

                            <th><?php echo $this->lang->line('nrProtokollit')?></th>
                            <th class="fixed-min-width"><?php echo $this->lang->line('emriLendes')?></th>
                            <th><?php echo $this->lang->line('vleraParashikuar')?></th>
                            <th><?php echo $this->lang->line('llojiKontrates')?></th>                            
                            <th><?php echo $this->lang->line('operatoriEkonomik')?></th>

                            <th><?php echo $this->lang->line('gjobeAK')?></th>
                            <th><?php echo $this->lang->line('shuma')?></th>
                            <th><?php echo $this->lang->line('terheqjeVerejtjes')?></th>

                            <th><?php echo $this->lang->line('heqjeLicense')?></th>
                            <th><?php echo $this->lang->line('nrVendimit')?></th>
                            <th><?php echo $this->lang->line('neni')?></th>

                            <th><?php echo $this->lang->line('kryetarPanel')?></th>
                            <th><?php echo $this->lang->line('referentPanel')?></th>
                            <th><?php echo $this->lang->line('anetari_I')?></th>
                            <th><?php echo $this->lang->line('anetari_II')?></th>
                            <th><?php echo $this->lang->line('anetari_III')?></th>
                            <th><?php echo $this->lang->line('nrPanelist')?></th>

                            <th><?php echo $this->lang->line('seancaHM')?></th>

                            <th><?php echo $this->lang->line('dataAnkeses')?></th>
                            <th><?php echo $this->lang->line('dataVendimit')?></th>
                            <th><?php echo $this->lang->line('dataPublikimit')?></th>
                            <th><?php echo $this->lang->line('kohaAV')?></th>
                            <th><?php echo $this->lang->line('kohaVP')?></th>
                            <th><?php echo $this->lang->line('kohaAP')?></th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th class="not-export-col"></th>
                            <th class="not-export-col"></th>

                            <th><div class="ui-widget"><input type="text" data-column="2" data-name="nr_protokollit" class="search-input-text" placeholder="<?php echo $this->lang->line('nrProtokollit')?>"></div></th>
                            <th><div class="ui-widget"><input type="text" data-column="3" data-name="emri_lendes" class="search-input-text" placeholder="<?php echo $this->lang->line('emriLendes')?>"></div></th>
                            <th><div class="ui-widget"><input type="text" data-column="4" data-name="vlera_parashikuar" class="search-input-text" placeholder="<?php echo $this->lang->line('vleraParashikuar')?>"></div></th>
                            <th><div class="ui-widget"><input type="text" data-column="5" data-name="lloji_kontrates" class="search-input-text" placeholder="<?php echo $this->lang->line('llojiKontrates')?>"></div></th>
                            <th><div class="ui-widget"><input type="text" data-column="6" data-name="operatori_ekonomik" class="search-input-text" placeholder="<?php echo $this->lang->line('operatoriEkonomik')?>"></div></th>

                            <th><div class="ui-widget"><input type="text" data-column="7" data-name="gjoba_kunder_ak" class="search-input-text" placeholder="<?php echo $this->lang->line('gjobeAK')?>"></div></th>
                            <th><div class="ui-widget"><input type="text" data-column="8" data-name="shuma" class="search-input-text" placeholder="<?php echo $this->lang->line('shuma')?>"></div></th>
                            <th><div class="ui-widget"><input type="text" data-column="9" data-name="terheqje_verejtje" class="search-input-text" placeholder="<?php echo $this->lang->line('terheqjeVerejtjes')?>"></div></th>

                            <th><div class="ui-widget"><input type="text" data-column="10" data-name="heqje_licence" class="search-input-text" placeholder="<?php echo $this->lang->line('heqjeLicense')?>"></div></th>
                            <th><div class="ui-widget"><input type="text" data-column="11" data-name="vendimi" class="search-input-text" placeholder="<?php echo $this->lang->line('nrVendimit')?>"></div></th>
                            <th><div class="ui-widget"><input type="text" data-column="12" data-name="neni" class="search-input-text" placeholder="<?php echo $this->lang->line('neni')?>"></div></th>

                            <th><div class="ui-widget"><input type="text" data-column="14" data-name="kryetar" class="search-input-text" placeholder="<?php echo $this->lang->line('kryetarPanel')?>"></div></th>
                            <th><div class="ui-widget"><input type="text" data-column="15" data-name="referent" class="search-input-text" placeholder="<?php echo $this->lang->line('referentPanel')?>"></div></th>
                            <th><div class="ui-widget"><input type="text" data-column="16" data-name="anetari_1" class="search-input-text" placeholder="<?php echo $this->lang->line('anetari_I')?>"></div></th>
                            <th><div class="ui-widget"><input type="text" data-column="17" data-name="anetari_2" class="search-input-text" placeholder="<?php echo $this->lang->line('anetari_II')?>"></div></th>
                            <th><div class="ui-widget"><input type="text" data-column="18" data-name="anetari_3" class="search-input-text" placeholder="<?php echo $this->lang->line('anetari_III')?>"></div></th>
                            <th><div class="ui-widget"><input type="text" data-column="19" data-name="nr_panelisteve" class="search-input-text" placeholder="<?php echo $this->lang->line('nrPanelist')?>"></div></th>

                            <th><div class="ui-widget"><input type="text" data-column="20" data-name="seanca" class="search-input-text" placeholder="<?php echo $this->lang->line('seancaHM')?>"></div></th>

                            <th><div class="ui-widget"><input type="text" data-column="21" data-name="data_kerkeses" class="search-input-text" placeholder="<?php echo $this->lang->line('dataAnkeses')?>"></div></th>
                            <th><div class="ui-widget"><input type="text" data-column="22" data-name="data_vendimit" class="search-input-text" placeholder="<?php echo $this->lang->line('dataVendimit')?>"></div></th>
                            <th><div class="ui-widget"><input type="text" data-column="23" data-name="data_publikimit" class="search-input-text" placeholder="<?php echo $this->lang->line('dataPublikimit')?>"></div></th>
                            <th><?php echo $this->lang->line('kohaAV')?></th>
                            <th><?php echo $this->lang->line('kohaVP')?></th>
                            <th><?php echo $this->lang->line('kohaAP')?></th>
                        </tr>
                    </tfoot>
            	</table>
        	</div>
        </div>
        <?php $this->load->view('template/legjenda', $legjenda); ?>
    </div>
</section>
<script>
var site_url = '<?php echo base_url(); ?>';
var encodedLogo = '<?php echo $this->config->item("encodedLogo"); ?>';
var xhr;
$(document).ready(function() {
	var gjobe_table = $('#gjobe_table').DataTable({
		dom: 'Bfrtip',
        buttons: [
        	'pageLength',
            {
                extend: 'excel',
                exportOptions: {
                    columns: [':visible :not(.not-export-col)']
                },
                title: '<?php echo $this->lang->line('gjobaKriter')?></',			                
            },
            {
                extend: 'pdfHtml5',
                orientation: 'landscape',
            	pageSize: 'LEGAL',
                exportOptions: {
                    columns: [':visible :not(.not-export-col)']             
                },
                title: '<?php echo $this->lang->line('gjobaKriter')?>',
                customize: function ( doc ) {
                    doc.content.splice( 1, 0, {
                        margin: [ 0, 0, 0, 12 ],
                        alignment: 'center',
                        image: encodedLogo,
                        width: 170
                    } );
                }		                
            },
            {
                extend: 'print',
                exportOptions: {
                    columns: [':visible :not(.not-export-col)']            
                },
                title: '<?php echo $this->lang->line('gjobaKriter')?>',			                
            },
            {
                extend: 'csvHtml5',
                exportOptions: {
                    columns: [':visible :not(.not-export-col)']             
                },
                title: '<?php echo $this->lang->line('gjobaKriter')?>',			                
            },
            {
                extend: 'colvis',
                collectionLayout: 'fixed one-column',
                columns: [5,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25],
                text: '<?php echo $this->lang->line('zgjedhKolonat')?>',
                prefixButtons: [
                    { extend:'columnToggle', text:'<?php echo $this->lang->line('zgjedhTeGjitha')?>', targets: '_all', className:"toggleBtnSelect toggleBtnGroup" },
                ],
                postfixButtons: [
                    { extend:'colvisRestore', text:'<?php echo $this->lang->line('kolonatFillestare')?>', targets: '_all', className:"toggleBtnRestore toggleBtnGroup" }
                ]
            },
        ],
        columnDefs: [
            { targets: [0, 1, 2, 3, 4, 6, 7, 8, 11], visible: true },
            { targets: '_all', visible: false },
            { className: "details-control", "targets": [0], "orderable": false, "width": "10px", "searchable": false },
            { className: "details-label", "targets": [1], "orderable": false, "searchable": false },
            { className: "lenda-width", targets: 3}
        ],
		"language": {
		        "processing": "<?php echo $this->lang->line('prit')?>", //add a loading image,simply putting <img src="loader.gif" /> tag.
                buttons: {
                    pageLength: {
                    _: "<?php echo $this->lang->line('shfaqRreshta')?>",
                    '-1': "<?php echo $this->lang->line('gjithRreshtat')?>"
                    }
                },
                "zeroRecords": "<?php echo $this->lang->line('empty')?>.",
                "info": "<?php echo $this->lang->line('startEnd')?>",
                "paginate": {
                            "previous": "<?php echo $this->lang->line('faqjaPrevious')?>",
                            "next": "<?php echo $this->lang->line('faqjaTjeter')?>",
                            "first": "<?php echo $this->lang->line('faqjaPare')?>",
                            "last": "<?php echo $this->lang->line('faqjaFundit')?>"
                        },
                "search": "<?php echo $this->lang->line('search')?>",
                "infoFiltered": " <?php echo $this->lang->line('totalMAX')?>"
		    },			
        "autoWidth": false,
        "lengthMenu": [ [15, 25, 50, -1], [15, 25, 50, "<?php echo $this->lang->line('teGjitha')?>"] ],
        "processing": true,
        "serverSide": true,
        "scrollX": true,
        "searchHighlight": true,		
		"ajax":{
			url :site_url + 'vendimet/gjobe/json', // json datasource
			type: "post",  // method  , by default get     
            data:  function ( d ) {
                $('.buttons-print').html('<span class="fa fa-print" data-toggle="tooltip" title="Printo"/>');
                $('.buttons-excel').html('<span class="fa fa-file-excel-o" data-toggle="tooltip" title="Excel"/>');
                $('.buttons-csv').html('<span class="fa fa-file-o" data-toggle="tooltip" title="CSV"/>');
                $('.buttons-pdf').html('<span class="fa fa-file-pdf-o" data-toggle="tooltip" title="PDF"/>');                
            },                
			error: function(){  // error handling
				$(".gjobe_table_error").html("");
				$("#gjobe_table").append('<tbody class="gjobe_table_error"><tr><th colspan="10"><?php echo $this->lang->line('empty')?>.</th></tr></tbody>');
				//$("#employee-grid_processing").css("display","none");					
			}
		}
	} );

    $('.search-input-text').autocomplete({
        minLength: 0,
        source: function(request, response){
            var el = $(this.element);
            var i =el.attr('data-column') // getting column index
            var n =el.attr('data-name') // getting name index
            var v =el.val();  // getting search input value
            if(v.length >= 1){
                if(xhr) {
                    xhr.abort();
                }
                var post_data = {table: 'gjobe', column: n, value: v};
                xhr = $.ajax({
                    'async': false,
                    'url': site_url + 'vendimet/autocomplete_data/',
                    'data': post_data,
                    'type': 'POST',
                    success:function(data){
                        data = JSON.parse(data);
                        response(data);
                    }
                });            
            }else{
                gjobe_table.columns(i).search('').draw();
            }
        },
        select: function(event, ui) {
            var i = $(this).attr('data-column') // getting column index
            gjobe_table.columns(i).search(ui.item.value).draw();
        }           
    });

    $('#gjobe_table tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        //var id = tr.find('.pezullimi_id').val();
        //console.log(id);
        var row = gjobe_table.row( tr );
        if ( row.child.isShown() ) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
            // Open this row
            row.child(fetch_data(row.data())).show();
            tr.addClass('shown');
            $(this).closest('tr').next().css('background', '#f5f5f5');
            $(this).closest('tr').next().find('tr').css('background', 'transparent');
            $(this).closest('tr').next().find('th').css({'border-bottom': '1px solid #9e9e9e', 'color': '#111'});
            $(this).closest('tr').next().find('tr td').css({'color': '#111'});
        }

    } );

    var buttons = gjobe_table.buttons('.buttons-columnVisibility').nodes();
    var groupArrOne = [1];
    var groupArrTwo = [2,3,4,5,6];
    var groupArrThree = [7,8,9,10,11,12];
    var groupArrFour = [13];
    var groupArrFive = [14,15,16,17,18,19];
    var groupOne = 0;
    var groupTwo = 0; 
    var groupThree = 0; 
    var groupFour = 0; 
    var groupFive = 0;
    $.each( buttons, function( key, value ) {
        console.log(key + ' ' +value.outerText)
        var group;
        var groupDevider;
        if(groupArrOne.indexOf(key) != -1){
            group = ' groupOne';
            if(groupOne == 0){
                groupDevider = 'AKTIVITETI I PROKURIMIT';
            }            
            groupOne++;
        }else if(groupArrTwo.indexOf(key) != -1){
            group = ' groupTwo';
            if(groupTwo == 0){
                groupDevider = 'VENDIMET';
            }            
            groupTwo++;
        }else if(groupArrThree.indexOf(key) != -1){
            group = ' groupThree';
            if(groupThree == 0){
                groupDevider = 'PANELI';
            }            
            groupThree++;
        }else if(groupArrFour.indexOf(key) != -1){
            group = ' groupFour';
            if(groupFour == 0){
                groupDevider = 'SEANCA HAPUR/MBYLLUR';
            }            
            groupFour++;
        }else if(groupArrFive.indexOf(key) != -1){
            group = ' groupFive';
            if(groupFive == 0){
                groupDevider = 'AFATET';
            }            
            groupFive++;
        }else{
            group = '';
            groupDevider = '';
        }
        value.className = value.className + group;
        if(groupDevider){
            value.className = value.className + " groupHeader";
        }
    });    
});

function fetch_data ( d ) {
    // `d` is the original data object for the row
    result = '<div class="col-md-12">'+
    '<table cellpadding="8" cellspacing="0" border="0" class="extra-details">' +
        '<th colspan="2">Aktiviteti i prokurimit</th>'+ 
        '<tr>'+
            '<td><?php echo $this->lang->line('nrProtokollit')?>:</td>'+
            '<td>'+((d[2] != null) ? d[2] : '')+'</a></td>'+
        '</tr>'+
        '<tr>'+
            '<td><?php echo $this->lang->line('emriLendes')?>:</td>'+
            '<td>'+((d[3] != null) ? d[3] : '')+'</a></td>'+
        '</tr>'+
        '<tr>'+
            '<td><?php echo $this->lang->line('vleraParashikuar')?>:</td>'+
            '<td>'+((d[4] != null) ? d[4] : '')+'</a></td>'+
        '</tr>'+
        '<tr>'+
            '<td><?php echo $this->lang->line('llojiKontrates')?>:</td>'+
            '<td>'+((d[5] != null) ? d[5] : '')+'</a></td>'+
        '</tr>'+
        '<tr>'+
            '<td><?php echo $this->lang->line('operatoriEkonomik')?>:</td>'+
            '<td>'+((d[6] != null) ? d[6] : '')+'</a></td>'+
        '</tr>'+  
        '<th colspan="2"><?php echo $this->lang->line('vendimi')?></th>'+
        '<tr>'+
            '<td>G<?php echo $this->lang->line('gjobeAK')?>:</td>'+
            '<td>'+((d[7] != null) ? d[7] : '')+'</a></td>'+
        '</tr>'+
        '<tr>'+
            '<td><?php echo $this->lang->line('shuma')?>:</td>'+
            '<td>'+((d[8] != null) ? d[8] : '')+'</a></td>'+
        '</tr>'+
        '<tr>'+
            '<td><?php echo $this->lang->line('terheqjeVerejtjes')?>:</td>'+
            '<td>'+((d[9] != null) ? d[9] : '')+'</a></td>'+
        '</tr>'+                         
        '<th colspan="2"><?php echo $this->lang->line('vendimet')?></th>'+
        '<tr>'+
            '<td><?php echo $this->lang->line('heqjeLicense')?>:</td>'+
            '<td>'+((d[10] != null) ? d[10] : '')+'</a></td>'+
        '</tr>'+
        '<tr>'+
            '<td><?php echo $this->lang->line('vendimi')?>:</td>'+
            '<td>'+((d[11] != null) ? d[11] : '')+'</td>'+
        '</tr>'+
        '<tr>'+
            '<td><?php echo $this->lang->line('neni')?>:</td>'+
            '<td>'+((d[12] != null) ? d[12] : '')+'</td>'+
        '</tr>'+      
        '<th colspan="2"><?php echo $this->lang->line('paneli')?></th>'+
        '<tr>'+
            '<td><?php echo $this->lang->line('kryetarPanel')?>:</td>'+
            '<td>'+((d[13] != null) ? d[13] : '')+'</td>'+
        '</tr>'+
        '<tr>'+
            '<td><?php echo $this->lang->line('referentPanel')?>:</td>'+
            '<td>'+((d[14] != null) ? d[14] : '')+'</td>'+
        '</tr>'+
        '<tr>'+
            '<td><?php echo $this->lang->line('anetari_I')?>:</td>'+
            '<td>'+((d[15] != null) ? d[15] : '')+'</td>'+
        '</tr>'+
        '<tr>'+
            '<td><?php echo $this->lang->line('anetari_II')?>:</td>'+
            '<td>'+((d[16] != null) ? d[16] : '')+'</td>'+
        '</tr>'+
        '<tr>'+
            '<td><?php echo $this->lang->line('anetari_III')?>:</td>'+
            '<td>'+((d[17] != null) ? d[17] : '')+'</td>'+
        '</tr>'+
        '<tr>'+
            '<td><?php echo $this->lang->line('nrPanelist')?>:</td>'+
            '<td>'+((d[18] != null) ? d[18] : '')+'</td>'+
        '</tr>'+ 
        '<th colspan="2"><?php echo $this->lang->line('seancaHM')?></th>'+
        '<tr>'+
            '<td><?php echo $this->lang->line('seancaHM')?>:</td>'+
            '<td>'+((d[19] != null) ? d[19] : '')+'</td>'+
        '</tr>'+
        '<th colspan="2"><?php echo $this->lang->line('afatet')?></th>'+
        '<tr>'+
            '<td><?php echo $this->lang->line('dataAnkeses')?>:</td>'+
            '<td>'+((d[20] != null) ? d[20] : '')+'</td>'+
        '</tr>'+
        '<tr>'+
            '<td><?php echo $this->lang->line('dataVendimit')?>:</td>'+
            '<td>'+((d[21] != null) ? d[21] : '')+'</td>'+
        '</tr>'+
        '<tr>'+
            '<td><?php echo $this->lang->line('dataPublikimit')?>:</td>'+
            '<td>'+((d[22] != null) ? d[22] : '')+'</td>'+
        '</tr>'+
        '<tr>'+
            '<td><?php echo $this->lang->line('kohaAV')?>:</td>'+
            '<td>'+((d[23] != null) ? d[23] : '')+'</td>'+
        '</tr>'+
        '<tr>'+
            '<td><?php echo $this->lang->line('kohaVP')?>:</td>'+
            '<td>'+((d[24] != null) ? d[24] : '')+'</td>'+
        '</tr>'+
        '<tr>'+
            '<td><?php echo $this->lang->line('kohaAP')?>:</td>'+
            '<td>'+((d[25] != null) ? d[25] : '')+'</td>'+
        '</tr>'+                                              
    '</table>'+
'</div>';

    return result;
}
</script>