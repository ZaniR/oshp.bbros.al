<style type="text/css">
    td.details-control {
        background: url('<?php echo base_url(); ?>assets/images/icons8-double-down-20.png') no-repeat center center;
        cursor: pointer;
    }
    tr.shown td.details-control {
        background: url('<?php echo base_url(); ?>assets/images/icons8-double-up-20.png') no-repeat center center;
    }
    table.extra-details {
        table-layout:fixed; 
        width:auto;
    }
    table.extra-details th, table.extra-details tr td {
        width:500px !important;
        word-wrap: break-word !important;
        white-space: normal !important;
    }
    div.dt-button-collection.four-column {
        width:80% !important;
    }
    div.dt-button-collection.fixed {
        left:10%;
        margin:0;
        padding:4px;
    }
    div.dt-button-collection.fixed.four-column {
        margin:0;
    }
    table th{
        text-transform: uppercase;
        font-size: 11px;
    }
    table td{
        font-size: 12px;
    }
    div.dt-button-collection.one-column {
        width:30% !important;
        height:80% !important;
        overflow-y: auto;
    }
    div.dt-button-collection.fixed {
        left:35%;
        margin:0;
        padding:4px;
    }
    div.dt-button-collection.fixed.one-column {
        margin:0;
    } 
    @media only screen and (max-width: 767px) {
        div.dt-button-collection.one-column {
            width:90% !important;
            height:80% !important;
        }
        div.dt-button-collection.fixed {
            left:5%;
        }        
    }    
</style>
<section id="general-section">
<?php if($this->input->get('success') == 3): ?>
  <div class="alert alert-success text-center"><strong><?php echo $this->lang->line('onDelete')?></strong></div>
<?php endif; ?>
    <div class="container">
        <div class="tags-section">
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-12">
                            <div id="advSearch" class="collapse">
                                <div class="tags-section">
                                <h4><?php echo $this->lang->line('filtro')?>:</h4>
                                <ul class="nav nav-tabs" id="myTab" role="tablist">
                                    <li class="nav-item active">
                                        <a class="nav-link active" id="fDatat-tab" data-toggle="tab" href="#fDatat" role="tab" aria-controls="fDatat" aria-selected="true"><?php echo $this->lang->line('dataPublikimit')?></a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="fVlera-tab" data-toggle="tab" href="#fVlera" role="tab" aria-controls="fVlera" aria-selected="false"><?php echo $this->lang->line('vleraParashikuar')?></a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="fAk-tab" data-toggle="tab" href="#fAk" role="tab" aria-controls="fAk" aria-selected="false"><?php echo $this->lang->line('autoritetiKontraktues')?></a>
                                    </li>                                    
                                    <li class="nav-item">
                                        <a class="nav-link" id="fOeAnkues-tab" data-toggle="tab" href="#fOeAnkues" role="tab" aria-controls="fOeAnkues" aria-selected="false"><?php echo $this->lang->line('OEAnkues')?></a>
                                    </li>                                    
                                    <li class="nav-item">
                                        <a class="nav-link" id="fPanelistet-tab" data-toggle="tab" href="#fPanelistet" role="tab" aria-controls="fPanelistet" aria-selected="false"><?php echo $this->lang->line('paneli')?></a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="fVendimet-tab" data-toggle="tab" href="#fVendimet" role="tab" aria-controls="fVendimet" aria-selected="false"><?php echo $this->lang->line('vendimetPanelit')?></a>
                                    </li>                                    
                                    <li class="nav-item">
                                        <a class="nav-link" id="fRekomandimi-tab" data-toggle="tab" href="#fRekomandimi" role="tab" aria-controls="fRekomandimi" aria-selected="false"><?php echo $this->lang->line('rekomandimet')?></a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="fNeni-tab" data-toggle="tab" href="#fNeni" role="tab" aria-controls="fNeni" aria-selected="false"><?php echo $this->lang->line('nenet')?></a>
                                    </li>                                
                                    <li class="nav-item">
                                        <a class="nav-link" id="fTagjet-tab" data-toggle="tab" href="#fTagjet" role="tab" aria-controls="fTagjet" aria-selected="false"><?php echo $this->lang->line('keywords')?></a>
                                    </li>
                                </ul>
                                <div class="tab-content" id="myTabContent">
                                    <div class="tab-pane fade active in" id="fDatat" role="tabpanel" aria-labelledby="fDatat-tab">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                  <label for="Nga"><?php echo $this->lang->line('ngaData')?>:</label>
                                                  <input type="text" class="form-control form_date advFilters ngaData" name="ngaData-filter">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                  <label for="Deri"><?php echo $this->lang->line('deriData')?>:</label>
                                                  <input type="text" class="form-control form_date advFilters deriData" name="deriData-filter">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="fVlera" role="tabpanel" aria-labelledby="fVlera-tab">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                  <label for="Vlera minimale"><?php echo $this->lang->line('vleraMinimale')?>:</label>
                                                  <input type="text" oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1');" class="form-control advFilters ngaVlera" name="ngaVlera-filter">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                  <label for="Vlera maximale"><?php echo $this->lang->line('vleraMaximale')?>:</label>
                                                  <input type="text" oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1');" class="form-control advFilters deriVlera" name="deriVlera-filter">
                                                </div>
                                            </div>
                                        </div>                                      
                                    </div>
                                    <div class="tab-pane fade" id="fAk" role="tabpanel" aria-labelledby="fAk-tab">
                                        <div class="form-group">
                                            <label for="Autoriteti_Kontraktues"><?php echo $this->lang->line('autoritetiKontraktues')?>:</label>
                                            <select class="form-control chosen-select advFilters ak" data-placeholder="- <?php echo $this->lang->line('zgjedhje')?> -" name="ak-filter[]" multiple>
                                                <?php foreach($ak as $aki): ?>
                                                    <option value="<?php echo $aki; ?>"><?php echo $aki; ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>  
                                    </div>                                     
                                    <div class="tab-pane fade" id="fOeAnkues" role="tabpanel" aria-labelledby="fOeAnkues-tab">
                                        <div class="form-group">
                                            <label for="OE_Ankues"><?php echo $this->lang->line('OEAnkues')?>:</label>
                                            <select class="form-control chosen-select advFilters oeankues" data-placeholder="- <?php echo $this->lang->line('zgjedhje')?> -" name="oeankues-filter[]" multiple>
                                                <?php foreach($oe_ankues as $oe): ?>
                                                    <option value="<?php echo $oe; ?>"><?php echo $oe; ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>  
                                    </div>                                    
                                    <div class="tab-pane fade" id="fPanelistet" role="tabpanel" aria-labelledby="fPanelistet-tab">
                                        <div class="form-group">
                                            <label for="Panelistet"><?php echo $this->lang->line('panelistat')?>:</label>
                                            <select class="form-control chosen-select advFilters panelistet" data-placeholder="- <?php echo $this->lang->line('zgjedhje')?> -" name="panelistet-filter[]" multiple >
                                                <?php foreach($panelistet as $panelist): ?>
                                                    <option value="<?php echo $panelist; ?>"><?php echo $panelist; ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>                                      
                                    </div>
                                    <div class="tab-pane fade" id="fVendimet" role="tabpanel" aria-labelledby="fVendimet-tab">
                                        <div class="form-group">
                                            <label for="Vendimi"><?php echo $this->lang->line('vendimetPanelit')?>:</label>
                                            <select class="form-control chosen-select advFilters vendimet" data-placeholder="- <?php echo $this->lang->line('zgjedhje')?> -" name="vendimi-filter[]" multiple>
                                                <?php foreach($vendimet as $vendimi): ?>
                                                    <option value="<?php echo $vendimi; ?>"><?php echo $vendimi; ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>  
                                    </div>                                    
                                    <div class="tab-pane fade" id="fRekomandimi" role="tabpanel" aria-labelledby="fRekomandimi-tab">
                                        <div class="form-group">
                                            <label for="Rekomandimet"><?php echo $this->lang->line('rekomandimet')?>:</label>
                                            <select class="form-control chosen-select advFilters rekomandimet" data-placeholder="- <?php echo $this->lang->line('zgjedhje')?> -" name="rekomandimi-filter[]" multiple>
                                                <?php foreach($rekomandimet as $rekomandimi): ?>
                                                    <option value="<?php echo $rekomandimi; ?>"><?php echo $rekomandimi; ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>  
                                    </div>
                                    <div class="tab-pane fade" id="fNeni" role="tabpanel" aria-labelledby="fNeni-tab">
                                        <div class="form-group">
                                            <label for="Nenet"><?php echo $this->lang->line('nenet')?>:</label>
                                            <select class="form-control chosen-select advFilters nenet" data-placeholder="- <?php echo $this->lang->line('zgjedhje')?> -" name="neni-filter[]" multiple>
                                                <?php foreach($nenet as $neni): ?>
                                                    <option value="<?php echo $neni; ?>"><?php echo $neni; ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>  
                                    </div>                                   
                                    <div class="tab-pane fade" id="fTagjet" role="tabpanel" aria-labelledby="fTagjet-tab">
                                    <?php if(!empty($used_tags)): ?>
                                        <?php foreach($used_tags as $ut): ?>
                                        <div class="checkbox">
                                            <input type="checkbox" class="tags-filter advFilters" name="tags-filter[]" value="<?php echo $ut->id; ?>" /><span><?php echo $ut->emri; ?></span>
                                        </div>
                                        <?php endforeach;?> 
                                    <?php else: ?>
                                        <div class="text-center">
                                            <p><?php echo $this->lang->line('noKeyWords')?>.</p>
                                        </div>
                                    <?php endif; ?>
                                    </div>                                    
                                </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div id="advFiltersTerms"></div>
                        </div>                
                        <div class="col-md-8">
                            <a class="btn btn-default" data-toggle="collapse" data-target="#advSearch"><i id="advSearchIcon" class="fa fa-search"></i><span class="kerkimi_btn"><?php echo $this->lang->line('kerkimAvancuar')?></span></a>
                        </div>
                        <div class="col-md-4">
                            <div class="pull-right">
                                <h5><?php echo $this->lang->line('perditsimi')?>: <strong><?php echo $last_updated; ?></strong></h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <table id="vendimet_table" class="row-border compact" style="width:100%">
                    <thead>
                        <tr>
                            <th class="not-export-col"></th>
                            <th class="not-export-col"></th>

                            <th><?php echo $this->lang->line('nrProtokollit')?></th>
                            <th class="fixed-min-width"><?php echo $this->lang->line('emriLendes')?></th>
                            <th><?php echo $this->lang->line('vleraParashikuar')?></th>
                            <th><?php echo $this->lang->line('llojiKontrates')?></th>
                            <th><?php echo $this->lang->line('autoritetiKontraktues')?></th>

                            <th><?php echo $this->lang->line('OEAnkues')?></th>
                            <th><?php echo $this->lang->line('rekomandimi')?></th>                           
                            <th><?php echo $this->lang->line('ankesaKunder')?></th>

                            <th><?php echo $this->lang->line('vendimi')?></th>
                            <th><?php echo $this->lang->line('terheqjeVerejtjes')?></th>
                            <th><?php echo $this->lang->line('neni')?></th>
                            <th><?php echo $this->lang->line('nrVendimit')?></th>
                            <th><?php echo $this->lang->line('vendimetOld')?></th>
                            <th><?php echo $this->lang->line('joKonsistent')?></th>
                            <th><?php echo $this->lang->line('konfiskimTarif')?></th>

                            <th><?php echo $this->lang->line('kryetarPanel')?></th>
                            <th><?php echo $this->lang->line('referentPanel')?></th>
                            <th><?php echo $this->lang->line('anetari_I')?></th>
                            <th><?php echo $this->lang->line('anetari_II')?></th>
                            <th><?php echo $this->lang->line('anetari_III')?></th>
                            <th><?php echo $this->lang->line('nrPanelist')?></th>

                            <th><?php echo $this->lang->line('ekspertiShqyrtues')?></th>
                            <th><?php echo $this->lang->line('ekspertiTeknik')?></th>

                            <th><?php echo $this->lang->line('seancaHM')?></th>

                            <th><?php echo $this->lang->line('dataAnkeses')?></th>
                            <th><?php echo $this->lang->line('dataVendimit')?></th>
                            <th><?php echo $this->lang->line('dataPublikimit')?></th>
                            <th><?php echo $this->lang->line('kohaAV')?></th>
                            <th><?php echo $this->lang->line('kohaVP')?></th>
                            <th><?php echo $this->lang->line('kohaAP')?></th>

                            <th><?php echo $this->lang->line('keywords')?></th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th class="not-export-col"></th>
                            <th class="not-export-col"></th>

                            <th><div class="ui-widget"><input type="text" data-column="2" data-name="nr_protokollit" class="search-input-text" placeholder="<?php echo $this->lang->line('nrProtokollit')?>"></div></th>
                            <th><div class="ui-widget"><input type="text" data-column="3" data-name="emri_lendes" class="search-input-text" placeholder="<?php echo $this->lang->line('emriLendes')?>"></div></th>
                            <th><div class="ui-widget"><input type="text" data-column="4" data-name="vlera_parashikuar" class="search-input-text" placeholder="<?php echo $this->lang->line('vleraParashikuar')?>"></div></th>
                            <th><div class="ui-widget"><input type="text" data-column="5" data-name="lloji_kontrates" class="search-input-text" placeholder="<?php echo $this->lang->line('llojiKontrates')?>"></div></th>
                            <th><div class="ui-widget"><input type="text" data-column="6" data-name="autoriteti_kontraktues" class="search-input-text" placeholder="<?php echo $this->lang->line('autoritetiKontraktues')?>"></div></th>

                            <th><div class="ui-widget"><input type="text" data-column="7" data-name="oe_ankues" class="search-input-text" placeholder="<?php echo $this->lang->line('OEAnkues')?>"></div></th>
                            <th><div class="ui-widget"><input type="text" data-column="8" data-name="rekomandimi_ekspertit" class="search-input-text" placeholder="<?php echo $this->lang->line('rekomandimet')?>"></div></th>                            
                            <th><div class="ui-widget"><input type="text" data-column="9" data-name="ankesa_kunder" class="search-input-text" placeholder="<?php echo $this->lang->line('ankesaKunder')?>"></div></th>

                            <th><div class="ui-widget"><input type="text" data-column="10" data-name="vendimi" class="search-input-text" placeholder="<?php echo $this->lang->line('vendimi')?>"></div></th>
                            <th><div class="ui-widget"><input type="text" data-column="11" data-name="terheqje_verejtje" class="search-input-text" placeholder="<?php echo $this->lang->line('terheqjeVerejtje')?>"></div></th>
                            <th><div class="ui-widget"><input type="text" data-column="12" data-name="neni" class="search-input-text" placeholder="<?php echo $this->lang->line('neni')?>"></div></th>
                            <th><div class="ui-widget"><input type="text" data-column="13" data-name="nr_vendimit" class="search-input-text" placeholder="<?php echo $this->lang->line('nrVendimit')?>"></div></th>
                            <th><div class="ui-widget"><input type="text" data-column="14" data-name="vendimet_meparshme" class="search-input-text" placeholder="<?php echo $this->lang->line('vendimetOld')?>"></div></th>
                            <th><div class="ui-widget"><input type="text" data-column="15" data-name="vendimi_jokonsistent" class="search-input-text" placeholder="<?php echo $this->lang->line('joKonsistent')?>"></div></th>
                            <th><div class="ui-widget"><input type="text" data-column="16" data-name="konfiskim_tarifes" class="search-input-text" placeholder="<?php echo $this->lang->line('konfiskimTarif')?>"></div></th>

                            <th><div class="ui-widget"><input type="text" data-column="17" data-name="kryetar" class="search-input-text" placeholder="<?php echo $this->lang->line('kryetarPanel')?>"></div></th>
                            <th><div class="ui-widget"><input type="text" data-column="18" data-name="referent" class="search-input-text" placeholder="<?php echo $this->lang->line('referantPanel')?>"></div></th>
                            <th><div class="ui-widget"><input type="text" data-column="19" data-name="anetari_1" class="search-input-text" placeholder="<?php echo $this->lang->line('anetari_I')?>"></div></th>
                            <th><div class="ui-widget"><input type="text" data-column="20" data-name="anetari_2" class="search-input-text" placeholder="<?php echo $this->lang->line('anetari_II')?>"></div></th>
                            <th><div class="ui-widget"><input type="text" data-column="21" data-name="anetari_3" class="search-input-text" placeholder="<?php echo $this->lang->line('anetari_III')?>"></div></th>
                            <th><div class="ui-widget"><input type="text" data-column="22" data-name="nr_panelisteve" class="search-input-text" placeholder="<?php echo $this->lang->line('nrPanelist')?>"></div></th>

                            <th><div class="ui-widget"><input type="text" data-column="23" data-name="eksperti_shqyrtues" class="search-input-text" placeholder="<?php echo $this->lang->line('ekspertiShqyrtues')?>"></div></th>
                            <th><div class="ui-widget"><input type="text" data-column="24" data-name="eksperti_teknik" class="search-input-text" placeholder="<?php echo $this->lang->line('ekspertiTeknik')?>"></div></th>

                            <th><div class="ui-widget"><input type="text" data-column="25" data-name="seanca" class="search-input-text" placeholder="<?php echo $this->lang->line('seancaHM')?>"></div></th>

                            <th><div class="ui-widget"><input type="text" data-column="26" data-name="data_ankeses" class="search-input-text" placeholder="<?php echo $this->lang->line('dataAnkeses')?>"></div></th>
                            <th><div class="ui-widget"><input type="text" data-column="27" data-name="data_vendimit" class="search-input-text" placeholder="<?php echo $this->lang->line('dataVendimit')?>"></div></th>
                            <th><div class="ui-widget"><input type="text" data-column="28" data-name="data_publikimit" class="search-input-text" placeholder="<?php echo $this->lang->line('dataPublikimit')?>"></div></th>

                            <th><?php echo $this->lang->line('kohaAV')?></th>
                            <th><?php echo $this->lang->line('kohaVP')?></th>
                            <th><?php echo $this->lang->line('kohaAP')?></th>

                            <th><?php echo $this->lang->line('keywords')?></th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
        <?php $this->load->view('template/legjenda', $legjenda); ?>
    </div>
</section>
<script>
var site_url = '<?php echo base_url(); ?>';
var encodedLogo = '<?php echo $this->config->item("encodedLogo"); ?>';
var xhr;
$(document).ready(function() {
    $(".form_date").datetimepicker({     
        //format: 'MM-DD-YYYY'
        format: 'YYYY-MM-DD'
    });
    $('#advSearch').on('hidden.bs.collapse', function (e) {
        $('#advFiltersTerms').html('');
        $('#advSearchIcon').addClass('fa-search').removeClass('fa-times');
        $('.kerkimi_btn').html('<?php echo $this->lang->line('kerkimAvancuar')?>');
        $('.tags-filter').removeAttr('checked');
        $('.ngaData, .deriData').val('');
        $('.advFilters').val('').trigger('chosen:updated');
        vendimet_table.draw();
    });
    $('#advSearch').on('shown.bs.collapse', function (e) {
        $('.kerkimi_btn').html('<?php echo $this->lang->line('pastroKerkimin')?>');
        $('#advSearchIcon').addClass('fa-times').removeClass('fa-search');
    });
    var vendimet_table = $('#vendimet_table').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'pageLength',
            {
                extend: 'excel',
                exportOptions: {
                    columns: [':visible :not(.not-export-col)']
                },
                title: '<?php echo $this->lang->line('vendimetKritere')?>',           
            },
            {
                extend: 'pdfHtml5',
                orientation: 'landscape',
                pageSize: 'LEGAL',
                exportOptions: {
                    columns: [':visible :not(.not-export-col)']             
                },
                title: '<?php echo $this->lang->line('vendimetKritere')?>',
                customize: function ( doc ) {
                    doc.content.splice( 1, 0, {
                        margin: [ 0, 0, 0, 12 ],
                        alignment: 'center',
                        image: encodedLogo,
                        width: 170
                    } );
                }             
            },
            {
                extend: 'print',
                exportOptions: {
                    columns: [':visible :not(.not-export-col)']            
                },
                title: '<?php echo $this->lang->line('vendimetKritere')?>',              
            },
            {
                extend: 'csvHtml5',
                exportOptions: {
                    columns: [':visible :not(.not-export-col)']             
                },
                title: '<?php echo $this->lang->line('vendimetKritere')?>',              
            },
            {
                extend: 'colvis',
                collectionLayout: 'fixed one-column',
                columns: [5,9,10,11,12,13,14,15,16,17,18,19,20,21,22,8,23,24,25,26,27,28,29,30,31,32],
                text: '<?php echo $this->lang->line('zgjedhKolonat')?>',
                prefixButtons: [
                    { extend:'columnToggle', text:'<?php echo $this->lang->line('zgjedhTeGjitha')?>', className:"toggleBtnSelect toggleBtnGroup" },
                ],
                postfixButtons: [
                    { extend:'colvisRestore', text:'<?php echo $this->lang->line('kolonatFillestare')?>', className:"toggleBtnRestore toggleBtnGroup" }
                ]
            },
        ],
        columnDefs: [
            { targets: [0, 1, 2, 3, 4, 6, 7, 8, 10, 13], visible: true },
            { targets: '_all', visible: false },
            { className: "details-control", "targets": [0], "orderable": false, "width": "10px", "searchable": false },
            { className: "details-label", "targets": [1], "orderable": false, "searchable": false },
            { className: "lenda-width", targets: 3}
        ],
        "language": {
                "processing": "<?php echo $this->lang->line('prit')?>...", //add a loading image,simply putting <img src="loader.gif" /> tag.
                buttons: {
                    pageLength: {
                    _: "<?php echo $this->lang->line('shfaqRreshta')?>",
                    '-1': "<?php echo $this->lang->line('gjithRreshtat')?>"
                    }
                },
                "zeroRecords": "<?php echo $this->lang->line('empty')?>.",
                "info": "<?php echo $this->lang->line('startEnd')?>",
                "paginate": {
                            "previous": "<?php echo $this->lang->line('faqjaPrevious')?>",
                            "next": "<?php echo $this->lang->line('faqjaTjeter')?>",
                            "first": "<?php echo $this->lang->line('faqjaPare')?>",
                            "last": "<?php echo $this->lang->line('faqjaFundit')?>"
                        },
                "search": "<?php echo $this->lang->line('search')?>",
                "infoFiltered": " <?php echo $this->lang->line('totalMAX')?>"
            },          
        "autoWidth": false,
        "lengthMenu": [ [15, 25, 50, -1], [15, 25, 50, "<?php echo $this->lang->line('teGjitha')?>"] ],
        "processing": true,
        "serverSide": true,
        "scrollX": true,
        "searchHighlight": true,             
        "ajax":{
            url :site_url + 'vendimet/index/json', // json datasource
            type: "post",  // method  , by default get     
            data:  function ( d ) {
                $('#advFiltersTerms').html('');
                d.advFilters = {};

                d.advFilters.datat = [];
                var ngaData = $(".ngaData").val();
                var deriData = $(".deriData").val();
                d.advFilters.datat.push({"ngaData":ngaData});
                d.advFilters.datat.push({"deriData":deriData});

                d.advFilters.vlerat = [];
                var ngaVlera = $(".ngaVlera").val();
                var deriVlera = $(".deriVlera").val();
                d.advFilters.vlerat.push({"ngaVlera":ngaVlera});
                d.advFilters.vlerat.push({"deriVlera":deriVlera});

                d.advFilters.ak = [];
                var ak = $(".ak").val();
                d.advFilters.ak.push({"ak":ak});

                d.advFilters.oeankues = [];
                var oeankues = $(".oeankues").val();
                d.advFilters.oeankues.push({"oeankues":oeankues});

                d.advFilters.panelistet = [];
                var paneli = $(".panelistet").val();
                d.advFilters.panelistet.push({"paneli":paneli});

                d.advFilters.rekomandimet = [];
                var rekomandimet = $(".rekomandimet").val();
                d.advFilters.rekomandimet.push({"rekomandimet":rekomandimet});

                d.advFilters.nenet = [];
                var nenet = $(".nenet").val();
                d.advFilters.nenet.push({"nenet":nenet});

                d.advFilters.vendimet = [];
                var vendimet = $(".vendimet").val();
                d.advFilters.vendimet.push({"vendimet":vendimet});

                d.advFilters.tags = [];
                var tags = [];
                $(".tags-filter").each(function(){
                    if (this.checked) {
                        d.advFilters.tags.push(parseInt($(this).val())); 
                    }
                });
                if((d.advFilters.datat[0].ngaData && d.advFilters.datat[1].deriData) || (d.advFilters.vlerat[0].ngaVlera && d.advFilters.vlerat[1].deriVlera) || d.advFilters.ak[0].ak || d.advFilters.oeankues[0].oeankues || d.advFilters.panelistet[0].paneli || d.advFilters.rekomandimet[0].rekomandimet || d.advFilters.nenet[0].nenet || d.advFilters.vendimet[0].vendimet || (d.advFilters.tags.length > 0))
                {
                    var result = '<div class="tags-section"><h4><?php echo $this->lang->line('rezultatetKriter')?>:</h4>';
                    result = result + '<ul>';

                    if(d.advFilters.datat[0].ngaData || d.advFilters.datat[1].deriData)
                    {
                        result = result + '<li><?php echo strtoupper($this->lang->line('datat'))?>: <?php echo $this->lang->line('nga')?> <span>'+d.advFilters.datat[0].ngaData+'</span> <?php echo $this->lang->line('deri')?> <span>'+d.advFilters.datat[1].deriData+'</span></li>'; 
                    }  

                    if(d.advFilters.vlerat[0].ngaVlera && d.advFilters.vlerat[1].deriVlera)
                    {
                        result = result + '<li><?php echo strtoupper($this->lang->line('vleraParashikuar'))?>: <?php echo $this->lang->line('nga')?> €<span>'+d.advFilters.vlerat[0].ngaVlera+'</span> <?php echo $this->lang->line('deri')?> €<span>'+d.advFilters.vlerat[1].deriVlera+'</span></li>'; 
                    }

                    if(d.advFilters.ak[0].ak){
                        var ak_str = '';
                        for(var i=0; i < d.advFilters.ak[0].ak.length; i++){
                            if(i != 0){
                                ak_str = ak_str + ', ';
                            }
                            ak_str = ak_str + '<span>'+d.advFilters.ak[0].ak[i]+'</span>';
                        }
                        result = result + '<li><?php echo strtoupper($this->lang->line('autoritetiKontraktues'))?>: '+ak_str+'</li>'; 
                    }

                    if(d.advFilters.oeankues[0].oeankues){
                        var oeankues_str = '';
                        for(var i=0; i < d.advFilters.oeankues[0].oeankues.length; i++){
                            if(i != 0){
                                oeankues_str = oeankues_str + ', ';
                            }
                            oeankues_str = oeankues_str + '<span>'+d.advFilters.oeankues[0].oeankues[i]+'</span>';
                        }
                        result = result + '<li><?php echo strtoupper($this->lang->line('OEAnkues'))?>: '+oeankues_str+'</li>'; 
                    }

                    if(d.advFilters.panelistet[0].paneli){
                        var panelistet_str = '';
                        for(var i=0; i < d.advFilters.panelistet[0].paneli.length; i++){
                            if(i != 0){
                                panelistet_str = panelistet_str + ', ';
                            }
                            panelistet_str = panelistet_str + '<span>'+d.advFilters.panelistet[0].paneli[i]+'</span>';
                        }
                        result = result + '<li><?php echo strtoupper($this->lang->line('paneli'))?>: '+panelistet_str+'</li>'; 
                    }

                    if(d.advFilters.rekomandimet[0].rekomandimet){
                        var rekomandimet_str = '';
                        for(var i=0; i < d.advFilters.rekomandimet[0].rekomandimet.length; i++){
                            if(i != 0){
                                rekomandimet_str = rekomandimet_str + ', ';
                            }
                            rekomandimet_str = rekomandimet_str + '<span>'+d.advFilters.rekomandimet[0].rekomandimet[i]+'</span>';
                        }
                        result = result + '<li><?php echo strtoupper($this->lang->line('rekomandimet'))?>: '+rekomandimet_str+'</li>'; 
                    }                    

                    if(d.advFilters.nenet[0].nenet){
                        var nenet_str = '';
                        for(var i=0; i < d.advFilters.nenet[0].nenet.length; i++){
                            if(i != 0){
                                nenet_str = nenet_str + ', ';
                            }
                            nenet_str = nenet_str + '<span>'+d.advFilters.nenet[0].nenet[i]+'</span>';
                        }
                        result = result + '<li><?php echo strtoupper($this->lang->line('nenet'))?>: '+nenet_str+'</li>'; 
                    }

                    if(d.advFilters.vendimet[0].vendimet){
                        var vendimet_str = '';
                        for(var i=0; i < d.advFilters.vendimet[0].vendimet.length; i++){
                            if(i != 0){
                                vendimet_str = vendimet_str + ', ';
                            }
                            vendimet_str = vendimet_str + '<span>'+d.advFilters.vendimet[0].vendimet[i]+'</span>';
                        }
                        result = result + '<li><?php echo strtoupper($this->lang->line('vendimetPanelit'))?>: '+vendimet_str+'</li>'; 
                    }
                    if(d.advFilters.tags.length > 0){
                        var tags_str = '';
                        for(var i=0; i < d.advFilters.tags.length; i++){
                            var tags = get_tag(d.advFilters.tags[i]);
                            if(tags.id){
                                if(i != 0){
                                    tags_str = tags_str + ', ';
                                }
                                tags_str = tags_str + '<span>'+tags.emri+'</span>';
                            }
                        }
                      
                        result = result + '<li><?php echo $this->lang->line('keyords')?>: '+tags_str+'</li>'; 
                    }                                         
                    result = result + '</ul>';
                    result = result + '</div>';
                    $('#advFiltersTerms').html(result);
                }

                $('.buttons-print').html('<span class="fa fa-print" data-toggle="tooltip" title="Printo"/>');
                $('.buttons-excel').html('<span class="fa fa-file-excel-o" data-toggle="tooltip" title="Excel"/>');
                $('.buttons-csv').html('<span class="fa fa-file-o" data-toggle="tooltip" title="CSV"/>');
                $('.buttons-pdf').html('<span class="fa fa-file-pdf-o" data-toggle="tooltip" title="PDF"/>');
            },   
            error: function(){  // error handling
                $(".vendimet_table_error").html("");
                $("#vendimet_table").append('<tbody class="vendimet_table_error"><tr><th colspan="10"><?php echo $this->lang->line('empty')?>.</th></tr></tbody>');
                //$("#employee-grid_processing").css("display","none");                 
            }
        }
    } );

    $('.advFilters').on('change', function (e) {
        vendimet_table.draw();
    } );
    $(".form_date").on("dp.change", function (e) {
        vendimet_table.draw();
    } );
    $('.search-input-text').autocomplete({
        minLength: 0,
        source: function(request, response){
            var el = $(this.element);
            var i =el.attr('data-column') // getting column index
            var n =el.attr('data-name') // getting name index
            var v =el.val();  // getting search input value
            if(v.length >= 1){
                if(xhr) {
                    xhr.abort();
                }
                var post_data = {table: 'vendimet', column: n, value: v};
                xhr = $.ajax({
                    'async': false,
                    'url': site_url + 'vendimet/autocomplete_data/',
                    'data': post_data,
                    'type': 'POST',
                    success:function(data){
                        data = JSON.parse(data);
                        response(data);
                    }
                });            
            }else{
                vendimet_table.columns(i).search('').draw();
            }
        },
        select: function(event, ui) {
            var i = $(this).attr('data-column') // getting column index
            vendimet_table.columns(i).search(ui.item.value).draw();
        }           
    });

    $('#vendimet_table tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        //var id = tr.find('.pezullimi_id').val();
        var row = vendimet_table.row( tr );
        
        if ( row.child.isShown() ) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
            // Open this row
            row.child(fetch_data(row.data())).show();
            tr.addClass('shown');
            $(this).closest('tr').next().css('background', '#f5f5f5');
            $(this).closest('tr').next().find('tr').css('background', 'transparent');
            $(this).closest('tr').next().find('th').css({'border-bottom': '1px solid #9e9e9e', 'color': '#111'});
            $(this).closest('tr').next().find('tr td').css({'color': '#111'});
        }

    } );

    var buttons = vendimet_table.buttons('.buttons-columnVisibility').nodes();
    var groupArrOne = [1];
    var groupArrTwo = [2];
    var groupArrThree = [3,4,5,6,7,8,9];
    var groupArrFour = [10,11,12,13,14,15];
    var groupArrFive = [16,17,18];
    var groupArrSix = [19];
    var groupArrSeven = [20,21,22,23,24,25];
    var groupArrEight = [26];
    var groupOne = 0;
    var groupTwo = 0; 
    var groupThree = 0; 
    var groupFour = 0; 
    var groupFive = 0;
    var groupSix = 0;
    var groupSeven = 0;
    var groupEight = 0;     
    $.each( buttons, function( key, value ) {
        var group;
        var groupDevider;
        if(groupArrOne.indexOf(key) != -1){
            group = ' groupOne';
            if(groupOne == 0){
                groupDevider = 'AKTIVITETI I PROKURIMIT';
            }            
            groupOne++;
        }else if(groupArrTwo.indexOf(key) != -1){
            group = ' groupTwo';
            if(groupTwo == 0){
                groupDevider = 'ANKESA';
            }            
            groupTwo++;
        }else if(groupArrThree.indexOf(key) != -1){
            group = ' groupThree';
            if(groupThree == 0){
                groupDevider = 'VENDIMET';
            }            
            groupThree++;
        }else if(groupArrFour.indexOf(key) != -1){
            group = ' groupFour';
            if(groupFour == 0){
                groupDevider = 'PANELI';
            }            
            groupFour++;
        }else if(groupArrFive.indexOf(key) != -1){
            group = ' groupFive';
            if(groupFive == 0){
                groupDevider = 'REKOMANDIMI I EKSPERTIT';
            }            
            groupFive++;
        }else if(groupArrSix.indexOf(key) != -1){
            group = ' groupSix';
            if(groupSix == 0){
                groupDevider = 'SEANCA HAPUR/MBYLLUR';
            }            
            groupSix++;
        }else if(groupArrSeven.indexOf(key) != -1){
            group = ' groupSeven';
            if(groupSeven == 0){
                groupDevider = 'AFATET';
            }            
            groupSeven++;
        }else if(groupArrEight.indexOf(key) != -1){
            group = ' groupSeven';
            if(groupEight == 0){
                groupDevider = 'TAG';
            }            
            groupEight++;
        }else{
            group = '';
            groupDevider = '';
        }
        value.className = value.className + group;
        if(groupDevider){
            value.className = value.className + " groupHeader";
        }
    });
});

function get_tag(id){
    var result = false;
    xhr = $.ajax({
        'async': false,
        'url': site_url + 'vendimet/get_tag/' + id,
        'type': 'GET',
        success:function(data){
            data = JSON.parse(data);
            result = data;
        }
    });
    return result; 
}
function fetch_data ( d ) {
    // `d` is the original data object for the row
    // Number order is mixed due to new code being added so old code is not mixed up and destroyed :)
    result = '<div class="col-md-12">'+
    '<table cellpadding="8" cellspacing="0" border="0" class="extra-details">' +
        '<th colspan="2"><?php echo $this->lang->line('aktivitetiProkurimit')?></th>'+ 
        '<tr>'+
            '<td><?php echo $this->lang->line('nrProtokollit')?>:</td>'+
            '<td>'+((d[2] != null) ? d[2] : '')+'</a></td>'+
        '</tr>'+
        '<tr>'+
            '<td><?php echo $this->lang->line('emriLendes')?>:</td>'+
            '<td>'+((d[3] != null) ? d[3] : '')+'</a></td>'+
        '</tr>'+
        '<tr>'+
            '<td>Kategoria Kontrates:</td>'+
            '<td>'+((d[35] != null) ? d[35] : '')+'</a></td>'+
        '</tr>'+
        '<tr>'+
            '<td><?php echo $this->lang->line('vleraParashikuar')?>:</td>'+
            '<td>'+((d[4] != null) ? d[4] : '')+'</a></td>'+
        '</tr>'+
        '<tr>'+
            '<td><?php echo $this->lang->line('llojiKontrates')?>:</td>'+
            '<td>'+((d[5] != null) ? d[5] : '')+'</a></td>'+
        '</tr>'+
        '<tr>'+
            '<td><?php echo $this->lang->line('autoritetiKontraktues')?>:</td>'+
            '<td>'+((d[6] != null) ? d[6] : '')+'</a></td>'+
        '</tr>'+     
        '<th colspan="2"><?php echo $this->lang->line('ankesa')?></th>'+
        '<tr>'+
            '<td><?php echo $this->lang->line('OEAnkues')?>:</td>'+ 
            '<td>'+((d[7] != null) ? d[7] : '')+'</a></td>'+
        '</tr>'+
        '<tr>'+
            '<td>Çmimi i ofertës së OE Ankues:</td>'+
            '<td>'+((d[36] != null) ? '€'+d[36] : 'N/A')+'</a></td>'+
        '</tr>'+
        '<tr>'+
            '<td>Çmimi i ofertës së OE të Rekomanduar:</td>'+
            '<td>'+((d[45] != null) ? '€'+d[45] : 'N/A')+'</a></td>'+
        '</tr>'+
        '<tr>'+
            '<td>Depozita per Ankesë:</td>'+
            '<td>'+((d[37] != null) ? '€'+d[37]+'.00' : 'N/A')+'</a></td>'+
        '</tr>'+
        '<tr>'+
            '<td><?php echo $this->lang->line('ankesaKunder')?>:</td>'+
            '<td>'+((d[9] != null) ? d[9] : '')+'</a></td>'+
        '</tr>'+
        '<tr>'+
            '<td>Teksti kryesor i Ankesë:</td>'+
            '<td>'+((d[41] != null) ? d[41] : '')+'</td>'+
        '</tr>'+
        '<tr>'+
            '<td>Data e njoftimit të AK:</td>'+
            '<td>'+((d[39] != null) ? d[39] : '')+'</td>'+
        '</tr>'+
        '<tr>'+
            '<td>Ankesa:</td>'+
            '<td>'+((d[40] != null) ? d[40] : '')+'</td>'+
        '</tr>'+
        '<tr>'+
            '<td>Statusi i Ankesës:</td>'+
            '<td>'+((d[38] != null) ? d[38] : '')+'</td>'+
        '</tr>'+
        '<tr>'+                
        '<th colspan="2"><?php echo $this->lang->line('vendimet')?></th>'+
        '<tr>'+
            '<td><?php echo $this->lang->line('vendimi')?>:</td>'+
            '<td>'+((d[10] != null) ? d[10] : '')+'</a></td>'+
        '</tr>'+
        '<tr>'+
            '<td><?php echo $this->lang->line('terheqjeVerejtjes')?>:</td>'+
            '<td>'+((d[11] != null) ? d[11] : '')+'</td>'+
        '</tr>'+
        '<tr>'+
            '<td><?php echo $this->lang->line('neni')?>:</td>'+
            '<td>'+((d[12] != null) ? d[12] : '')+'</td>'+
        '</tr>'+
        '<tr>'+
            '<td><?php echo $this->lang->line('nrVendimit')?>:</td>'+
            '<td>'+((d[13] != null) ? d[13] : '')+'</td>'+
        '</tr>'+
        '<tr>'+
            '<td><?php echo $this->lang->line('vendimetOld')?>:</td>'+
            '<td>'+((d[14] != null) ? d[14] : '')+'</td>'+  
        '</tr>'+
        '<tr>'+
            '<td><?php echo $this->lang->line('joKonsistent')?>:</td>'+
            '<td>'+((d[15] != null) ? d[15] : '')+'</td>'+                                                                                                                                                                                                                                                  //Etnik Pruthi
        '</tr>'+
        '<tr>'+
            '<td><?php echo $this->lang->line('konfiskimTarif')?>:</td>'+
            '<td>'+((d[16] != null) ? d[16] : '')+'</td>'+
        '</tr>'+
        '<th colspan="2"><?php echo $this->lang->line('paneli')?></th>'+ 
        '<tr>'+
            '<td><?php echo $this->lang->line('kryetarPanel')?>:</td>'+
            '<td>'+((d[17] != null) ? d[17] : '')+'</td>'+
        '</tr>'+
        '<tr>'+
            '<td><?php echo $this->lang->line('referentPanel')?>:</td>'+
            '<td>'+((d[18] != null) ? d[18] : '')+'</td>'+
        '</tr>'+
        '<tr>'+
            '<td><?php echo $this->lang->line('anetari_I')?>:</td>'+
            '<td>'+((d[19] != null) ? d[19] : '')+'</td>'+
        '</tr>'+
        '<tr>'+
            '<td><?php echo $this->lang->line('anetari_II')?>:</td>'+
            '<td>'+((d[20] != null) ? d[20] : '')+'</td>'+
        '</tr>'+ 
        '<tr>'+
            '<td><?php echo $this->lang->line('anetari_III')?>:</td>'+
            '<td>'+((d[21] != null) ? d[21] : '')+'</td>'+
        '</tr>'+ 
        '<tr>'+
            '<td><?php echo $this->lang->line('nrPanelist')?>:</td>'+
            '<td>'+((d[22] != null) ? d[22] : '')+'</td>'+
        '</tr>'+
        '<th colspan="2"><?php echo $this->lang->line('rekomandimet')?></th>'+          
        '<tr>'+
            '<td><?php echo $this->lang->line('rekomandimet')?>:</td>'+
            '<td>'+((d[8] != null) ? d[8] : '')+'</td>'+
        '</tr>'+ 
        '<tr>'+
            '<td><?php echo $this->lang->line('ekspertiShqyrtues')?>:</td>'+
            '<td>'+((d[23] != null) ? d[23] : '')+'</td>'+
        '</tr>'+ 
        '<tr>'+
            '<td><?php echo $this->lang->line('ekspertiTeknik')?>:</td>'+
            '<td>'+((d[24] != null) ? d[24] : '')+'</td>'+
        '</tr>'+
        '<tr>'+
            '<td>Eksperti Profesional:</td>'+
            '<td>'+((d[42] != null) ? d[42] : '')+'</td>'+
        '</tr>'+
        '<tr>'+
            '<td>Data e njoftimit me ekspertizë për AK dhe OE:</td>'+
            '<td>'+((d[43] != null) ? d[43] : '')+'</td>'+
        '</tr>'+
        '<tr>'+
            '<td>Raporti Ekspertit:</td>'+
            '<td>'+((d[44] != null) ? d[44] : '')+'</td>'+
        '</tr>'+    
        '<tr>'+
            '<td>Përgjigja e AK:</td>'+
            '<td>'+((d[29] != null) ? d[29] : '')+'</td>'+
        '</tr>'+
        '<tr>'+
            '<td>Përgjigja e OE:</td>'+
            '<td>'+((d[34] != null) ? d[34] : '')+'</td>'+
        '</tr>'+
        '<th colspan="2"><?php echo $this->lang->line('seancaHM')?></th>'+
        '<tr>'+
            '<td><?php echo $this->lang->line('seancaHM')?>:</td>'+
            '<td>'+((d[25] != null) ? d[25] : '')+'</td>'+
        '</tr>'+
        '<tr>'+
            '<td>Data e Seancës:</td>'+
            '<td>'+((d[46] != null) ? d[46] : '')+'</td>'+
        '</tr>'+
        '<th colspan="2"><?php echo $this->lang->line('afatet')?></th>'+
        '<tr>'+
            '<td><?php echo $this->lang->line('dataAnkeses')?>:</td>'+
            '<td>'+((d[26] != null) ? d[26] : '')+'</td>'+
        '</tr>'+
            '<td><?php echo $this->lang->line('dataVendimit')?>:</td>'+
            '<td>'+((d[27] != null) ? d[27] : '')+'</td>'+
        '</tr>'+
        '<tr>'+
            '<td><?php echo $this->lang->line('dataPublikimit')?>:</td>'+
            '<td>'+((d[28] != null) ? d[28] : '')+'</td>'+
        '</tr>'+
        '<tr>'+
            '<td><?php echo $this->lang->line('kohaAV')?>:</td>'+
            '<td>'+((d[30] != null) ? d[30] : '')+'</td>'+
        '</tr>'+
        '<tr>'+
            '<td><?php echo $this->lang->line('kohaVP')?>:</td>'+
            '<td>'+((d[31] != null) ? d[31] : '')+'</td>'+
        '</tr>'+
        '<tr>'+
            '<td><?php echo $this->lang->line('kohaAP')?>:</td>'+
            '<td>'+((d[32] != null) ? d[32] : '')+'</td>'+
        '</tr>'+
    '</table>'+
'</div>';

    return result;
}
</script>