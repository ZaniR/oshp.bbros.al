<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends MY_Controller {

	public function __construct() 
	{
        parent::__construct();
        $this->load->model('Vendimet_model');
    }

	public function index($type = null) {
		if($type == 'json'){
			$year = $this->input->post('year');
			$json_data['vendimet_total'] = $this->Vendimet_model->get_total_vendime_by_filter('vendimet', $year);
			$json_data['lista_total'] = $this->Vendimet_model->get_total_vendime_by_filter('liste_e_zeze', $year);
			$json_data['gjobe_total'] = $this->Vendimet_model->get_total_vendime_by_filter('gjobe', $year);
			$json_data['pezullimet_total'] = $this->Vendimet_model->get_total_vendime_by_filter('heqje_pezullimi', $year);
			echo json_encode($json_data);
			return;
		}else{
			$data['years'] = $this->Vendimet_model->get_all_years();
			$data['vendimet_total'] = $this->Vendimet_model->get_total_vendime_by_filter('vendimet');
			$data['lista_total'] = $this->Vendimet_model->get_total_vendime_by_filter('liste_e_zeze');
			$data['gjobe_total'] = $this->Vendimet_model->get_total_vendime_by_filter('gjobe');
			$data['pezullimet_total'] = $this->Vendimet_model->get_total_vendime_by_filter('heqje_pezullimi');
			$this->view('index', $data);
		}
	}

	public function about() {
		$this->view('about');
	}

	public function howTo(){
		$this->view('howTo');
	}

	public function mail(){

		$this->load->library('email');

		$subject = 'This is a test';
		$message = '<p>This message has been sent for testing purposes.</p>';

		// Get full html:
		$body = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
		<html xmlns="http://www.w3.org/1999/xhtml">
		<head>
		    <meta http-equiv="Content-Type" content="text/html; charset=' . strtolower(config_item('charset')) . '" />
		    <title>' . html_escape($subject) . '</title>
		    <style type="text/css">
		        body {
		            font-family: Arial, Verdana, Helvetica, sans-serif;
		            font-size: 16px;
		        }
		    </style>
		</head>
		<body>
		' . $message . '
		</body>
		</html>';
		// Also, for getting full html you may use the following internal method:
		//$body = $this->email->full_html($subject, $message);

		$result = $this->email
		    ->from('oshpdplusks@gmail.com')
		    ->reply_to('oshpdplusks@gmail.com')    // Optional, an account where a human being reads.
		    ->to('butrint.xh.babuni@gmail.com')
		    ->subject($subject)
		    ->message($body)
		    ->send();

		var_dump($result);
		echo '<br />';
		echo $this->email->print_debugger();
	}
}
