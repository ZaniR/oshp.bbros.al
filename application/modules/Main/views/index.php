<style type="text/css">
  .numbers-holder{
    padding: 30px;
    margin:10px 0px;
    border:1px dotted rgb(22, 103, 178,1);
  }
  .icon-holder{
    color:#fecc0a;
    text-align: center;
    margin: 0 auto;
    font-size: 40px;
    -webkit-transition: font-size 1s, -webkit-transform 1s; /* Safari */
    transition: font-size 1s, transform 1s;
  }
  .text-holder{
    text-transform: uppercase;
    text-align: center;
    display: block;
    font-weight: normal;
    font-size: 16px;
    color: #444;
  }
  .number-holder{
    text-align: center;
    font-size: 35px;
    font-weight: 500;
  }
  .numbers-holder:hover .icon-holder{
    -webkit-transform: rotate(360deg); /* Safari */
    transform: rotate(360deg);
  }
</style>
<section id="contact-section">
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 pull-right">
        <div class="form-group">
          <form action="<?php echo base_url(); ?>" method="POST">
          <label><?php echo $this->lang->line('kerkoMeVite')?>:</label>
          <select class="form-control" name="year" id="year"> 
            <option value="">- <?php echo $this->lang->line('teGjitha')?> -</option>
            <?php foreach($years as $year): ?>
              <option value="<?php echo $year; ?>"><?php echo $year; ?></option>
            <?php endforeach; ?>
          </select>
        </div>
      </div>
    </div>
    <div class="row">
      <a href="<?php echo base_url(); ?>vendimet">
      <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
        <div class="numbers-holder">
          <div class="icon-holder">
            <span class="glyphicon glyphicon-pawn" aria-hidden="true"></span>
          </div>
           <div class="number-holder">
            <span class="count" id="vendimet_total"><?php echo $vendimet_total; ?></span>
          </div>           
          <span class="text-holder">
            <?php echo $this->lang->line('vendimet')?>
          </span>
        </div>
      </div>
      </a>
      <a href="<?php echo base_url(); ?>vendimet/lista-e-zeze">
        <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
          <div class="numbers-holder">
            <div class="icon-holder">
              <span class="glyphicon glyphicon-ban-circle" aria-hidden="true"></span>
            </div>
            <div class="number-holder">
              <span class="count" id="lista_total"><?php echo $lista_total; ?></span>
            </div>            
            <span class="text-holder">
            <?php echo $this->lang->line('lista_e_zeze')?>
            </span>
          </div>
        </div>
      </a>
      <a href="<?php echo base_url(); ?>vendimet/gjobe">
        <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
          <div class="numbers-holder">
            <div class="icon-holder">
              <span class="glyphicon glyphicon-list-alt" aria-hidden="true"></span>
            </div>
            <div class="number-holder">
              <span class="count" id="gjobe_total"><?php echo $gjobe_total; ?></span>
            </div>
            <span class="text-holder">
            <?php echo $this->lang->line('gjobe')?>
            </span>
          </div>
        </div>
      </a>
      <a href="<?php echo base_url(); ?>vendimet/heqje-pezullimi">
        <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
          <div class="numbers-holder">
            <div class="icon-holder">
              <span class="glyphicon glyphicon-ok-sign" aria-hidden="true"></span>
            </div>
            <div class="number-holder">
              <span class="count" id="pezullimet_total"><?php echo $pezullimet_total; ?></span>
            </div>
            <span class="text-holder">
              <?php echo $this->lang->line('heqje_pezullimi')?>
            </span>
          </div>
        </div>  
        </a>                      
    </div>
  </div>
</section>
<script type="text/javascript">
  $(document).ready(function() {
    var site_url = '<?php echo base_url(); ?>';
    $('#year').change(function(){
      var year = $(this).val();
      $.ajax({
          url: site_url + 'main/index/json/',
          data: {year: year},
          type: 'POST',
          success:function(data){
            data = JSON.parse(data);
            $('#vendimet_total').html(data.vendimet_total);
            $('#lista_total').html(data.lista_total);
            $('#gjobe_total').html(data.gjobe_total);
            $('#pezullimet_total').html(data.pezullimet_total);
            counter();
          }
      }); 
    });   
    counter();
  });
function counter(){
  $('.count').each(function () {
    $(this).prop('Counter',0).animate({
        Counter: $(this).text()
      }, {
      duration: 1000,
      easing: 'swing',
      step: function (now) {
          $(this).text(Math.ceil(now));
      }
    });
  });
}
</script>