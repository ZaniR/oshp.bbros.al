<section id="contact-section">
	<div class="container about-us">
    	<div class="row">
    		<div class="col-lg-12">
				<h3><?php echo $this->lang->line('rreth_projektit') ?></h3>
				<p><?php echo $this->lang->line('rreth_projektit_tekst')?></p>
				<h3><?php echo $this->lang->line('tedhenat_nedb')?></h3>
				<p><?php echo $this->lang->line('tedhenat_nedb_tekst1')?></p>
				<p><?php echo $this->lang->line('tedhenat_nedb_paragraf')?></p>
				<h3><?php echo $this->lang->line('kufizimet')?></h3>
				<p><?php echo $this->lang->line('kufizimet_tekst')?></p>
				<h3><?php echo $this->lang->line('rreth_donatorit')?></h3>
				<p><?php echo $this->lang->line('rreth_donatorit_tekst')?></p>
				<p><?php echo $this->lang->line('rreth_donatorit_tekstother')?></p>
				<h3><?php echo $this->lang->line('sqarim')?></h3>
				<p><?php echo $this->lang->line('sqarim_tekst')?></p>

				<div class="about-address">
					<h4><strong><?php echo $this->lang->line('company_name_uppercase') ?></strong></h4>
					<p><?php echo $this->lang->line('rruga')?></p>
					<p>E-Mail: info@dplus-ks.org</p>
					<p>Tel: +383 (0)38 749 288</p>
				</div>
				<div class="about-managed">
					<p><?php echo $this->lang->line('pronesi')?></p>
				</div>
			</div>
		</div>
	</div>
</section>