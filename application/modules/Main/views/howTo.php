<style type="text/css">
    .howto  h2,h3 {
        color: #337ab7!important;
    }
    .howto img{
        margin-bottom: 20px;
        border: 1px dotted #337ab7;
        padding: 4px;
        max-width: 100%;
    }
    .howto ul {
        margin-left: 25px;
    }
    .howto ul li{
        list-style: circle;
    }
</style>


<section id="contact-section">
    <div class="container howto">
        <div class="row">
            <div class="col-lg-12">
           
                <h2><?php echo $this->lang->line('siTePerdoret')?></h2>
                <p><?php echo $this->lang->line('katerKategori')?>:</p>
                <ul>
                    <li><?php echo $this->lang->line('Vendimet për ankesat e OE')?></li>
                    <li><?php echo $this->lang->line('Vendimet për futje në listë të zezë')?></li>
                    <li><?php echo $this->lang->line('Vendimet për gjobë ndaj AK')?></li>
                    <li><?php echo $this->lang->line('Vendimet për heqje pezullimi')?></li>
                </ul>
                <h3><?php echo $this->lang->line('Vendimet për ankesat e OE')?></h3>
                <p><?php echo $this->lang->line('ankesatInfo')?></p>
                <p><?php echo $this->lang->line('kolonatStandarte')?></p>
                <img src="<?php echo $this->lang->line('kolonat_stand_img')?>" title="Kolonat standarde" alt="Kolonat standarde">
                <p><?php echo $this->lang->line('varesisht_nga_butoni')?></p>
                <img src="<?php echo $this->lang->line('kolonat_extra_img')?>" title="Kolonat extra" alt="Kolonat extra">  
                <p><?php echo $this->lang->line('total_qe_shfaqen')?></p>
                <p><?php echo $this->lang->line('databaza_jep_mundsi')?></p>
                <img src="<?php echo $this->lang->line('kerkimi_avancuar_img')?>" title="Kerkim i avancuar" alt="Kerkim i avancuar">
                <p><?php echo $this->lang->line('vendimet_nje_muaji')?></p>
                <p><?php echo $this->lang->line('filtrimi_teDhenave')?></p>
                <img src="<?php echo $this->lang->line('fjalet_kyqe_img')?>" title="Fjalet kyqe" alt="Fjalet kyqe">
                <p><?php echo $this->lang->line('kriteret_zgjedhje')?></p>
                <p><?php echo $this->lang->line('gjendje_fillestare')?></p>
                <p><?php echo $this->lang->line('menyre_tjeter_kerkimi')?></p>
                <img src="<?php echo $this->lang->line('kerko_img')?>" title="Kerko" alt="Kerko">
                <p><?php echo $this->lang->line('kerkimi_behet')?></p>
                <p><?php echo $this->lang->line('shiko_teDhenat')?></p>
                <img src="<?php echo $this->lang->line('vendimi_detaje_img')?>" title="Vendimi Detaje" alt="Vendimi Detaje">
                <p><?php echo $this->lang->line('lexo_vendimin')?></p>
                <p><?php echo $this->lang->line('shembull_tjeter')?></p>
                <img src="<?php echo $this->lang->line('kerkimi_1_img')?>" title="Kerkimi 1" alt="Kerkimi 1">
                <p><?php echo $this->lang->line('opsioni_shto_kolona')?></p>
                <img src="<?php echo $this->lang->line('kerkimi_2_img')?>" title="Kerkimi 2" alt="Kerkimi 2">
                <p><?php echo $this->lang->line('menyre_tjeter_kerkimi')?></p>
                <img src="<?php echo $this->lang->line('kerkimi_3_img')?>" title="Kerkimi 3" alt="Kerkimi 3">
                <p><?php echo $this->lang->line('vetem_vendimet')?></p>
                <img src="<?php echo $this->lang->line('kerkimi_4_img')?>" title="Kerkimi 4" alt="Kerkimi 4">
                <h3><?php echo $this->lang->line('eksportimi_titull')?></h3>
                <p><?php echo $this->lang->line('mundesi_eksportimi')?></p>
                <img src="<?php echo $this->lang->line('eksporti_img')?>" title="Eksporti" alt="Eksporti">
                <p><?php echo $this->lang->line('foto_me_larte')?></p>
                <h3><?php echo $this->lang->line('Vendimet për futje në listë të zezë')?></h3>
                <p><?php echo $this->lang->line('liste_e_zeze_info')?></p>
                <h3><?php echo $this->lang->line('vendimet_gjoba')?></h3>
                <p><?php echo $this->lang->line('gjoba_info')?></p>
                <h3><?php echo $this->lang->line('Vendimet për heqje pezullimi')?></h3>
                <p><?php echo $this->lang->line('heqje_pezullimi_info')?></p>
            </div>
        </div>
    </div>
</section>

