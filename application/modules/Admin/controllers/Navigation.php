<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Navigation extends MY_Controller {
	public function __construct() 
	{
        parent::__construct();
        $this->load->model('Navigation_model');
    }

    public function index($type = null)
    {
    	if($type == 'json'){
			$requestData = $_REQUEST;
			$columns = array(
				0	=> 'name',
				1	=> 'description',
				2  	=> 'url'
				);

			$totalData = $this->Navigation_model->get_total_navigations();
			$totalFiltered = $totalData;
			$fieldset = null;
			if(!empty($requestData['search']['value'])){
				$fieldset = array(
					'name'						=> $requestData['search']['value'],
					'description'				=> $requestData['search']['value'],
					'url'						=> $requestData['search']['value']
					);
			}
			$filters = null;
			$order_by = array('column' => 'id', 'direction' => 'asc');
			$limit = array('start' => $requestData['start'], 'length' => $requestData['length']);
			$navigations = $this->Navigation_model->get_navigations($fieldset, $order_by, $limit, $filters);
			if( !empty($requestData['search']['value']) || !empty($requestData['columns'][0]['search']['value'])) { 
				$totalFiltered = count($this->Navigation_model->get_navigations($fieldset, $order_by, null, $filters));
			}

			$data =array();
			foreach($navigations as $navigation){
				$nestedData = array();
				$nestedData[] = $navigation->name;
				$nestedData[] = $navigation->url;
				$nestedData[] = $navigation->parent->name;
				$nestedData[] = $navigation->active ? "Yes" : "No";
				$nestedData[] = $navigation->hidden ? "Yes" : "No";
				$nestedData[] = $navigation->authorization ? "Yes" : "No";
				$nestedData[] = '<a href="'.base_url().'admin/navigation/edit/'.$navigation->id.'" target="_blank" class="btn btn-xs btn-default"><i class="fa fa-pencil" title="Edit"></i></a> <a href="'.base_url().'admin/navigation/delete/'.$navigation->id.'" class="delete btn btn-xs btn-danger"><i class="fa fa-trash-o" title="Delete"></i></a>';

				$data[] = $nestedData;
			}
			$json_data = array(
				"draw"		=> intval($requestData["draw"]),
				"recordsTotal"    => intval( $totalData ),
				"recordsFiltered" => intval( $totalFiltered ),
				"data"		=> $data
				);
			echo json_encode($json_data);
			return;
		}else{
			$data['oshp'] = array();
		}
		$this->view('navigations', $data);		
	}

	public function edit($id = null){
    	if($this->input->post()) {
    		$navigation = $this->Navigation_model->object_from_post();
			$navigation = $this->Navigation_model->save($navigation);
			if($navigation->id){
				redirect('admin/navigation/edit/'.$navigation->id.'?success=1');
			}
    	}		
		$data['user_groups'] = $this->ion_auth->groups()->result();
		$data['navigation'] = $this->Navigation_model->get_navigation($id);
		$data['parent_navigations'] = $this->Navigation_model->get_parent_navigations($id);
		$this->view('edit_navigation', $data);
	}

    public function delete($id = null){
    	if(!$id) return;
		$navigation = $this->Navigation_model->get_navigation($id);
		if($navigation->id){
			if($this->Navigation_model->delete($navigation->id)) { 
				redirect('admin/navigation/?success=3');
			}
		}
		return;
    }

}