<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Upload extends MY_Controller {

	public function __construct() 
	{
        parent::__construct();
   	}

   	public function index(){
   		if($this->input->post()){
			$upload_dir = APPPATH .'modules/Backups/files/excel/';

			$config['upload_path']   = $upload_dir;
			$config['allowed_types'] = 'xlsx'; 
			$config['max_size']      = 5000; 
			$config['max_width']     = 1024; 
			$config['max_height']    = 768;
			$config['remove_spaces'] = TRUE;
           	$config['encrypt_name'] = TRUE;
			$this->load->library('upload', $config);

			if ( ! $this->upload->do_upload('file')) {
				$error = $this->upload->display_errors();
				redirect('admin/upload?error=1');
			} else { 
				$data = $this->upload->data(); 
				$database = $this->db->database;
				$user =  $this->db->username;
				$pass =  $this->db->password;
				$host =  $this->db->hostname;
				$tables = 'gjobe heqje_pezullimi liste_e_zeze vendimet tags vendimet_tags';
				if($_SERVER['HTTP_HOST'] == 'oshp.dplus-ks.loc'){
					$mysqldump = 'C:\"Program Files"\MySQL\"MySQL Server 5.6"\bin\mysqldump';
				}else{
					$mysqldump = 'mysqldump';
				}
				$xlsx_file_name = $data['file_name'];
				$xlsx_file_dir = $upload_dir.$xlsx_file_name;
				$sql_file_name = 'recoveryDump_'.$data["raw_name"].'.sql';
				$sql_file_dir = $upload_dir.$sql_file_name;

				exec("{$mysqldump} --user={$user} --password={$pass} --host={$host} --databases {$database} {$tables} --result-file={$sql_file_dir} 2>&1", $output);

				if(file_exists($xlsx_file_dir) && file_exists($sql_file_dir)){

					$stable = $this->input->post('stable');
					$overwrite = $this->input->post('overwrite');
					$types = $this->input->post('types');
					if(!$types) $types = array();

					//load the excel library
					$this->load->library('Excel');

					//read file from path
					$objPHPExcel = PHPExcel_IOFactory::load($xlsx_file_dir);
					
					$sheetnames = $objPHPExcel->getSheetNames();
					foreach($types as $type){
						if(in_array($type, $sheetnames)){
							//get the worksheet of your choice by its name
							$worksheet = $objPHPExcel->getSheetByName($type);

							//get only the sheet Cell Collection
							$cell_collection = $worksheet->getCellCollection();
							$highestRow = $worksheet->getHighestRow();
							$highestCol = $worksheet->getHighestColumn();
							$range_arr = $this->_createColumnsArray($highestCol);

							if ($type == 'Vendimet') {
								$table = 'vendimet';
								$arr_data = array();
								//extract to a PHP readable array format
								foreach ($cell_collection as $cell) {
								    $column = $worksheet->getCell($cell)->getColumn();
								    $row = $worksheet->getCell($cell)->getRow();
								    if($row < 3) continue;

								    if(($column == "M") || ($column == "N") || ($column == "O")){
										$data_value = $worksheet->getCell($cell)->getCalculatedValue(). '|';
										$data_value .= $worksheet->getCell($cell)->getHyperlink()->getUrl();
									}elseif(in_array($column, array("E", "AB", "AC", "AD"))){
										$data_value = $worksheet->getCell($cell)->getFormattedValue();
									}else{
										$data_value = $worksheet->getCell($cell)->getCalculatedValue();
									}

								    $arr_data[$row][$column] = $data_value;
								    foreach($range_arr as $keyCell){
								    	if(!isset($arr_data[$row][$keyCell]))
   										{
   											$arr_data[$row][$keyCell] = NULL;
   										}
								    }
								}
								$insert_data = array();
								foreach ($arr_data as $key => $data) {
									if($data['A'] != null){
										$nr_vendimit_arr = explode("|", $data['M']);
										if(isset($nr_vendimit_arr[0])){
											$nr_vendimit = json_encode(array(array('titulli'=>$nr_vendimit_arr[0], 'url'=>(isset($nr_vendimit_arr[1]) ? $nr_vendimit_arr[1] : NULL))));
										}else{
											$nr_vendimit = NULL;
										}

										$vendimet_meparshme_arr = explode("|", $data['N']);
										if(isset($vendimet_meparshme_arr[0])){
											$vendimet_meparshme = json_encode(array(array('titulli'=>$vendimet_meparshme_arr[0], 'url'=>(isset($vendimet_meparshme_arr[1]) ? $vendimet_meparshme_arr[1] : NULL))));
										}else{
											$vendimet_meparshme = NULL;
										}

										$vendimi_jokonsistent_arr = explode("|", $data['O']);
										if(isset($vendimi_jokonsistent_arr[0])){
											$vendimi_jokonsistent = json_encode(array(array('titulli'=>$vendimi_jokonsistent_arr[0], 'url'=>(isset($vendimi_jokonsistent_arr[1]) ? $vendimi_jokonsistent_arr[1] : NULL))));
										}else{
											$vendimi_jokonsistent = NULL;
										}
										$insert_data[] = array(
											'nr_protokollit' => $data['B'],
											'emri_lendes' => $data['C'],
											'vlera_parashikuar' => $data['D'],
											'lloji_kontrates' => $data['E'],
											'autoriteti_kontraktues' => $data['F'],
											'oe_ankues' => $data['G'],
											'ankesa_kunder' => $data['H'],
											'vendimi' => $data['I'],
											'terheqje_verejtje' => $data['J'],
											'neni' => $data['K'],
											'komente_paneli' => $data['L'],
											'nr_vendimit' => $nr_vendimit,
											'vendimet_meparshme' => $vendimet_meparshme,
											'vendimi_jokonsistent' => $vendimi_jokonsistent,
											'konfiskim_tarifes' =>  $data['P'],
											'kryetar' =>  $data['Q'],
											'referent' =>  $data['R'],
											'anetari_1' =>  $data['S'],
											'anetari_2' =>  $data['T'],
											'anetari_3' =>  $data['U'],
											'nr_panelisteve' =>  $data['V'],
											'rekomandimi_ekspertit' => $data['W'],
											'komente_eksperti' =>  $data['X'],
											'eksperti_shqyrtues' => $data['Y'],
											'eksperti_teknik' => $data['Z'],
											'seanca' =>  $data['AA'],
											'data_ankeses' => date('Y-m-d', strtotime($data['AB'])),
											'data_vendimit' => date('Y-m-d', strtotime($data['AC'])),
											'data_publikimit' => date('Y-m-d', strtotime($data['AD'])),
											'komente_gjenerale' =>  $data['AH'],
											'created_by' => 1,
											'created_on' => date('Y-m-d H:i:s'),
											'updated_by' => 1,
											'updated_on' => date('Y-m-d H:i:s')
										);
									}
								}

							} elseif ($type == 'Liste e Zeze') {
								$table = 'liste_e_zeze';
								$arr_data = array();
								//extract to a PHP readable array format
								foreach ($cell_collection as $cell) {
								    $column = $worksheet->getCell($cell)->getColumn();
								    $row = $worksheet->getCell($cell)->getRow();
								    if($row < 3) continue;

								    if($column == "M"){
										$data_value = $worksheet->getCell($cell)->getValue(). '|';
										$data_value .= $worksheet->getCell($cell)->getHyperlink()->getUrl();
									}elseif(in_array($column, array("E", "Y", "Z", "AA"))){
										$data_value = $worksheet->getCell($cell)->getFormattedValue();
									}else{
										$data_value = $worksheet->getCell($cell)->getValue();
									}
								 
								    $arr_data[$row][$column] = $data_value;
								    foreach($range_arr as $keyCell){
								    	if(!isset($arr_data[$row][$keyCell]))
   										{
   											$arr_data[$row][$keyCell] = NULL;
   										}
								    }								    
								}	

								$insert_data = array();
								foreach ($arr_data as $key => $data) {
									if($data['A'] != null){
										$vendimi_liste_zeze_arr = explode("|", $data['M']);
										if(isset($vendimi_liste_zeze_arr[0])){
											$vendimi_liste_zeze = json_encode(array(array('titulli'=>$vendimi_liste_zeze_arr[0], 'url'=>(isset($vendimi_liste_zeze_arr[1]) ? $vendimi_liste_zeze_arr[1] : NULL))));
										}else{
											$vendimi_liste_zeze = NULL;
										}

										$insert_data[] = array(
											'nr_protokollit' => $data['B'],
											'emri_lendes' => $data['C'],
											'vlera_parashikuar' => $data['D'],
											'lloji_kontrates' => $data['E'],
											'autoriteti_kontraktues' => $data['F'],
											'kerkesa_kunder_oe' => $data['G'],
											'pretendimet_ak' => $data['H'],
											'vendimi' => $data['I'],
											'diskualifikimi_kohezgjatja' => $data['J'],
											'neni' => $data['K'],
											'komente_paneli' => $data['L'],
											'vendimi_liste_zeze' => $vendimi_liste_zeze,
											'kryetar' => $data['N'],
											'referent' => $data['O'],
											'anetari_1' => $data['P'],
											'anetari_2' => $data['Q'],
											'anetari_3' => $data['R'],
											'nr_panelisteve' => $data['S'],
											'rekomandimi_ekspertit' => $data['T'],
											'komente_eksperti' => $data['U'],
											'eksperti_shqyrtues' => $data['V'],
											'eksperti_teknik' => $data['W'],
											'seanca' => $data['X'],
											'data_kerkeses' => date('Y-m-d', strtotime($data['Y'])),
											'data_vendimit' => date('Y-m-d', strtotime($data['Z'])),
											'data_publikimit' => date('Y-m-d', strtotime($data['AA'])),
											'komente_gjenerale' => $data['AE'],
											'created_by' => 1,
											'created_on' => date('Y-m-d H:i:s'),
											'updated_by' => 1,
											'updated_on' => date('Y-m-d H:i:s')
										);
									}
								}

							} elseif ($type == 'Gjobe') {
								$table = 'gjobe';
								$arr_data = array();
								//extract to a PHP readable array format
								foreach ($cell_collection as $cell) {
								    $column = $worksheet->getCell($cell)->getColumn();
								    $row = $worksheet->getCell($cell)->getRow();
								    if($row < 3) continue;

								    if($column == "K"){
										$data_value = $worksheet->getCell($cell)->getValue(). '|';
										$data_value .= $worksheet->getCell($cell)->getHyperlink()->getUrl();
									}elseif(in_array($column, array("E", "U", "V", "W"))){
										$data_value = $worksheet->getCell($cell)->getFormattedValue();
									}else{
										$data_value = $worksheet->getCell($cell)->getValue();
									}
		 
								    $arr_data[$row][$column] = $data_value;
								    foreach($range_arr as $keyCell){
								    	if(!isset($arr_data[$row][$keyCell]))
   										{
   											$arr_data[$row][$keyCell] = NULL;
   										}
								    }								    
								}
								$insert_data = array();
								foreach ($arr_data as $key => $data) {
									if($data['A'] != null){
										$vendim_arr = explode("|", $data['K']);
										if(isset($vendim_arr[0])){
											$vendimi = json_encode(array(array('titulli'=>$vendim_arr[0], 'url'=>(isset($vendim_arr[1]) ? $vendim_arr[1] : NULL))));
										}else{
											$vendimi = NULL;
										}

										$insert_data[] = array(
											'nr_protokollit' => $data['B'],
											'emri_lendes' => $data['C'],
											'vlera_parashikuar' => $data['D'],
											'lloji_kontrates' => $data['E'],
											'operatori_ekonomik' => $data['F'],
											'gjoba_kunder_ak' => $data['G'],
											'shuma' => $data['H'],
											'terheqje_verejtje' => $data['I'],
											'heqje_licence' => $data['J'],
											'vendimi' =>  $vendimi,
											'neni' => $data['L'],
											'komente_paneli' => $data['M'],
											'kryetar' => $data['N'],
											'referent' => $data['O'],
											'anetari_1' => $data['P'],
											'anetari_2' => $data['Q'],
											'anetari_3' => $data['R'],
											'nr_panelisteve' => $data['S'],
											'seanca' => $data['T'],
											'data_kerkeses' =>  date('Y-m-d', strtotime($data['U'])),
											'data_vendimit' =>  date('Y-m-d', strtotime($data['V'])),
											'data_publikimit' => date('Y-m-d', strtotime($data['W'])),
											'komente_gjenerale' => $data['AA'],
											'created_by' => 1,
											'created_on' => date('Y-m-d H:i:s'),
											'updated_by' => 1,
											'updated_on' => date('Y-m-d H:i:s')
										);
									}
								}

							} elseif ($type == 'Heqje pezullimi') {
								$table = 'heqje_pezullimi';
								$arr_data = array();
								//extract to a PHP readable array format
								foreach ($cell_collection as $cell) {
								    $column = $worksheet->getCell($cell)->getColumn();
								    $row = $worksheet->getCell($cell)->getRow();
								    if($row < 3) continue;

									if($column == "I"){
										$data_value = $worksheet->getCell($cell)->getValue(). '|';
										$data_value .= $worksheet->getCell($cell)->getHyperlink()->getUrl();
									}elseif(in_array($column, array("E", "S", "T" ,"U"))){
										$data_value = $worksheet->getCell($cell)->getFormattedValue();
									}else{
										$data_value = $worksheet->getCell($cell)->getValue();
									}

									$arr_data[$row][$column] = $data_value;
								    foreach($range_arr as $keyCell){
								    	if(!isset($arr_data[$row][$keyCell]))
   										{
   											$arr_data[$row][$keyCell] = NULL;
   										}
								    }									
								}

								$insert_data = array();
								foreach ($arr_data as $key => $data) {
									if($data['A'] != null){
										$vendim_arr = explode("|", $data['I']);
										if(isset($vendim_arr[0])){
											$vendimi = json_encode(array(array('titulli'=>$vendim_arr[0], 'url'=>(isset($vendim_arr[1]) ? $vendim_arr[1] : NULL))));
										}else{
											$vendimi = NULL;
										}
			 
										$insert_data[] = array(
											'nr_protokollit' => $data['B'],
											'emri_lendes' => $data['C'],
											'vlera_parashikuar' => $data['D'],
											'lloji_kontrates' => $data['E'],
											'autoriteti_kontraktuar' => $data['F'],
											'oe_rekomanduar' => $data['G'],
											'heqje_pezullimi' => $data['H'],
											'vendimi' =>  $vendimi,
											'neni' => $data['J'],
											'komente' => $data['K'],
											'kryetar' => $data['L'],
											'referent' => $data['M'],
											'anetari_1' => $data['N'],
											'anetari_2' => $data['O'],
											'anetari_3' => $data['P'],
											'nr_panelisteve' => $data['Q'],
											'seanca' => $data['R'],
											'data_kerkeses' => date('Y-m-d', strtotime($data['S'])),
											'data_vendimit' => date('Y-m-d', strtotime($data['T'])),
											'data_publikimit' => date('Y-m-d', strtotime($data['U'])),
											'komente_gjenerale' => $data['Y'],
											'created_by' => 1,
											'created_on' => date('Y-m-d H:i:s'),
											'updated_by' => 1,
											'updated_on' => date('Y-m-d H:i:s')
										);
									}
								}																
							} else {
								$table = null;
							}

							if($overwrite){
								$this->db->truncate($table);
								if($this->db->table_exists($table.'_tags')){
									$this->db->truncate($table.'_tags');
								}
							}

							$this->db->insert_batch($table, $insert_data);							
						}
					}

					$log_data = array(
						'upload_file' => $xlsx_file_name,
						'recovery_file' => $sql_file_name,
						'stable' => $this->input->post('stable'),
						'created_by' => $this->ion_auth->get_user_id(),
						'created_on' => date('Y-m-d H:i:s')
					);

					$this->db->insert('upload_excel', $log_data);
				}
				redirect('admin/upload?success=1');
			}    			
   		}
   		$data['recovery'] = $this->db->select('*')->from('upload_excel')->order_by('id','DESC')->get()->row();
   		$this->view('upload_excel', $data);
   	}

   	public function recovery($id){
   		if(!$id) redirect('admin/upload?error=2');
   		$recovery = $this->db->select('*')->from('upload_excel')->where('id', $id)->get()->row();
   		if(isset($recovery->id)){
   			$file_dir = APPPATH .'modules/Backups/files/excel/'.$recovery->recovery_file;
   			if(file_exists($file_dir)){
				$backup = read_file($file_dir);
                $sql_clean = '';
                foreach (explode("\n", $backup) as $line){
                    
                    if(isset($line[0]) && $line[0] != "#"){
                        $sql_clean .= $line."\n";
                    }
                    
                }
                $run_script = FALSE;            
                foreach (explode(";\n", $sql_clean) as $sql){
                    $sql = trim($sql);
                    if($sql) 
                    {
                        if($this->db->query($sql)){
                        	$run_script = TRUE;
                        }
                    } 
                }
                if($run_script){
                	redirect('admin/upload?success=2');
                }else{
                	redirect('admin/upload?error=2');
                }
   			}else{
   				redirect('admin/upload?error=2');
   			}
   		}else{
   			redirect('admin/upload?error=2');
   		}
   	}

	private function _createColumnsArray($end_column, $first_letters = '')
	{
	  $columns = array();
	  $length = strlen($end_column);
	  $letters = range('A', 'Z');

	  // Iterate over 26 letters.
	  foreach ($letters as $letter) {
	      // Paste the $first_letters before the next.
	      $column = $first_letters . $letter;

	      // Add the column to the final array.
	      $columns[] = $column;

	      // If it was the end column that was added, return the columns.
	      if ($column == $end_column)
	          return $columns;
	  }

	  // Add the column children.
	  foreach ($columns as $column) {
	      // Don't itterate if the $end_column was already set in a previous itteration.
	      // Stop iterating if you've reached the maximum character length.
	      if (!in_array($end_column, $columns) && strlen($column) < $length) {
	          $new_columns = $this->_createColumnsArray($end_column, $column);
	          // Merge the new columns which were created with the final columns array.
	          $columns = array_merge($columns, $new_columns);
	      }
	  }

	  return $columns;
	}

}