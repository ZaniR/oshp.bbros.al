<?php if ( ! defined('BASEPATH')) exit('Direct access allowed');

class LanguageSwitcher extends MY_Controller

{

   public function __construct() {

       parent::__construct();

   }

   
   function switchLang($language = "") {

       $language = ($language != "") ? $language : "albanian";

       $this->session->set_userdata('site_lang', $language);

       redirect($_SERVER['HTTP_REFERER']);

   }

}