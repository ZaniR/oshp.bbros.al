<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Vendimet extends MY_Controller {
	public function __construct() 
	{
        parent::__construct();
        $this->load->model(array('Heqje_pezullimi_model', 'Gjobe_model', 'Lista_zeze_model', 'Vendimet_model'));
    }

    // LISTA: VENDIMEVET
    public function index($type = null) {
    	if($type == 'json'){
			$requestData = $_REQUEST;
			$columns = array(
				0	=> null,
				1	=> null,
				2	=> 'nr_protokollit',
				3	=> 'emri_lendes',
				4	=> 'vlera_parashikuar',
				5  	=> 'lloji_kontrates',
				6	=> 'autoriteti_kontraktues',
				7 	=> 'oe_ankues',
				8	=> 'rekomandimi_ekspertit',				
				9	=> 'ankesa_kunder',
				10	=> 'vendimi',
				11	=> 'terheqje_verejtje',
				12	=> 'neni',
				13	=> 'komente_paneli',
				14	=> 'nr_vendimit',
				15	=> 'vendimet_meparshme',
				16	=> 'vendimi_jokonsistent',
				17	=> 'konfiskim_tarifes',
				18	=> 'kryetar',
				19	=> 'referent',
				20	=> 'anetari_1',
				21	=> 'anetari_2',
				22	=> 'anetari_3',
				23	=> 'nr_panelisteve',
				24	=> 'komente_eksperti',
				25	=> 'eksperti_shqyrtues',
				26	=> 'eksperti_teknik',
				27	=> 'seanca',
				28	=> 'data_ankeses',
				29	=> 'data_vendimit',
				30	=> 'data_publikimit',
				34	=> 'komente_gjenerale'
			);

			$totalData = $this->Vendimet_model->get_total_vendimet();
			$totalFiltered = $totalData;
			$fieldset = null;
			if(!empty($requestData['search']['value'])){
				$fieldset = array(
					'nr_protokollit'		=> $requestData['search']['value'],
					'emri_lendes'			=> $requestData['search']['value'],
					'vlera_parashikuar'		=> $requestData['search']['value'],
					'lloji_kontrates'		=> $requestData['search']['value'],
					'autoriteti_kontraktues'=> $requestData['search']['value'],
					'oe_ankues'				=> $requestData['search']['value'],
					'rekomandimi_ekspertit'	=> $requestData['search']['value'],
					'ankesa_kunder'			=> $requestData['search']['value'],
					'vendimi'				=> $requestData['search']['value'],
					'terheqje_verejtje'		=> $requestData['search']['value'],
					'neni'					=> $requestData['search']['value'],
					'komente_paneli'		=> $requestData['search']['value'],
					'nr_vendimit'			=> $requestData['search']['value'],
					'vendimet_meparshme'	=> $requestData['search']['value'],
					'vendimi_jokonsistent'	=> $requestData['search']['value'],
					'konfiskim_tarifes'		=> $requestData['search']['value'],
					'kryetar'				=> $requestData['search']['value'],
					'referent'				=> $requestData['search']['value'],
					'anetari_1'				=> $requestData['search']['value'],
					'anetari_2'				=> $requestData['search']['value'],
					'anetari_3'				=> $requestData['search']['value'],
					'nr_panelisteve'		=> $requestData['search']['value'],
					'komente_eksperti'		=> $requestData['search']['value'],
					'eksperti_shqyrtues'	=> $requestData['search']['value'],
					'eksperti_teknik'		=> $requestData['search']['value'],
					'seanca'				=> $requestData['search']['value'],
					'data_ankeses'			=> $requestData['search']['value'],
					'data_vendimit'			=> $requestData['search']['value'],
					'data_publikimit'		=> $requestData['search']['value'],
					'komente_gjenerale'		=> $requestData['search']['value']
				);
			}
			$filters = null;
			if(!empty($requestData['columns'][2]['search']['value']) || 
				!empty($requestData['columns'][3]['search']['value']) || 
				!empty($requestData['columns'][4]['search']['value']) || 
				!empty($requestData['columns'][5]['search']['value']) ||
				!empty($requestData['columns'][6]['search']['value']) ||
				!empty($requestData['columns'][7]['search']['value']) ||
				!empty($requestData['columns'][8]['search']['value']) ||
				!empty($requestData['columns'][9]['search']['value']) ||
				!empty($requestData['columns'][10]['search']['value']) ||
				!empty($requestData['columns'][11]['search']['value']) ||
				!empty($requestData['columns'][12]['search']['value']) ||
				!empty($requestData['columns'][13]['search']['value']) ||
				!empty($requestData['columns'][14]['search']['value']) ||
				!empty($requestData['columns'][15]['search']['value']) ||
				!empty($requestData['columns'][16]['search']['value']) ||
				!empty($requestData['columns'][17]['search']['value']) ||
				!empty($requestData['columns'][18]['search']['value']) ||
				!empty($requestData['columns'][19]['search']['value']) ||
				!empty($requestData['columns'][20]['search']['value']) ||
				!empty($requestData['columns'][21]['search']['value']) ||
				!empty($requestData['columns'][22]['search']['value']) ||
				!empty($requestData['columns'][23]['search']['value']) ||
				!empty($requestData['columns'][24]['search']['value']) ||
				!empty($requestData['columns'][25]['search']['value']) ||
				!empty($requestData['columns'][26]['search']['value']) ||
				!empty($requestData['columns'][27]['search']['value']) ||
				!empty($requestData['columns'][28]['search']['value']) ||
				!empty($requestData['columns'][29]['search']['value']) ||
				!empty($requestData['columns'][30]['search']['value']) ||
				!empty($requestData['columns'][34]['search']['value']) ||
				!empty($requestData['advFilters']))
			{
				if(!empty($requestData['columns'][2]['search']['value'])){
					$filters['nr_protokollit'] = $requestData['columns'][2]['search']['value'];
				}
				if(!empty($requestData['columns'][3]['search']['value'])){
					$filters['emri_lendes'] = $requestData['columns'][3]['search']['value'];
				}
				if(!empty($requestData['columns'][4]['search']['value'])){
					$filters['vlera_parashikuar'] = $requestData['columns'][4]['search']['value'];
				}
				if(!empty($requestData['columns'][5]['search']['value'])){
					$filters['lloji_kontrates'] = $requestData['columns'][5]['search']['value'];
				}				
				if(!empty($requestData['columns'][6]['search']['value'])){
					$filters['autoriteti_kontraktues'] = $requestData['columns'][6]['search']['value'];
				}
				if(!empty($requestData['columns'][7]['search']['value'])){
					$filters['oe_ankues'] = $requestData['columns'][7]['search']['value'];
				}
				if(!empty($requestData['columns'][8]['search']['value'])){
					$filters['rekomandimi_ekspertit'] = $requestData['columns'][8]['search']['value'];
				}				
				if(!empty($requestData['columns'][9]['search']['value'])){
					$filters['ankesa_kunder'] = $requestData['columns'][9]['search']['value'];
				}
				if(!empty($requestData['columns'][10]['search']['value'])){
					$filters['vendimi'] = $requestData['columns'][10]['search']['value'];
				}
				if(!empty($requestData['columns'][11]['search']['value'])){
					$filters['terheqje_verejtje'] = $requestData['columns'][11]['search']['value'];
				}
				if(!empty($requestData['columns'][12]['search']['value'])){
					$filters['neni'] = $requestData['columns'][12]['search']['value'];
				}
				if(!empty($requestData['columns'][13]['search']['value'])){
					$filters['komente_paneli'] = $requestData['columns'][13]['search']['value'];
				}
				if(!empty($requestData['columns'][14]['search']['value'])){
					$filters['nr_vendimit'] = $requestData['columns'][14]['search']['value'];
				}
				if(!empty($requestData['columns'][15]['search']['value'])){
					$filters['vendimet_meparshme'] = $requestData['columns'][15]['search']['value'];
				}
				if(!empty($requestData['columns'][16]['search']['value'])){
					$filters['vendimi_jokonsistent'] = $requestData['columns'][16]['search']['value'];
				}
				if(!empty($requestData['columns'][17]['search']['value'])){
					$filters['konfiskim_tarifes'] = $requestData['columns'][17]['search']['value'];
				}
				if(!empty($requestData['columns'][18]['search']['value'])){
					$filters['kryetar'] = $requestData['columns'][18]['search']['value'];
				}
				if(!empty($requestData['columns'][19]['search']['value'])){
					$filters['referent'] = $requestData['columns'][19]['search']['value'];
				}
				if(!empty($requestData['columns'][20]['search']['value'])){
					$filters['anetari_1'] = $requestData['columns'][20]['search']['value'];
				}
				if(!empty($requestData['columns'][21]['search']['value'])){
					$filters['anetari_2'] = $requestData['columns'][21]['search']['value'];
				}
				if(!empty($requestData['columns'][22]['search']['value'])){
					$filters['anetari_3'] = $requestData['columns'][22]['search']['value'];
				}
				if(!empty($requestData['columns'][23]['search']['value'])){
					$filters['nr_panelisteve'] = $requestData['columns'][23]['search']['value'];
				}
				if(!empty($requestData['columns'][24]['search']['value'])){
					$filters['komente_eksperti'] = $requestData['columns'][24]['search']['value'];
				}
				if(!empty($requestData['columns'][25]['search']['value'])){
					$filters['eksperti_shqyrtues'] = $requestData['columns'][25]['search']['value'];
				}
				if(!empty($requestData['columns'][26]['search']['value'])){
					$filters['eksperti_teknik'] = $requestData['columns'][26]['search']['value'];
				}
				if(!empty($requestData['columns'][27]['search']['value'])){
					$filters['seanca'] = $requestData['columns'][27]['search']['value'];
				}
				if(!empty($requestData['columns'][28]['search']['value'])){
					$filters['data_ankeses'] = $requestData['columns'][28]['search']['value'];
				}
				if(!empty($requestData['columns'][29]['search']['value'])){
					$filters['data_vendimit'] = $requestData['columns'][29]['search']['value'];
				}
				if(!empty($requestData['columns'][30]['search']['value'])){
					$filters['data_publikimit'] = $requestData['columns'][30]['search']['value'];
				}
				if(!empty($requestData['columns'][34]['search']['value'])){
					$filters['komente_gjenerale'] = $requestData['columns'][34]['search']['value'];
				}
				if(!empty($requestData['advFilters'])){
					$filters['advanced_filters'] = $requestData['advFilters'];
				}
			}			
			$order_by = array('column' => $columns[$requestData['order'][0]['column']], 'direction' => $requestData['order'][0]['dir']);
			$limit = array('start' => $requestData['start'], 'length' => $requestData['length']);
			$vendimet = $this->Vendimet_model->get_vendimet_data($fieldset, $order_by, $limit, $filters);
			if(!empty($requestData['search']['value']) || 
				!empty($requestData['columns'][2]['search']['value']) || 
				!empty($requestData['columns'][3]['search']['value']) || 
				!empty($requestData['columns'][4]['search']['value']) || 
				!empty($requestData['columns'][5]['search']['value']) ||
				!empty($requestData['columns'][6]['search']['value']) ||
				!empty($requestData['columns'][7]['search']['value']) ||
				!empty($requestData['columns'][8]['search']['value']) ||
				!empty($requestData['columns'][9]['search']['value']) ||
				!empty($requestData['columns'][10]['search']['value']) ||
				!empty($requestData['columns'][11]['search']['value']) ||
				!empty($requestData['columns'][12]['search']['value']) ||
				!empty($requestData['columns'][13]['search']['value']) ||
				!empty($requestData['columns'][14]['search']['value']) ||
				!empty($requestData['columns'][15]['search']['value']) ||
				!empty($requestData['columns'][16]['search']['value']) ||
				!empty($requestData['columns'][17]['search']['value']) ||
				!empty($requestData['columns'][18]['search']['value']) ||
				!empty($requestData['columns'][19]['search']['value']) ||
				!empty($requestData['columns'][20]['search']['value']) ||
				!empty($requestData['columns'][21]['search']['value']) ||
				!empty($requestData['columns'][22]['search']['value']) ||
				!empty($requestData['columns'][23]['search']['value']) ||
				!empty($requestData['columns'][24]['search']['value']) ||
				!empty($requestData['columns'][25]['search']['value']) ||
				!empty($requestData['columns'][26]['search']['value']) ||
				!empty($requestData['columns'][27]['search']['value']) ||
				!empty($requestData['columns'][28]['search']['value']) ||
				!empty($requestData['columns'][29]['search']['value']) ||
				!empty($requestData['columns'][30]['search']['value']) ||
				!empty($requestData['columns'][34]['search']['value']) ||
				!empty($requestData['advFilters']))
			{ 
				$totalFiltered = $this->Vendimet_model->get_vendimet_data($fieldset, $order_by, null, $filters, TRUE);
			}
			$data = array();
			foreach($vendimet as $v){
				$count_tags = count($v->tags);
				$tags = '';
				if($count_tags > 0){
					$tags = 'file-text';
				}else{
					$tags = 'file-o';
				}
				$tag_tooltip = null;
				foreach ($v->tags as $key => $tag) {
					$delimiter = ' ';
					if($key != $count_tags-1){
						$delimiter = ', ';
					}
					$tag_tooltip .= $tag->emri.$delimiter;
				}
				$tags_icon = '<a href="javascript:void(0)" class="btn btn-xs btn-default" title="'.$tag_tooltip.'"><i class="fa fa-'.$tags.'"></i></a>';
				$label = '';
				if($v->vendimet_meparshme[0]->url){
					$label .= '<a href="#'.$this->config->item('legjenda')['vendimet_meparshme']['item_id'].'"><i class="'.$this->config->item('legjenda')['vendimet_meparshme']['icon'].'" style="'.$this->config->item('legjenda')['vendimet_meparshme']['style'].'"></i></a> ';
				}
				if($this->date_diff($v->data_ankeses,$v->data_publikimit)->days > $this->config->item('afati_pergjigjjes')){
					$label .= '<a href="#'.$this->config->item('legjenda')['afati_pergjigjjes']['item_id'].'"><i class="'.$this->config->item('legjenda')['afati_pergjigjjes']['icon'].'" style="'.$this->config->item('legjenda')['afati_pergjigjjes']['style'].'"></i></a>';
				}
				
				$nestedData = array();
				$nestedData[] = '';
				$nestedData[] = $label;

				$nestedData[] = $v->nr_protokollit; // 2
				$nestedData[] = $v->emri_lendes; // 3
				$nestedData[] = ($v->vlera_parashikuar != 0) ? '€'.number_format($v->vlera_parashikuar, 2) : 'N/A'; // 4
				$nestedData[] = $v->lloji_kontrates; // 5
				$nestedData[] = $v->autoriteti_kontraktues; // 6

				$nestedData[] = $v->oe_ankues; // 7
				$nestedData[] = $v->rekomandimi_ekspertit; // 8				
				$nestedData[] = $v->ankesa_kunder; // 9

				$nestedData[] = $v->vendimi; // 10
				$nestedData[] = $v->terheqje_verejtje; // 11
				$nestedData[] = $v->neni; // 12			
				$nestedData[] = $v->komente_paneli; // 13
				$nestedData[] = '<a href="'.$v->nr_vendimit[0]->url.'" target="_blank">'.$v->nr_vendimit[0]->titulli.'</a>'; // 14
				$nestedData[] = '<a href="'.$v->vendimet_meparshme[0]->url.'" target="_blank">'.$v->vendimet_meparshme[0]->titulli.'</a>'; // 15
				$nestedData[] = '<a href="'.$v->vendimi_jokonsistent[0]->url.'" target="_blank">'.$v->vendimi_jokonsistent[0]->titulli.'</a>'; // 16
				$nestedData[] = $v->konfiskim_tarifes; // 17

				$nestedData[] = $v->kryetar; // 18
				$nestedData[] = $v->referent; // 19
				$nestedData[] = $v->anetari_1; // 20
				$nestedData[] = $v->anetari_2; // 21
				$nestedData[] = $v->anetari_3; // 22
				$nestedData[] = $v->nr_panelisteve; // 23

				$nestedData[] = $v->komente_eksperti; // 24

				$nestedData[] = $v->eksperti_shqyrtues; // 25
				$nestedData[] = $v->eksperti_teknik; // 26

				$nestedData[] = $v->seanca; // 27

				$nestedData[] = $v->data_ankeses; // 28
				$nestedData[] = $v->data_vendimit; // 29
				$nestedData[] = $v->data_publikimit; // 30
				$nestedData[] = $this->date_diff($v->data_ankeses,$v->data_vendimit)->days.' ditë'; // 31
				$nestedData[] = $this->date_diff($v->data_vendimit,$v->data_publikimit)->days.' ditë'; // 32
				$nestedData[] = $this->date_diff($v->data_ankeses,$v->data_publikimit)->days.' ditë'; // 33

				$nestedData[] = $v->komente_gjenerale; // 34

				$nestedData[] = '<div class="vendimi_tags_lbl" id="'.$v->id.'"><div class="vtlbl">'.$tags_icon.'</div></div>';

				$nestedData[] = '<a href="'.base_url().'admin/vendimet/edit_vendimi/'.$v->id.'" target="_blank" class="btn btn-xs btn-default"><i class="fa fa-pencil" title="Edit"></i></a> <a href="'.base_url().'admin/vendimet/delete_vendimi/'.$v->id.'" class="delete btn btn-xs btn-danger"><i class="fa fa-trash-o" title="Delete"></i></a>';

				$data[] = $nestedData;
			}
			$json_data = array(
				"draw"		=> intval($requestData["draw"]),
				"recordsTotal"    => intval( $totalData ),
				"recordsFiltered" => intval( $totalFiltered ),
				"data"		=> $data
				);
			echo json_encode($json_data);
			return;
		}else{
			$data['vendimet'] = array();
		}
		$data['used_tags'] = $this->Vendimet_model->get_all_unique_used_tags();
		$data['last_updated'] = $this->Vendimet_model->get_last_updated();
		$data['panelistet'] = $this->Vendimet_model->get_unique_fields_by_column('vendimet', 'paneli');
		$data['vendimet'] = $this->Vendimet_model->get_unique_fields_by_column('vendimet', 'vendimi');
		$data['rekomandimet'] = $this->Vendimet_model->get_unique_fields_by_column('vendimet', 'rekomandimi_ekspertit');
		$data['nenet'] = $this->Vendimet_model->get_unique_fields_by_column('vendimet', 'neni');
		$data['oe_ankues'] = $this->Vendimet_model->get_unique_fields_by_column('vendimet', 'oe_ankues');
		$data['ak'] = $this->Vendimet_model->get_unique_fields_by_column('vendimet', 'autoriteti_kontraktues');
		$this->view('vendimet', $data);
    }

    public function edit_vendimi($id = null){
    	if($this->input->post()){
    		$vendimi = $this->Vendimet_model->object_from_post();
			$vendimi = $this->Vendimet_model->save($vendimi);
			if($vendimi->id){
				redirect('admin/vendimet/edit_vendimi/'.$vendimi->id.'?success=1');
			}
    	}
    	
    	$data['vendimi'] = $this->Vendimet_model->get_vendimet($id);
    	$data['vendimet'] = $this->Vendimet_model->get_unique_fields_by_column('vendimet', 'vendimi');
    	$data['rekomandimet_ekspertit'] = $this->Vendimet_model->get_unique_fields_by_column('vendimet', 'rekomandimi_ekspertit');
    	$data['tags'] = $this->Vendimet_model->get_tags();
    	$this->view('edit_vendimi', $data);
    }

    public function delete_vendimi($id = null){
    	if(!$id) return;
		$vendimi = $this->Vendimet_model->get_vendimet($id);
		if($vendimi->id){
			if($this->Vendimet_model->delete_vendimet($vendimi->id)) { 
				redirect('admin/vendimet/?success=3');
			}
		}
		return;
    }

    // LISTA: LISTA E ZEZE
    public function lista_e_zeze($type = null) {
    	if($type == 'json'){
			$requestData = $_REQUEST;
			$columns = array(
				0	=> null,
				1	=> null,
				2	=> 'kerkesa_kunder_oe',
				3	=> 'nr_protokollit',
				4	=> 'emri_lendes',
				5	=> 'vlera_parashikuar',
				6  	=> 'lloji_kontrates',
				7	=> 'autoriteti_kontraktues',
				8	=> 'pretendimet_ak',
				9	=> 'vendimi',
				10	=> 'diskualifikimi_kohezgjatja',
				11	=> 'neni',
				12	=> 'komente_paneli',
				13	=> 'vendimi_liste_zeze',
				14	=> 'kryetar',
				15	=> 'referent',
				16	=> 'anetari_1',
				17	=> 'anetari_2',
				18	=> 'anetari_3',
				19	=> 'nr_panelisteve',
				20	=> 'rekomandimi_ekspertit',
				21	=> 'komente_eksperti',
				22	=> 'eksperti_shqyrtues',
				23	=> 'eksperti_teknik',
				24	=> 'seanca',
				25	=> 'data_kerkeses',
				26	=> 'data_vendimit',
				27	=> 'data_publikimit',
				31	=> 'komente_gjenerale'
				);

			$totalData = $this->Lista_zeze_model->get_total_listazeze();
			$totalFiltered = $totalData;
			$fieldset = null;
			if(!empty($requestData['search']['value'])){
				$fieldset = array(
					'nr_protokollit'			=> $requestData['search']['value'],
					'emri_lendes'				=> $requestData['search']['value'],
					'vlera_parashikuar'			=> $requestData['search']['value'],
					'lloji_kontrates'			=> $requestData['search']['value'],
					'autoriteti_kontraktues'	=> $requestData['search']['value'],
					'kerkesa_kunder_oe'			=> $requestData['search']['value'],
					'pretendimet_ak'			=> $requestData['search']['value'],
					'vendimi'					=> $requestData['search']['value'],
					'diskualifikimi_kohezgjatja'=> $requestData['search']['value'],
					'neni'						=> $requestData['search']['value'],
					'komente_paneli'			=> $requestData['search']['value'],
					'vendimi_liste_zeze'		=> $requestData['search']['value'],
					'kryetar'					=> $requestData['search']['value'],
					'referent'					=> $requestData['search']['value'],
					'anetari_1'					=> $requestData['search']['value'],
					'anetari_2'					=> $requestData['search']['value'],
					'anetari_3'					=> $requestData['search']['value'],
					'nr_panelisteve'			=> $requestData['search']['value'],
					'rekomandimi_ekspertit'		=> $requestData['search']['value'],
					'komente_eksperti'			=> $requestData['search']['value'],
					'eksperti_shqyrtues'		=> $requestData['search']['value'],
					'eksperti_teknik'			=> $requestData['search']['value'],
					'seanca'					=> $requestData['search']['value'],
					'data_kerkeses'				=> $requestData['search']['value'],
					'data_vendimit'				=> $requestData['search']['value'],
					'data_publikimit'			=> $requestData['search']['value'],
					'komente_gjenerale'			=> $requestData['search']['value']
				);
			}
			$filters = null;
			if(!empty($requestData['columns'][2]['search']['value']) || 
				!empty($requestData['columns'][3]['search']['value']) || 
				!empty($requestData['columns'][4]['search']['value']) || 
				!empty($requestData['columns'][5]['search']['value']) ||
				!empty($requestData['columns'][6]['search']['value']) ||
				!empty($requestData['columns'][7]['search']['value']) ||
				!empty($requestData['columns'][8]['search']['value']) ||
				!empty($requestData['columns'][9]['search']['value']) ||
				!empty($requestData['columns'][10]['search']['value']) ||
				!empty($requestData['columns'][11]['search']['value']) ||
				!empty($requestData['columns'][12]['search']['value']) ||
				!empty($requestData['columns'][13]['search']['value']) ||
				!empty($requestData['columns'][14]['search']['value']) ||
				!empty($requestData['columns'][15]['search']['value']) ||
				!empty($requestData['columns'][16]['search']['value']) ||
				!empty($requestData['columns'][17]['search']['value']) ||
				!empty($requestData['columns'][18]['search']['value']) ||
				!empty($requestData['columns'][19]['search']['value']) ||
				!empty($requestData['columns'][20]['search']['value']) ||
				!empty($requestData['columns'][21]['search']['value']) ||
				!empty($requestData['columns'][22]['search']['value']) ||
				!empty($requestData['columns'][23]['search']['value']) ||
				!empty($requestData['columns'][24]['search']['value']) ||
				!empty($requestData['columns'][25]['search']['value']) ||
				!empty($requestData['columns'][26]['search']['value']) ||
				!empty($requestData['columns'][27]['search']['value']) ||
				!empty($requestData['columns'][31]['search']['value']))
			{
				if(!empty($requestData['columns'][2]['search']['value'])){
					$filters['kerkesa_kunder_oe'] = $requestData['columns'][2]['search']['value'];
				}
				if(!empty($requestData['columns'][3]['search']['value'])){
					$filters['nr_protokollit'] = $requestData['columns'][3]['search']['value'];
				}
				if(!empty($requestData['columns'][4]['search']['value'])){
					$filters['emri_lendes'] = $requestData['columns'][4]['search']['value'];
				}
				if(!empty($requestData['columns'][5]['search']['value'])){
					$filters['vlera_parashikuar'] = $requestData['columns'][5]['search']['value'];
				}
				if(!empty($requestData['columns'][6]['search']['value'])){
					$filters['lloji_kontrates'] = $requestData['columns'][6]['search']['value'];
				}
				if(!empty($requestData['columns'][7]['search']['value'])){
					$filters['autoriteti_kontraktues'] = $requestData['columns'][7]['search']['value'];
				}
				if(!empty($requestData['columns'][8]['search']['value'])){
					$filters['pretendimet_ak'] = $requestData['columns'][8]['search']['value'];
				}
				if(!empty($requestData['columns'][9]['search']['value'])){
					$filters['vendimi'] = $requestData['columns'][9]['search']['value'];
				}
				if(!empty($requestData['columns'][10]['search']['value'])){
					$filters['diskualifikimi_kohezgjatja'] = $requestData['columns'][10]['search']['value'];
				}
				if(!empty($requestData['columns'][11]['search']['value'])){
					$filters['neni'] = $requestData['columns'][11]['search']['value'];
				}
				if(!empty($requestData['columns'][12]['search']['value'])){
					$filters['komente_paneli'] = $requestData['columns'][12]['search']['value'];
				}
				if(!empty($requestData['columns'][13]['search']['value'])){
					$filters['vendimi_liste_zeze'] = $requestData['columns'][13]['search']['value'];
				}
				if(!empty($requestData['columns'][14]['search']['value'])){
					$filters['kryetar'] = $requestData['columns'][14]['search']['value'];
				}
				if(!empty($requestData['columns'][15]['search']['value'])){
					$filters['referent'] = $requestData['columns'][15]['search']['value'];
				}
				if(!empty($requestData['columns'][16]['search']['value'])){
					$filters['anetari_1'] = $requestData['columns'][16]['search']['value'];
				}
				if(!empty($requestData['columns'][17]['search']['value'])){
					$filters['anetari_2'] = $requestData['columns'][17]['search']['value'];
				}
				if(!empty($requestData['columns'][18]['search']['value'])){
					$filters['anetari_3'] = $requestData['columns'][18]['search']['value'];
				}
				if(!empty($requestData['columns'][19]['search']['value'])){
					$filters['nr_panelisteve'] = $requestData['columns'][19]['search']['value'];
				}
				if(!empty($requestData['columns'][20]['search']['value'])){
					$filters['rekomandimi_ekspertit'] = $requestData['columns'][20]['search']['value'];
				}
				if(!empty($requestData['columns'][21]['search']['value'])){
					$filters['komente_eksperti'] = $requestData['columns'][21]['search']['value'];
				}
				if(!empty($requestData['columns'][22]['search']['value'])){
					$filters['eksperti_shqyrtues'] = $requestData['columns'][22]['search']['value'];
				}
				if(!empty($requestData['columns'][23]['search']['value'])){
					$filters['eksperti_teknik'] = $requestData['columns'][23]['search']['value'];
				}
				if(!empty($requestData['columns'][24]['search']['value'])){
					$filters['seanca'] = $requestData['columns'][24]['search']['value'];
				}
				if(!empty($requestData['columns'][25]['search']['value'])){
					$filters['data_kerkeses'] = $requestData['columns'][25]['search']['value'];
				}
				if(!empty($requestData['columns'][26]['search']['value'])){
					$filters['data_vendimit'] = $requestData['columns'][26]['search']['value'];
				}
				if(!empty($requestData['columns'][27]['search']['value'])){
					$filters['data_publikimit'] = $requestData['columns'][27]['search']['value'];
				}
				if(!empty($requestData['columns'][31]['search']['value'])){
					$filters['komente_gjenerale'] = $requestData['columns'][31]['search']['value'];
				}
			}
			$order_by = array('column' => $columns[$requestData['order'][0]['column']], 'direction' => $requestData['order'][0]['dir']);
			$limit = array('start' => $requestData['start'], 'length' => $requestData['length']);
			$listazeze = $this->Lista_zeze_model->get_listazeze_data($fieldset, $order_by, $limit, $filters);
			if(!empty($requestData['search']['value']) || 
				!empty($requestData['columns'][2]['search']['value']) ||
				!empty($requestData['columns'][3]['search']['value']) || 
				!empty($requestData['columns'][4]['search']['value']) || 
				!empty($requestData['columns'][5]['search']['value']) ||
				!empty($requestData['columns'][6]['search']['value']) ||
				!empty($requestData['columns'][7]['search']['value']) ||
				!empty($requestData['columns'][8]['search']['value']) ||
				!empty($requestData['columns'][9]['search']['value']) ||
				!empty($requestData['columns'][10]['search']['value']) ||
				!empty($requestData['columns'][11]['search']['value']) ||
				!empty($requestData['columns'][12]['search']['value']) ||
				!empty($requestData['columns'][13]['search']['value']) ||
				!empty($requestData['columns'][14]['search']['value']) ||
				!empty($requestData['columns'][15]['search']['value']) ||
				!empty($requestData['columns'][16]['search']['value']) ||
				!empty($requestData['columns'][17]['search']['value']) ||
				!empty($requestData['columns'][18]['search']['value']) ||
				!empty($requestData['columns'][19]['search']['value']) ||
				!empty($requestData['columns'][20]['search']['value']) ||
				!empty($requestData['columns'][21]['search']['value']) ||
				!empty($requestData['columns'][22]['search']['value']) ||
				!empty($requestData['columns'][23]['search']['value']) ||
				!empty($requestData['columns'][24]['search']['value']) ||
				!empty($requestData['columns'][25]['search']['value']) ||
				!empty($requestData['columns'][26]['search']['value']) ||
				!empty($requestData['columns'][27]['search']['value']) ||
				!empty($requestData['columns'][31]['search']['value']))
			{
				$totalFiltered = $this->Lista_zeze_model->get_listazeze_data($fieldset, $order_by, null, $filters, TRUE);
			}
			$data = array();
			foreach($listazeze as $lz){
				$label = '';
				if($this->date_diff($lz->data_kerkeses,$lz->data_publikimit)->days > $this->config->item('afati_pergjigjjes')){
					$label .= '<a href="#'.$this->config->item('legjenda')['afati_pergjigjjes']['item_id'].'"><i class="'.$this->config->item('legjenda')['afati_pergjigjjes']['icon'].'" style="'.$this->config->item('legjenda')['afati_pergjigjjes']['style'].'"></i></a>';
				}
				
				$nestedData = array();
				$nestedData[] = '';
				$nestedData[] = $label;

				$nestedData[] = $lz->kerkesa_kunder_oe; // 2
				$nestedData[] = $lz->nr_protokollit; // 3
				$nestedData[] = $lz->emri_lendes; // 4
				$nestedData[] = ($lz->vlera_parashikuar != 0) ? '€'.number_format($lz->vlera_parashikuar, 2) : 'N/A'; // 5
				$nestedData[] = $lz->lloji_kontrates; // 6			
				$nestedData[] = $lz->autoriteti_kontraktues; // 7

				$nestedData[] = $lz->pretendimet_ak; // 8

				$nestedData[] = $lz->vendimi; // 9
				$nestedData[] = $lz->diskualifikimi_kohezgjatja; // 10
				$nestedData[] = $lz->neni; // 11
				$nestedData[] = $lz->komente_paneli; // 12
				$nestedData[] = '<a href="'.$lz->vendimi_liste_zeze[0]->url.'" target="_blank">'.$lz->vendimi_liste_zeze[0]->titulli.'</a>'; // 13

				$nestedData[] = $lz->kryetar; // 14
				$nestedData[] = $lz->referent; // 15
				$nestedData[] = $lz->anetari_1; // 16
				$nestedData[] = $lz->anetari_2; // 17
				$nestedData[] = $lz->anetari_3; // 18
				$nestedData[] = $lz->nr_panelisteve; // 19

				$nestedData[] = $lz->rekomandimi_ekspertit; // 20

				$nestedData[] = $lz->eksperti_shqyrtues; // 21
				$nestedData[] = $lz->eksperti_teknik; // 22

				$nestedData[] = $lz->seanca; // 23

				$nestedData[] = $lz->data_kerkeses; // 24
				$nestedData[] = $lz->data_vendimit; // 25
				$nestedData[] = $lz->data_publikimit; // 26
				$nestedData[] = $this->date_diff($lz->data_kerkeses,$lz->data_vendimit)->days.' ditë'; // 27
				$nestedData[] = $this->date_diff($lz->data_vendimit,$lz->data_publikimit)->days.' ditë'; // 28
				$nestedData[] = $this->date_diff($lz->data_kerkeses,$lz->data_publikimit)->days.' ditë'; // 29

				$nestedData[] = $lz->komente_gjenerale; // 30

				$nestedData[] = '<a href="'.base_url().'admin/vendimet/edit_lista_e_zeze/'.$lz->id.'" target="_blank" class="btn btn-xs btn-default"><i class="fa fa-pencil" title="Edit"></i></a> <a href="'.base_url().'admin/vendimet/delete_lista_e_zeze/'.$lz->id.'" class="delete btn btn-xs btn-danger"><i class="fa fa-trash-o" title="Delete"></i></a>'; // 31

				$data[] = $nestedData;
			}
			$json_data = array(
				"draw"		=> intval($requestData["draw"]),
				"recordsTotal"    => intval( $totalData ),
				"recordsFiltered" => intval( $totalFiltered ),
				"data"		=> $data
				);
			echo json_encode($json_data);
			return;
		}else{
			$data['listazeze'] = array();
		}
		$data['last_updated'] = $this->Lista_zeze_model->get_last_updated();
		$this->view('lista_e_zeze', $data);
    }
    public function edit_lista_e_zeze($id = null){
    	if($this->input->post()){
    		$listazeze = $this->Lista_zeze_model->object_from_post();
			$listazeze = $this->Lista_zeze_model->save($listazeze);
			if($listazeze->id){
				redirect('admin/vendimet/edit_lista_e_zeze/'.$listazeze->id.'?success=1');
			}
    	}
    	
    	$data['listazeze'] = $this->Lista_zeze_model->get_listazeze($id);
    	$this->view('edit_lista_e_zeze', $data);
    }
    public function delete_lista_e_zeze($id = null){
    	if(!$id) return;
		$listazeze = $this->Lista_zeze_model->get_listazeze($id);
		if($listazeze->id){
			if($this->Lista_zeze_model->delete_listazeze($listazeze->id)) { 
				redirect('admin/vendimet/lista_e_zeze/?success=3');
			}
		}
		return;
    }

    // LISTA: GJOBE
    public function gjobe($type = null) {
    	if($type == 'json'){
			$requestData = $_REQUEST;
			$columns = array(
				0	=> null,
				1	=> null,
				2	=> 'nr_protokollit',
				3	=> 'emri_lendes',
				4	=> 'vlera_parashikuar',
				5  	=> 'lloji_kontrates',
				6	=> 'operatori_ekonomik',
				7	=> 'gjoba_kunder_ak',
				8 	=> 'shuma',
				9	=> 'terheqje_verejtje',
				10	=> 'heqje_licence',
				11	=> 'vendimi',
				12	=> 'neni',
				13	=> 'komente_paneli',
				14	=> 'kryetar',
				15	=> 'referent',
				16	=> 'anetari_1',
				17	=> 'anetari_2',
				18 	=> 'anetari_3',
				19	=> 'nr_panelisteve',
				20	=> 'seanca',
				21	=> 'data_kerkeses',
				22	=> 'data_vendimit',
				23	=> 'data_publikimit',
				27	=> 'komente_gjenerale'
				);

			$totalData = $this->Gjobe_model->get_total_gjobe_data();
			$totalFiltered = $totalData;
			$fieldset = null;
			if(!empty($requestData['search']['value'])){
				$fieldset = array(
					'nr_protokollit'	=> $requestData['search']['value'],
					'emri_lendes'		=> $requestData['search']['value'],
					'vlera_parashikuar'	=> $requestData['search']['value'],
					'lloji_kontrates'	=> $requestData['search']['value'],
					'operatori_ekonomik'=> $requestData['search']['value'],
					'gjoba_kunder_ak'	=> $requestData['search']['value'],
					'shuma'				=> $requestData['search']['value'],
					'terheqje_verejtje'	=> $requestData['search']['value'],
					'heqje_licence'		=> $requestData['search']['value'],
					'vendimi'			=> $requestData['search']['value'],
					'neni'				=> $requestData['search']['value'],
					'komente_paneli'	=> $requestData['search']['value'],
					'kryetar'			=> $requestData['search']['value'],
					'referent'			=> $requestData['search']['value'],
					'anetari_1'			=> $requestData['search']['value'],
					'anetari_2'			=> $requestData['search']['value'],
					'anetari_3'			=> $requestData['search']['value'],
					'nr_panelisteve'	=> $requestData['search']['value'],
					'seanca'			=> $requestData['search']['value'],
					'data_kerkeses'		=> $requestData['search']['value'],
					'data_vendimit'		=> $requestData['search']['value'],
					'data_publikimit'	=> $requestData['search']['value'],
					'komente_gjenerale'	=> $requestData['search']['value']
				);
			}
			$filters = null;
			if(!empty($requestData['columns'][2]['search']['value']) || 
				!empty($requestData['columns'][3]['search']['value']) || 
				!empty($requestData['columns'][4]['search']['value']) || 
				!empty($requestData['columns'][5]['search']['value']) ||
				!empty($requestData['columns'][6]['search']['value']) ||
				!empty($requestData['columns'][7]['search']['value']) ||
				!empty($requestData['columns'][8]['search']['value']) ||
				!empty($requestData['columns'][9]['search']['value']) ||
				!empty($requestData['columns'][10]['search']['value']) ||
				!empty($requestData['columns'][11]['search']['value']) ||
				!empty($requestData['columns'][12]['search']['value']) ||
				!empty($requestData['columns'][13]['search']['value']) ||
				!empty($requestData['columns'][14]['search']['value']) ||
				!empty($requestData['columns'][15]['search']['value']) ||
				!empty($requestData['columns'][16]['search']['value']) ||
				!empty($requestData['columns'][17]['search']['value']) ||
				!empty($requestData['columns'][18]['search']['value']) ||
				!empty($requestData['columns'][19]['search']['value']) ||
				!empty($requestData['columns'][20]['search']['value']) ||
				!empty($requestData['columns'][21]['search']['value']) ||
				!empty($requestData['columns'][22]['search']['value']) ||
				!empty($requestData['columns'][23]['search']['value']) ||
				!empty($requestData['columns'][27]['search']['value']))
			{
				if(!empty($requestData['columns'][2]['search']['value'])){
					$filters['nr_protokollit'] = $requestData['columns'][2]['search']['value'];
				}
				if(!empty($requestData['columns'][3]['search']['value'])){
					$filters['emri_lendes'] = $requestData['columns'][3]['search']['value'];
				}
				if(!empty($requestData['columns'][4]['search']['value'])){
					$filters['vlera_parashikuar'] = $requestData['columns'][4]['search']['value'];
				}
				if(!empty($requestData['columns'][5]['search']['value'])){
					$filters['lloji_kontrates'] = $requestData['columns'][5]['search']['value'];
				}
				if(!empty($requestData['columns'][6]['search']['value'])){
					$filters['operatori_ekonomik'] = $requestData['columns'][6]['search']['value'];
				}
				if(!empty($requestData['columns'][7]['search']['value'])){
					$filters['gjoba_kunder_ak'] = $requestData['columns'][7]['search']['value'];
				}
				if(!empty($requestData['columns'][8]['search']['value'])){
					$filters['shuma'] = $requestData['columns'][8]['search']['value'];
				}
				if(!empty($requestData['columns'][9]['search']['value'])){
					$filters['terheqje_verejtje'] = $requestData['columns'][9]['search']['value'];
				}
				if(!empty($requestData['columns'][10]['search']['value'])){
					$filters['heqje_licence'] = $requestData['columns'][10]['search']['value'];
				}
				if(!empty($requestData['columns'][11]['search']['value'])){
					$filters['vendimi'] = $requestData['columns'][11]['search']['value'];
				}
				if(!empty($requestData['columns'][12]['search']['value'])){
					$filters['neni'] = $requestData['columns'][12]['search']['value'];
				}
				if(!empty($requestData['columns'][13]['search']['value'])){
					$filters['komente_paneli'] = $requestData['columns'][13]['search']['value'];
				}
				if(!empty($requestData['columns'][14]['search']['value'])){
					$filters['kryetar'] = $requestData['columns'][14]['search']['value'];
				}
				if(!empty($requestData['columns'][15]['search']['value'])){
					$filters['referent'] = $requestData['columns'][15]['search']['value'];
				}
				if(!empty($requestData['columns'][16]['search']['value'])){
					$filters['anetari_1'] = $requestData['columns'][16]['search']['value'];
				}
				if(!empty($requestData['columns'][17]['search']['value'])){
					$filters['anetari_2'] = $requestData['columns'][17]['search']['value'];
				}
				if(!empty($requestData['columns'][18]['search']['value'])){
					$filters['anetari_3'] = $requestData['columns'][18]['search']['value'];
				}
				if(!empty($requestData['columns'][19]['search']['value'])){
					$filters['nr_panelisteve'] = $requestData['columns'][19]['search']['value'];
				}
				if(!empty($requestData['columns'][20]['search']['value'])){
					$filters['seanca'] = $requestData['columns'][20]['search']['value'];
				}
				if(!empty($requestData['columns'][21]['search']['value'])){
					$filters['data_kerkeses'] = $requestData['columns'][21]['search']['value'];
				}
				if(!empty($requestData['columns'][22]['search']['value'])){
					$filters['data_vendimit'] = $requestData['columns'][22]['search']['value'];
				}
				if(!empty($requestData['columns'][23]['search']['value'])){
					$filters['data_publikimit'] = $requestData['columns'][23]['search']['value'];
				}
				if(!empty($requestData['columns'][27]['search']['value'])){
					$filters['komente_gjenerale'] = $requestData['columns'][27]['search']['value'];
				}
			}
			$order_by = array('column' => $columns[$requestData['order'][0]['column']], 'direction' => $requestData['order'][0]['dir']);
			$limit = array('start' => $requestData['start'], 'length' => $requestData['length']);
			$gjobat = $this->Gjobe_model->get_gjobe_data($fieldset, $order_by, $limit, $filters);
			if(!empty($requestData['search']['value']) || 
				!empty($requestData['columns'][2]['search']['value']) || 
				!empty($requestData['columns'][3]['search']['value']) || 
				!empty($requestData['columns'][4]['search']['value']) || 
				!empty($requestData['columns'][5]['search']['value']) ||
				!empty($requestData['columns'][6]['search']['value']) ||
				!empty($requestData['columns'][7]['search']['value']) ||
				!empty($requestData['columns'][8]['search']['value']) ||
				!empty($requestData['columns'][9]['search']['value']) ||
				!empty($requestData['columns'][10]['search']['value']) ||
				!empty($requestData['columns'][11]['search']['value']) ||
				!empty($requestData['columns'][12]['search']['value']) ||
				!empty($requestData['columns'][13]['search']['value']) ||
				!empty($requestData['columns'][14]['search']['value']) ||
				!empty($requestData['columns'][15]['search']['value']) ||
				!empty($requestData['columns'][16]['search']['value']) ||
				!empty($requestData['columns'][17]['search']['value']) ||
				!empty($requestData['columns'][18]['search']['value']) ||
				!empty($requestData['columns'][19]['search']['value']) ||
				!empty($requestData['columns'][20]['search']['value']) ||
				!empty($requestData['columns'][21]['search']['value']) ||
				!empty($requestData['columns'][22]['search']['value']) ||
				!empty($requestData['columns'][23]['search']['value']) ||
				!empty($requestData['columns'][27]['search']['value']))
			{ 
				$totalFiltered = $this->Gjobe_model->get_gjobe_data($fieldset, $order_by, null, $filters, TRUE);
			}

			$data =array();
			foreach($gjobat as $gj){
				$label = '';
				if($this->date_diff($gj->data_kerkeses,$gj->data_publikimit)->days > $this->config->item('afati_pergjigjjes')){
					$label = '<a href="#'.$this->config->item('legjenda')['afati_pergjigjjes']['item_id'].'"><i class="'.$this->config->item('legjenda')['afati_pergjigjjes']['icon'].'" style="'.$this->config->item('legjenda')['afati_pergjigjjes']['style'].'"></i></a>';
				}
				
				$nestedData = array();
				$nestedData[] = '';
				$nestedData[] = $label;

				$nestedData[] = $gj->nr_protokollit; // 2
				$nestedData[] = $gj->emri_lendes; // 3
				$nestedData[] = ($gj->vlera_parashikuar != 0) ? '€'.number_format($gj->vlera_parashikuar, 2) : 'N/A'; // 4
				$nestedData[] = $gj->lloji_kontrates; // 5 				
				$nestedData[] = $gj->operatori_ekonomik; // 6

				$nestedData[] = $gj->gjoba_kunder_ak; // 7
				$nestedData[] = ($gj->shuma != 0) ? '€'.number_format($gj->shuma, 2) : 'N/A'; // 8
				$nestedData[] = $gj->terheqje_verejtje; // 9

				$nestedData[] = $gj->heqje_licence; // 10
				$nestedData[] = '<a href="'.$gj->vendimi[0]->url.'" target="_blank">'.$gj->vendimi[0]->titulli.'</a>'; // 11
				$nestedData[] = $gj->neni; // 12
				$nestedData[] = $gj->komente_paneli; // 13

				$nestedData[] = $gj->kryetar; // 14
				$nestedData[] = $gj->referent; // 15
				$nestedData[] = $gj->anetari_1; // 16
				$nestedData[] = $gj->anetari_2; // 17
				$nestedData[] = $gj->anetari_3; // 18
				$nestedData[] = $gj->nr_panelisteve; // 19

				$nestedData[] = $gj->seanca; // 20

				$nestedData[] = $gj->data_kerkeses; // 21
				$nestedData[] = $gj->data_vendimit; // 22
				$nestedData[] = $gj->data_publikimit; // 23
				$nestedData[] = $this->date_diff($gj->data_kerkeses,$gj->data_vendimit)->days.' ditë'; // 24
				$nestedData[] = $this->date_diff($gj->data_vendimit,$gj->data_publikimit)->days.' ditë'; // 25
				$nestedData[] = $this->date_diff($gj->data_kerkeses,$gj->data_publikimit)->days.' ditë'; // 26

				$nestedData[] = $gj->komente_gjenerale; // 27

				$nestedData[] = '<a href="'.base_url().'admin/vendimet/edit_gjobe/'.$gj->id.'" target="_blank" class="btn btn-xs btn-default"><i class="fa fa-pencil" title="Edit"></i></a> <a href="'.base_url().'admin/vendimet/delete_gjobe/'.$gj->id.'" class="delete btn btn-xs btn-danger"><i class="fa fa-trash-o" title="Delete"></i></a>'; //28

				$data[] = $nestedData;
			}
			$json_data = array(
				"draw"		=> intval($requestData["draw"]),
				"recordsTotal"    => intval( $totalData ),
				"recordsFiltered" => intval( $totalFiltered ),
				"data"		=> $data
				);
			echo json_encode($json_data);
			return;
		}else{
			$data['gjobe'] = array();
		}
		$data['last_updated'] = $this->Gjobe_model->get_last_updated();
    	$this->view('gjobe', $data);
    }
    public function edit_gjobe($id = null){
    	if($this->input->post()){
    		$gjobe = $this->Gjobe_model->object_from_post();
			$gjobe = $this->Gjobe_model->save($gjobe);
			if($gjobe->id){
				redirect('admin/vendimet/edit_gjobe/'.$gjobe->id.'?success=1');
			}
    	}
    	
    	$data['gjobe'] = $this->Gjobe_model->get_gjobe($id);
    	$this->view('edit_gjobe', $data);
    }
    public function delete_gjobe($id = null){
    	if(!$id) return;
		$gjobe = $this->Gjobe_model->get_gjobe($id);
		if($gjobe->id){
			if($this->Gjobe_model->delete_gjobe($gjobe->id)) { 
				redirect('admin/vendimet/gjobe/?success=3');
			}
		}
		return;
    }

    // LISTA: HEQJE E PEZULLIMIEVE
    public function heqje_pezullimi($type = null){
    	if($type == 'json'){
			$requestData = $_REQUEST;
			$columns = array(
				0	=> null,
				1	=> null,
				2	=> 'nr_protokollit',
				3	=> 'emri_lendes',
				4	=> 'vlera_parashikuar',
				5   => 'lloji_kontrates',
				6  	=> 'autoriteti_kontraktuar',
				7	=> 'oe_rekomanduar',
				8 	=> 'heqje_pezullimi',
				9	=> 'vendimi',
				10	=> 'neni',
				11	=> 'komente',
				12	=> 'kryetar',
				13	=> 'referent',
				14	=> 'anetari_1',
				15	=> 'anetari_2',
				16	=> 'anetari_3',
				17	=> 'nr_panelisteve',
				18 	=> 'seanca',
				19	=> 'data_kerkeses',
				20	=> 'data_vendimit',
				21	=> 'data_publikimit',
				25	=> 'komente_gjenerale'
			);

			$totalData = $this->Heqje_pezullimi_model->get_total_heqje_pezullimi();
			$totalFiltered = $totalData;
			$fieldset = null;
			if(!empty($requestData['search']['value'])){
				$fieldset = array(
					'nr_protokollit'		=> $requestData['search']['value'],
					'emri_lendes'			=> $requestData['search']['value'],
					'vlera_parashikuar'		=> $requestData['search']['value'],
					'lloji_kontrates'		=> $requestData['search']['value'],
					'autoriteti_kontraktuar'=> $requestData['search']['value'],
					'oe_rekomanduar'		=> $requestData['search']['value'],
					'heqje_pezullimi'		=> $requestData['search']['value'],
					'vendimi'				=> $requestData['search']['value'],
					'neni'					=> $requestData['search']['value'],
					'komente'				=> $requestData['search']['value'],
					'kryetar'				=> $requestData['search']['value'],
					'referent'				=> $requestData['search']['value'],
					'anetari_1'				=> $requestData['search']['value'],
					'anetari_2'				=> $requestData['search']['value'],
					'anetari_3'				=> $requestData['search']['value'],
					'nr_panelisteve'		=> $requestData['search']['value'],
					'seanca'				=> $requestData['search']['value'],
					'data_kerkeses'			=> $requestData['search']['value'],
					'data_vendimit'			=> $requestData['search']['value'],
					'data_publikimit'		=> $requestData['search']['value'],
					'komente_gjenerale'		=> $requestData['search']['value']
				);
			}
			$filters = null;
			if(!empty($requestData['columns'][2]['search']['value']) || 
				!empty($requestData['columns'][3]['search']['value']) || 
				!empty($requestData['columns'][4]['search']['value']) || 
				!empty($requestData['columns'][5]['search']['value']) ||
				!empty($requestData['columns'][6]['search']['value']) ||
				!empty($requestData['columns'][7]['search']['value']) ||
				!empty($requestData['columns'][8]['search']['value']) ||
				!empty($requestData['columns'][9]['search']['value']) ||
				!empty($requestData['columns'][10]['search']['value']) ||
				!empty($requestData['columns'][11]['search']['value']) ||
				!empty($requestData['columns'][12]['search']['value']) ||
				!empty($requestData['columns'][13]['search']['value']) ||
				!empty($requestData['columns'][14]['search']['value']) ||
				!empty($requestData['columns'][15]['search']['value']) ||
				!empty($requestData['columns'][16]['search']['value']) ||
				!empty($requestData['columns'][17]['search']['value']) ||
				!empty($requestData['columns'][18]['search']['value']) ||
				!empty($requestData['columns'][19]['search']['value']) ||
				!empty($requestData['columns'][20]['search']['value']) ||
				!empty($requestData['columns'][21]['search']['value']) ||
				!empty($requestData['columns'][25]['search']['value']))
			{
				if(!empty($requestData['columns'][2]['search']['value'])){
					$filters['nr_protokollit'] = $requestData['columns'][2]['search']['value'];
				}
				if(!empty($requestData['columns'][3]['search']['value'])){
					$filters['emri_lendes'] = $requestData['columns'][3]['search']['value'];
				}
				if(!empty($requestData['columns'][4]['search']['value'])){
					$filters['vlera_parashikuar'] = $requestData['columns'][4]['search']['value'];
				}
				if(!empty($requestData['columns'][5]['search']['value'])){
					$filters['lloji_kontrates'] = $requestData['columns'][5]['search']['value'];
				}
				if(!empty($requestData['columns'][6]['search']['value'])){
					$filters['autoriteti_kontraktuar'] = $requestData['columns'][6]['search']['value'];
				}
				if(!empty($requestData['columns'][7]['search']['value'])){
					$filters['oe_rekomanduar'] = $requestData['columns'][7]['search']['value'];
				}
				if(!empty($requestData['columns'][8]['search']['value'])){
					$filters['heqje_pezullimi'] = $requestData['columns'][8]['search']['value'];
				}
				if(!empty($requestData['columns'][9]['search']['value'])){
					$filters['vendimi'] = $requestData['columns'][9]['search']['value'];
				}
				if(!empty($requestData['columns'][10]['search']['value'])){
					$filters['neni'] = $requestData['columns'][10]['search']['value'];
				}
				if(!empty($requestData['columns'][11]['search']['value'])){
					$filters['komente'] = $requestData['columns'][11]['search']['value'];
				}
				if(!empty($requestData['columns'][12]['search']['value'])){
					$filters['kryetar'] = $requestData['columns'][12]['search']['value'];
				}
				if(!empty($requestData['columns'][13]['search']['value'])){
					$filters['referent'] = $requestData['columns'][13]['search']['value'];
				}
				if(!empty($requestData['columns'][14]['search']['value'])){
					$filters['anetari_1'] = $requestData['columns'][14]['search']['value'];
				}
				if(!empty($requestData['columns'][15]['search']['value'])){
					$filters['anetari_2'] = $requestData['columns'][15]['search']['value'];
				}
				if(!empty($requestData['columns'][16]['search']['value'])){
					$filters['anetari_3'] = $requestData['columns'][16]['search']['value'];
				}
				if(!empty($requestData['columns'][17]['search']['value'])){
					$filters['nr_panelisteve'] = $requestData['columns'][17]['search']['value'];
				}
				if(!empty($requestData['columns'][18]['search']['value'])){
					$filters['seanca'] = $requestData['columns'][18]['search']['value'];
				}
				if(!empty($requestData['columns'][19]['search']['value'])){
					$filters['data_kerkeses'] = $requestData['columns'][19]['search']['value'];
				}
				if(!empty($requestData['columns'][20]['search']['value'])){
					$filters['data_vendimit'] = $requestData['columns'][20]['search']['value'];
				}
				if(!empty($requestData['columns'][22]['search']['value'])){
					$filters['data_publikimit'] = $requestData['columns'][21]['search']['value'];
				}
				if(!empty($requestData['columns'][25]['search']['value'])){
					$filters['komente_gjenerale'] = $requestData['columns'][25]['search']['value'];
				}				
			}
			$order_by = array('column' => $columns[$requestData['order'][0]['column']], 'direction' => $requestData['order'][0]['dir']);
			$limit = array('start' => $requestData['start'], 'length' => $requestData['length']);
			$heqje_pezullimi = $this->Heqje_pezullimi_model->get_heqje_pezullimi_data($fieldset, $order_by, $limit, $filters);
			if(!empty($requestData['search']['value']) || 
				!empty($requestData['columns'][2]['search']['value']) || 
				!empty($requestData['columns'][3]['search']['value']) || 
				!empty($requestData['columns'][4]['search']['value']) || 
				!empty($requestData['columns'][5]['search']['value']) ||
				!empty($requestData['columns'][6]['search']['value']) ||
				!empty($requestData['columns'][7]['search']['value']) ||
				!empty($requestData['columns'][8]['search']['value']) ||
				!empty($requestData['columns'][9]['search']['value']) ||
				!empty($requestData['columns'][10]['search']['value']) ||
				!empty($requestData['columns'][11]['search']['value']) ||
				!empty($requestData['columns'][12]['search']['value']) ||
				!empty($requestData['columns'][13]['search']['value']) ||
				!empty($requestData['columns'][14]['search']['value']) ||
				!empty($requestData['columns'][15]['search']['value']) ||
				!empty($requestData['columns'][16]['search']['value']) ||
				!empty($requestData['columns'][17]['search']['value']) ||
				!empty($requestData['columns'][18]['search']['value']) ||
				!empty($requestData['columns'][19]['search']['value']) ||
				!empty($requestData['columns'][20]['search']['value']) ||
				!empty($requestData['columns'][21]['search']['value']) ||
				!empty($requestData['columns'][25]['search']['value']))
			{ 
				$totalFiltered = $this->Heqje_pezullimi_model->get_heqje_pezullimi_data($fieldset, $order_by, null, $filters, TRUE);
			}
			$data =array();
			foreach($heqje_pezullimi as $pezullimi){
				$label = '';
				if($this->date_diff($pezullimi->data_kerkeses,$pezullimi->data_publikimit)->days > $this->config->item('afati_pergjigjjes')){
					$label = '<a href="#'.$this->config->item('legjenda')['afati_pergjigjjes']['item_id'].'"><i class="'.$this->config->item('legjenda')['afati_pergjigjjes']['icon'].'" style="'.$this->config->item('legjenda')['afati_pergjigjjes']['style'].'"></i></a>';
				}
								
				$nestedData = array();
				$nestedData[] = '';
				$nestedData[] = $label;

				$nestedData[] = $pezullimi->nr_protokollit; // 2
				$nestedData[] = $pezullimi->emri_lendes; // 3
				$nestedData[] = ($pezullimi->vlera_parashikuar != 0) ? '€'.number_format($pezullimi->vlera_parashikuar, 2) : 'N/A'; // 4
				$nestedData[] = $pezullimi->lloji_kontrates; // 5
				$nestedData[] = $pezullimi->autoriteti_kontraktuar; //6
				$nestedData[] = $pezullimi->oe_rekomanduar; // 7

				$nestedData[] = $pezullimi->heqje_pezullimi; // 8
				$nestedData[] = '<a href="'.$pezullimi->vendimi[0]->url.'" target="_blank">'.$pezullimi->vendimi[0]->titulli.'</a>'; // 9
				$nestedData[] = $pezullimi->neni; // 10
				$nestedData[] = $pezullimi->komente; // 11

				$nestedData[] = $pezullimi->kryetar; // 12
				$nestedData[] = $pezullimi->referent; // 13
				$nestedData[] = $pezullimi->anetari_1; // 14
				$nestedData[] = $pezullimi->anetari_2; // 15
				$nestedData[] = $pezullimi->anetari_3; // 16
				$nestedData[] = $pezullimi->nr_panelisteve; // 17

				$nestedData[] = $pezullimi->seanca; // 18

				$nestedData[] = $pezullimi->data_kerkeses; // 19
				$nestedData[] = $pezullimi->data_vendimit; // 20
				$nestedData[] = $pezullimi->data_publikimit; // 21
				$nestedData[] = $this->date_diff($pezullimi->data_kerkeses,$pezullimi->data_vendimit)->days.' ditë'; // 22
				$nestedData[] = $this->date_diff($pezullimi->data_vendimit,$pezullimi->data_publikimit)->days.' ditë'; // 23
				$nestedData[] = $this->date_diff($pezullimi->data_kerkeses,$pezullimi->data_publikimit)->days.' ditë'; // 24

				$nestedData[] = $pezullimi->komente_gjenerale; // 25

				$nestedData[] = '<a href="'.base_url().'admin/vendimet/edit_pezullimi/'.$pezullimi->id.'" target="_blank" class="btn btn-xs btn-default"><i class="fa fa-pencil" title="Edit"></i></a> <a href="'.base_url().'admin/vendimet/delete_pezullimi/'.$pezullimi->id.'" class="delete btn btn-xs btn-danger"><i class="fa fa-trash-o" title="Delete"></i></a>'; //26

				$data[] = $nestedData;
			}
			$json_data = array(
				"draw"		=> intval($requestData["draw"]),
				"recordsTotal"    => intval( $totalData ),
				"recordsFiltered" => intval( $totalFiltered ),
				"data"		=> $data
				);
			echo json_encode($json_data);
			return;
		}else{
			$data['pezullimet'] = array();
		}
		$data['last_updated'] = $this->Heqje_pezullimi_model->get_last_updated();
		$this->view('pezullimet', $data);
    }

    public function edit_pezullimi($id = null){
    	if($this->input->post()){
    		$pezullimi = $this->Heqje_pezullimi_model->object_from_post();
			$pezullimi = $this->Heqje_pezullimi_model->save($pezullimi);
			if($pezullimi->id){
				redirect('admin/vendimet/edit_pezullimi/'.$pezullimi->id.'?success=1');
			}
    	}
    	
    	$data['pezullimi'] = $this->Heqje_pezullimi_model->get_pezullimi($id);
    	$this->view('edit_pezullimi', $data);
    }

    public function delete_pezullimi($id = null){
    	if(!$id) return;
		$pezullimi = $this->Heqje_pezullimi_model->get_pezullimi($id);
		if($pezullimi->id){
			if($this->Heqje_pezullimi_model->delete_pezullimi($pezullimi->id)) { 
				redirect('admin/vendimet/heqje_pezullimi/?success=3');
			}
		}
		return;
    }

    private function date_diff($date1, $date2){
    	$date1 = date_create($date1);
		$date2 = date_create($date2);		
		$diff = date_diff($date1,$date2);
		return $diff;
	}

	public function autocomplete_data(){
		$result = array();
		if($this->input->post('table') && $this->input->post('column') && $this->input->post('value')) {
			$table = $this->input->post('table');
			$column = $this->input->post('column');
			$value = $this->input->post('value');

 			$result = $this->Vendimet_model->get_autocomplete_data($table, $column, $value);
 		}	
		echo json_encode($result);	
	}

	public function tagjet_ajax(){
		$vendimi_id = $this->input->post('vendimi_id');
		$vendimi = $this->Vendimet_model->get_vendimet($vendimi_id);
		$tags = $this->Vendimet_model->get_tags();
		$select_option_tags = '<i class="btn btn-xs btn-default fa fa-check tags_submit"></i><select class="form-control chosen-select tags_select" id="'.$vendimi->id.'" name="tags[]" data-placeholder="Zgjedh tags" multiple>';
		foreach($tags as $tag){
			$selected = FALSE;
			foreach($vendimi->tags as $vendimi_tag){
				if($vendimi_tag->id == $tag->id) {
					$selected = TRUE;
					break;
				}
			}
			$select_option_tags .= '<option value="'. $tag->id.'"'.($selected ? ' selected="selected"' : '').'>'.$tag->emri.'</option>';
		}
	  	$select_option_tags .='</select></div>';
	  	echo json_encode($select_option_tags);	
	}

	public function save_tags(){
		$vendimi_id = $this->input->post('vendimi_id');
		$tags = $this->input->post('tags');
		$result = $this->Vendimet_model->save_tags($vendimi_id, $tags);
		return $result;
	}

	public function get_tag($id = null){
		$this->load->model('Tags_model');
		$tag = $this->Tags_model->get_tag($id);
		echo json_encode($tag);
	}	
}