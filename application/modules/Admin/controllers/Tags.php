<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tags extends MY_Controller {
	
	public function __construct() 
	{
        parent::__construct();
        $this->load->model('Tags_model');
    }

    public function index($type = null) {
    	if($type == 'json'){
			$requestData = $_REQUEST;
			$columns = array(
				0	=> 'emri'
				);

			$totalData = $this->Tags_model->get_total_tags();
			$totalFiltered = $totalData;
			$fieldset = null;
			if(!empty($requestData['search']['value'])){
				$fieldset = array(
					'emri'						=> $requestData['search']['value']
				);
			}
			$filters = null;
			$order_by = array('column' => $columns[$requestData['order'][0]['column']], 'direction' => $requestData['order'][0]['dir']);
			$limit = array('start' => $requestData['start'], 'length' => $requestData['length']);
			$tags = $this->Tags_model->get_tags($fieldset, $order_by, $limit, $filters);
			if( !empty($requestData['search']['value'])) { 
				$totalFiltered = count($this->Tags_model->get_tags($fieldset, $order_by, null, $filters));
			}
			$data = array();
			foreach($tags as $tag){
				$nestedData = array();

				$nestedData[] = $tag->emri;
				$nestedData[] = '<div style="text-align: right;"><a href="'.base_url().'admin/tags/edit_tag/'.$tag->id.'" target="_blank" class="btn btn-xs btn-default" ><i class="fa fa-pencil" title="Edit"></i></a> <a href="'.base_url().'admin/tags/delete_tag/'.$tag->id.'" class="delete btn btn-xs btn-danger"><i class="fa fa-trash-o" title="Delete"></i></a></div>';

				$data[] = $nestedData;
			}
			$json_data = array(
				"draw"		=> intval($requestData["draw"]),
				"recordsTotal"    => intval( $totalData ),
				"recordsFiltered" => intval( $totalFiltered ),
				"data"		=> $data
				);
			echo json_encode($json_data);
			return;
		}else{
			$data['tag'] = array();
		}
		$data['last_updated'] = $this->Tags_model->get_last_updated();
		$this->view('tags', $data);
    }

    public function edit_tag($id = null){
    	if($this->input->post()){
    		$tag = $this->Tags_model->object_from_post();
			$tag = $this->Tags_model->save($tag);
			if($tag->id){
				redirect('admin/tags/edit_tag/'.$tag->id.'?success=1');
			}
    	}
    	
    	$data['tag'] = $this->Tags_model->get_tag($id);
    	$this->view('edit_tag', $data);
    }

    public function delete_tag($id = null){
    	if(!$id) return;
		$tag = $this->Tags_model->get_tag($id);
		if($tag->id){
			if($this->Tags_model->delete_tag($tag->id)) { 
				redirect('admin/tags/?success=3');
			}
		}
		return;
    }
}