<style type="text/css">
    td.details-control {
        background: url('<?php echo base_url(); ?>assets/images/icons8-double-down-20.png') no-repeat center center;
        cursor: pointer;
    }
    tr.shown td.details-control {
        background: url('<?php echo base_url(); ?>assets/images/icons8-double-up-20.png') no-repeat center center;
    }
    table.extra-details {
        table-layout:fixed; 
        width:auto;
    }
    table.extra-details th, table.extra-details tr td {
        width:500px !important;
        word-wrap: break-word !important;
        white-space: normal !important;
    }
    div.dt-button-collection.four-column {
        width:80% !important;
    }
    div.dt-button-collection.fixed {
        left:10%;
        margin:0;
        padding:4px;
    }
    div.dt-button-collection.fixed.four-column {
        margin:0;
    }
    table th{
        text-transform: uppercase;
        font-size: 12px;
    }
    table td{
        font-size: 12px;
    }
    div.dt-button-collection.one-column {
        width:30% !important;
        height:80% !important;
        overflow-y: auto;
    }
    div.dt-button-collection.fixed {
        left:35%;
        margin:0;
        padding:4px;
    }
    div.dt-button-collection.fixed.one-column {
        margin:0;
    } 
    @media only screen and (max-width: 767px) {
        div.dt-button-collection.one-column {
            width:90% !important;
            height:80% !important;
        }
        div.dt-button-collection.fixed {
            left:5%;
        }        
    }
</style>
<section id="general-section">
<?php if($this->input->get('success') == 3): ?>
  <div class="alert alert-success text-center"><strong>Të dhënat u fshinë me sukses.</strong></div>
<?php endif; ?>
    <div class="container">
        <div class="tags-section">
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-12">
                            <div id="advSearch" class="collapse">
                                <div class="tags-section">
                                <h4>Filtro në bazë të kritereve:</h4>
                                <ul class="nav nav-tabs" id="myTab" role="tablist">
                                    <li class="nav-item active">
                                        <a class="nav-link active" id="fDatat-tab" data-toggle="tab" href="#fDatat" role="tab" aria-controls="fDatat" aria-selected="true">Data e publikimit</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="fVlera-tab" data-toggle="tab" href="#fVlera" role="tab" aria-controls="fVlera" aria-selected="false">Vlera e parashikuar</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="fAk-tab" data-toggle="tab" href="#fAk" role="tab" aria-controls="fAk" aria-selected="false">Autoriteti Kontraktues</a>
                                    </li>                                    
                                    <li class="nav-item">
                                        <a class="nav-link" id="fOeAnkues-tab" data-toggle="tab" href="#fOeAnkues" role="tab" aria-controls="fOeAnkues" aria-selected="false">OE Ankues</a>
                                    </li>                                    
                                    <li class="nav-item">
                                        <a class="nav-link" id="fPanelistet-tab" data-toggle="tab" href="#fPanelistet" role="tab" aria-controls="fPanelistet" aria-selected="false">Paneli</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="fVendimet-tab" data-toggle="tab" href="#fVendimet" role="tab" aria-controls="fVendimet" aria-selected="false">Vendimet e panelit</a>
                                    </li>                                    
                                    <li class="nav-item">
                                        <a class="nav-link" id="fRekomandimi-tab" data-toggle="tab" href="#fRekomandimi" role="tab" aria-controls="fRekomandimi" aria-selected="false">Rekomandimet e ekspertit</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="fNeni-tab" data-toggle="tab" href="#fNeni" role="tab" aria-controls="fNeni" aria-selected="false">Nenet</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="fTagjet-tab" data-toggle="tab" href="#fTagjet" role="tab" aria-controls="fTagjet" aria-selected="false">Fjalë kyçe</a>
                                    </li>
                                </ul>
                                <div class="tab-content" id="myTabContent">
                                    <div class="tab-pane fade active in" id="fDatat" role="tabpanel" aria-labelledby="fDatat-tab">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                  <label for="Nga">Nga data:</label>
                                                  <input type="text" class="form-control form_date advFilters ngaData" name="ngaData-filter">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                  <label for="Deri">Deri në datën:</label>
                                                  <input type="text" class="form-control form_date advFilters deriData" name="deriData-filter">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="fVlera" role="tabpanel" aria-labelledby="fVlera-tab">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                  <label for="Vlera minimale">Vlera minimale:</label>
                                                  <input type="text" oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1');" class="form-control advFilters ngaVlera" name="ngaVlera-filter">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                  <label for="Vlera maximale">Vlera maximale:</label>
                                                  <input type="text" oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1');" class="form-control advFilters deriVlera" name="deriVlera-filter">
                                                </div>
                                            </div>
                                        </div>                                      
                                    </div>
                                    <div class="tab-pane fade" id="fAk" role="tabpanel" aria-labelledby="fAk-tab">
                                        <div class="form-group">
                                            <label for="Autoriteti_Kontraktues">Autoriteti Kontraktues:</label>
                                            <select class="form-control chosen-select advFilters ak" data-placeholder="- Zgjedhje -" name="ak-filter[]" multiple>
                                                <?php foreach($ak as $aki): ?>
                                                    <option value="<?php echo $aki; ?>"><?php echo $aki; ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>  
                                    </div>                                     
                                    <div class="tab-pane fade" id="fOeAnkues" role="tabpanel" aria-labelledby="fOeAnkues-tab">
                                        <div class="form-group">
                                            <label for="OE_Ankues">OE Ankues:</label>
                                            <select class="form-control chosen-select advFilters oeankues" data-placeholder="- Zgjedhje -" name="oeankues-filter[]" multiple>
                                                <?php foreach($oe_ankues as $oe): ?>
                                                    <option value="<?php echo $oe; ?>"><?php echo $oe; ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>  
                                    </div>                                     
                                    <div class="tab-pane fade" id="fPanelistet" role="tabpanel" aria-labelledby="fPanelistet-tab">
                                        <div class="form-group">
                                            <label for="Panelistet">Panelistët:</label>
                                            <select class="form-control chosen-select advFilters panelistet" data-placeholder="- Zgjedhje -" name="panelistet-filter[]" multiple >
                                                <?php foreach($panelistet as $panelist): ?>
                                                    <option value="<?php echo $panelist; ?>"><?php echo $panelist; ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>                                      
                                    </div>
                                    <div class="tab-pane fade" id="fVendimet" role="tabpanel" aria-labelledby="fVendimet-tab">
                                        <div class="form-group">
                                            <label for="Vendimi">Vendimet e panelit:</label>
                                            <select class="form-control chosen-select advFilters vendimet" data-placeholder="- Zgjedhje -" name="vendimi-filter[]" multiple>
                                                <?php foreach($vendimet as $vendimi): ?>
                                                    <option value="<?php echo $vendimi; ?>"><?php echo $vendimi; ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>  
                                    </div>                                    
                                    <div class="tab-pane fade" id="fRekomandimi" role="tabpanel" aria-labelledby="fRekomandimi-tab">
                                        <div class="form-group">
                                            <label for="Rekomandimet">Rekomandimet e ekspertit:</label>
                                            <select class="form-control chosen-select advFilters rekomandimet" data-placeholder="- Zgjedhje -" name="rekomandimi-filter[]" multiple>
                                                <?php foreach($rekomandimet as $rekomandimi): ?>
                                                    <option value="<?php echo $rekomandimi; ?>"><?php echo $rekomandimi; ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>  
                                    </div>
                                    <div class="tab-pane fade" id="fNeni" role="tabpanel" aria-labelledby="fNeni-tab">
                                        <div class="form-group">
                                            <label for="Nenet">Nenet:</label>
                                            <select class="form-control chosen-select advFilters nenet" data-placeholder="- Zgjedhje -" name="neni-filter[]" multiple>
                                                <?php foreach($nenet as $neni): ?>
                                                    <option value="<?php echo $neni; ?>"><?php echo $neni; ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>  
                                    </div>
                                    <div class="tab-pane fade" id="fTagjet" role="tabpanel" aria-labelledby="fTagjet-tab">
                                        <?php if(!empty($used_tags)): ?>
                                            <?php foreach($used_tags as $ut): ?>
                                                <div class="checkbox">
                                                    <input type="checkbox" class="tags-filter advFilters" name="tags-filter[]" value="<?php echo $ut->id; ?>" /><span><?php echo $ut->emri; ?></span>
                                                </div>
                                            <?php endforeach;?>
                                            <?php else: ?>
                                                <div class="text-center">
                                                    <p>Nuk ka asnjë fjalë kyçe.</p>
                                                </div>
                                            <?php endif; ?>
                                    </div>                                    
                                </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div id="advFiltersTerms"></div>
                        </div>                                                
                        <div class="col-md-8">
                            <a class="btn btn-default" data-toggle="collapse" data-target="#advSearch"><i id="advSearchIcon" class="fa fa-search"></i><span class="kerkimi_btn">Kërkim i avancuar</span></a> <a href="<?php echo base_url(); ?>admin/vendimet/edit_vendimi" class="btn btn-default" target="_blank"><i class="fa fa-plus"></i> Shto vendim</a>
                        </div>
                        <div class="col-md-4">
                            <div class="pull-right">
                                <h5>Përditësimi i fundit: <strong><?php echo $last_updated; ?></strong></h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>        
        <div class="row">
            <div class="col-md-12">
                <table id="vendimet_table" class="row-border compact" style="width:100%">
                    <thead>
                        <tr>
                            <th class="not-export-col"></th>
                            <th class="not-export-col"></th>

                            <th>Nr. Protokollit</th>
                            <th class="fixed-min-width">Emri i lëndës</th>
                            <th>Vlera e parashikuar</th>
                            <th>Lloji i kontratës</th>
                            <th>Autoriteti kontraktues</th>

                            <th>OE Ankues</th>
                            <th>Rekomandimi i ekspertit</th>
                            <th>Ankesa kundër</th>

                            <th>Vendimi</th>
                            <th>Tërheqje e vërejtjes (Paneli)</th>
                            <th>Neni</th>
                            <th>Komente në lidhje me vendimin e panelit</th>
                            <th>Nr. Vendimit</th>
                            <th>Vendimet e mëparshme</th>
                            <th>Vendimi jo konsistent me vendimin</th>
                            <th>Konfiskim i tarifës</th>

                            <th>Kryetar i panelit</th>
                            <th>Referent i panelit</th>
                            <th>Anëtari i I-rë i panelit</th>
                            <th>Anëtari i II-të i panelit</th>
                            <th>Anëtari i III-të i panelit</th>
                            <th>Nr. i panelistëve</th>

                            <th>Koment (Eksperti)</th>

                            <th>Eksperti shqyrtues</th>
                            <th>Eksperti teknik</th>

                            <th>Seanca Hapur/Mbyllur</th>

                            <th>Data e Ankesës</th>
                            <th>Data e Vendimit</th>
                            <th>Data e Publikimit</th>
                            <th>Kohëzgjatja Ankesë/Vendim</th>
                            <th>Kohëzgjatja Vendim/Publikim</th>
                            <th>Kohëzgjatja Ankesë/Publikim</th>

                            <th>Komente të përgjithshme</th>

                            <th class="not-export-col">Fjalë kyçe</th>
                            <th class="not-export-col">Actions</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th class="not-export-col"></th>
                            <th class="not-export-col"></th>
                            
                            <th><div class="ui-widget"><input type="text" data-column="2" data-name="nr_protokollit" class="search-input-text" placeholder="Nr. Protokollit"></div></th>
                            <th><div class="ui-widget"><input type="text" data-column="3" data-name="emri_lendes" class="search-input-text" placeholder="Emri i lëndës"></div></th>
                            <th><div class="ui-widget"><input type="text" data-column="4" data-name="vlera_parashikuar" class="search-input-text" placeholder="Vlera e parashikuar"></div></th>
                            <th><div class="ui-widget"><input type="text" data-column="5" data-name="lloji_kontrates" class="search-input-text" placeholder="Lloji i kontrates"></div></th>
                            <th><div class="ui-widget"><input type="text" data-column="6" data-name="autoriteti_kontraktues" class="search-input-text" placeholder="AK"></div></th>

                            <th><div class="ui-widget"><input type="text" data-column="7" data-name="oe_ankues" class="search-input-text" placeholder="OE Ankues"></div></th>
                            <th><div class="ui-widget"><input type="text" data-column="8" data-name="rekomandimi_ekspertit" class="search-input-text" placeholder="Rekomandimi i ekspertit"></div></th>                            
                            <th><div class="ui-widget"><input type="text" data-column="9" data-name="ankesa_kunder" class="search-input-text" placeholder="Ankesa kundër"></div></th>

                            <th><div class="ui-widget"><input type="text" data-column="10" data-name="vendimi" class="search-input-text" placeholder="Vendimi"></div></th>
                            <th><div class="ui-widget"><input type="text" data-column="11" data-name="terheqje_verejtje" class="search-input-text" placeholder="Tërheqje e vërejtjes (Paneli)"></div></th>
                            <th><div class="ui-widget"><input type="text" data-column="12" data-name="neni" class="search-input-text" placeholder="Neni"></div></th>
                            <th><div class="ui-widget"><input type="text" data-column="13" data-name="komente_paneli" class="search-input-text" placeholder="Komente (Paneli)"></div></th>
                            <th><div class="ui-widget"><input type="text" data-column="14" data-name="nr_vendimit" class="search-input-text" placeholder="Nr. Vendimit"></div></th>
                            <th><div class="ui-widget"><input type="text" data-column="15" data-name="vendimet_meparshme" class="search-input-text" placeholder="Vendimet e mëparshme"></div></th>
                            <th><div class="ui-widget"><input type="text" data-column="16" data-name="vendimi_jokonsistent" class="search-input-text" placeholder="Vendimi jo konsistent me vendimin"></div></th>
                            <th><div class="ui-widget"><input type="text" data-column="17" data-name="konfiskim_tarifes" class="search-input-text" placeholder="Konfiskim i tarifës"></div></th>

                            <th><div class="ui-widget"><input type="text" data-column="18" data-name="kryetar" class="search-input-text" placeholder="Kryetar i panelit"></div></th>
                            <th><div class="ui-widget"><input type="text" data-column="19" data-name="referent" class="search-input-text" placeholder="Referent i panelit"></div></th>
                            <th><div class="ui-widget"><input type="text" data-column="20" data-name="anetari_1" class="search-input-text" placeholder="Anëtari i I-rë i panelit"></div></th>
                            <th><div class="ui-widget"><input type="text" data-column="21" data-name="anetari_2" class="search-input-text" placeholder="Anëtari i II-të i panelit"></div></th>
                            <th><div class="ui-widget"><input type="text" data-column="22" data-name="anetari_3" class="search-input-text" placeholder="Anëtari i III-të i panelit"></div></th>
                            <th><div class="ui-widget"><input type="text" data-column="23" data-name="nr_panelisteve" class="search-input-text" placeholder="Nr. i panelistëve"></div></th>

                            <th><div class="ui-widget"><input type="text" data-column="24" data-name="komente_eksperti" class="search-input-text" placeholder="Koment (Eksperti)"></div></th>

                            <th><div class="ui-widget"><input type="text" data-column="25" data-name="eksperti_shqyrtues" class="search-input-text" placeholder="Eksperti shqyrtues"></div></th>
                            <th><div class="ui-widget"><input type="text" data-column="26" data-name="eksperti_teknik" class="search-input-text" placeholder="Eksperti teknik"></div></th>

                            <th><div class="ui-widget"><input type="text" data-column="27" data-name="seanca" class="search-input-text" placeholder="Seanca Hapur/Mbyllur"></div></th>

                            <th><div class="ui-widget"><input type="text" data-column="28" data-name="data_ankeses" class="search-input-text" placeholder="Data e Ankesës"></div></th>
                            <th><div class="ui-widget"><input type="text" data-column="29" data-name="data_vendimit" class="search-input-text" placeholder="Data e Vendimit"></div></th>
                            <th><div class="ui-widget"><input type="text" data-column="30" data-name="data_publikimit" class="search-input-text" placeholder="Data e Publikimit"></div></th>

                            <th>Kohëzgjatja Ankesë/Vendim</th>
                            <th>Kohëzgjatja Vendim/Publikim</th>
                            <th>Kohëzgjatja Ankesë/Publikim</th>

                            <th><div class="ui-widget"><input type="text" data-column="34" data-name="komente_gjenerale" class="search-input-text" placeholder="Komente të përgjithshme"></div></th>

                            <th class="not-export-col">Fjalë kyçe</th>
                            <th class="not-export-col">Actions</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
        <?php $this->load->view('template/legjenda', $legjenda); ?>
    </div>
</section>
<script>
var site_url = '<?php echo base_url(); ?>';
var encodedLogo = '<?php echo $this->config->item("encodedLogo"); ?>';
var xhr;
$(document).ready(function() {
    $(".form_date").datetimepicker({     
        //format: 'MM-DD-YYYY'
        format: 'YYYY-MM-DD'
    });
    $('#advSearch').on('hidden.bs.collapse', function (e) {
        $('#advFiltersTerms').html('');
        $('#advSearchIcon').addClass('fa-search').removeClass('fa-times');
        $('.kerkimi_btn').html('Kërkim i avancuar');
        $('.tags-filter').removeAttr('checked');
        $('.ngaData, .deriData').val('');
        $('.advFilters').val('').trigger('chosen:updated');
        vendimet_table.draw();
    });
    $('#advSearch').on('shown.bs.collapse', function (e) {
        $('.kerkimi_btn').html('Pastro kërkimin');
        $('#advSearchIcon').addClass('fa-times').removeClass('fa-search');
    });
    var vendimet_table = $('#vendimet_table').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'pageLength',
            {
                extend: 'excel',
                exportOptions: {
                    columns: [':visible :not(.not-export-col)']
                },
                title: 'Vendimet e OSHP-së në bazë të kritereve të zgjedhura',                            
            },
            {
                extend: 'pdfHtml5',
                orientation: 'landscape',
                pageSize: 'LEGAL',
                exportOptions: {
                    columns: [':visible :not(.not-export-col)']             
                },
                title: 'Vendimet e OSHP-së në bazë të kritereve të zgjedhura',
                customize: function ( doc ) {
                    doc.content.splice( 1, 0, {
                        margin: [ 0, 0, 0, 12 ],
                        alignment: 'center',
                        image: encodedLogo,
                        width: 170
                    } );
                }                          
            },
            {
                extend: 'print',
                exportOptions: {
                    columns: [':visible :not(.not-export-col)']            
                },
                title: 'Vendimet e OSHP-së në bazë të kritereve të zgjedhura',                            
            },
            {
                extend: 'csvHtml5',
                exportOptions: {
                    columns: [':visible :not(.not-export-col)']             
                },
                title: 'Vendimet e OSHP-së në bazë të kritereve të zgjedhura',                            
            },
            {
                extend: 'colvis',
                collectionLayout: 'fixed one-column',
                columns: [5,6,7,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,8,24,25,26,27,28,29,30,31,32,33,34,35,36],
                text: 'Zgjedh kolonat',
                prefixButtons: [
                    { extend:'columnToggle', text:'Zgjedh të gjitha/asnjë', className:"toggleBtnSelect toggleBtnGroup" },
                ],
                postfixButtons: [
                    { extend:'colvisRestore', text:'Rikthe kolonat fillestare', className:"toggleBtnRestore toggleBtnGroup" }
                ]
            },
        ],
        columnDefs: [
            { targets: [0, 1, 2, 3, 4, 6, 7, 8, 10, 14, 35, 36], visible: true },
            { targets: '_all', visible: false },
            { className: "details-control", "targets": [0], "orderable": false, "width": "10px", "searchable": false },
            { className: "details-label", "targets": [1], "orderable": false, "searchable": false },
            { className: "lenda-width", targets: 3}
        ],
        "language": {
                "processing": "Ju lutem prisni...", //add a loading image,simply putting <img src="loader.gif" /> tag.
                buttons: {
                    pageLength: {
                    _: "Shfaq %d rreshta",
                    '-1': "Të gjithë rreshtat"
                    }
                },
                "zeroRecords": "Nuk ka të dhëna.",
                "info": "Shfaqur _START_ deri _END_ prej _TOTAL_ të dhënave",
                "paginate": {
                            "previous": "Faqja paraprake",
                            "next": "Faqja tjetër",
                            "first": "Faqja parë",
                            "last": "Faqja fundit"
                        },
                "search": "Kërko",
                "infoFiltered": " (prej total _MAX_ të të dhënave)"
            },          
        "autoWidth": false,
        "lengthMenu": [ [15, 25, 50, -1], [15, 25, 50, "Të gjitha"] ],
        "processing": true,
        "serverSide": true,
        "scrollX": true,
        "searchHighlight": true,         
        "ajax":{
            url :site_url + 'admin/vendimet/index/json', // json datasource
            type: "post",  // method  , by default get
            data:  function ( d ) {
                $('#advFiltersTerms').html('');
                d.advFilters = {};

                d.advFilters.datat = [];
                var ngaData = $(".ngaData").val();
                var deriData = $(".deriData").val();
                d.advFilters.datat.push({"ngaData":ngaData});
                d.advFilters.datat.push({"deriData":deriData});

                d.advFilters.vlerat = [];
                var ngaVlera = $(".ngaVlera").val();
                var deriVlera = $(".deriVlera").val();
                d.advFilters.vlerat.push({"ngaVlera":ngaVlera});
                d.advFilters.vlerat.push({"deriVlera":deriVlera});

                d.advFilters.ak = [];
                var ak = $(".ak").val();
                d.advFilters.ak.push({"ak":ak});

                d.advFilters.oeankues = [];
                var oeankues = $(".oeankues").val();
                d.advFilters.oeankues.push({"oeankues":oeankues});

                d.advFilters.panelistet = [];
                var paneli = $(".panelistet").val();
                d.advFilters.panelistet.push({"paneli":paneli});

                d.advFilters.rekomandimet = [];
                var rekomandimet = $(".rekomandimet").val();
                d.advFilters.rekomandimet.push({"rekomandimet":rekomandimet});

                d.advFilters.nenet = [];
                var nenet = $(".nenet").val();
                d.advFilters.nenet.push({"nenet":nenet});

                d.advFilters.vendimet = [];
                var vendimet = $(".vendimet").val();
                d.advFilters.vendimet.push({"vendimet":vendimet});

                d.advFilters.tags = [];
                var tags = [];
                $(".tags-filter").each(function(){
                    if (this.checked) {
                        d.advFilters.tags.push(parseInt($(this).val())); 
                    }
                });
                if((d.advFilters.datat[0].ngaData && d.advFilters.datat[1].deriData) || (d.advFilters.vlerat[0].ngaVlera && d.advFilters.vlerat[1].deriVlera) || d.advFilters.ak[0].ak || d.advFilters.oeankues[0].oeankues || d.advFilters.panelistet[0].paneli || d.advFilters.rekomandimet[0].rekomandimet || d.advFilters.nenet[0].nenet || d.advFilters.vendimet[0].vendimet || (d.advFilters.tags.length > 0))
                {
                    var result = '<div class="tags-section"><h4>Rezultatet në bazë të kritereve:</h4>';
                    result = result + '<ul>';

                    if(d.advFilters.datat[0].ngaData || d.advFilters.datat[1].deriData)
                    {
                        result = result + '<li>DATAT: nga <span>'+d.advFilters.datat[0].ngaData+'</span> deri <span>'+d.advFilters.datat[1].deriData+'</span></li>'; 
                    }  

                    if(d.advFilters.vlerat[0].ngaVlera && d.advFilters.vlerat[1].deriVlera)
                    {
                        result = result + '<li>VLERA E PARASHIKUAR: nga €<span>'+d.advFilters.vlerat[0].ngaVlera+'</span> deri €<span>'+d.advFilters.vlerat[1].deriVlera+'</span></li>'; 
                    }

                    if(d.advFilters.ak[0].ak){
                        var ak_str = '';
                        for(var i=0; i < d.advFilters.ak[0].ak.length; i++){
                            if(i != 0){
                                ak_str = ak_str + ', ';
                            }
                            ak_str = ak_str + '<span>'+d.advFilters.ak[0].ak[i]+'</span>';
                        }
                        result = result + '<li>AUTORITETI KONTRAKTUES: '+ak_str+'</li>'; 
                    }

                    if(d.advFilters.oeankues[0].oeankues){
                        var oeankues_str = '';
                        for(var i=0; i < d.advFilters.oeankues[0].oeankues.length; i++){
                            if(i != 0){
                                oeankues_str = oeankues_str + ', ';
                            }
                            oeankues_str = oeankues_str + '<span>'+d.advFilters.oeankues[0].oeankues[i]+'</span>';
                        }
                        result = result + '<li>OE ANKUES: '+oeankues_str+'</li>'; 
                    }

                    if(d.advFilters.panelistet[0].paneli){
                        var panelistet_str = '';
                        for(var i=0; i < d.advFilters.panelistet[0].paneli.length; i++){
                            if(i != 0){
                                panelistet_str = panelistet_str + ', ';
                            }
                            panelistet_str = panelistet_str + '<span>'+d.advFilters.panelistet[0].paneli[i]+'</span>';
                        }
                        result = result + '<li>PANELI: '+panelistet_str+'</li>'; 
                    }

                    if(d.advFilters.rekomandimet[0].rekomandimet){
                        var rekomandimet_str = '';
                        for(var i=0; i < d.advFilters.rekomandimet[0].rekomandimet.length; i++){
                            if(i != 0){
                                rekomandimet_str = rekomandimet_str + ', ';
                            }
                            rekomandimet_str = rekomandimet_str + '<span>'+d.advFilters.rekomandimet[0].rekomandimet[i]+'</span>';
                        }
                        result = result + '<li>REKOMANDIMET E EKSPERTIT: '+rekomandimet_str+'</li>'; 
                    }                    

                    if(d.advFilters.nenet[0].nenet){
                        var nenet_str = '';
                        for(var i=0; i < d.advFilters.nenet[0].nenet.length; i++){
                            if(i != 0){
                                nenet_str = nenet_str + ', ';
                            }
                            nenet_str = nenet_str + '<span>'+d.advFilters.nenet[0].nenet[i]+'</span>';
                        }
                        result = result + '<li>NENET: '+nenet_str+'</li>'; 
                    }

                    if(d.advFilters.vendimet[0].vendimet){
                        var vendimet_str = '';
                        for(var i=0; i < d.advFilters.vendimet[0].vendimet.length; i++){
                            if(i != 0){
                                vendimet_str = vendimet_str + ', ';
                            }
                            vendimet_str = vendimet_str + '<span>'+d.advFilters.vendimet[0].vendimet[i]+'</span>';
                        }
                        result = result + '<li>VENDIMET E PANELIT: '+vendimet_str+'</li>'; 
                    }
                    if(d.advFilters.tags.length > 0){
                        var tags_str = '';
                        for(var i=0; i < d.advFilters.tags.length; i++){
                            var tags = get_tag(d.advFilters.tags[i]);
                            if(tags.id){
                                if(i != 0){
                                    tags_str = tags_str + ', ';
                                }
                                tags_str = tags_str + '<span>'+tags.emri+'</span>';
                            }
                        }
                      
                        result = result + '<li>Fjalët kyçe: '+tags_str+'</li>'; 
                    }                                         
                    result = result + '</ul>';
                    result = result + '</div>';
                    $('#advFiltersTerms').html(result);
                }
                
                $('.buttons-print').html('<span class="fa fa-print" data-toggle="tooltip" title="Printo"/>');
                $('.buttons-excel').html('<span class="fa fa-file-excel-o" data-toggle="tooltip" title="Excel"/>');
                $('.buttons-csv').html('<span class="fa fa-file-o" data-toggle="tooltip" title="CSV"/>');
                $('.buttons-pdf').html('<span class="fa fa-file-pdf-o" data-toggle="tooltip" title="PDF"/>');                
            },     
            error: function(){  // error handling
                $(".vendimet_table_error").html("");
                $("#vendimet_table").append('<tbody class="vendimet_table_error"><tr><th colspan="10">Nuk ka të dhëna.</th></tr></tbody>');
                //$("#employee-grid_processing").css("display","none");                 
            }
        }
    } );

    $('.advFilters').on('change', function (e) {
        vendimet_table.draw();
    } );
    $(".form_date").on("dp.change", function (e) {
        vendimet_table.draw();
    } );
    
    $('.search-input-text').autocomplete({
        minLength: 0,
        source: function(request, response){
            var el = $(this.element);
            var i =el.attr('data-column') // getting column index
            var n =el.attr('data-name') // getting name index
            var v =el.val();  // getting search input value
            if(v.length >= 1){
                if(xhr) {
                    xhr.abort();
                }
                var post_data = {table: 'vendimet', column: n, value: v};
                xhr = $.ajax({
                    'async': false,
                    'url': site_url + 'admin/vendimet/autocomplete_data/',
                    'data': post_data,
                    'type': 'POST',
                    success:function(data){
                        data = JSON.parse(data);
                        response(data);
                    }
                });            
            }else{
                vendimet_table.columns(i).search('').draw();
            }
        },
        select: function(event, ui) {
            var i = $(this).attr('data-column') // getting column index
            vendimet_table.columns(i).search(ui.item.value).draw();
        }           
    });

    $('#vendimet_table tbody').on('click', '.vtlbl', function () {
        var el = $(this).closest('.vendimi_tags_lbl');
        var vendimi_id = el.attr('id');
        var post_data = {vendimi_id: vendimi_id};
        $.ajax({
            'async': false,
            'url': site_url + 'admin/vendimet/tagjet_ajax/',
            'data': post_data,
            'type': 'POST',
            success:function(data){
                data = JSON.parse(data);
                el.closest('.vendimi_tags_lbl').html(data);
                $('.chosen-select').chosen();
            }
        }); 
    } );

    $('#vendimet_table tbody').on('click', '.tags_submit', function () {
        var el = $(this).next('.tags_select');
        var vendimi_id = el.attr('id');
        var tags = el.val();
        var post_data = {vendimi_id: vendimi_id, tags:tags};
        $.ajax({
            'async': false,
            'url': site_url + 'admin/vendimet/save_tags/',
            'data': post_data,
            'type': 'POST',
            success:function(data){
                vendimet_table.draw(false);
            }
        }); 
    } );

    $('#vendimet_table tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        //var id = tr.find('.pezullimi_id').val();
        var row = vendimet_table.row( tr );
        if ( row.child.isShown() ) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
            // Open this row
            row.child(fetch_data(row.data())).show();
            tr.addClass('shown');
            $(this).closest('tr').next().css('background', '#f5f5f5');
            $(this).closest('tr').next().find('tr').css('background', 'transparent');
            $(this).closest('tr').next().find('th').css({'border-bottom': '1px solid #9e9e9e', 'color': '#111'});
            $(this).closest('tr').next().find('tr td').css({'color': '#111'});
        }

    } );

    var buttons = vendimet_table.buttons('.buttons-columnVisibility').nodes();
    var groupArrOne = [1,2];
    var groupArrTwo = [3,4];
    var groupArrThree = [5,6,7,8,9,10,11,12];
    var groupArrFour = [13,14,15,16,17,18];
    var groupArrFive = [19,20,21,22];
    var groupArrSix = [23];
    var groupArrSeven = [24,25,26,27,28,29];
    var groupArrEight = [30];
    var groupArrNine = [31];
    var groupOne = 0;
    var groupTwo = 0; 
    var groupThree = 0; 
    var groupFour = 0; 
    var groupFive = 0;
    var groupSix = 0;
    var groupSeven = 0;
    var groupEight = 0;
    var groupNine= 0;      
    $.each( buttons, function( key, value ) {
        var group;
        var groupDevider;
        if(groupArrOne.indexOf(key) != -1){
            group = ' groupOne';
            if(groupOne == 0){
                groupDevider = 'AKTIVITETI I PROKURIMIT';
            }            
            groupOne++;
        }else if(groupArrTwo.indexOf(key) != -1){
            group = ' groupTwo';
            if(groupTwo == 0){
                groupDevider = 'ANKESA';
            }            
            groupTwo++;
        }else if(groupArrThree.indexOf(key) != -1){
            group = ' groupThree';
            if(groupThree == 0){
                groupDevider = 'VENDIMET';
            }            
            groupThree++;
        }else if(groupArrFour.indexOf(key) != -1){
            group = ' groupFour';
            if(groupFour == 0){
                groupDevider = 'PANELI';
            }            
            groupFour++;
        }else if(groupArrFive.indexOf(key) != -1){
            group = ' groupFive';
            if(groupFive == 0){
                groupDevider = 'REKOMANDIMI I EKSPERTIT';
            }            
            groupFive++;
        }else if(groupArrSix.indexOf(key) != -1){
            group = ' groupSix';
            if(groupSix == 0){
                groupDevider = 'SEANCA HAPUR/MBYLLUR';
            }            
            groupSix++;
        }else if(groupArrSeven.indexOf(key) != -1){
            group = ' groupSeven';
            if(groupSeven == 0){
                groupDevider = 'AFATET';
            }            
            groupSeven++;
        }else if(groupArrEight.indexOf(key) != -1){
            group = ' groupEight';
            if(groupEight == 0){
                groupDevider = 'KOMENTE TË PËRGJITHSHME';
            }            
            groupEight++;
        }else if(groupArrNine.indexOf(key) != -1){
            group = ' groupNine';
            if(groupNine == 0){
                groupDevider = 'TAG';
            }            
            groupNine++;
        }else{
            group = '';
            groupDevider = '';
        }
        value.className = value.className + group;
        if(groupDevider){
            value.className = value.className + " groupHeader";
        }
    });    
});
function get_tag(id){
    var result = false;
    xhr = $.ajax({
        'async': false,
        'url': site_url + 'vendimet/get_tag/' + id,
        'type': 'GET',
        success:function(data){
            data = JSON.parse(data);
            result = data;
        }
    });
    return result; 
}
function fetch_data ( d ) {
    // `d` is the original data object for the row
    result = '<div class="col-md-12">'+
    '<table cellpadding="8" cellspacing="0" border="0" class="extra-details">' +
        '<th colspan="2">Aktiviteti i prokurimit</th>'+ 
        '<tr>'+
            '<td>Nr. i protokollit:</td>'+
            '<td>'+((d[2] != null) ? d[2] : '')+'</a></td>'+
        '</tr>'+
        '<tr>'+
            '<td>Emri i lëndës:</td>'+
            '<td>'+((d[3] != null) ? d[3] : '')+'</a></td>'+
        '</tr>'+
        '<tr>'+
            '<td>Vlera e parashikuar:</td>'+
            '<td>'+((d[4] != null) ? d[4] : '')+'</a></td>'+
        '</tr>'+
        '<tr>'+
            '<td>Lloji i kontratës:</td>'+
            '<td>'+((d[5] != null) ? d[5] : '')+'</a></td>'+
        '</tr>'+
        '<tr>'+
            '<td>Autoriteti kontraktues:</td>'+
            '<td>'+((d[6] != null) ? d[6] : '')+'</a></td>'+
        '</tr>'+     
        '<th colspan="2">Ankesa</th>'+
        '<tr>'+
            '<td>OE Ankues:</td>'+
            '<td>'+((d[7] != null) ? d[7] : '')+'</a></td>'+
        '</tr>'+
        '<tr>'+
            '<td>Ankesa kundër:</td>'+
            '<td>'+((d[9] != null) ? d[9] : '')+'</a></td>'+
        '</tr>'+                
        '<th colspan="2">Vendimet</th>'+
        '<tr>'+
            '<td>Vendimi:</td>'+
            '<td>'+((d[10] != null) ? d[10] : '')+'</a></td>'+
        '</tr>'+
        '<tr>'+
            '<td>Tërheqje e vërejtjes:</td>'+
            '<td>'+((d[11] != null) ? d[11] : '')+'</td>'+
        '</tr>'+
        '<tr>'+
            '<td>Neni:</td>'+
            '<td>'+((d[12] != null) ? d[12] : '')+'</td>'+
        '</tr>'+
        '<tr>'+
            '<td>Komente (Paneli):</td>'+
            '<td>'+((d[13] != null) ? d[13] : '')+'</td>'+
        '</tr>'+
        '<tr>'+
            '<td>Nr. Vendimit:</td>'+
            '<td>'+((d[14] != null) ? d[14] : '')+'</td>'+
        '</tr>'+
        '<tr>'+
            '<td>Vendimet e mëparshme:</td>'+
            '<td>'+((d[15] != null) ? d[15] : '')+'</td>'+
        '</tr>'+
        '<tr>'+
            '<td>Vendimi jo konsistent me vendimin:</td>'+
            '<td>'+((d[16] != null) ? d[16] : '')+'</td>'+
        '</tr>'+
        '<tr>'+
            '<td>Konfiskim i tarifës:</td>'+
            '<td>'+((d[17] != null) ? d[17] : '')+'</td>'+
        '</tr>'+
        '<th colspan="2">Paneli</th>'+ 
        '<tr>'+
            '<td>Kryetar i panelit:</td>'+
            '<td>'+((d[18] != null) ? d[18] : '')+'</td>'+
        '</tr>'+
        '<tr>'+
            '<td>Referent i panelit:</td>'+
            '<td>'+((d[19] != null) ? d[19] : '')+'</td>'+
        '</tr>'+
        '<tr>'+
            '<td>Anëtari i I-rë i panelit:</td>'+
            '<td>'+((d[20] != null) ? d[20] : '')+'</td>'+
        '</tr>'+
        '<tr>'+
            '<td>Anëtari i II-të i panelit:</td>'+
            '<td>'+((d[21] != null) ? d[21] : '')+'</td>'+
        '</tr>'+ 
        '<tr>'+
            '<td>Anëtari i III-të i panelit:</td>'+
            '<td>'+((d[22] != null) ? d[22] : '')+'</td>'+
        '</tr>'+ 
        '<tr>'+
            '<td>Nr. i panelistëve:</td>'+
            '<td>'+((d[23] != null) ? d[23] : '')+'</td>'+
        '</tr>'+
        '<th colspan="2">Rekomandimi i ekspertit</th>'+          
        '<tr>'+
            '<td>Rekomandimi i ekspertit:</td>'+
            '<td>'+((d[8] != null) ? d[8] : '')+'</td>'+
        '</tr>'+ 
        '<th colspan="2">Komente për rekomandimin e ekspertit</th>'+        
        '<tr>'+
            '<td>Koment (Eksperti):</td>'+
            '<td>'+((d[24] != null) ? d[24] : '')+'</td>'+
        '</tr>'+
        '<tr>'+
            '<td>Eksperti shqyrtues:</td>'+
            '<td>'+((d[26] != null) ? d[25] : '')+'</td>'+
        '</tr>'+ 
        '<tr>'+
            '<td>Eksperti teknik:</td>'+
            '<td>'+((d[26] != null) ? d[26] : '')+'</td>'+
        '</tr>'+ 
        '<th colspan="2">Seanca Hapur/Mbyllur</th>'+
        '<tr>'+
            '<td>Seanca Hapur/Mbyllur:</td>'+
            '<td>'+((d[27] != null) ? d[27] : '')+'</td>'+
        '</tr>'+
        '<th colspan="2">Afatet</th>'+
        '<tr>'+
            '<td>Data e ankesës:</td>'+
            '<td>'+((d[28] != null) ? d[28] : '')+'</td>'+
        '</tr>'+
        '<tr>'+
            '<td>Data e vendimit:</td>'+
            '<td>'+((d[29] != null) ? d[29] : '')+'</td>'+
        '</tr>'+
        '<tr>'+
            '<td>Data publikimit:</td>'+
            '<td>'+((d[30] != null) ? d[30] : '')+'</td>'+
        '</tr>'+
        '<tr>'+
            '<td>Kohëzgjatja Ankesë/Vendim:</td>'+
            '<td>'+((d[31] != null) ? d[31] : '')+'</td>'+
        '</tr>'+
        '<tr>'+
            '<td>Kohëzgjatja Vendim/Publikim:</td>'+
            '<td>'+((d[32] != null) ? d[32] : '')+'</td>'+
        '</tr>'+
        '<tr>'+
            '<td>Kohëzgjatja Ankesë/Publikim:</td>'+
            '<td>'+((d[33] != null) ? d[33] : '')+'</td>'+
        '</tr>'+
        '<tr>'+
            '<td>Komente të përgjithshme:</td>'+
            '<td>'+((d[34] != null) ? d[34] : '')+'</td>'+
        '</tr>'+                                         
    '</table>'+
'</div>';

    return result;
}
</script>