<section id="contact-section">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<form method="post" action="<?php echo base_url(); ?>admin/vendimet/edit_lista_e_zeze" accept-charset="utf-8" role="form">
					<input type="hidden" name="id" value="<?php echo $listazeze->id; ?>">
					<?php if($this->input->get('success') == 1): ?>
  						<div class="alert alert-success text-center"><strong>Të dhënat u <?php if($listazeze->id): ?>ndryshuan <?php else: ?> u insertuan <?php endif; ?> me sukses.</strong></div>
					<?php endif; ?>
					<div class="col-md-12">
						<div class="custom-well">
							<h4>Aktiviteti prokurimit</h4>
							<div class="form-group">
								Nr. i protokollit
								<input type="text" placeholder="Nr. Protokollit" class="form-control" name="nr_protokollit" id="nr_protokollit" value="<?php echo $listazeze->nr_protokollit; ?>">
							</div>
							<div class="form-group">
								Emri i lëndës
								<input type="text" placeholder="Emri i lëndës" class="form-control" name="emri_lendes" id="emri_lendes" value="<?php echo $listazeze->emri_lendes; ?>">
							</div>
							<div class="form-group">
								Vlera e parashikuar <small>(Nëse dëshironi që vlera të jetë N/A mos e plotëso këtë fushë)</small>
								<input type="text" placeholder="Vlera parashikuar" class="form-control" name="vlera_parashikuar" id="vlera_parashikuar" oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1');" value="<?php echo $listazeze->vlera_parashikuar; ?>">
							</div>
							<div class="form-group">
								Lloji i kontratës
								<input type="text" placeholder="Lloji i kontratës" class="form-control" name="lloji_kontrates" id="lloji_kontrates" value="<?php echo $listazeze->lloji_kontrates; ?>">
							</div>
							<div class="form-group">
								Autoriteti kontraktues
								<input type="text" placeholder="Autoriteti kontraktues" class="form-control" name="autoriteti_kontraktues" id="autoriteti_kontraktues" value="<?php echo $listazeze->autoriteti_kontraktues; ?>">
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="custom-well">
							<h4>Vendimi</h4>
							<div class="form-group">
								Kërkesa kundër Operaorit Ekonomik
								<input type="text" placeholder="Kërkesa kundër Operaorit Ekonomik" class="form-control" name="kerkesa_kunder_oe" id="kerkesa_kunder_oe" value="<?php echo $listazeze->kerkesa_kunder_oe; ?>">
							</div>
							<div class="form-group">
								Pretendimet e Autoritetit Kontraktues
								<textarea class="form-control rounded-0" placeholder="Pretendimet e Autoritetit Kontraktues" id="pretendimet_ak" name="pretendimet_ak" rows="3"><?php echo $listazeze->pretendimet_ak; ?></textarea>
							</div>
							<div class="form-group">
								Vendimi
								<input type="text" placeholder="Vendimi" class="form-control" name="vendimi" id="vendimi" value="<?php echo $listazeze->vendimi; ?>">
							</div>
							<div class="form-group">
								Diskualifikimi Kohëzgjatja
								<input type="text" placeholder="Diskualifikimi Kohëzgjatja" class="form-control" name="diskualifikimi_kohezgjatja" id="diskualifikimi_kohezgjatja" value="<?php echo $listazeze->diskualifikimi_kohezgjatja; ?>">
							</div>
							<div class="form-group">
								Neni
								<input type="text" placeholder="Neni" class="form-control" name="neni" value="<?php echo $listazeze->neni; ?>">
							</div>
							<div class="form-group">
								Komente në lidhje me vendimin e panelit
								<textarea class="form-control rounded-0" placeholder="Komente në lidhje me vendimin e panelit" id="komente_paneli" name="komente_paneli" rows="3"><?php echo $listazeze->komente_paneli; ?></textarea>
							</div>
							<div class="form-group">
								Vendimi për listë të zezë
								<input type="text" placeholder="Titulli" class="form-control" name="titulli" value="<?php echo $listazeze->vendimi_liste_zeze[0]->titulli; ?>">
							</div>
							<div class="form-group">
								Vendimi për listë të zezë URL
								<input type="text" placeholder="Vendimi URL" class="form-control" name="url" value="<?php echo $listazeze->vendimi_liste_zeze[0]->url; ?>">
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="custom-well">
						<h4>Paneli</h4>
							<div class="form-group">
								Kryetar
								<input type="text" placeholder="Kryetari i panelit" class="form-control" name="kryetar" id="kryetar" value="<?php echo $listazeze->kryetar; ?>">
							</div>
							<div class="form-group">
								Referent
								<input type="text" placeholder="Referenti i panelit" class="form-control" name="referent" id="referent" value="<?php echo $listazeze->referent; ?>">
							</div>
							<div class="form-group">
								Anëtari
								<input type="text" placeholder="Anëtari i I-rë i panelit" class="form-control" name="anetari_1" id="anetari_1" value="<?php echo $listazeze->anetari_1; ?>">
							</div>
							<div class="form-group">
								Anëtari
								<input type="text" placeholder="Anëtari i II-të i panelit" class="form-control" name="anetari_2" id="anetari_2" value="<?php echo $listazeze->anetari_2; ?>">
							</div>
							<div class="form-group">
								Anëtari
								<input type="text" placeholder="Anëtari i III-të i panelit" class="form-control" name="anetari_3" id="anetari_3" value="<?php echo $listazeze->anetari_3; ?>">
							</div>
							<div class="form-group">
								Nr. i panelistëve
								<input type="number" placeholder="Nr. i panelistëve" class="form-control" name="nr_panelisteve" id="nr_panelisteve" value="<?php echo $listazeze->nr_panelisteve; ?>">
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="custom-well">
						<h4>Eksperti</h4>
							<div class="form-group">
								Rekomandimi i ekspertit
								<input type="text" placeholder="Rekomandimi i ekspertit" class="form-control" name="rekomandimi_ekspertit" id="rekomandimi_ekspertit" value="<?php echo $listazeze->rekomandimi_ekspertit; ?>">
							</div>
							<div class="form-group">
								Komente për rekomandimin e ekspertit
								<textarea class="form-control rounded-0" placeholder="Komente në lidhje me vendimin e panelit" id="komente_paneli" name="komente_paneli" rows="3"><?php echo $listazeze->komente_eksperti; ?></textarea>
							</div>
							<div class="form-group">
								Eksperti shqyrtues
								<input type="text" placeholder="Eksperti shqyrtues" class="form-control" name="eksperti_shqyrtues" id="eksperti_shqyrtues" value="<?php echo $listazeze->eksperti_shqyrtues; ?>">
							</div>
							<div class="form-group">
								Eksperti Teknik
								<input type="text" placeholder="Eksperti Teknik" class="form-control" name="eksperti_teknik" id="eksperti_teknik" value="<?php echo $listazeze->eksperti_teknik; ?>">
							</div>
						</div>
					</div>
					<div class="col-md-3">
						<div class="custom-well">
							<h4>Seanca Hapur/Mbyllur</h4>
							<div class="form-group">
								<select class="form-control" id="seanca" name="seanca">
								<option value="">- Zgjedh -</option>
									<option value="Hapur" <?php if($listazeze->seanca == 'Hapur'): ?>selected<?php endif; ?>>Hapur</option>
									<option value="Mbyllur" <?php if($listazeze->seanca == 'Mbyllur'): ?>selected<?php endif; ?>>Mbyllur</option>
									<option value="N/A" <?php if($listazeze->seanca == 'N/A'): ?>selected<?php endif; ?>>N/A</option>
							  	</select>
						</div>
						</div>
					</div>
					<div class="col-md-9">
						<div class="custom-well">
							<h4>Afatet</h4>
							<div class="form-group">
								Data Kërkeses
								<input type="text" class="form-control form_date" name="data_kerkeses" id="data_kerkeses" value="<?php echo $listazeze->data_kerkeses; ?>">
							</div>
							<div class="form-group">
								Data Vendimit
								<input type="text" class="form-control form_date" name="data_vendimit" id="data_vendimit" value="<?php echo $listazeze->data_vendimit; ?>">
							</div>
							<div class="form-group">
								Data Publikimit
								<input type="text" class="form-control form_date" name="data_publikimit" id="data_publikimit" value="<?php echo $listazeze->data_publikimit; ?>">
							</div>
						</div>
					</div>
						<div class="col-md-12">
							<div class="custom-well">
								<h4>Komente</h4>
								<div class="form-group">
									Komente të përgjithshme
									<textarea class="form-control rounded-0" placeholder="Komente të përgjithshme" id="komente_gjenerale" name="komente_gjenerale" rows="3"><?php echo $listazeze->komente_gjenerale; ?></textarea>
								</div>

							</div>
						</div>
					<div class="col-md-12">
						<div id="submit" class="pull-right">
							<input type="submit" name="submit" class="btn btn-default btn-send" <?php if($listazeze->id): ?> value="Edito" <?php else: ?> value="Ruaj" <?php endif; ?>>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>  
</section>
<script type="text/javascript">
	$(document).ready(function(){
		$(".form_date").datetimepicker({
	        //format: 'MM-DD-YYYY'
	        format: 'YYYY-MM-DD'
	    });

	});
</script>