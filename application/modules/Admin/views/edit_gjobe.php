<section id="contact-section">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<form method="post" action="<?php echo base_url(); ?>admin/vendimet/edit_gjobe" accept-charset="utf-8" role="form">
					<input type="hidden" name="id" value="<?php echo $gjobe->id; ?>">
					<?php if($this->input->get('success') == 1): ?>
  						<div class="alert alert-success text-center"><strong>Të dhënat u <?php if($gjobe->id): ?>ndryshuan <?php else: ?> u insertuan <?php endif; ?> me sukses.</strong></div>
					<?php endif; ?>
					<div class="col-md-6">
						<div class="custom-well">
							<h4>Aktiviteti i prokurimit</h4>
							<div class="form-group">
								Nr. i protokollit
								<input type="text" placeholder="Nr. Protokollit" class="form-control" name="nr_protokollit" id="nr_protokollit" value="<?php echo $gjobe->nr_protokollit; ?>">
							</div>
							<div class="form-group">
								Emri i lëndës
								<input type="text" placeholder="Emri Lendes" class="form-control" name="emri_lendes" id="emri_lendes" value="<?php echo $gjobe->emri_lendes; ?>">
							</div>
							<div class="form-group">
								Vlera e parashikuar <small>(Nëse dëshironi që vlera të jetë N/A mos e plotëso këtë fushë)</small>
								<input type="text" placeholder="Vlera parashikuar" class="form-control" name="vlera_parashikuar" id="vlera_parashikuar" oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1');" value="<?php echo $gjobe->vlera_parashikuar; ?>">
							</div>
							<div class="form-group">
								Lloji i kontratës
								<input type="text" placeholder="Lloji i kontratës" class="form-control" name="lloji_kontrates" id="lloji_kontrates" value="<?php echo $gjobe->lloji_kontrates; ?>">
							</div>
							<div class="form-group">
								Operatori ekonomik
								<input type="text" placeholder="Operatori Ekonomik" class="form-control" name="operatori_ekonomik" id="operatori_ekonomik" value="<?php echo $gjobe->operatori_ekonomik; ?>">
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="custom-well">
							<h4>Vendimi</h4>
							<div class="form-group">
								Gjoba kundër AK
								<input type="text" placeholder="Gjoba kundër AK" class="form-control" name="gjoba_kunder_ak" id="gjoba_kunder_ak" value="<?php echo $gjobe->gjoba_kunder_ak; ?>">
							</div>
							<div class="form-group">
								Shuma
								<input type="text" placeholder="Shuma" class="form-control" name="shuma" id="shuma" value="<?php echo number_format($gjobe->shuma, 2); ?>">
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="custom-well">
							<h4>Vendimet</h4>
							<div class="form-group">
								Tërheqje Vërejtje
								<div class="checkbox">
	 								<select class="form-control" id="terheqje_verejtje" name="terheqje_verejtje">
										<option value="">- Zgjedh -</option>
										<option value="Po" <?php if($gjobe->terheqje_verejtje == 'Po'): ?>selected<?php endif; ?>>Po</option>
										<option value="Jo" <?php if($gjobe->terheqje_verejtje == 'Jo'): ?>selected<?php endif; ?>>Jo</option>
							  		</select>
								</div>
							</div>
							<div class="form-group">
								Kërkesë për heqje licence
								<div class="checkbox">
	 								<select class="form-control" id="terheqje_verejtje" name="terheqje_verejtje">
										<option value="">- Zgjedh -</option>
										<option value="Po" <?php if($gjobe->heqje_licence == 'Po'): ?>selected<?php endif; ?>>Po</option>
										<option value="Jo" <?php if($gjobe->heqje_licence == 'Jo'): ?>selected<?php endif; ?>>Jo</option>
							  		</select>
								</div>
							</div>
							<div class="form-group">
								Vendimi
								<input type="text" placeholder="Vendimi" class="form-control" name="titulli" value="<?php echo $gjobe->vendimi[0]->titulli; ?>">
							</div>
							<div class="form-group">
								Vendimi URL
								<input type="text" placeholder="Vendimi URL" class="form-control" name="url" value="<?php echo $gjobe->vendimi[0]->url; ?>">
							</div>
							<div class="form-group">
								Neni
								<input type="text" placeholder="Neni" class="form-control" name="neni" value="<?php echo $gjobe->neni; ?>">
							</div>
							<div class="form-group">
								Komente në lidhje me vendimin e panelit
								<textarea class="form-control rounded-0" placeholder="Komente në lidhje me vendimin e panelit" id="komente_paneli" name="komente_paneli" rows="3"><?php echo $gjobe->komente_paneli; ?></textarea>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="custom-well">
							<h4>Paneli</h4>
							<div class="form-group">
								Kryetar
								<input type="text" placeholder="Kryetari i panelit" class="form-control" name="kryetar" id="kryetar" value="<?php echo $gjobe->kryetar; ?>">
							</div>
							<div class="form-group">
								Referent
								<input type="text" placeholder="Referenti i panelit" class="form-control" name="referent" id="referent" value="<?php echo $gjobe->referent; ?>">
							</div>
							<div class="form-group">
								Anëtari
								<input type="text" placeholder="Anëtari i I-rë i panelit" class="form-control" name="anetari_1" id="anetari_1" value="<?php echo $gjobe->anetari_1; ?>">
							</div>
							<div class="form-group">
								Anëtari
								<input type="text" placeholder="Anëtari i II-të i panelit" class="form-control" name="anetari_2" id="anetari_2" value="<?php echo $gjobe->anetari_2; ?>">
							</div>
							<div class="form-group">
								Anëtari
								<input type="text" placeholder="Anëtari i III-të i panelit" class="form-control" name="anetari_3" id="anetari_3" value="<?php echo $gjobe->anetari_3; ?>">
							</div>
							<div class="form-group">
								Nr. i panelistëve
								<input type="number" placeholder="Nr. i panelistëve" class="form-control" name="nr_panelisteve" id="nr_panelisteve" value="<?php echo $gjobe->nr_panelisteve; ?>">
							</div>
						</div>
					</div>
						
						<!-- <div class="form-group">
							Aprovohet Kërkesa
							<select class="form-control" id="aprovohet_kerkesa" name="aprovohet_kerkesa">
							<option value=""></option>
								<option value="1" <?php if($gjobe->aprovohet_kerkesa == '1'): ?>selected<?php endif; ?>>Po</option>
								<option value="2" <?php if($gjobe->aprovohet_kerkesa == '2'): ?>selected<?php endif; ?>>Jo</option>
						  </select>
						</div>
						<div class="form-group">
							Refuzohet Kërkesa
							<select class="form-control" id="refuzohet_kerkesa" name="refuzohet_kerkesa">
							<option value=""></option>
								<option value="1" <?php if($gjobe->refuzohet_kerkesa == '1'): ?>selected<?php endif; ?>>Po</option>
								<option value="2" <?php if($gjobe->refuzohet_kerkesa == '2'): ?>selected<?php endif; ?>>Jo</option>
						  </select>
						</div> -->
						<!-- <div class="form-group">
							Komente për rekomandimin e ekspertit
							<textarea class="form-control rounded-0" placeholder="Komente në lidhje me vendimin e panelit" id="komente_paneli" name="komente_paneli" rows="3"><?php //echo $gjobe->komente_eksperti; ?></textarea>
						</div> -->
						<!-- <div class="form-group">
							Shqyrtues i jashtëm
							<input type="text" placeholder="Shqyrtues i jashtëm" class="form-control" name="shqyrtues_jashtem" id="shqyrtues_jashtem" value="<?php //echo $gjobe->shqyrtues_jashtem; ?>">
						</div>
						<div class="form-group">
							Shqyrtues i brendshëm
							<input type="text" placeholder="Shqyrtues i brendshëm" class="form-control" name="shqyrtues_brendshem" id="shqyrtues_brendshem" value="<?php //echo $gjobe->shqyrtues_brendshem; ?>">
						</div>
						<div class="form-group">
							Teknik
							<input type="text" placeholder="Teknik" class="form-control" name="teknik" id="teknik" value="<?php //echo $gjobe->teknik; ?>">
						</div> -->
						<div class="col-md-6">
							<div class="custom-well">
								<h4>Seanca Hapur/Mbyllur</h4>
								<div class="form-group">
									<select class="form-control" id="seanca" name="seanca">
									<option value="">- Zgjedh -</option>
										<option value="Hapur" <?php if($gjobe->seanca == 'Hapur'): ?>selected<?php endif; ?>>Hapur</option>
										<option value="Mbyllur" <?php if($gjobe->seanca == 'Mbyllur'): ?>selected<?php endif; ?>>Mbyllur</option>
										<option value="N/A" <?php if($gjobe->seanca == 'N/A'): ?>selected<?php endif; ?>>N/A</option>
								  </select>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="custom-well">
								<h4>Afatet</h4>
								<div class="form-group">
								Data Kërkeses
								<input type="text" class="form-control form_date" name="data_kerkeses" id="data_kerkeses" value="<?php echo $gjobe->data_kerkeses; ?>">
								</div>
								<div class="form-group">
									Data Vendimit
									<input type="text" class="form-control form_date" name="data_vendimit" id="data_vendimit" value="<?php echo $gjobe->data_vendimit; ?>">
								</div>
								<div class="form-group">
									Data Publikimit
									<input type="text" class="form-control form_date" name="data_publikimit" id="data_publikimit" value="<?php echo $gjobe->data_publikimit; ?>">
								</div>
								<div class="form-group">
									Komente të përgjithshme
									<textarea class="form-control rounded-0" placeholder="Komente të përgjithshme" id="komente_gjenerale" name="komente_gjenerale" rows="3"><?php echo $gjobe->komente_gjenerale; ?></textarea>
								</div>
							</div>
						</div>
					<div class="col-md-12">
						<div id="submit" class="pull-right">
							<input type="submit" name="submit" class="btn btn-default btn-send" <?php if($gjobe->id): ?> value="Edito" <?php else: ?> value="Ruaj" <?php endif; ?>>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>  
</section>
<script type="text/javascript">
	$(document).ready(function(){
		$(".form_date").datetimepicker({
	        //format: 'MM-DD-YYYY'
	        format: 'YYYY-MM-DD'
	    });

	});
</script>