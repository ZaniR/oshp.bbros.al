<section id="contact-section">
	<div class="container">
		<?php if($this->input->get('success') == 1): ?>
			<div class="row">
				<div class="col-md-12">
					<div class="alert alert-success text-center"><strong>Të dhënat u <?php if($vendimi->id): ?>ndryshuan <?php else: ?> u insertuan <?php endif; ?> me sukses.</strong>
					</div>
				</div>
			</div>
		<?php endif; ?>		
		<div class="row">
			<form method="post" action="<?php echo base_url(); ?>admin/vendimet/edit_vendimi" accept-charset="utf-8" role="form" enctype='multipart/form-data'>
			<input type="hidden" name="id" value="<?php echo $vendimi->id; ?>">
				<div class="col-md-6">
					<div class="custom-well">
						<h4>Aktiviteti i prokurimit</h4>
						<div class="form-group">
							<label>Nr. protokollit</label>
							<input type="text" placeholder="Nr. Protokollit" class="form-control" name="nr_protokollit" id="nr_protokollit" value="<?php echo $vendimi->nr_protokollit; ?>">
						</div>
						<div class="form-group">
							<label>Emri i lëndës</label>
							<input type="text" placeholder="Emri Lëndës" class="form-control" name="emri_lendes" id="emri_lendes" value="<?php echo $vendimi->emri_lendes; ?>">
						</div>
						<div class="form-group">
							<label>Lloji i kontratës</label>
							<input type="text" placeholder="Lloji i kontratës" class="form-control" name="lloji_kontrates" id="lloji_kontrates" value="<?php echo $vendimi->lloji_kontrates; ?>">
						</div>
						<div class="form-group">
							<label>Vlera e parashikuar <small>(Nëse dëshironi që vlera të jetë N/A mos e plotëso këtë fushë)</small></label>
							<input type="text" placeholder="Vlera parashikuar" class="form-control" name="vlera_parashikuar" id="vlera_parashikuar" oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1');" value="<?php echo $vendimi->vlera_parashikuar; ?>">
						</div>
						<div class="form-group">
							<label>Autoriteti kontraktues</label>
							<input type="text" placeholder="Autoriteti kontraktues" class="form-control" name="autoriteti_kontraktues" id="autoriteti_kontraktues" value="<?php echo $vendimi->autoriteti_kontraktues; ?>">
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="custom-well">
						<h4>Ankesa</h4>
						<div class="form-group">
							<label>OE Ankues</label>
							<input type="text" placeholder="OE Ankues" class="form-control" name="oe_ankues" id="oe_ankues" value="<?php echo $vendimi->oe_ankues; ?>">
						</div>
						<div class="form-group">
							<label>Ankesa kundër</label>
							<input type="text" placeholder="Ankesa kundër" class="form-control" name="ankesa_kunder" id="ankesa_kunder" value="<?php echo $vendimi->ankesa_kunder; ?>">
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="custom-well">
						<h4>Vendimet Paneli</h4>
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label>Vendimi <small>(nëse nuk e gjeni opsionin - <input type="checkbox" class="show_input" style="display:inline-flex; height:12px;" /> Shto të ri)</small></label>
									<div id="vendimi_container">
									<select name="vendimi" class="form-control" placeholder="Vendimi">
										<option value="">- Zgjedh -</option>
										<?php foreach ($vendimet as $ven): ?>s
											<option value="<?php echo $ven; ?>" <?php if($vendimi->vendimi == $ven): ?>selected="selected"<?php endif; ?>><?php echo $ven; ?></option>
										<?php endforeach; ?>
									</select>
									</div>
									
								</div>
								<div class="form-group">
									<label>Tërheqje e vërejtjes</label>
									<select class="form-control" id="terheqje_verejtje" name="terheqje_verejtje">
										<option value="">- Zgjedh -</option>
										<option value="Po" <?php if($vendimi->terheqje_verejtje == 'Po'): ?>selected<?php endif; ?>>Po</option>
										<option value="Jo" <?php if($vendimi->terheqje_verejtje == 'Jo'): ?>selected<?php endif; ?>>Jo</option>
									</select>
								</div>
								<div class="form-group">
									<label>Neni</label>
									<input type="text" placeholder="Neni" class="form-control" name="neni" id="neni" value="<?php echo $vendimi->neni; ?>">
								</div>
								<div class="form-group">
									<label>Komente në lidhje me vendimin e panelit</label>
									<textarea class="form-control rounded-0" placeholder="Komente në lidhje me vendimin e panelit" id="komente_paneli" name="komente_paneli" rows="3"><?php echo $vendimi->komente_paneli; ?></textarea>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label>Numri i vendimit</label>
									<input type="text" placeholder="Vendimi" class="form-control" name="titulli" value="<?php echo $vendimi->nr_vendimit[0]->titulli; ?>">
								</div>
								<div class="form-group">
									<label>Numri i vendimit URL</label>
									<input type="text" placeholder="Vendimi URL" class="form-control" name="url" value="<?php echo $vendimi->nr_vendimit[0]->url; ?>">
								</div>
								<div class="form-group">
									<label>Vendimi i mëparshëm</label>
									<input type="text" placeholder="Vendimi" class="form-control" name="titulli_meparshem" value="<?php echo $vendimi->vendimet_meparshme[0]->titulli; ?>">
								</div>
								<div class="form-group">
									<label>Vendimi i mëparshëm URL</label>
									<input type="text" placeholder="Vendimi URL" class="form-control" name="url_meparshem" value="<?php echo $vendimi->vendimet_meparshme[0]->url; ?>">
								</div>
								<div class="form-group">
									<label>Vendimi jo konsistent me vendimin</label>
									<input type="text" placeholder="Vendimi" class="form-control" name="titulli_konsistent" value="<?php echo $vendimi->vendimi_jokonsistent[0]->titulli; ?>">
								</div>
								<div class="form-group">
									<label>Vendimi jo konsistent me vendimin URL</label>
									<input type="text" placeholder="Vendimi URL" class="form-control" name="url_konsistent" value="<?php echo $vendimi->vendimi_jokonsistent[0]->url; ?>">
								</div>
								<div class="form-group">
									<label>Konfiskim i tarifës Po/Jo</label>
									<select class="form-control" id="konfiskim_tarifes" name="konfiskim_tarifes">
										<option value="">- Zgjedh -</option>
										<option value="Po" <?php if($vendimi->konfiskim_tarifes == 'Po'): ?>selected<?php endif; ?>>Po</option>
										<option value="Jo" <?php if($vendimi->konfiskim_tarifes == 'Jo'): ?>selected<?php endif; ?>>Jo</option>
									</select>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="custom-well">
						<h4>Paneli</h4>
						<div class="form-group">
							<label>Kryetar</label>
							<input type="text" placeholder="Kryetar" class="form-control" name="kryetar" id="kryetar" value="<?php echo $vendimi->kryetar; ?>">
						</div>
						<div class="form-group">
							<label>Referent</label>
							<input type="text" placeholder="Referent" class="form-control" name="referent" id="referent" value="<?php echo $vendimi->referent; ?>">
						</div>
						<div class="form-group">
							<label>Anëtari I</label>
							<input type="text" placeholder="Anëtari I" class="form-control" name="anetari_1" id="anetari_1" value="<?php echo $vendimi->anetari_1; ?>">
						</div>
						<div class="form-group">
							<label>Anëtari II</label>
							<input type="text" placeholder="Anëtari II" class="form-control" name="anetari_2" id="anetari_2" value="<?php echo $vendimi->anetari_2; ?>">
						</div>
						<div class="form-group">
							<label>Anëtari III</label>
							<input type="text" placeholder="Anëtari III" class="form-control" name="anetari_3" id="anetari_3" value="<?php echo $vendimi->anetari_3; ?>">
						</div>
						<div class="form-group">
							<label>Numri i panelistëve</label>
							<input type="text" placeholder="Numri i panelistëve" class="form-control" name="nr_panelisteve" id="nr_panelisteve" value="<?php echo $vendimi->nr_panelisteve; ?>">
						</div>
					</div>
				</div>
				<div class="col-md-12">	
					<div class="custom-well">
						<h4>REKOMANDIMI I EKSPERTIT</h4>
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label>Rekomandimi i ekspertit <small>(nëse nuk e gjeni opsionin - <input type="checkbox" class="show_input_ekserti" style="display:inline-flex; height:12px;" /> Shto të ri)</small></label>
									<div id="eksperti_container">
									<select name="rekomandimi_ekspertit" class="form-control" placeholder="Rekomandimi i ekspertit">
										<option value="">- Zgjedh -</option>
										<?php foreach ($rekomandimet_ekspertit as $re): ?>s
											<option value="<?php echo $re; ?>" <?php if($vendimi->rekomandimi_ekspertit == $re): ?>selected="selected"<?php endif; ?>><?php echo $re; ?></option>
										<?php endforeach; ?>
									</select>
									</div>
									
								</div>
								<div class="form-group">
									<label>Komente në lidhje me rekomandimin e ekspertit</label>
									<textarea class="form-control rounded-0" placeholder="Komente në lidhje me vendimin e panelit" id="komente_eksperti" name="komente_eksperti" rows="3"><?php echo $vendimi->komente_eksperti; ?></textarea>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="custom-well">
						<h4>Eksperti</h4>
						<div class="form-group">
							<label>Eksperti shqyrtues</label>
							<input type="text" placeholder="Eksperti shqyrtues" class="form-control" name="eksperti_shqyrtues" id="eksperti_shqyrtues" value="<?php echo $vendimi->eksperti_shqyrtues; ?>">
						</div>
						<div class="form-group">
							<label>Eksperti teknik</label>
							<input type="text" placeholder="Eksperti teknik" class="form-control" name="eksperti_teknik" id="eksperti_teknik" value="<?php echo $vendimi->eksperti_teknik; ?>">
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="custom-well">
						<h4>Seanca Hapur/Mbyllur</h4>
						<div class="form-group">
							<label>Seanca Hapur/Mbyllur</label>
							<select class="form-control" id="seanca" name="seanca">
							<option value="">- Zgjedh -</option>
								<option value="Mbyllur" <?php if($vendimi->seanca == 'Mbyllur'): ?>selected<?php endif; ?>>E Mbyllur</option>
								<option value="Hapur" <?php if($vendimi->seanca == 'Hapur'): ?>selected<?php endif; ?>>E Hapur</option>
								<option value="N/A" <?php if($vendimi->seanca == 'N/A'): ?>selected<?php endif; ?>>N/A</option>
						  </select>
						</div>
						<div class="form-group">
							<label>Fjalët kyçe</label>
							<select class="form-control chosen-select" id="tags" name="tags[]" data-placeholder="Zgjedh fjalët kyçe" multiple>
							<?php foreach($tags as $tag): ?>
								<?php $selected = FALSE; ?>
								<?php foreach($vendimi->tags as $vendimi_tag): ?>
									<?php 
										if($vendimi_tag->id == $tag->id) {
											$selected = TRUE;
											break;
										}
									?>
								<?php endforeach; ?>
								<option value="<?php echo $tag->id; ?>" <?php if($selected): ?>selected="selected"<?php endif; ?>><?php echo $tag->emri; ?></option>
							<?php endforeach; ?>
						  </select>
						</div>
						<div class="form-group">
							<label>Komente të përgjithshme</label>
							<textarea class="form-control rounded-0" placeholder="Komente të përgjithshme" id="komente_gjenerale" name="komente_gjenerale" rows="3"><?php echo $vendimi->komente_gjenerale; ?></textarea>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="custom-well">
						<h4>Afatet</h4>
						<div class="form-group">
							<label>Data Kërkesës</label>
							<input type="text" class="form-control form_date" name="data_ankeses" id="data_ankeses" value="<?php echo $vendimi->data_ankeses; ?>">
						</div>
						<div class="form-group">
							<label>Data Vendimit</label>
							<input type="text" class="form-control form_date" name="data_vendimit" id="data_vendimit" value="<?php echo $vendimi->data_vendimit; ?>">
						</div>
						<div class="form-group">
							<label>Data Publikimit</label>
							<input type="text" class="form-control form_date" name="data_publikimit" id="data_publikimit" value="<?php echo $vendimi->data_publikimit; ?>">
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div id="submit" class="pull-right">
						<input type="submit" name="submit" class="btn btn-default btn-send" <?php if($vendimi->id): ?> value="Edito" <?php else: ?> value="Ruaj" <?php endif; ?>>
					</div>
				</div>
			</form>
		</div>
	</div>  
</section>
<script type="text/javascript">
	$(document).ready(function(){
		$(".form_date").datetimepicker({
			//format: 'MM-DD-YYYY'
			format: 'YYYY-MM-DD'
		});

		$(".show_input").on('change', function(){
			$("#vendimi_container").html('<input type="text" placeholder="Vendimi" class="form-control" name="vendimi">');
		});
		$(".show_input_ekserti").on('change', function(){
			$("#eksperti_container").html('<input type="text" placeholder="Rekomandimi i ekspertit" class="form-control" name="rekomandimi_ekspertit" id="rekomandimi_ekspertit">');
		});

	});
</script>