<section id="general-section">
    <div class="container">
    <?php if($this->input->get('success') == 3): ?>
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-success text-center"><strong>Your data has been deleted successfully.</strong></div>
        </div>
    </div>
    <?php endif; ?>        
    <div class="row">
        <div class="col-md-12">
            <div class="pull-right">
                <a href="<?php echo base_url(); ?>admin/navigation/edit" class="btn btn-primary"><i class="fa fa-plus"></i> Shto Navigim</a>
            </div>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-md-12">
            <div class="table-responsive">
        	<table id="navigations_table" class="display" style="width:100%">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>URL</th>
                        <th>Parent</th>
                        <th>Active</th>
                        <th>Hidden</th>
                        <th>Authorization</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>Name</th>
                        <th>URL</th>
                        <th>Parent</th>
                        <th>Active</th>
                        <th>Hidden</th>
                        <th>Authorization</th>
                        <th>Actions</th>
                    </tr>
                </tfoot>
        	</table>
            </div>
    	</div>
    </div>
</section>
<script>
$(document).ready(function() {
	var site_url = '<?php echo base_url(); ?>';
	var navigation_table = $('#navigations_table').DataTable({
		dom: 'Bfrtip',
        buttons: [
        	'pageLength',
            {
                extend: 'excel',
                exportOptions: {
                    columns: ':visible'             
                },
			                
            },
            {
                extend: 'pdfHtml5',
                orientation: 'landscape',
            	pageSize: 'LEGAL',
                exportOptions: {
                    columns: ':visible'             
                },
			                
            },
            {
                extend: 'print',
                exportOptions: {
                    columns: ':visible'             
                },
			                
            },
            {
                extend: 'csvHtml5',
                exportOptions: {
                    columns: ':visible'             
                },
			                
            },                
            'colvis'
        ],
		"language": {
		        "processing": "Hang on. Loading Data..." //add a loading image,simply putting <img src="loader.gif" /> tag.
		    },			
		"order": [[ 0, "asc" ]],
        "autoWidth": false,
        "lengthMenu": [ [10, 25, 50, -1], [10, 25, 50, "All"] ],
        "processing": true,
        "serverSide": true,			
		"ajax":{
			url :site_url + 'admin/navigation/index/json', // json datasource
			type: "post",  // method  , by default get
			error: function(){  // error handling
				$(".navigation_error").html("");
				$("#example").append('<tbody class="navigation_error"><tr><th colspan="10">No data found in the server</th></tr></tbody>');
				//$("#employee-grid_processing").css("display","none");					
			}
		}

	} );

	$('.search-input-text').on('keyup click', function () {   // for text boxes
	    var i =$(this).attr('data-column');  // getting column index
	    var v =$(this).val();  // getting search input value
	    navigation_table.columns(i).search(v).draw();
	} );
});
</script>