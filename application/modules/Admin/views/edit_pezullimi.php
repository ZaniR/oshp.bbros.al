<section id="contact-section">
	<div class="container">
		<?php if($this->input->get('success') == 1): ?>
			<div class="row">
				<div class="col-md-12">
					<div class="alert alert-success text-center"><strong>Të dhënat u <?php if($pezullimi->id): ?>ndryshuan <?php else: ?> u insertuan <?php endif; ?> me sukses.</strong>
					</div>
				</div>
			</div>
		<?php endif; ?>		
		<div class="row">
			<form method="post" action="<?php echo base_url(); ?>admin/vendimet/edit_pezullimi" accept-charset="utf-8" role="form">
			<input type="hidden" name="id" value="<?php echo $pezullimi->id; ?>">
				<div class="col-md-6">
					<div class="custom-well">
						<h4>Aktiviteti i prokurimit</h4>
						<div class="form-group">
							<label>Nr. protokollit</label>
							<input type="text" placeholder="Nr. Protokollit" class="form-control" name="nr_protokollit" id="nr_protokollit" value="<?php echo $pezullimi->nr_protokollit; ?>">
						</div>
						<div class="form-group">
							<label>Emri i lëndës</label>
							<input type="text" placeholder="Emri Lendes" class="form-control" name="emri_lendes" id="emri_lendes" value="<?php echo $pezullimi->emri_lendes; ?>">
						</div>
						<div class="form-group">
							<label>Vlera e parashikuar <small>(Nëse dëshironi që vlera të jetë N/A mos e plotëso këtë fushë)</small></label>
							<input type="text" placeholder="Vlera parashikuar" class="form-control" name="vlera_parashikuar" id="vlera_parashikuar" oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1');" value="<?php echo $pezullimi->vlera_parashikuar; ?>">
						</div>
						<div class="form-group">
							<label> Lloji i kontratës </label>
							<input type="text" placeholder=" Lloji i kontratës " class="form-control" name="lloji_kontrates" id="lloji_kontrates" value="<?php echo $pezullimi->lloji_kontrates; ?>">
						</div>
						<div class="form-group">
							<label>Autoriteti kontraktues</label>
							<input type="text" placeholder="Autoriteti kontraktues" class="form-control" name="autoriteti_kontraktuar" id="autoriteti_kontraktuar" value="<?php echo $pezullimi->autoriteti_kontraktuar; ?>">
						</div>
						<div class="form-group">
							<label>OE i rekomanduar</label>
							<input type="text" placeholder="OE i rekomanduar" class="form-control" name="oe_rekomanduar" id="oe_rekomanduar" value="<?php echo $pezullimi->oe_rekomanduar; ?>">
						</div>
					</div>
					<div class="custom-well">
						<h4>Vendimi</h4>
						<div class="form-group">
							<label>Heqje pezullimi</label>
							<select class="form-control" id="heqje_pezullimi" name="heqje_pezullimi">
								<option value="">- Zgjedh -</option>
								<option value="Refuzohet" <?php if($pezullimi->heqje_pezullimi == 'Refuzohet'): ?>selected<?php endif; ?>>Refuzohet</option>
								<option value="Aprovohet" <?php if($pezullimi->heqje_pezullimi == 'Aprovohet'): ?>selected<?php endif; ?>>Aprovohet</option>
						  	</select>
						</div>
						<div class="form-group">
							<label>Vendimi</label>
							<input type="text" placeholder="Vendimi" class="form-control" name="titulli" value="<?php echo $pezullimi->vendimi[0]->titulli; ?>">
						</div>
						<div class="form-group">
							<label>Vendimi URL</label>
							<input type="text" placeholder="Vendimi URL" class="form-control" name="url" value="<?php echo $pezullimi->vendimi[0]->url; ?>">
						</div>
						<div class="form-group">
							<label>Neni</label>
							<input type="text" placeholder="Neni" class="form-control" name="neni" value="<?php echo $pezullimi->neni; ?>">
						</div>
						<div class="form-group">
							<label>Komente në lidhje me vendimin e panelit</label>
							<textarea class="form-control rounded-0" placeholder="Komente në lidhje me vendimin e panelit" id="komente" name="komente" rows="3"><?php echo $pezullimi->komente; ?></textarea>
						</div>
					</div>
					<div class="custom-well">
						<h4>Paneli</h4>
						<div class="form-group">
							<label>Kryetari i panelit</label>
							<input type="text" placeholder="Kryetari i panelit" class="form-control" name="kryetar" id="kryetar" value="<?php echo $pezullimi->kryetar; ?>">
						</div>
						<div class="form-group">
							<label>Referenti i panelit</label>
							<input type="text" placeholder="Referenti i panelit" class="form-control" name="referent" id="referent" value="<?php echo $pezullimi->referent; ?>">
						</div>
						<div class="form-group">
							<label>Anëtari I</label>
							<input type="text" placeholder="Anëtari i I-re i panelit" class="form-control" name="anetari_1" id="anetari_1" value="<?php echo $pezullimi->anetari_1; ?>">
						</div>
						<div class="form-group">
							<label>Anëtari II</label>
							<input type="text" placeholder="Anëtari i II-te i panelit" class="form-control" name="anetari_2" id="anetari_2" value="<?php echo $pezullimi->anetari_2; ?>">
						</div>
						<div class="form-group">
							<label>Anëtari III</label>
							<input type="text" placeholder="Anëtari i III-te i panelit" class="form-control" name="anetari_3" id="anetari_3" value="<?php echo $pezullimi->anetari_3; ?>">
						</div>
						<div class="form-group">
							<label>Nr. i panelistëve</label>
							<input type="number" placeholder="Nr. i panelistëve" class="form-control" name="nr_panelisteve" id="nr_panelisteve" value="<?php echo $pezullimi->nr_panelisteve; ?>">
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="custom-well">
						<h4>Seanca Hapur/Mbyllur</h4>
						<div class="form-group">
							<label>Seanca Hapur/Mbyllur</label>
							<select class="form-control" id="seanca" name="seanca">
							<option value="">- Zgjedh -</option>
								<option value="Mbyllur" <?php if($pezullimi->seanca == 'Mbyllur'): ?>selected<?php endif; ?>>E Mbyllur</option>
								<option value="Hapur" <?php if($pezullimi->seanca == 'Hapur'): ?>selected<?php endif; ?>>E Hapur</option>
								<option value="N/A" <?php if($pezullimi->seanca == 'N/A'): ?>selected<?php endif; ?>>N/A</option>
						  </select>
						</div>
					</div>
					<div class="custom-well">
						<h4>Afatet</h4>
						<div class="form-group">
							<label>Data Kërkesës</label>
							<input type="text" class="form-control form_date" name="data_kerkeses" id="data_kerkeses" value="<?php echo $pezullimi->data_kerkeses; ?>">
						</div>
						<div class="form-group">
							<label>Data Vendimit</label>
							<input type="text" class="form-control form_date" name="data_vendimit" id="data_vendimit" value="<?php echo $pezullimi->data_vendimit; ?>">
						</div>
						<div class="form-group">
							<label>Data Publikimit</label>
							<input type="text" class="form-control form_date" name="data_publikimit" id="data_publikimit" value="<?php echo $pezullimi->data_publikimit; ?>">
						</div>
					</div>
					<div class="custom-well">
						<h4>Komente të përgjithshme</h4>
						<div class="form-group">
							<textarea class="form-control rounded-0" placeholder="Komente të përgjithshme" id="komente_gjenerale" name="komente_gjenerale" rows="3"><?php echo $pezullimi->komente_gjenerale; ?></textarea>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div id="submit" class="pull-right">
						<input type="submit" name="submit" class="btn btn-default btn-send" <?php if($pezullimi->id): ?> value="Edito" <?php else: ?> value="Ruaj" <?php endif; ?>>
					</div>
				</div>
			</form>
		</div>
	</div>  
</section>
<script type="text/javascript">
	$(document).ready(function(){
		$(".form_date").datetimepicker({
	        //format: 'MM-DD-YYYY'
	        format: 'YYYY-MM-DD'
	    });

	});
</script>