<section id="contact-section">
	<div class="container">
		<?php if($this->input->get('success') == 1): ?>
			<div class="row">
				<div class="col-md-12">
					<div class="alert alert-success text-center"><strong>Të dhënat u <?php if($navigation->id): ?>ndryshuan <?php else: ?> u insertuan <?php endif; ?> me sukses.</strong>
					</div>
				</div>
			</div>
		<?php endif; ?>			
		<div class="row">
			<form method="post" action="<?php echo base_url(); ?>admin/navigation/edit" accept-charset="utf-8" role="form">
				<input type="hidden" name="id" value="<?php echo $navigation->id; ?>" />
				<div class="col-md-5">
					<div class="custom-well">
						<h4>Details</h4>
						<div class="form-group">
							<label>Name</label>
							<input type="text" placeholder="Name" class="form-control" name="name" id="name" value="<?php echo $navigation->name; ?>">
						</div>
						<div class="form-group">
							<label>Description</label>
							<textarea class="form-control rounded-0" placeholder="Description" id="description" name="description" rows="3"><?php echo $navigation->description; ?></textarea>
						</div>
						<div class="form-group">
							<label>URL</label>
							<input type="text" placeholder="URL" class="form-control" name="url" id="url" value="<?php echo $navigation->url; ?>">
						</div>
						<div class="form-group">
							<label>Parent</label>
							<select class="form-control chosen-select" id="parent" name="parent" data-placeholder="Select Parent">
							<option value="">- Choose Parent -</option>
							<?php foreach($parent_navigations as $parent_navigation): ?>
								<option value="<?php echo $parent_navigation->id; ?>" <?php if(isset($navigation->parent->id) && ($navigation->parent->id == $parent_navigation->id)): ?>selected="selected"<?php endif; ?>><?php echo $parent_navigation->name; ?> - (<?php echo $parent_navigation->url ?>)</option>
							<?php endforeach; ?>
						  </select>
						</div>						
					</div>
				</div>
				<div class="col-md-3">
					<div class="custom-well">
						<h4>Visibility</h4>
						<div class="form-group">
					        <table class="table table-striped">
					        	<tr>
					        		<td>Active<input type="checkbox" name="active" value="1" <?php if($navigation->active): ?>checked="checked"<?php endif; ?>></td><td>Hidden<input type="checkbox" name="hidden" value="1"  <?php if($navigation->hidden): ?>checked="checked"<?php endif; ?>></td>
					        	</tr>
					        </table>
						</div>
					</div>
					<div class="custom-well">
						<h4>Authorization</h4>
						<div class="form-group">
					        <table class="table table-striped">
					        	<tr>
					        		<td>Authorized<input type="checkbox" name="authorization" value="1" <?php if($navigation->authorization): ?>checked="checked"<?php endif; ?>></td>
					        	</tr>
					        </table>
						</div>						
						<div class="form-group">
							<label>Groups</label>
							<select class="form-control chosen-select" id="groups" name="groups[]" multiple data-placeholder="Select Groups">
								<?php foreach($user_groups as $user_group): ?>
									<?php $exists = FALSE; ?>
									<?php foreach($navigation->groups as $navigation_group): ?>
										<?php 
											if($navigation_group->group_id == $user_group->id){
												$exists = TRUE;
												break;
											}
										?>
									<?php endforeach; ?>
									<option value="<?php echo $user_group->id; ?>" <?php if($exists): ?>selected="selected"<?php endif; ?>><?php echo $user_group->name; ?></option>
								<?php endforeach; ?>
							</select>
						</div>						
					</div>
				</div>
				<div class="col-md-4">
					<div class="custom-well">
						<h4>Meta Information</h4>					
						<div class="form-group">
							<label>Title</label>
							<input type="text" placeholder="Page Title" class="form-control" name="page_title" id="page_title"  value="<?php echo $navigation->page_title; ?>">
						</div>
						<div class="form-group">
							<label>Keyword</label>
							<input type="text" placeholder="Page Keyword" class="form-control" name="page_keyword" id="page_keyword"  value="<?php echo $navigation->page_keyword; ?>">
						</div>
					</div>
					<div class="custom-well">
						<h4>Order Position</h4>					
						<div class="form-group">
							<label>Index</label>
							<input type="number" min="0" placeholder="Index" class="form-control" name="index" id="index" value="<?php echo $navigation->index; ?>">
						</div>
					</div>					
				</div>
				<div class="col-md-12">
					<div id="submit" class="pull-right">
						<input type="submit" name="submit" class="btn btn-default btn-send" value="Save">
					</div>
				</div>
			</form>
		</div>
	</div>  
</section>