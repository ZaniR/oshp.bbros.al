<section id="contact-section">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
		<?php if($this->input->get('success') == 1): ?>
			<div class="alert alert-success text-center"><strong>Të dhënat u insertuan me sukses.</strong></div>
		<?php elseif($this->input->get('error') == 1): ?>
			<div class="alert alert-danger text-center"><strong>Të dhënat nuk u insertuan! Diçka shkoj gabim.</strong></div>	
		<?php endif; ?>	
			</div>
		</div>		
		<div class="row">
			<div class="col-md-8">
				<form method="post" action="<?php echo base_url(); ?>admin/upload" accept-charset="utf-8" role="form" enctype="multipart/form-data">
					<div class="custom-well">
						<div class="form-group">
							<h4>Vendimet</h4>
							<span>Përzgjedh llojet e vendimeve qe do të afektohen nga ky ndryshim</span>
							<table class="row-border compact" style="width:100%;">
								<tr>
									<td style="width:25px;"><input type="checkbox" name="types[]" value="Vendimet" /></td><td><strong>Vendimet</strong></td>
									<td style="width:25px;"><input type="checkbox" name="types[]" value="Liste e Zeze" /></td><td><strong>Lista e zezë</strong></td>
									<td style="width:25px;"><input type="checkbox" name="types[]" value="Gjobe" /></td><td><strong>Gjobë</strong></td>
									<td style="width:25px;"><input type="checkbox" name="types[]" value="Heqje pezullimi" /></td><td><strong>Heqje pezullimi</strong></td>
								</tr>
							</table>
						</div>
						<div class="form-group">
							<h4>Dokumenti</h4>
							<label>(formati: xlsx)</label>
							<input type="file" placeholder="Excel" class="form-control" name="file" id="file">
						</div>							
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<h4>Mbishkruaj</h4>
									<table class="row-border compact" style="width:100%;">
										<tr>
											<td style="width:25px;"><input type="checkbox" name="overwrite" value="1" /></td><td>Fshij të gjitha të vjetrat dhe mbishkruaj me të reja</td>
										</tr>
									</table>
								</div>															
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<h4>Rikuperim</h4>
									<table class="row-border compact" style="width:100%;">
										<tr>
											<td style="width:25px;"><input type="checkbox" name="stable" value="1" /></td><td>Ruaj version stabil të tabelave aktuale për rikuperim</td>
										</tr>
									</table>
								</div>							
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">					
								<button type="submit" name="submit" class="btn btn-default pull-right"><i class="fa fa-upload"></i> Ngarko</button>
							</div>
						</div>
					</div>
				</form>
			</div>

			<div class="col-md-4">
				<div class="custom-well">
				<h4>Mostra e Excel File për ngarkim</h4>
				<h5>Shkarko dhe plotëso të dhënat</h5>
				<a href="<?php echo base_url(); ?>modules/Backups/files/excel/sample.xlsx" target="_blank" download><i class="fa fa-file-excel-o fa-2x"></i></a>
				</div>
				<?php if(isset($recovery->id)): ?>
				<div class="custom-well">	
            	<h4>Rikthim i gjendjes paraprake</h4>
                <h5>Rikuperimi i fundit: <strong><?php echo $recovery->created_on; ?></strong></h5>
           		<p><a href="<?php echo base_url(); ?>admin/upload/recovery/<?php echo $recovery->id; ?>" class="btn btn-default"><i class="fa fa-history"></i> Rikupero</a></p>
           		</div>
           		<?php endif; ?>			
			</div>          				
		</div>
	</div>  
</section>