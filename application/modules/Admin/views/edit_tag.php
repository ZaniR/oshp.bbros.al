<section id="contact-section">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<form method="post" action="<?php echo base_url(); ?>admin/tags/edit_tag" accept-charset="utf-8" role="form">
					<input type="hidden" name="id" value="<?php echo $tag->id; ?>">
					<?php if($this->input->get('success') == 1): ?>
  						<div class="alert alert-success text-center"><strong>Të dhënat u <?php if($tag->id): ?>ndryshuan <?php else: ?> u insertuan <?php endif; ?> me sukses.</strong></div>
					<?php endif; ?>
					<div class="col-md-12">
						Emri i tag
						<div class="form-group">
							<input type="text" placeholder="Emri i tag" class="form-control" name="emri" id="emri" value="<?php echo $tag->emri; ?>">
						</div>
					</div>
					<div class="col-md-12">
						<div id="submit" class="pull-right">
							<input type="submit" name="submit" class="btn btn-default btn-send" <?php if($tag->id): ?> value="Edito" <?php else: ?> value="Ruaj" <?php endif; ?>>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>  
</section>