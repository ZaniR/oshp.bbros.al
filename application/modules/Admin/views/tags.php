<style type="text/css">
    td.details-control {
        background: url('<?php echo base_url(); ?>assets/images/details_open.png') no-repeat center center;
        cursor: pointer;
    }
    tr.shown td.details-control {
        background: url('<?php echo base_url(); ?>assets/images/details_close.png') no-repeat center center;
    }
    table.extra-details {
        table-layout:fixed; 
        width:100%;
    }
    div.dt-button-collection.four-column {
        width:90% !important;
    }
    div.dt-button-collection.fixed {
        left:5%;
        margin:0;
        padding:4px;
    }
    div.dt-button-collection.fixed.four-column {
        margin:0;
    }
</style>
<section id="general-section">
<?php if($this->input->get('success') == 3): ?>
  <div class="alert alert-success text-center"><strong>Të dhënat janë fshirë me sukses.</strong></div>
<?php endif; ?>    
    <div class="container">
        <div class="tags-section">
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="pull-left">
                                <a href="<?php echo base_url(); ?>admin/tags/edit_tag" class="btn btn-default" target="_blank"><i class="fa fa-plus"></i> Shto tag</a>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="pull-right">
                                <h5>Përditësimi i fundit: <strong><?php echo $last_updated; ?></strong></h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>        
        <div class="row">
            <div class="col-md-12">
            	<table id="tags_table" class="row-border compact" style="width:100%">
                    <thead>
                        <tr>
                            <th>Emri</th>
                            <th class="not-export-col" style="text-align: right;">Actions</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>Emri</th>
                            <th class="not-export-col" style="text-align: right;">Actions</th>
                        </tr>
                    </tfoot>
            	</table>
        	</div>
        </div>
    </div>
</section>
<script>
var site_url = '<?php echo base_url(); ?>';
$(document).ready(function() {
	var tags_table = $('#tags_table').DataTable({
		dom: 'Bfrtip',
        buttons: [
        	'pageLength',
            {
                extend: 'excel',
			                
            },
            {
                extend: 'pdfHtml5',
                orientation: 'landscape',
            	pageSize: 'LEGAL'
			                
            },
            {
                extend: 'print'
			                
            },
            {
                extend: 'csvHtml5'
			                
            }
        ],
		"language": {
		        "processing": "Hang on. Loading Data...", //add a loading image,simply putting <img src="loader.gif" /> tag.
                buttons: {
                    pageLength: {
                    _: "Shfaq %d rreshta",
                    '-1': "Të gjithë rreshtat"
                    }
                },
                "info": "Shfaqur _START_ deri _END_ prej _TOTAL_ të dhënave",
                "paginate": {
                            "previous": "Faqja paraprake",
                            "next": "Faqja tjetër",
                            "first": "Faqja parë",
                            "last": "Faqja fundit"
                        },
                "search": "Kërko"
		    },			
        "autoWidth": false,
        "lengthMenu": [ [15, 25, 50, -1], [15, 25, 50, "Të gjitha"] ],
        "processing": true,
        "serverSide": true,			
		"ajax":{
			url :site_url + 'admin/tags/index/json', // json datasource
			type: "post",  // method  , by default get         
			error: function(){  // error handling
				$(".tags_table_error").html("");
				$("#example").append('<tbody class="tags_table_error"><tr><th colspan="10">No data found in the server</th></tr></tbody>');
				//$("#employee-grid_processing").css("display","none");					
			}
		},
        initComplete: function () {
            $('.buttons-print').html('<span class="fa fa-print" data-toggle="tooltip" title="Printo"/>');
            $('.buttons-excel').html('<span class="fa fa-file-excel-o" data-toggle="tooltip" title="Excel"/>');
            $('.buttons-csv').html('<span class="fa fa-file-o" data-toggle="tooltip" title="CSV"/>');
            $('.buttons-pdf').html('<span class="fa fa-file-pdf-o" data-toggle="tooltip" title="PDF"/>');
            $('.buttons-colvis').html('<span class="glyphicon glyphicon-th-list" data-toggle="tooltip" title="Kolonat"/>');
        }

	} );

	$('.search-input-text').on('keyup click', function () {   // for text boxes
	    var i =$(this).attr('data-column');  // getting column index
	    var v =$(this).val();  // getting search input value
	    listazeze_table.columns(i).search(v).draw();
	} );
});
</script>