<style type="text/css">
    td.details-control {
        background: url('<?php echo base_url(); ?>assets/images/icons8-double-down-20.png') no-repeat center center;
        cursor: pointer;
    }
    tr.shown td.details-control {
        background: url('<?php echo base_url(); ?>assets/images/icons8-double-up-20.png') no-repeat center center;
    }
    table.extra-details {
        table-layout:fixed; 
        width:auto;
    }
    table.extra-details th, table.extra-details tr td {
        width:500px !important;
        word-wrap: break-word !important;
        white-space: normal !important;
    }
    div.dt-button-collection.four-column {
        width:80% !important;
    }
    div.dt-button-collection.fixed {
        left:10%;
        margin:0;
        padding:4px;
    }
    div.dt-button-collection.fixed.four-column {
        margin:0;
    }
    table th{
        text-transform: uppercase;
        font-size: 12px;
    }
    table td{
        font-size: 12px;
    }
    div.dt-button-collection.one-column {
        width:30% !important;
        height:80% !important;
        overflow-y: auto;
    }
    div.dt-button-collection.fixed {
        left:35%;
        margin:0;
        padding:4px;
    }
    div.dt-button-collection.fixed.one-column {
        margin:0;
    } 
    @media only screen and (max-width: 767px) {
        div.dt-button-collection.one-column {
            width:90% !important;
            height:80% !important;
        }
        div.dt-button-collection.fixed {
            left:5%;
        }        
    }
</style>
<section id="general-section">
<?php if($this->input->get('success') == 3): ?>
  <div class="alert alert-success text-center"><strong>Të dhënat u fshinë me sukses.</strong></div>
<?php endif; ?>
    <div class="container">
        <div class="tags-section">
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="pull-left">
                                <a href="<?php echo base_url(); ?>admin/vendimet/edit_lista_e_zeze" class="btn btn-default" target="_blank"><i class="fa fa-plus"></i> Shto listë e zezë</a>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="pull-right">
                                <h5>Përditësimi i fundit: <strong><?php echo $last_updated; ?></strong></h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <table id="listazeze_table" class="row-border compact" style="width:100%">
                    <thead>
                        <tr>
                            <th class="not-export-col"></th>
                            <th class="not-export-col"></th>

                            <th>Kërkesa kundër OE</th>
                            <th>Nr. Protokollit</th>
                            <th class="fixed-min-width">Emri i lëndës</th>
                            <th>Vlera e parashikuar</th>
                            <th>Lloji i kontratës</th>
                            <th>Autoriteti kontraktues</th>

                            <th>Pretendimet e AK</th>

                            <th>Vendimi</th>
                            <th>Diskualifikimi/Kohëzgjatja</th>
                            <th>Neni</th>
                            <th>Komente Paneli</th>
                            <th>Nr. i vendimit</th>

                            <th>Kryetar i panelit</th>
                            <th>Referent i panelit</th>
                            <th>Anëtari i I-rë i panelit</th>
                            <th>Anëtari i II-të i panelit</th>
                            <th>Anëtari i III-të i panelit</th>
                            <th>Nr. i panelistëve</th>

                            <th>Rekomandimi i ekspertit</th>

                            <th>Eksperti shqyrtues</th>
                            <th>Eksperti teknik</th>

                            <th>Seanca Hapur/Mbyllur</th>

                            <th>Data e kërkesës</th>
                            <th>Data e vendimit</th>
                            <th>Data e publikimit</th>
                            <th>Kohëzgjatja Ankesë/Vendim</th>
                            <th>Kohëzgjatja Vendim/Publikim</th>
                            <th>Kohëzgjatja Ankesë/Publikim</th>

                            <th>Komente të përgjithshme</th>

                            <th class="not-export-col">Actions</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th class="not-export-col"></th>
                            <th class="not-export-col"></th>

                            <th><div class="ui-widget"><input type="text" data-column="2" data-name="kerkesa_kunder_oe" class="search-input-text" placeholder="Kërkesa kundër OE"></div></th>
                            <th><div class="ui-widget"><input type="text" data-column="3" data-name="nr_protokollit" class="search-input-text" placeholder="Nr. Protokollit"></div></th>
                            <th><div class="ui-widget"><input type="text" data-column="4" data-name="emri_lendes" class="search-input-text" placeholder="Emri i lëndës"></div></th>
                            <th><div class="ui-widget"><input type="text" data-column="5" data-name="vlera_parashikuar" class="search-input-text" placeholder="Vlera e parashikuar"></div></th>
                            <th><div class="ui-widget"><input type="text" data-column="6" data-name="lloji_kontrates" class="search-input-text" placeholder="Lloji i kontrates"></div></th>
                            <th><div class="ui-widget"><input type="text" data-column="7" data-name="autoriteti_kontraktues" class="search-input-text" placeholder="AK"></div></th>

                            <th><div class="ui-widget"><input type="text" data-column="8" data-name="pretendimet_ak" class="search-input-text" placeholder="Pretendimet e AK"></div></th>

                            <th><div class="ui-widget"><input type="text" data-column="9" data-name="vendimi" class="search-input-text" placeholder="Vendimi"></div></th>
                            <th><div class="ui-widget"><input type="text" data-column="10" data-name="diskualifikimi_kohezgjatja" class="search-input-text" placeholder="Diskualifikimi/Kohëzgjatja"></div></th>
                            <th><div class="ui-widget"><input type="text" data-column="11" data-name="neni" class="search-input-text" placeholder="Neni"></div></th>
                            <th><div class="ui-widget"><input type="text" data-column="12" data-name="komente_paneli" class="search-input-text" placeholder="Komente Paneli"></div></th>
                            <th><div class="ui-widget"><input type="text" data-column="13" data-name="vendimi_liste_zeze" class="search-input-text" placeholder="Nr. i vendimit"></div></th>
    
                            <th><div class="ui-widget"><input type="text" data-column="14" data-name="kryetar" class="search-input-text" placeholder="Kryetar i panelit"></div></th>
                            <th><div class="ui-widget"><input type="text" data-column="15" data-name="referent" class="search-input-text" placeholder="Referent i panelit"></div></th>
                            <th><div class="ui-widget"><input type="text" data-column="16" data-name="anetari_1" class="search-input-text" placeholder="Anëtari i I-rë i panelit"></div></th>
                            <th><div class="ui-widget"><input type="text" data-column="17" data-name="anetari_2" class="search-input-text" placeholder="Anëtari i II-të i panelit"></div></th>
                            <th><div class="ui-widget"><input type="text" data-column="18" data-name="anetari_3" class="search-input-text" placeholder="Anëtari i III-të i panelit"></div></th>
                            <th><div class="ui-widget"><input type="text" data-column="19" data-name="nr_panelisteve" class="search-input-text" placeholder="Nr. i panelistëve"></div></th>

                            <th><div class="ui-widget"><input type="text" data-column="20" data-name="rekomandimi_ekspertit" class="search-input-text" placeholder="Rekomandimi i ekspertit"></div></th>

                            <th><div class="ui-widget"><input type="text" data-column="21" data-name="eksperti_shqyrtues" class="search-input-text" placeholder="Eksperti shqyrtues"></div></th>
                            <th><div class="ui-widget"><input type="text" data-column="22" data-name="eksperti_teknik" class="search-input-text" placeholder="Eksperti teknik"></div></th>

                            <th><div class="ui-widget"><input type="text" data-column="23" data-name="seanca" class="search-input-text" placeholder="Seanca Hapur/Mbyllur"></div></th>

                            <th><div class="ui-widget"><input type="text" data-column="24" data-name="data_kerkeses" class="search-input-text" placeholder="Data e kërkesës"></div></th>
                            <th><div class="ui-widget"><input type="text" data-column="25" data-name="data_vendimit" class="search-input-text" placeholder="Data e vendimit"></div></th>
                            <th><div class="ui-widget"><input type="text" data-column="26" data-name="data_publikimit" class="search-input-text" placeholder="Data e publikimit"></div></th>
                            <th>Kohëzgjatja Ankesë/Vendim</th>
                            <th>Kohëzgjatja Vendim/Publikim</th>
                            <th>Kohëzgjatja Ankesë/Publikim</th>
                            <th><div class="ui-widget"><input type="text" data-column="30" data-name="komente_gjenerale" class="search-input-text" placeholder="Komente të përgjithshme"></div></th>

                            <th class="not-export-col">Actions</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
        <?php $this->load->view('template/legjenda', $legjenda); ?>
    </div>
</section>
<script>
var site_url = '<?php echo base_url(); ?>';
var encodedLogo = '<?php echo $this->config->item("encodedLogo"); ?>';
var xhr;
$(document).ready(function() {
    var listazeze_table = $('#listazeze_table').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'pageLength',
            {
                extend: 'excel',
                exportOptions: {
                    columns: [':visible :not(.not-export-col)']
                },
                title: 'Lista e zezë e OSHP-së në bazë të kritereve të zgjedhura',                            
            },
            {
                extend: 'pdfHtml5',
                orientation: 'landscape',
                pageSize: 'LEGAL',
                exportOptions: {
                    columns: [':visible :not(.not-export-col)']             
                },
                title: 'Lista e zezë e OSHP-së në bazë të kritereve të zgjedhura',
                customize: function ( doc ) {
                    doc.content.splice( 1, 0, {
                        margin: [ 0, 0, 0, 12 ],
                        alignment: 'center',
                        image: encodedLogo,
                        width: 170
                    } );
                }                            
            },
            {
                extend: 'print',
                exportOptions: {
                    columns: [':visible :not(.not-export-col)']            
                },
                title: 'Lista e zezë e OSHP-së në bazë të kritereve të zgjedhura',                            
            },
            {
                extend: 'csvHtml5',
                exportOptions: {
                    columns: [':visible :not(.not-export-col)']             
                },
                title: 'Lista e zezë e OSHP-së në bazë të kritereve të zgjedhura',                            
            },
            {
                extend: 'colvis',
                collectionLayout: 'fixed one-column',
                columns: [5,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31],
                text: 'Zgjedh kolonat',
                prefixButtons: [
                    { extend:'columnToggle', text:'Zgjedh të gjitha/asnjë', targets: '_all', className:"toggleBtnSelect toggleBtnGroup" },
                ],
                postfixButtons: [
                    { extend:'colvisRestore', text:'Rikthe kolonat fillestare', targets: '_all', className:"toggleBtnRestore toggleBtnGroup" }
                ]
            },
        ],
        columnDefs: [
            { targets: [0, 1, 2, 3, 4, 6, 7, 9, 11, 31], visible: true },
            { targets: '_all', visible: false },
            { className: "details-control", "targets": [0], "orderable": false, "width": "10px", "searchable": false },
            { className: "details-label", "targets": [1], "orderable": false, "searchable": false },
        ],
        "language": {
                "processing": "Ju lutem prisni...", //add a loading image,simply putting <img src="loader.gif" /> tag.
                buttons: {
                    pageLength: {
                    _: "Shfaq %d rreshta",
                    '-1': "Të gjithë rreshtat"
                    }
                },
                "zeroRecords": "Nuk ka të dhëna.",
                "info": "Shfaqur _START_ deri _END_ prej _TOTAL_ të dhënave",
                "paginate": {
                            "previous": "Faqja paraprake",
                            "next": "Faqja tjetër",
                            "first": "Faqja parë",
                            "last": "Faqja fundit"
                        },
                "search": "Kërko",
                "infoFiltered": " (prej total _MAX_ të të dhënave)"
            },          
        "autoWidth": false,
        "lengthMenu": [ [15, 25, 50, -1], [15, 25, 50, "Të gjitha"] ],
        "processing": true,
        "serverSide": true,
        "scrollX": true,
        "searchHighlight": true,        
        "ajax":{
            url :site_url + 'admin/vendimet/lista_e_zeze/json', // json datasource
            type: "post",  // method  , by default get 
            data:  function ( d ) {
                $('.buttons-print').html('<span class="fa fa-print" data-toggle="tooltip" title="Printo"/>');
                $('.buttons-excel').html('<span class="fa fa-file-excel-o" data-toggle="tooltip" title="Excel"/>');
                $('.buttons-csv').html('<span class="fa fa-file-o" data-toggle="tooltip" title="CSV"/>');
                $('.buttons-pdf').html('<span class="fa fa-file-pdf-o" data-toggle="tooltip" title="PDF"/>');                
            },                    
            error: function(){  // error handling
                $(".listazeze_table_error").html("");
                $("#listazeze_table").append('<tbody class="listazeze_table_error"><tr><th colspan="10">Nuk ka të dhëna.</th></tr></tbody>');
                //$("#employee-grid_processing").css("display","none");                 
            }
        }
    } );

    $('.search-input-text').autocomplete({
        minLength: 0,
        source: function(request, response){
            var el = $(this.element);
            var i =el.attr('data-column') // getting column index
            var n =el.attr('data-name') // getting name index
            var v =el.val();  // getting search input value
            if(v.length >= 1){
                if(xhr) {
                    xhr.abort();
                }
                var post_data = {table: 'liste_e_zeze', column: n, value: v};
                xhr = $.ajax({
                    'async': false,
                    'url': site_url + 'admin/vendimet/autocomplete_data/',
                    'data': post_data,
                    'type': 'POST',
                    success:function(data){
                        data = JSON.parse(data);
                        response(data);
                    }
                });            
            }else{
                listazeze_table.columns(i).search('').draw();
            }
        },
        select: function(event, ui) {
            var i = $(this).attr('data-column') // getting column index
            listazeze_table.columns(i).search(ui.item.value).draw();
        }           
    });
    
    $('#listazeze_table tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        //var id = tr.find('.pezullimi_id').val();
        //console.log(id);
        var row = listazeze_table.row( tr );
        if ( row.child.isShown() ) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
            // Open this row
            row.child(fetch_data(row.data())).show();
            tr.addClass('shown');
            $(this).closest('tr').next().css('background', '#f5f5f5');
            $(this).closest('tr').next().find('tr').css('background', 'transparent');
            $(this).closest('tr').next().find('th').css({'border-bottom': '1px solid #9e9e9e', 'color': '#111'});
            $(this).closest('tr').next().find('tr td').css({'color': '#111'});
        }

    } );

    var buttons = listazeze_table.buttons('.buttons-columnVisibility').nodes();
    var groupArrOne = [1];
    var groupArrTwo = [2];
    var groupArrThree = [3,4,5,6,7];
    var groupArrFour = [8,9,10,11,12,13];
    var groupArrFive = [14,15,16];
    var groupArrSix = [17];
    var groupArrSeven = [18,19,20,21,22,23];
    var groupArrEight = [24];
    var groupArrNine = [25];
    var groupOne = 0;
    var groupTwo = 0; 
    var groupThree = 0; 
    var groupFour = 0; 
    var groupFive = 0;
    var groupSix = 0;
    var groupSeven = 0;
    var groupEight = 0;
    var groupNine = 0;
    $.each( buttons, function( key, value ) {
        console.log(key + ' ' +value.outerText)
        var group;
        var groupDevider;
        if(groupArrOne.indexOf(key) != -1){
            group = ' groupOne';
            if(groupOne == 0){
                groupDevider = 'AKTIVITETI I PROKURIMIT';
            }            
            groupOne++;
        }else if(groupArrTwo.indexOf(key) != -1){
            group = ' groupTwo';
            if(groupTwo == 0){
                groupDevider = 'KËRKESA';
            }            
            groupTwo++;
        }else if(groupArrThree.indexOf(key) != -1){
            group = ' groupThree';
            if(groupThree == 0){
                groupDevider = 'VENDIMET';
            }            
            groupThree++;
        }else if(groupArrFour.indexOf(key) != -1){
            group = ' groupFour';
            if(groupFour == 0){
                groupDevider = 'PANELI';
            }            
            groupFour++;
        }else if(groupArrFive.indexOf(key) != -1){
            group = ' groupFive';
            if(groupFive == 0){
                groupDevider = 'REKOMANDIMI I EKSPERTIT';
            }            
            groupFive++;
        }else if(groupArrSix.indexOf(key) != -1){
            group = ' groupSix';
            if(groupSix == 0){
                groupDevider = 'SEANCA HAPUR/MBYLLUR';
            }            
            groupSix++;
        }else if(groupArrSeven.indexOf(key) != -1){
            group = ' groupSeven';
            if(groupSeven == 0){
                groupDevider = 'AFATET';
            }            
            groupSeven++;
        }else if(groupArrEight.indexOf(key) != -1){
            group = ' groupSeven';
            if(groupEight == 0){
                groupDevider = 'KOMENTE TË PËRGJITHSHME';
            }            
            groupEight++;
        }else if(groupArrNine.indexOf(key) != -1){
            group = ' groupNine';
            if(groupNine == 0){
                groupDevider = 'ACTIONS';
            }            
            groupNine++;
        }else{
            group = '';
            groupDevider = '';
        }
        value.className = value.className + group;
        if(groupDevider){
            value.className = value.className + " groupHeader";
        }
    });    
});
function fetch_data ( d ) {
    // `d` is the original data object for the row
    result = '<div class="col-md-12">'+
    '<table cellpadding="8" cellspacing="0" border="0" class="extra-details">' +
        '<th colspan="2">Aktiviteti i prokurimit</th>'+ 
        '<tr>'+
            '<td>Nr. i protokollit:</td>'+
            '<td>'+((d[3] != null) ? d[3] : '')+'</a></td>'+
        '</tr>'+
        '<tr>'+
            '<td>Emri i lëndës:</td>'+
            '<td>'+((d[4] != null) ? d[4] : '')+'</a></td>'+
        '</tr>'+
        '<tr>'+
            '<td>Vlera e parashikuar:</td>'+
            '<td>'+((d[5] != null) ? d[5] : '')+'</a></td>'+
        '</tr>'+
        '<tr>'+
            '<td>Lloji i kontratës:</td>'+
            '<td>'+((d[6] != null) ? d[6] : '')+'</a></td>'+
        '</tr>'+
        '<tr>'+
            '<td>Autoriteti kontraktues:</td>'+
            '<td>'+((d[7] != null) ? d[7] : '')+'</a></td>'+
        '</tr>'+    
        '<th colspan="2">Kërkesa</th>'+
        '<tr>'+
            '<td>Kërkesa kundër OE:</td>'+
            '<td>'+((d[2] != null) ? d[2] : '')+'</a></td>'+
        '</tr>'+
        '<tr>'+
            '<td>Pretendimet e AK:</td>'+
            '<td>'+((d[8] != null) ? d[8] : '')+'</a></td>'+
        '</tr>'+                
        '<th colspan="2">Vendimet</th>'+
        '<tr>'+
            '<td>Vendimi:</td>'+
            '<td>'+((d[9] != null) ? d[9] : '')+'</a></td>'+
        '</tr>'+
        '<tr>'+
            '<td>Diskualifikimi/Kohëzgjatja:</td>'+
            '<td>'+((d[10] != null) ? d[10] : '')+'</td>'+
        '</tr>'+
        '<tr>'+
            '<td>Neni:</td>'+
            '<td>'+((d[11] != null) ? d[11] : '')+'</td>'+
        '</tr>'+
        '<tr>'+
            '<td>Komente (Paneli):</td>'+
            '<td>'+((d[12] != null) ? d[12] : '')+'</td>'+
        '</tr>'+
        '<tr>'+
            '<td>Nr. i vendimit:</td>'+
            '<td>'+((d[13] != null) ? d[13] : '')+'</td>'+
        '</tr>'+             
        '<th colspan="2">Paneli</th>'+
        '<tr>'+
            '<td>Kryetar i panelit:</td>'+
            '<td>'+((d[14] != null) ? d[14] : '')+'</td>'+
        '</tr>'+
        '<tr>'+
            '<td>Referent i panelit:</td>'+
            '<td>'+((d[15] != null) ? d[15] : '')+'</td>'+
        '</tr>'+
        '<tr>'+
            '<td>Anetari i I-rë i panelit:</td>'+
            '<td>'+((d[16] != null) ? d[16] : '')+'</td>'+
        '</tr>'+
        '<tr>'+
            '<td>Anetari i II-të i panelit:</td>'+
            '<td>'+((d[17] != null) ? d[17] : '')+'</td>'+
        '</tr>'+
        '<tr>'+
            '<td>Anetari i III-të i panelit:</td>'+
            '<td>'+((d[18] != null) ? d[18] : '')+'</td>'+
        '</tr>'+
        '<tr>'+
            '<td>Nr. i panelistëve:</td>'+
            '<td>'+((d[19] != null) ? d[19] : '')+'</td>'+
        '</tr>'+
        '<th colspan="2">Rekomandimi i ekspertit</th>'+
        '<tr>'+
            '<td>Rekomandimi i ekspertit:</td>'+
            '<td>'+((d[20] != null) ? d[20] : '')+'</td>'+
        '</tr>'+
        '<th colspan="2">Eksperti</th>'+
        '<tr>'+
            '<td>Eksperti shqyrtues:</td>'+
            '<td>'+((d[21] != null) ? d[21] : '')+'</td>'+
        '</tr>'+
        '<tr>'+
            '<td>Eksperti teknik:</td>'+
            '<td>'+((d[22] != null) ? d[22] : '')+'</td>'+
        '</tr>'+
        '<th colspan="2">Seanca Hapur/Mbyllur</th>'+
        '<tr>'+
            '<td>Seanca Hapur/Mbyllur:</td>'+
            '<td>'+((d[23] != null) ? d[23] : '')+'</td>'+
        '</tr>'+
        '<th colspan="2">Afatet</th>'+
        '<tr>'+
            '<td>Data e kërkëses:</td>'+
            '<td>'+((d[24] != null) ? d[24] : '')+'</td>'+
        '</tr>'+
        '<tr>'+
            '<td>Data e vendimit:</td>'+
            '<td>'+((d[25] != null) ? d[25] : '')+'</td>'+
        '</tr>'+
        '<tr>'+
            '<td>Data Publikimit:</td>'+
            '<td>'+((d[26] != null) ? d[26] : '')+'</td>'+
        '</tr>'+
        '<tr>'+
            '<td>Kohëzgjatja Ankesë/Vendim:</td>'+
            '<td>'+((d[27] != null) ? d[27] : '')+'</td>'+
        '</tr>'+
        '<tr>'+
            '<td>Kohëzgjatja Vendim/Publikim:</td>'+
            '<td>'+((d[28] != null) ? d[28] : '')+'</td>'+
        '</tr>'+
        '<tr>'+
            '<td>Kohëzgjatja Ankesë/Publikim:</td>'+
            '<td>'+((d[29] != null) ? d[29] : '')+'</td>'+
        '</tr>'+
        '<tr>'+
            '<td>Komente të përgjithshme:</td>'+
            '<td>'+((d[30] != null) ? d[30] : '')+'</td>'+
        '</tr>'+                                        
    '</table>'+
'</div>';

    return result;
}
</script>