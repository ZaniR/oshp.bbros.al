<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Backups extends MY_Controller {

	public function __construct() 
	{
        parent::__construct();
		$this->load->library('email');
    }

	public function index($password = null) {
		if($password){
			if($password == 'Temp1234.'){

			$database = $this->db->database;
			$user =  $this->db->username;
			$pass =  $this->db->password;
			$host =  $this->db->hostname;
			if($_SERVER['HTTP_HOST'] == 'oshp.dplus-ks.loc'){
				$mysqldump = 'C:\"Program Files"\MySQL\"MySQL Server 5.6"\bin\mysqldump';
			}else{
				$mysqldump = 'mysqldump';
			}

			$file_dir = APPPATH . 'modules/Backups/files/dump.sql';

			echo "<h3>Backing up database to `<code>{$file_dir}</code>`</h3>";

			exec("{$mysqldump} --user={$user} --password={$pass} --host={$host} {$database} --result-file={$file_dir} 2>&1", $output);

			$subject = 'OSHP.DPLUS-KS Database Backup';
			$message = '<p>This is a database DUMP</p>';
			$list = array('butrint.xh.babuni@gmail.com', 'granitpeja20@gmail.com');
			$result = $this->email
			    ->from('oshpdplusks@gmail.com')
			    ->reply_to('oshpdplusks@gmail.com')    // Optional, an account where a human being reads.
			    ->to($list)
			    ->subject($subject)
			    ->message($message)
			    ->attach($file_dir)
			    ->send();
			var_dump($result);
			echo '<br />';
			echo $this->email->print_debugger();

			}
		}
	}
}
