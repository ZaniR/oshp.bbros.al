-- MySQL dump 10.16  Distrib 10.1.37-MariaDB, for Win32 (AMD64)
--
-- Host: localhost    Database: dplus
-- ------------------------------------------------------
-- Server version	10.1.37-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `dplus`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `dplus` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `dplus`;

--
-- Table structure for table `gjobe`
--

DROP TABLE IF EXISTS `gjobe`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gjobe` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nr_protokollit` varchar(70) DEFAULT NULL,
  `emri_lendes` varchar(500) DEFAULT NULL,
  `vlera_parashikuar` decimal(18,2) DEFAULT NULL,
  `lloji_kontrates` varchar(150) DEFAULT NULL,
  `operatori_ekonomik` varchar(255) DEFAULT NULL,
  `gjoba_kunder_ak` varchar(255) DEFAULT NULL,
  `shuma` decimal(18,2) DEFAULT NULL,
  `terheqje_verejtje` int(1) DEFAULT NULL,
  `heqje_licence` int(1) DEFAULT NULL,
  `vendimi` text,
  `neni` varchar(50) DEFAULT NULL,
  `komente_paneli` text,
  `kryetar` varchar(100) DEFAULT NULL,
  `referent` varchar(100) DEFAULT NULL,
  `anetari_1` varchar(100) DEFAULT NULL,
  `anetari_2` varchar(100) DEFAULT NULL,
  `anetari_3` varchar(100) DEFAULT NULL,
  `nr_panelisteve` int(11) DEFAULT NULL,
  `aprovohet_kerkesa` int(1) DEFAULT NULL,
  `refuzohet_kerkesa` int(1) DEFAULT NULL,
  `komente_eksperti` text,
  `shqyrtues_jashtem` varchar(255) DEFAULT NULL,
  `shqyrtues_brendshem` varchar(255) DEFAULT NULL,
  `teknik` varchar(255) DEFAULT NULL,
  `seanca` int(1) DEFAULT NULL,
  `data_kerkeses` date DEFAULT NULL,
  `data_vendimit` date DEFAULT NULL,
  `data_publikimit` date DEFAULT NULL,
  `komente_gjenerale` text,
  `created_by` int(11) DEFAULT NULL,
  `created_on` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_on` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gjobe`
--

LOCK TABLES `gjobe` WRITE;
/*!40000 ALTER TABLE `gjobe` DISABLE KEYS */;
INSERT INTO `gjobe` VALUES (1,'621-16-031-521','Ndërtimi i ujësjellësit në Dragash- vazhdimi i punimeve',495000.00,'Vlerë e madhe','Puna & K-Ing','KK Dragash',5000.00,NULL,NULL,'[{\"titulli\":\"50\\/17\",\"url\":\"https:\\/\\/oshp.rks-gov.net\\/repository\\/docs\\/vendimet\\/2017\\/50-17-vendim_1.PDF\"}]',NULL,'AK nuk ka respektuar tri vendime paraprake','Nuhi Pacarizi','Tefik Sylejmani','Blerim Dina',NULL,NULL,3,NULL,NULL,NULL,NULL,NULL,NULL,1,'2017-02-21','2017-03-20','2017-04-06',NULL,1,'2018-12-07 20:54:50',1,'2018-12-07 20:54:50'),(2,'651-16-052-211','Pastrimi i rrugëve, trotuareve, mirëmbajtja e parqeve dhe hapësirave publike',900000.00,'Vlerë e madhe','El Bau & Ve-Mor','KK Gjilan',10000.00,NULL,1,'[{\"titulli\":\"94\\/17\",\"url\":\"https:\\/\\/oshp.rks-gov.net\\/repository\\/docs\\/vendimet\\/2017\\/94-17vendim.pdf\"}]',NULL,'Per shkelje te perseritura te vendimeve te OSHP, ceket 7 vendime','Ekrem Salihu','Tefik Sylejmani','Goran Milenkovic','Nuhi Pacarizi','Blerim Dina',5,NULL,NULL,NULL,NULL,NULL,NULL,2,'2017-03-22','2017-05-23','2017-05-26',NULL,1,'2018-12-07 20:54:50',1,'2018-12-07 20:54:50'),(3,'622-16-086-511','Shenjëzimi horizontal, vertikal dhe vendosja e barrierave në rrugët e Prizrenit lot 2',800000.00,'Vlerë e madhe','Limit L&B','KK Prizren',5000.00,1,NULL,'[{\"titulli\":\"206\\/17\",\"url\":\"https:\\/\\/oshp.rks-gov.net\\/repository\\/docs\\/vendimet\\/2017\\/206-17vendim.pdf\"}]',NULL,'Per nenshkrim te kontrates, kur ka pase ankese ne OSHP dhe moszbatim te vendimit paraprak','Goran Milenkovic','Nuhi Pacarizi','Blerim Dina',NULL,NULL,3,NULL,NULL,NULL,NULL,NULL,NULL,2,'2017-07-03','2017-07-31','2017-08-03',NULL,1,'2018-12-07 20:54:50',1,'2018-12-07 20:54:50'),(4,'KOST-17-3289-211','Sigurimi fizik i objekteve të KOSTT',600000.00,'Vlerë e madhe','Delta Security','KOST',25000.00,NULL,1,'[{\"titulli\":\"81\\/18\",\"url\":\"https:\\/\\/oshp.rks-gov.net\\/repository\\/docs\\/81-18vendim_1.PDF\"}]',NULL,'Moszbatim te vendimeve 386/17 dhe 525/17, tri here ka zgjedhe OE fitues i cili nuk i permbush per page neto 230 ndersa buxheti i parashikuar se mbulon po te llogariten te gjitha shpenzimet per punetore. Oferta me e ulet se kostoja','Nuhi Pacarizi','Blerim Dina','Goran Milenkovic',NULL,NULL,3,NULL,NULL,NULL,NULL,NULL,NULL,2,'2018-03-02','2018-03-21','2018-05-02',NULL,1,'2018-12-07 20:54:50',1,'2018-12-07 20:54:50'),(5,'643-17-1885-511 / 643-17-2466-511 / 643-17-3004-511','Rikonstruimi i rrugës Rezall- Likovc L=4.700 m / Ndërtimi i kanalizimit fekal në fshatra Likovc-Abri-Murgë L=56000m / Asfaltimi i rrugës Kuçicë- Ternafc L=21.000',3800000.00,'Vlerë e madhe','R&Rukolli / Graniti Com & Infra Plus / Graniti Com & Saba Belca & ISPE Group','KK Skenderaj',5000.00,NULL,1,'[{\"titulli\":\"414-425-426\\/18\",\"url\":\"https:\\/\\/oshp.rks-gov.net\\/repository\\/docs\\/vendimet\\/2018\\/414-425-426-18gjobe.pdf\"}]','62','AK ska kerkuar vazhdimin e validitetit ne menyre qe te anuloje tenderin','Blerim Dina','Nuhi Pacarizi','Goran Milenkovic',NULL,NULL,3,NULL,NULL,NULL,NULL,NULL,NULL,1,'2018-08-13','2018-09-19','2018-09-20',NULL,1,'2018-12-07 20:54:50',1,'2018-12-07 20:54:50');
/*!40000 ALTER TABLE `gjobe` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `groups`
--

DROP TABLE IF EXISTS `groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `groups` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `groups`
--

LOCK TABLES `groups` WRITE;
/*!40000 ALTER TABLE `groups` DISABLE KEYS */;
INSERT INTO `groups` VALUES (1,'admin','Administrator'),(2,'moderators','Moderator User'),(3,'members','Member User');
/*!40000 ALTER TABLE `groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `groups_navigation`
--

DROP TABLE IF EXISTS `groups_navigation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `groups_navigation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) DEFAULT NULL,
  `navigation_id` int(11) DEFAULT NULL,
  `_created_on` datetime DEFAULT NULL,
  `_created_by` int(11) DEFAULT NULL,
  `_updated_on` datetime DEFAULT NULL,
  `_updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `groups_navigation`
--

LOCK TABLES `groups_navigation` WRITE;
/*!40000 ALTER TABLE `groups_navigation` DISABLE KEYS */;
INSERT INTO `groups_navigation` VALUES (1,1,8,NULL,NULL,NULL,NULL),(2,1,9,NULL,NULL,NULL,NULL),(3,1,10,NULL,NULL,NULL,NULL),(4,1,11,NULL,NULL,NULL,NULL),(5,1,12,NULL,NULL,NULL,NULL),(6,1,13,NULL,NULL,NULL,NULL),(7,1,15,NULL,NULL,NULL,NULL),(8,1,16,NULL,NULL,NULL,NULL),(9,2,8,NULL,NULL,NULL,NULL),(10,2,9,NULL,NULL,NULL,NULL),(11,2,10,NULL,NULL,NULL,NULL),(12,2,11,NULL,NULL,NULL,NULL),(13,2,12,NULL,NULL,NULL,NULL),(14,2,15,NULL,NULL,NULL,NULL),(17,1,17,NULL,NULL,NULL,NULL),(28,1,19,'2018-12-02 16:11:15',1,'2018-12-02 16:11:15',1),(29,2,19,'2018-12-02 16:11:15',1,'2018-12-02 16:11:15',1),(30,1,20,'2018-12-02 16:11:36',1,'2018-12-02 16:11:36',1),(31,2,20,'2018-12-02 16:11:36',1,'2018-12-02 16:11:36',1),(32,1,21,'2018-12-02 16:15:07',1,'2018-12-02 16:15:07',1),(33,2,21,'2018-12-02 16:15:07',1,'2018-12-02 16:15:07',1),(34,1,22,'2018-12-07 13:28:31',1,'2018-12-07 13:28:31',1),(35,2,22,'2018-12-07 13:28:31',1,'2018-12-07 13:28:31',1);
/*!40000 ALTER TABLE `groups_navigation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `heqje_pezullimi`
--

DROP TABLE IF EXISTS `heqje_pezullimi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `heqje_pezullimi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nr_protokollit` varchar(70) DEFAULT NULL,
  `emri_lendes` varchar(500) DEFAULT NULL,
  `vlera_parashikuar` decimal(18,2) DEFAULT NULL,
  `lloji_kontrates` varchar(150) DEFAULT NULL,
  `autoriteti_kontraktuar` varchar(255) DEFAULT NULL,
  `oe_rekomanduar` varchar(255) DEFAULT NULL,
  `heqje_pezullimi` int(1) DEFAULT NULL,
  `vendimi` text,
  `neni` varchar(50) DEFAULT NULL,
  `komente` text,
  `kryetar` varchar(100) DEFAULT NULL,
  `referent` varchar(100) DEFAULT NULL,
  `anetari_1` varchar(100) DEFAULT NULL,
  `anetari_2` varchar(100) DEFAULT NULL,
  `anetari_3` varchar(100) DEFAULT NULL,
  `nr_panelisteve` int(11) DEFAULT NULL,
  `seanca` varchar(15) DEFAULT NULL,
  `data_kerkeses` date DEFAULT NULL,
  `data_vendimit` date DEFAULT NULL,
  `data_publikimit` date DEFAULT NULL,
  `komente_gjenerale` text,
  `created_by` int(11) DEFAULT NULL,
  `created_on` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_on` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `heqje_pezullimi`
--

LOCK TABLES `heqje_pezullimi` WRITE;
/*!40000 ALTER TABLE `heqje_pezullimi` DISABLE KEYS */;
INSERT INTO `heqje_pezullimi` VALUES (1,'12998-17-433-121','Furnizim me vajra dhe yndyra',55000.00,'Vlerë e mesme','Trepca','SNR',1,'[{\"titulli\":\"90\\/17\",\"url\":\"https:\\/\\/oshp.rks-gov.net\\/repository\\/docs\\/vendimet\\/2017\\/90-17vendim_1.PDF\"}]',NULL,'AK thote vajrat shume te rendesishme, mund te nderprehet prodhimi nese ska vajra. Paneli refuzon se ska arrit te deshmoje','Blerim Dina',NULL,NULL,NULL,NULL,1,'3','2017-03-31','2017-04-05','2017-04-11',NULL,1,'2018-12-07 20:54:50',1,'2018-12-07 20:54:50'),(2,'AQP-17-001-111','Furnizim me karburante',14650000.00,'Vlerë e madhe','AQP','N/A (Tenderi është anuluar)',1,'[{\"titulli\":\"184\\/17\",\"url\":\"https:\\/\\/oshp.rks-gov.net\\/repository\\/docs\\/vendimet\\/2017\\/184-17vendim_1.PDF\"}]',NULL,'Shume AK jane pa kontrata sipas AQP. Paneli thote nuk eshte arsye bindese, ka mund ti filloje me heret procedurat','Blerim Dina',NULL,NULL,NULL,NULL,1,'3','2017-06-16','2017-06-16','2017-06-19',NULL,1,'2018-12-07 20:54:50',1,'2018-12-07 20:54:50'),(3,'70010-17-172-121','Furnizim me aparat analajzer biokimik- automatik',55000.00,'Vlerë e mesme','Spitali Prizren','Biotek Kosovo',1,'[{\"titulli\":\"234\\/17\",\"url\":\"https:\\/\\/oshp.rks-gov.net\\/repository\\/docs\\/vendimet\\/2017\\/234--17vendim_1.PDF\"}]',NULL,'AK thote qe kane mungese te madhe te aparatures jane para kolapsit, te respektohet vendimi paraprak, AK ska deshmuar se interesi eshte me i madh','Blerim Dina',NULL,NULL,NULL,NULL,1,'3','2017-07-31','2017-08-07','2017-08-08',NULL,1,'2018-12-07 20:54:50',1,'2018-12-07 20:54:50'),(4,'206-17-5226-115','Furnizim me material shpenzues për hemodializë nga lista esenciale',1086381.00,'Vlerë e madhe','Ministria e Shendetesise','Meditech',2,'[{\"titulli\":\"504\\/17\",\"url\":\"https:\\/\\/oshp.rks-gov.net\\/repository\\/docs\\/vendimet\\/2017\\/504-17vendim-i-heqjes-se-pezullimit.pdf\"}]','112.2','Gjendja alarmante thote AK, mungese materiale edhe pse vete eshte vonuar per procedura por rrezikimi i jetes eshte me i madh se interesi i OE Liri Med','Blerim Dina',NULL,NULL,NULL,NULL,1,'3','2017-12-14','2017-12-15','2017-12-18',NULL,1,'2018-12-07 20:54:50',1,'2018-12-07 20:54:50'),(5,'632-18-843-511','Ndërtimi i qarkores së Gjakovës',3678681.36,'Vlerë e madhe','KK Gjakove','Euroasphalt',1,'[{\"titulli\":\"489-496-497\\/18\",\"url\":\"https:\\/\\/oshp.rks-gov.net\\/repository\\/docs\\/vendimet\\/2018\\/489-496-497-18vendrefuzim-kerkeses.pdf\"}]',NULL,'AK thote qe rrezikohet fondi, per ndertim kolektorit nga KFW, i pamundur qarkullimi ne komunikacion, per keqplanifikim smund te pesojne OE','Blerim Dina',NULL,NULL,NULL,NULL,1,'3','2018-09-24','2018-10-01','2018-10-02',NULL,1,'2018-12-07 20:54:50',1,'2018-12-07 20:54:50');
/*!40000 ALTER TABLE `heqje_pezullimi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `liste_e_zeze`
--

DROP TABLE IF EXISTS `liste_e_zeze`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `liste_e_zeze` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nr_protokollit` varchar(70) DEFAULT NULL,
  `emri_lendes` varchar(500) DEFAULT NULL,
  `vlera_parashikuar` decimal(18,2) DEFAULT NULL,
  `lloji_kontrates` varchar(150) DEFAULT NULL,
  `autoriteti_kontraktues` varchar(255) DEFAULT NULL,
  `kerkesa_kunder_oe` varchar(255) DEFAULT NULL,
  `pretendimet_ak` text,
  `vendimi` varchar(255) DEFAULT NULL,
  `diskualifikimi_kohezgjatja` varchar(255) DEFAULT NULL,
  `aprovohet_kerkesa_p` int(1) DEFAULT NULL,
  `refuzohet_kerkesa_p` int(1) DEFAULT NULL,
  `neni` varchar(50) DEFAULT NULL,
  `komente_paneli` text,
  `vendimi_liste_zeze` text,
  `kryetar` varchar(100) DEFAULT NULL,
  `referent` varchar(100) DEFAULT NULL,
  `anetari_1` varchar(100) DEFAULT NULL,
  `anetari_2` varchar(100) DEFAULT NULL,
  `anetari_3` varchar(100) DEFAULT NULL,
  `nr_panelisteve` int(11) DEFAULT NULL,
  `rekomandimi_ekspertit` varchar(200) DEFAULT NULL,
  `aprovohet_kerkesa_e` int(1) DEFAULT NULL,
  `refuzohet_kerkesa_e` int(1) DEFAULT NULL,
  `komente_eksperti` text,
  `eksperti_shqyrtues` varchar(200) DEFAULT NULL,
  `eksperti_teknik` varchar(200) DEFAULT NULL,
  `shqyrtues_jashtem` varchar(255) DEFAULT NULL,
  `shqyrtues_brendshem` varchar(255) DEFAULT NULL,
  `teknik` varchar(255) DEFAULT NULL,
  `seanca` int(1) DEFAULT NULL,
  `data_kerkeses` date DEFAULT NULL,
  `data_vendimit` date DEFAULT NULL,
  `data_publikimit` date DEFAULT NULL,
  `komente_gjenerale` text,
  `created_by` int(11) DEFAULT NULL,
  `created_on` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_on` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `liste_e_zeze`
--

LOCK TABLES `liste_e_zeze` WRITE;
/*!40000 ALTER TABLE `liste_e_zeze` DISABLE KEYS */;
INSERT INTO `liste_e_zeze` VALUES (1,'214-16-060-221','Sherbime Projektimi',43160.00,'Vlerë e mesme','Policia e Kosoves','Kapital X','Numrat fiskal nuk korrespondojne, pretendim per falsifikim. OE thote gabim i ATK','Refuzohet kërkesa',NULL,NULL,NULL,NULL,'Gabim teknik i ATK i cili eshte sqaruar','[{\"titulli\":\"524\\/16\",\"url\":\"https:\\/\\/oshp.rks-gov.net\\/repository\\/docs\\/vendimet\\/2017\\/524-16vendim_1.PDF\"}]','Ekrem Salihu','Blerim Dina','Tefik Sylejmani',NULL,NULL,3,NULL,NULL,NULL,'Ne vendim nuk jepet arsyetimi i ekspertit','',NULL,NULL,NULL,NULL,2,'2016-06-06','2017-01-19','2017-01-25',NULL,1,'2018-12-07 21:05:25',1,'2018-12-07 21:05:25'),(2,'214-16-045-521','Renovimi i qendrave të ndalimit në Stacionin Policor Prishtinë, Gllogovc, Podujevë dhe Lipjan\n',52000.00,'Vlerë e mesme','Policia e Kosoves','5 Vellezerit Mulaku','Ka falsifikuar nje diplome te ing te arkitektures, ka fals vulen e noteres','Aprovohet kërkesa','6 Muaj',NULL,NULL,NULL,'AK ka kerkuar qe te disk konzorciumi por faktet jane vetem per OE 5 Vellezerit Mulaku','[{\"titulli\":\"464\\/16\",\"url\":\"https:\\/\\/oshp.rks-gov.net\\/repository\\/docs\\/vendimet\\/2017\\/464-16lista-e-zeze_1.PDF\"}]','Ekrem Salihu','Nuhi Pacarizi','Goran Milenkovic',NULL,NULL,3,NULL,NULL,NULL,'Ne vendim nuk jepet arsyetimi i ekspertit','',NULL,NULL,NULL,NULL,1,'2016-06-02','2017-01-20','2017-01-25',NULL,1,'2018-12-07 21:05:25',1,'2018-12-07 21:05:25'),(3,'214-16-045-521','Renovimi i qendrave të ndalimit në Stacionin Policor Prishtinë, Gllogovc, Podujevë dhe Lipjan\n',52000.00,'Vlerë e mesme','Policia e Kosoves','Euro Construction','Faktet jane vetem per 5 Vellezerit Mulaku si lider i konsorciumit','Refuzohet kërkesa',NULL,NULL,NULL,NULL,'Nuk ka deshmi per Euro Construction qe ka falsifikuar','[{\"titulli\":\"464\\/16\",\"url\":\"https:\\/\\/oshp.rks-gov.net\\/repository\\/docs\\/vendimet\\/2017\\/464-16lista-e-zeze_1.PDF\"}]','Ekrem Salihu','Nuhi Pacarizi','Goran Milenkovic',NULL,NULL,3,NULL,NULL,NULL,'Ne vendim nuk jepet arsyetimi i ekspertit','',NULL,NULL,NULL,NULL,1,'2016-06-02','2017-01-20','2017-01-25',NULL,1,'2018-12-07 21:05:25',1,'2018-12-07 21:05:25'),(4,'214-16-129-111','Furnizim me rroba civile për femra',126000.00,'Vlerë e madhe','Policia e Kosoves','Ajani & Ademi','OE ka zvogeluar sasine indikative per 50% duke demtuar buxhetin','Refuzohet kërkesa',NULL,NULL,NULL,'26','Kontrata eshte nenshkruar ne menyre te rregullut, per shkelje kontrates ceshjte gjyqesore','[{\"titulli\":\"15\\/17\",\"url\":\"https:\\/\\/oshp.rks-gov.net\\/repository\\/docs\\/15-17vendim-shd-lz.pdf\"}]','Nuhi Pacarizi','Ekrem Salihu','Tefik Sylejmani',NULL,NULL,3,NULL,NULL,NULL,'Ka zvogeluar sasine indikative per 50%, AK ka prishur kontraten','',NULL,NULL,NULL,NULL,1,'2017-01-11','2017-02-27','2017-03-03',NULL,1,'2018-12-07 21:05:25',1,'2018-12-07 21:05:25'),(5,'KEK-16-164-511','Riparimi mekanik dhe elektrik i stacionit veprues të shiritit transportues me gjerësi 1800 mm',3500000.00,'Vlerë e madhe','KEK','Solid Company','AK ka kerkuar vertetimin e katalogut nga prodhuesi por prape dyshon qe prodhuesi e ka marre nga prodhues te tjere','Refuzohet kërkesa',NULL,NULL,NULL,NULL,'AK ska ofruar prova se katalogu eshte i falsifikuar, prodhuesi ka thene qe jane te tij','[{\"titulli\":\"02\\/17\",\"url\":\"https:\\/\\/oshp.rks-gov.net\\/repository\\/docs\\/02-17-vendim-shd.pdf\"}]','Blerim Dina','Nuhi Pacarizi','Ekrem Salihu',NULL,NULL,3,NULL,NULL,NULL,'Viva Tech ka thene qe katalogu eshte origjinal, AK dyshon qe eshte nga prodhues te tjere','',NULL,NULL,NULL,NULL,1,'2017-01-06','2017-03-14','2017-03-20',NULL,1,'2018-12-07 21:05:25',1,'2018-12-07 21:05:25'),(6,'KEK-16-025-111','Furnizim me pompa centrifugale për largimin e ujit',200000.00,'Vlerë e madhe','KEK','Eldi Com','Katalogu i falsifikuar te nje kompanie tjeter e ka paraqitur si te vetin','Refuzohet kërkesa',NULL,NULL,NULL,NULL,'Pretendim per falsifikim te katalogut, AK ka kontaktuar prodhuesin i cili eshte distributor. Konfirmon distributori me prodhuesin qe jane bashke','[{\"titulli\":\"217\\/17\",\"url\":\"https:\\/\\/oshp.rks-gov.net\\/repository\\/docs\\/217-17vendim-shd-lz.pdf\"}]','Blerim Dina','Nuhi Pacarizi','Goran Milenkovic',NULL,NULL,3,NULL,NULL,NULL,'Nuk ka pase ekspert','',NULL,NULL,NULL,NULL,1,'2017-03-02','2017-03-28','2017-03-30',NULL,1,'2018-12-07 21:05:25',1,'2018-12-07 21:05:25'),(7,'KEK-16-194-511','Projektimi, ndërtimi dhe komisionimi i rezervuarit të ri të mazutit',500000.00,'Vlerë e madhe','KEK','Ripten Engenering & Top Sistem','ISO i ka skaduar afati, certifikuesi ka thene e falsifikuar','Aprovohet kërkesa','1 Vit',NULL,NULL,NULL,'Anetari i konzorciumit Top Sistem ka falsifikuar cert ISO, duke zgjatur afatin vet','[{\"titulli\":\"321\\/16\",\"url\":\"https:\\/\\/oshp.rks-gov.net\\/repository\\/docs\\/vendimet\\/2017\\/321-16vendim_1.PDF\"}]','Blerim Dina','Ekrem Salihu','Goran Milenkovic',NULL,NULL,3,NULL,NULL,NULL,'OE ka falsifikuar ISO 9001 ne baze te kom. Qe AK ka bere me certifikuesin','',NULL,NULL,NULL,NULL,2,'2016-12-29','2017-04-25','2017-04-28',NULL,1,'2018-12-07 21:05:25',1,'2018-12-07 21:05:25'),(8,'622-16-086-511','Shenjëzimi horizontal dhe vertikal dhe vendosja e barrierave në rrugët e Prizrenit',490000.00,'Vlerë e madhe','KK Prizren','Limit L&B','OE ka dorezuar marreveshje te re pas hapjes','Refuzohet kërkesa',NULL,NULL,NULL,NULL,'Anetari i konzorciumit ka thene qe qendron prapa marreveshjes, te dyjat','[{\"titulli\":\"360\\/17\",\"url\":\"https:\\/\\/oshp.rks-gov.net\\/repository\\/docs\\/vendimet\\/2017\\/360-17vendim_1.PDF\"}]','Tefik Sylejmani','Nuhi Pacarizi','Blerim Dina',NULL,NULL,3,NULL,NULL,NULL,'Ne vendim nuk jepet arsyetimi i ekspertit','',NULL,NULL,NULL,NULL,2,'2017-04-20','2017-05-04','2017-05-23',NULL,1,'2018-12-07 21:05:25',1,'2018-12-07 21:05:25'),(9,'206-16-103-111','Furnizim me barna dhe material shpenzues medicinal nga lista esenciale',800000.00,'Vlerë e madhe','Ministria e Shendetesise','Trio Med','Ka prezentuar certifikate false','Aprovohet kërkesa','6 Muaj',NULL,NULL,NULL,'Ka falsifikuar cert duke zgjatur validitetin vet','[{\"titulli\":\"404\\/17\",\"url\":\"https:\\/\\/oshp.rks-gov.net\\/repository\\/docs\\/vendimet\\/2017\\/404-17vendimm_1.PDF\"}]','Nuhi Pacarizi','Goran Milenkovic','Blerim Dina',NULL,NULL,3,NULL,NULL,NULL,'Ne vendim nuk jepet arsyetimi i ekspertit','Visar Basha',NULL,NULL,NULL,NULL,2,'2017-06-12','2017-07-18','2017-08-08',NULL,1,'2018-12-07 21:05:25',1,'2018-12-07 21:05:25'),(10,'318-17-011-221','Sigurimi fizik i objektit të KPMM-së',13615.08,'Vlerë e mesme','KPMM','Luani','OE ka qene fitues, OE ankues ka verejtur kete dhe pas sqarimeve me ATK, AK ka verejtur qe OE ka ndryshuar nr serik','Aprovohet kërkesa','6 Muaj',NULL,NULL,NULL,'Ka falsifikuar vertetimin e ATK','[{\"titulli\":\"806\\/17\",\"url\":\"https:\\/\\/oshp.rks-gov.net\\/repository\\/docs\\/vendimet\\/2017\\/806-17vendim_1.PDF\"}]','Blerim Dina','Nuhi Pacarizi','Goran Milenkovic',NULL,NULL,3,NULL,NULL,NULL,'Vertetimi nuk eshte leshuar me 2017 por me 2015','Sabrije Bullatovci',NULL,NULL,NULL,NULL,2,'2017-08-15','2017-09-14','2017-09-20',NULL,1,'2018-12-07 21:05:25',1,'2018-12-07 21:05:25'),(11,'614-17-026-536','Ndërtimi dhe mirëmbajtja e stacioneve të qytetit',9995.34,'Vlerë e vogël','KK Obiliq','Adra','OE ska kryer punimet','Refuzohet kërkesa',NULL,NULL,NULL,NULL,'Sipas LPP nuk e rregullon kete, por kontrata e rregullon. AK ska ofruar deshmi','[{\"titulli\":\"906\\/17\",\"url\":\"https:\\/\\/oshp.rks-gov.net\\/repository\\/docs\\/906-17vendim-diskualifikim_1.PDF\"}]','Goran Milenkovic','Nuhi Pacarizi','Blerim Dina',NULL,NULL,3,NULL,NULL,NULL,'Ne vendim nuk jepet arsyetimi i ekspertit','',NULL,NULL,NULL,NULL,2,'2017-10-17','2017-10-26','2017-11-01',NULL,1,'2018-12-07 21:05:25',1,'2018-12-07 21:05:25'),(12,'KEK-17-189-121','Dozator Rotativ',39900.00,'Vlerë e mesme','KEK','Xani Inex','Cert e manipuluar, kualiteti i shkronjave dallon ne pjese, ka dorezuar link ku sipas AK i bjen qe eshte cert perseri','Refuzohet kërkesa',NULL,NULL,NULL,NULL,'AK ska mund ta deshmoje se ISO 9001:2008 eshte e falsifikuar edhe pse kjo cert ne faqen e leshuesit nuk gjendet me nr RS-Q-0859 nga IQNET','[{\"titulli\":\"811\\/17\",\"url\":\"https:\\/\\/oshp.rks-gov.net\\/repository\\/docs\\/811-17vendim-diskualifikim_1.PDF\"}]','Goran Milenkovic','Nuhi Pacarizi','Blerim Dina',NULL,NULL,3,NULL,NULL,NULL,'AK ska deshmi duhet te merren organet. Ato bejen hetim penal kjo eshte per diskualifikim','Agim Sheqiri',NULL,NULL,NULL,NULL,2,'2017-09-21','2017-10-26','2017-11-01',NULL,1,'2018-12-07 21:05:25',1,'2018-12-07 21:05:25'),(13,'KEK-16-145-221','Pastrimi i kanalizimeve dhe pusetave, gypave të kracerit',35000.00,'Vlerë e mesme','KEK','Vemor','Ref e Erlis nga Birra Peja ka thene qe ska kryer pune, pastaj me vone ka thene qe ka kryer ne vitin 2016','Refuzohet kërkesa',NULL,NULL,NULL,NULL,'Keqkuptim me Birren Peja qe njehere ka thene qe Erlis ska kryer pune pastaj ka thene qe ka kryer por ne vitin 2016','[{\"titulli\":\"837\\/17\",\"url\":\"https:\\/\\/oshp.rks-gov.net\\/repository\\/docs\\/837-17vendim-diskualifikim_1.PDF\"}]','Nuhi Pacarizi','Blerim Dina','Goran Milenkovic',NULL,NULL,3,NULL,NULL,NULL,'Ne vendim nuk jepet arsyetimi i ekspertit','Sabrije Bullatovci',NULL,NULL,NULL,NULL,2,'2017-09-29','2017-10-26','2017-11-01',NULL,1,'2018-12-07 21:05:25',1,'2018-12-07 21:05:25'),(14,'KEK-17-281-111','Furnzim me gazra industriale për KEK',300000.00,'Vlerë e madhe','KEK','Bubeari Comerc','Per cert ISO 9001:2008 dyshimi te data e leshimit dhe skadimit, ne internet skane mund ta gjejne','Refuzohet kërkesa',NULL,NULL,NULL,NULL,'Smund ta deshmoje qe cert IT-1975 nga IQNET eshte e falsifikuar e prodhuesit SOL S.P.a','[{\"titulli\":\"1122\\/17\",\"url\":\"https:\\/\\/oshp.rks-gov.net\\/repository\\/docs\\/vendimet\\/2018\\/1122-17vendim_1.PDF\"}]','Nuhi Pacarizi','Blerim Dina','Goran Milenkovic',NULL,NULL,3,NULL,NULL,NULL,'Ska deshmi per ta vertetuar','Agim Sheqiri',NULL,NULL,NULL,NULL,2,'2017-12-15','2018-01-18','2018-02-13',NULL,1,'2018-12-07 21:05:25',1,'2018-12-07 21:05:25'),(15,'TRA-16-041-111','Riparimi i lokomotivave- rifabrikimi, furnizimi me vagon të përdorur të udhëtarëve me riparim të mesëm',1300000.00,'Vlerë e madhe','Trainkos','Gredelj doo','Per shkak te keqperdorimeve te info zyrtare dyshimi qe nje zyrtar i AK ka pas shkembim te info. Gjykata e ka shpalle fajtor zyrtarin','Refuzohet kërkesa',NULL,NULL,NULL,NULL,'AK ska ofruar deshmi per te dhena te rreme ose falsifikim','[{\"titulli\":\"214\\/18\",\"url\":\"https:\\/\\/oshp.rks-gov.net\\/repository\\/docs\\/214-18vendim_1.PDF\"}]','Blerim Dina','Nuhi Pacarizi','Goran Milenkovic',NULL,NULL,3,NULL,NULL,NULL,'Eksperti thote AK ska dorezuar prova, e prove eshte qe eshte denuar zyrtari','',NULL,NULL,NULL,NULL,2,'2018-02-21','2018-03-20','2018-03-22',NULL,1,'2018-12-07 21:05:25',1,'2018-12-07 21:05:25'),(16,'KEK-17-1306-111','Furnizim me pajisje personale mbrojtëse lot 2',50000.00,'Vlerë e mesme','KEK','Pastor Kosova','OE ka dorezuar autorizim te falsifikuar i cili ka qene nga prodhuesi per nje OE tjeter','Aprovohet kërkesa','6 Muaj',NULL,NULL,NULL,'Ka dorezuar autorizim te rreme me Draeger me te cilin ka pasur ne vitin 2011. Konfirmuar nga prodhuesi qe Medicom eshte i autorizuar','[{\"titulli\":\"1135\\/17\",\"url\":\"https:\\/\\/oshp.rks-gov.net\\/repository\\/docs\\/1135-17vend.pdf\"}]','Blerim Dina','Nuhi Pacarizi','Goran Milenkovic',NULL,NULL,3,NULL,NULL,NULL,'Ne vendim nuk jepet arsyetimi i ekspertit','',NULL,NULL,NULL,NULL,2,'2017-12-14','2018-03-14','2018-04-04',NULL,1,'2018-12-07 21:05:25',1,'2018-12-07 21:05:25'),(17,'KEK-17-2965-111','Furnizim me çeliqe të profiluara dhe gypa të çelikut pa tegel',45000.00,'Vlerë e mesme','KEK','Hekurani','Dy oferta kane qene Hekurani dhe GHM kane ofruar autorizim nga prodhuesi Arcelor Ukraine, autorizimet sjane te njejta dhe dallojne vula e nenshkrimi. AK pasi ka kerkuar sqarime prodhuesi ka kthyer email dhe ka thene qe asnjeri autorizim nuk eshte i tyre','Aprovohet kërkesa','6 Muaj',NULL,NULL,NULL,'Paneli ka kerkuar nga AK ti verifikoje edhe njehere autorizimet, AK ka kerkuar nga Hekurani email i cili e ka dhene me @gmail.com te prodhuesit. AK ka kerkuar konfirmim edhe nga dega ne Rumani pa pergjigje','[{\"titulli\":\"112\\/18\",\"url\":\"https:\\/\\/oshp.rks-gov.net\\/repository\\/docs\\/112-18list-e-zeze-vendim_1.PDF\"}]','Nuhi Pacarizi','Goran Milenkovic','Blerim Dina',NULL,NULL,3,NULL,NULL,NULL,'Kane ofruar me produkte te te njejtit prodhues por autorizimet te dhena kontradiktore, vula e ndryshme, teksti, nenshkrimi. AK ka kerkuar nga prodhuesi i cili ka thene qe asnjeri autorizim nuk eshte i tyre','Visar Basha',NULL,NULL,NULL,NULL,2,'2018-01-25','2018-04-24','2018-05-02',NULL,1,'2018-12-07 21:05:25',1,'2018-12-07 21:05:25'),(18,'KEK-17-2965-111','Furnizim me çeliqe të profiluara dhe gypa të çelikut pa tegel',45000.00,'Vlerë e mesme','KEK','GHM Company','Dy oferta kane qene Hekurani dhe GHM kane ofruar autorizim nga prodhuesi Arcelor Ukraine, autorizimet sjane te njejta dhe dallojne vula e nenshkrimi. AK pasi ka kerkuar sqarime prodhuesi ka kthyer email dhe ka thene qe asnjeri autorizim nuk eshte i tyre','Refuzohet kërkesa',NULL,NULL,NULL,NULL,'Njejet edhe per GHM ku AK ka derguar email ne degen ne Rumani e cila ka kthyer pergjigje prej email @galati-arcelormittal.ro, kjo eshte konfirmuar edhe me bazen e kompanise ne Ukraine pastaj. Ka ofruar deshmi','[{\"titulli\":\"\",\"url\":\"https:\\/\\/oshp.rks-gov.net\\/repository\\/docs\\/112-18list-e-zeze-vendim_1.PDF\"}]','Nuhi Pacarizi','Goran Milenkovic','Blerim Dina',NULL,NULL,3,NULL,NULL,NULL,'Kane ofruar me produkte te te njejtit prodhues por autorizimet te dhena kontradiktore, por ky OE thote qe eshte autorizuar nga pika e prodhuesit ne Rumani e OE tjeter Ukraine','Visar Basha',NULL,NULL,NULL,NULL,2,'2018-01-25','2018-04-24','2018-05-02',NULL,1,'2018-12-07 21:05:25',1,'2018-12-07 21:05:25'),(19,'KEK-17-4887-121','Furnizim me bosht të kushinetës të mullirit',60000.00,'Vlerë e mesme','KEK','Enggroup','AK i ka kerkuar sqarime per deklarate per spec, OE nuk eshte pergjigjur edhe pse fitues AK dyshon marreveshje te fshehte','Refuzohet kërkesa',NULL,NULL,NULL,NULL,'AK mund te kerkoje sqarime per cdo dok, provat ndaj OE nuk jane bindes dhe refuzohet kerkesa','[{\"titulli\":\"427\\/18\",\"url\":\"https:\\/\\/oshp.rks-gov.net\\/repository\\/docs\\/427-18vendim-lz_1.PDF\"}]','Blerim Dina','Nuhi Pacarizi','Goran Milenkovic',NULL,NULL,3,NULL,NULL,NULL,'AK e ka cilesuar te pergjegjshem por deklaraten per 3.1 FDT se ka sjelle pasi AK i ka kerkuar sqarime. AK dyshon per marreveshje me te dytin','',NULL,NULL,NULL,NULL,2,'2018-04-23','2018-05-17','2018-05-30',NULL,1,'2018-12-07 21:05:25',1,'2018-12-07 21:05:25'),(20,'213-18-875-521','Programi i zbatimi të masave të efiçencës së energjisë në objektet publike në nivel lokal për arsim dhe shëndetësi',345903.00,'Vlerë e madhe','Ministria e Zhvillimit Ekonomik','Blenor','Me i liri ka qene grupi Blenor & Menti te cilet kane prezentuar ing e ndertimtarise te njejte me Dajting. Nenshkrimet sjane te njejte. Ing ka thene qe punon per Dajting','Refuzohet kërkesa',NULL,NULL,NULL,NULL,'Blenor ka marreveshje per staf me Arbotec per te cilen punon ing (cka ndodh kur huazohet stafi pa dijen). AK ska ofruar prova bindese','[{\"titulli\":\"404\\/18\",\"url\":\"https:\\/\\/oshp.rks-gov.net\\/repository\\/docs\\/408-18vendim-lz_1.PDF\"}]','Blerim Dina','Nuhi Pacarizi','Goran Milenkovic',NULL,NULL,3,NULL,NULL,NULL,'Te dy OE kane ofruar si konsocrium dhe nje ing e kane te njejte me Dajting dhe nenshkrimi nuk perputhej, ndersa ing ka thene qe eshte i punesuar te Dajting','',NULL,NULL,NULL,NULL,2,'2018-04-17','2018-05-17','2018-05-30',NULL,1,'2018-12-07 21:05:25',1,'2018-12-07 21:05:25'),(21,'213-18-875-521','Programi i zbatimi të masave të efiçencës së energjisë në objektet publike në nivel lokal për arsim dhe shëndetësi',345903.00,'Vlerë e madhe','Ministria e Zhvillimit Ekonomik','Menti','Me i liri ka qene grupi Blenor & Menti te cilet kane prezentuar ing e ndertimtarise te njejte me Dajting. Nenshkrimet sjane te njejte. Ing ka thene qe punon per Dajting','Refuzohet kërkesa',NULL,NULL,NULL,NULL,'Blenor ka marreveshje per staf me Arbotec per te cilen punon ing (cka ndodh kur huazohet stafi pa dijen). AK ska ofruar prova bindese','[{\"titulli\":\"\",\"url\":\"\"}]','Blerim Dina','Nuhi Pacarizi','Goran Milenkovic',NULL,NULL,3,NULL,NULL,NULL,'Te dy OE kane ofruar si konsocrium dhe nje ing e kane te njejte me Dajting dhe nenshkrimi nuk perputhej, ndersa ing ka thene qe eshte i punesuar te Dajting','',NULL,NULL,NULL,NULL,2,'2018-04-17','2018-05-17','2018-05-30',NULL,1,'2018-12-07 21:05:25',1,'2018-12-07 21:05:25'),(22,'213-18-1135-521','Programi mbështetës- Zbatimi i masave të efiçencës së energjisë në ndërtesat e shërbimit publik',226208.36,'Vlerë e madhe','Ministria e Zhvillimit Ekonomik','Euro Project','Euro Project ka paraqitur ing te makinerise te njejte me Termovision, ku nenshkrimet dallojne. Ing ka deklaruar qe punon per Termovision dhe kerkon te merren masa. OE thote qe ne kemi pase me heret marreveshje se di si ka ndodh kjo','Refuzohet kërkesa',NULL,NULL,NULL,NULL,'Nuk jane bindese thote prova, cka eshte me bindese se vet deklarata me shkrim e ing. Merr per baze fjalet e OE qe thote qe e ka te noterizuar diplomen qe ka mund te jete per ndonje projekt tjeter. Paneli nuk ben fare verifikim te vetin dhe thote nese konstatohet nga organet kompetente','[{\"titulli\":\"515\\/18\",\"url\":\"https:\\/\\/oshp.rks-gov.net\\/repository\\/docs\\/515-18vend-lz.pdf\"}]','Nuhi Pacarizi','Blerim Dina','Goran Milenkovic',NULL,NULL,3,NULL,NULL,NULL,'AK dyshon per dok te falsifikuar, OE kishte cmim me te lire, OE te njejtin ing te makinerise me Termovision, nuk perputhen nenshkrimet, ing ka deklaruar qe spunon per Euro Project por per Termovision','Hysni Muhadri',NULL,NULL,NULL,NULL,2,'2018-05-18','2018-06-07','2018-06-12',NULL,1,'2018-12-07 21:05:25',1,'2018-12-07 21:05:25'),(23,'KEK-14-290-111','Transporti i punëtorëve të KEK-ut',0.00,'Vlerë e vogël','KEK','Autotransporti','Ka gjetur shkelje ne zbatim te kontrates nga raporti i auditorit te brendshem','Refuzohet kërkesa',NULL,NULL,NULL,NULL,'Ka masa tjera qe AK mund te marre per mos zbatim te duhur ska te dhena te rreme dhe falsifikim','[{\"titulli\":\"775\\/18\",\"url\":\"https:\\/\\/oshp.rks-gov.net\\/repository\\/docs\\/775-18lz.PDF\"}]','Blerim Dina','Nuhi Pacarizi','Goran Milenkovic',NULL,NULL,3,NULL,NULL,NULL,'OE ka shkele kontraten ne disa pika te cilat i ka gjetur auditori i brendshem','Visar Basha',NULL,NULL,NULL,NULL,2,'2018-08-13','2018-08-29','2018-09-05',NULL,1,'2018-12-07 21:05:25',1,'2018-12-07 21:05:25'),(24,'217-18-1552-521','Renovimi i kulmit të kazermës së FSK-së në Mitrovicë',80000.00,'Vlerë e mesme','Ministria e Forces se Sigurise','Astraplan','OE ka qene i rekomanduar por ska ardhe te nenshkruaje kontraten, AK ka anuluar tenderin. OE thote qe eshte rekomanduar pa i plotesuar kerkesat','Refuzohet kërkesa',NULL,NULL,NULL,NULL,'OE ska dorezuar te dhena te rreme ose falsifikim, AK ska fakte, pasi OE thote qe AK e ka rekomanduar pa i plotesuar kushtet, ka borxhe ndaj ATK','[{\"titulli\":\"737\\/18\",\"url\":\"https:\\/\\/oshp.rks-gov.net\\/repository\\/docs\\/737-18lz.PDF\"}]','Blerim Dina','Nuhi Pacarizi','Goran Milenkovic',NULL,NULL,3,NULL,NULL,NULL,'OE ka qene fitues por ska nenshkruar kontraten, AK e ka anuluar tenderin','Shqipe Hoti',NULL,NULL,NULL,NULL,2,'2018-08-01','2018-08-29','2018-09-05',NULL,1,'2018-12-07 21:05:25',1,'2018-12-07 21:05:25'),(25,'KEK-17-1889-111','Furnizim me pompa centrifugale horizontale për largimin e ujit',200000.00,'Vlerë e madhe','KEK','SNR','AK pretenden qe ISO e falsifikuar pasi nr nuk gjendet ne faqen e leshuesit VDI','Refuzohet kërkesa',NULL,NULL,NULL,NULL,'AK ska ofruar deshmi per te dhena te rreme ose falsifikim pasi te dy OE kane prezentuar ISO','[{\"titulli\":\"729\\/18\",\"url\":\"https:\\/\\/oshp.rks-gov.net\\/repository\\/docs\\/729-730-18lz.PDF\"}]','Blerim Dina','Nuhi Pacarizi','Goran Milenkovic',NULL,NULL,3,NULL,NULL,NULL,'OE ND ka pretenduar qe ISO jo valide nga leshuesi i cert qe eshte VDI','Shqipe Hoti',NULL,NULL,NULL,NULL,2,'2018-07-27','2018-08-29','2018-09-05',NULL,1,'2018-12-07 21:05:25',1,'2018-12-07 21:05:25'),(26,'KEK-17-1889-111','Furnizim me pompa centrifugale horizontale për largimin e ujit',200000.00,'Vlerë e madhe','KEK','Union LLC','AK pretenden qe ISO e falsifikuar pasi nr nuk gjendet ne faqen e leshuesit VDI','Refuzohet kërkesa',NULL,NULL,NULL,NULL,'AK ska ofruar deshmi per te dhena te rreme ose falsifikim pasi te dy OE kane prezentuar ISO','[{\"titulli\":\"730\\/18\",\"url\":\"https:\\/\\/oshp.rks-gov.net\\/repository\\/docs\\/729-730-18lz.PDF\"}]','Blerim Dina','Nuhi Pacarizi','Goran Milenkovic',NULL,NULL,3,NULL,NULL,NULL,'OE ND ka pretenduar qe ISO jo valide nga leshuesi i cert qe eshte VDI','Shqipe Hoti',NULL,NULL,NULL,NULL,2,'2018-07-27','2018-08-29','2018-09-05',NULL,1,'2018-12-07 21:05:25',1,'2018-12-07 21:05:25'),(27,'2016-18-3751-221','Sigurimi fizik i objekteve',65878.00,'Vlerë e mesme','AQP','Commando','OE ka deklarate nen betim qe thote qe ska borxhe ndaj ATK, dmth e ka ditur qe eshte me borxhe para hapjes dhe ka ofertuar. Dmth ka dhene te dhena te rreme','Refuzohet kërkesa',NULL,NULL,NULL,NULL,'AK ska deshmi per te dhena te rreme sipas panelit, e deklarat nen betim qe eshte nenshkruar qe thote qe sje me borxhe e ne fakt je si te cilesohet?','[{\"titulli\":\"723\\/18\",\"url\":\"https:\\/\\/oshp.rks-gov.net\\/repository\\/docs\\/723-18lz.PDF\"}]','Blerim Dina','Nuhi Pacarizi','Goran Milenkovic',NULL,NULL,3,NULL,NULL,NULL,'OE ka qene fitues por ka dorezuar vetem vertetim nga gjykata, ndersa per te ATK dy dite me vone ka thene se eshte me borxhe','Shqipe Hoti',NULL,NULL,NULL,NULL,2,'2018-07-26','2018-08-29','2018-09-05',NULL,1,'2018-12-07 21:05:25',1,'2018-12-07 21:05:25'),(28,'652-18-4021-136','Furnizim me gjenerator',8000.00,'Vlerë e vogël','KK Kacanik','As Tech','OE eshte shpalle fitues dhe AK dy here i ka shkruar qe te vije te nenshkruaje kontraten por nuk eshte lajmeruar','Refuzohet kërkesa',NULL,NULL,NULL,NULL,'AK ska prezentuar deshmi se OE ka ofruar te dhena te rreme ose falsifikim sipas nenit 99.2','[{\"titulli\":\"677\\/18\",\"url\":\"https:\\/\\/oshp.rks-gov.net\\/repository\\/docs\\/677-18lz.PDF\"}]','Blerim Dina','Nuhi Pacarizi','Goran Milenkovic',NULL,NULL,3,NULL,NULL,NULL,'Eshte zgjedhur fitues por nuk eshte lajmeruar per nenshkrim te kontrates, ska paraqitur deshmi thote per falsifikim ose te dhena te rreme','',NULL,NULL,NULL,NULL,2,'2018-07-09','2018-08-29','2018-09-05',NULL,1,'2018-12-07 21:05:25',1,'2018-12-07 21:05:25'),(29,'616-18-3046-121','Furnizim me pajisje elektrike dhe mekanike',24000.00,'Vlerë e mesme','KK Prishtine','Megrant Ing','OE eshte shpalle fitues pos ska sjelle deklaraten e ATK pasi ka qene me borxhe','Refuzohet kërkesa',NULL,NULL,NULL,NULL,'AK ska deshmi per te dhena te rreme sipas panelit, e deklarat nen betim qe eshte nenshkruar qe thote qe sje me borxhe e ne fakt je si te cilesohet?','[{\"titulli\":\"631\\/18\",\"url\":\"https:\\/\\/oshp.rks-gov.net\\/repository\\/docs\\/631-18vendim_1.PDF\"}]','Goran Milenkovic','Nuhi Pacarizi','Blerim Dina',NULL,NULL,3,NULL,NULL,NULL,'OE eshte shpalle fitues por ska sjelle vertetimin e ATK per shkak qe ka obligime e qe ne fakt deklarata nen betim kur nenshkruhet e pranon qe ski obligime','Agim Sheqiri',NULL,NULL,NULL,NULL,2,'2018-06-22','2018-08-29','2018-09-05',NULL,1,'2018-12-07 21:05:25',1,'2018-12-07 21:05:25');
/*!40000 ALTER TABLE `liste_e_zeze` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `login_attempts`
--

DROP TABLE IF EXISTS `login_attempts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `login_attempts` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(45) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `login_attempts`
--

LOCK TABLES `login_attempts` WRITE;
/*!40000 ALTER TABLE `login_attempts` DISABLE KEYS */;
/*!40000 ALTER TABLE `login_attempts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `navigation`
--

DROP TABLE IF EXISTS `navigation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `navigation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `description` text,
  `url` varchar(255) DEFAULT NULL,
  `parent` int(11) DEFAULT NULL,
  `active` int(1) DEFAULT '0',
  `hidden` int(1) DEFAULT '0',
  `page_title` varchar(255) DEFAULT NULL,
  `page_keyword` varchar(255) DEFAULT NULL,
  `authorization` int(1) DEFAULT '0' COMMENT '0 - Unauthorized\n1 - Authenticated',
  `index` int(11) DEFAULT '0',
  `_created_on` datetime DEFAULT NULL,
  `_created_by` int(11) DEFAULT NULL,
  `_updated_on` datetime DEFAULT NULL,
  `_updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `navigation`
--

LOCK TABLES `navigation` WRITE;
/*!40000 ALTER TABLE `navigation` DISABLE KEYS */;
INSERT INTO `navigation` VALUES (1,'Ballina','Ballina','',NULL,1,1,'Ballina','Ballina',0,0,'2018-11-19 16:32:06',1,'2018-11-19 16:32:06',1),(2,'Vendimet','Vendimet','vendimet',NULL,1,0,'Vendimet','Vendimet',0,1,'2018-11-19 16:32:06',1,'2018-11-19 16:32:06',1),(3,'Lista e zezë','Lista e zezë','vendimet/lista-e-zeze',NULL,1,0,'Lista e zezë','Lista e zezë',0,2,'2018-11-19 16:32:06',1,'2018-11-19 16:32:06',1),(4,'Gjobë','Gjobë','vendimet/gjobe',NULL,1,0,'Gjobë','Gjobë',0,3,'2018-11-19 16:32:06',1,'2018-11-19 16:32:06',1),(5,'Heqje pezullimi','Heqje pezullimi','vendimet/heqje-pezullimi',NULL,1,0,'Heqje pezullimi','Heqje pezullimi',0,4,'2018-11-19 16:32:06',1,'2018-11-19 16:32:06',1),(6,'Kyçu','Kyçu','auth/login',NULL,1,1,'Kyçu','Kyçu',0,100,'2018-11-19 16:32:06',1,'2018-11-19 16:32:06',1),(7,'Rikthe fjalëkalimin','Rikthe fjalëkalimin','auth/forgot_password',NULL,1,1,'Rikthe fjalëkalimin','Rikthe fjalëkalimin',0,100,'2018-11-19 16:32:06',1,'2018-11-19 16:32:06',1),(8,'Administrimi','Administrimi','admin',NULL,1,0,'Administrimi','Administrimi',1,100,'2018-11-19 16:32:06',1,'2018-11-19 16:32:06',1),(9,'Vendime','Menaxho Vendimet','admin/vendimet',8,1,0,'Menaxho Vendimet','Menaxho Vendimet',1,0,'2018-11-19 16:32:06',1,'2018-11-19 16:32:06',1),(10,'Lista e zezë','Menaxho Listën e zezë','admin/vendimet/lista-e-zeze',8,1,0,'Menaxho Listën e zezë','Menaxho Listën e zezë',1,1,'2018-11-19 16:32:06',1,'2018-11-19 16:32:06',1),(11,'Gjobë','Menaxho Gjobat','admin/vendimet/gjobe',8,1,0,'Menaxho Gjobat','Menaxho Gjobat',1,2,'2018-11-19 16:32:06',1,'2018-11-19 16:32:06',1),(12,'Heqje pezullimi','Menaxho Pezullimet','admin/vendimet/heqje-pezullimi',8,1,0,'Menaxho Pezullimet','Menaxho Pezullimet',1,3,'2018-11-19 16:32:06',1,'2018-11-19 16:32:06',1),(13,'Navigimi','Menaxho Navigimin','admin/navigation',8,1,0,'Menaxho Navigimin','Menaxho Navigimin',1,4,'2018-11-19 16:32:06',1,'2018-11-19 16:32:06',1),(14,'Kyçu','Kyçu','login',NULL,1,1,'Kyçu','Kyçu',0,100,'2018-11-19 16:32:06',1,'2018-11-19 16:32:06',1),(15,'Tagjet','Menaxho Tagjet','admin/tags',8,1,0,'Menaxho Tagjet','Menaxho Tagjet',1,5,'2018-11-19 16:32:06',1,'2018-11-19 16:32:06',1),(16,'Autorizimet','Menaxho Autorizimet','auth',8,1,1,'Menaxho Autorizimet','Menaxho Autorizimet',1,1,'2018-11-19 16:32:06',1,'2018-11-19 16:32:06',1),(17,'Përdoruesit','Menaxho Përdoruesit','auth/users',8,1,0,'Menaxho Përdoruesit','Menaxho Përdoruesit',1,6,'2018-11-19 16:32:06',1,'2018-11-19 16:32:06',1),(18,'Ndrysho fjalëkalimin','Ndrysho fjalëkalimin','auth/reset_password',NULL,1,1,'Ndrysho fjalëkalimin','Ndrysho fjalëkalimin',0,100,'2018-11-19 16:32:06',1,'2018-11-19 16:32:06',1),(19,'Lista e zezë','Menaxho Listën e zezë','admin/vendimet/edit_lista_e_zeze',10,1,1,'Menaxho Listën e zezë','Menaxho Listën e zezë',1,100,'2018-12-02 16:06:18',1,'2018-12-02 16:11:15',1),(20,'Gjobë','Menaxho Gjobat','admin/vendimet/edit_gjobe',11,1,1,'Menaxho Gjobat','Menaxho Gjobat',1,100,'2018-12-02 16:09:31',1,'2018-12-02 16:11:36',1),(21,'Heqje pezullimi','Menaxho Pezullimet','admin/vendimet/edit_pezullimi',12,1,1,'Menaxho Pezullimet','Menaxho Pezullimet',1,100,'2018-12-02 16:15:07',1,'2018-12-02 16:15:07',1),(22,'Ngarko të dhëna','Ngarko të dhëna','admin/upload',8,1,1,'Ngarko të dhëna','Ngarko të dhëna',1,100,'2018-12-07 13:28:31',1,'2018-12-07 13:28:31',1);
/*!40000 ALTER TABLE `navigation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tags`
--

DROP TABLE IF EXISTS `tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tags` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `emri` varchar(255) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_on` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_on` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tags`
--

LOCK TABLES `tags` WRITE;
/*!40000 ALTER TABLE `tags` DISABLE KEYS */;
INSERT INTO `tags` VALUES (1,'205-18-1572-511',1,'2018-11-29 21:39:31',1,'2018-11-29 21:39:31'),(2,'KEK',1,'2018-11-29 21:39:48',1,'2018-11-29 21:39:48'),(3,'Gjakove',1,'2018-11-29 21:39:57',1,'2018-11-29 21:39:57'),(4,'Agjencia',1,'2018-11-29 21:40:09',1,'2018-11-29 21:40:09'),(5,'Policia',1,'2018-11-29 21:40:33',1,'2018-11-29 21:40:33');
/*!40000 ALTER TABLE `tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `upload_excel`
--

DROP TABLE IF EXISTS `upload_excel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `upload_excel` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `upload_file` varchar(255) DEFAULT NULL,
  `recovery_file` varchar(255) DEFAULT NULL,
  `stable` int(1) DEFAULT '0',
  `created_by` int(11) DEFAULT NULL,
  `created_on` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `upload_excel`
--

LOCK TABLES `upload_excel` WRITE;
/*!40000 ALTER TABLE `upload_excel` DISABLE KEYS */;
/*!40000 ALTER TABLE `upload_excel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(45) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `email` varchar(254) NOT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) unsigned DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) unsigned NOT NULL,
  `last_login` int(11) unsigned DEFAULT NULL,
  `active` tinyint(1) unsigned DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'127.0.0.1','administrator','$2a$07$SeBknntpZror9uyftVopmu61qg0ms8Qv1yV6FG.kQOSM.9QhmTo36','','admin@admin.com','','U8rvlZMiM5zVLdNeTs0NUu325640b80c14dc4270',1542754338,NULL,1268889823,1544222796,1,'Admin','istrator','ADMIN','0'),(2,'127.0.0.1','butrint.xh.babuni@gmail.com','$2y$08$ChXCZyc55U8V4P9Pg/dM9.q.672UI4zP6XrquNz.NNDkcX25y3TIm',NULL,'butrint.xh.babuni@gmail.com',NULL,NULL,NULL,NULL,1543618131,1543703455,1,'Butrint','Babuni','BBSOFT','044424235'),(3,'127.0.0.1','granitpeja20@gmail.com','$2y$08$Zpbyk7JNN0tlKVJiMvp1Sept0j3DZH1ba8PvHT6OsaFkauLJgx8sS',NULL,'granitpeja20@gmail.com',NULL,NULL,NULL,NULL,1543618719,NULL,1,'Granit','Peja','BBsoft','044111111');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_groups`
--

DROP TABLE IF EXISTS `users_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users_groups` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `group_id` mediumint(8) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`),
  KEY `fk_users_groups_users1_idx` (`user_id`),
  KEY `fk_users_groups_groups1_idx` (`group_id`),
  CONSTRAINT `fk_users_groups_groups1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_users_groups_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_groups`
--

LOCK TABLES `users_groups` WRITE;
/*!40000 ALTER TABLE `users_groups` DISABLE KEYS */;
INSERT INTO `users_groups` VALUES (5,1,1),(6,1,2),(3,2,2),(7,3,2);
/*!40000 ALTER TABLE `users_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vendimet`
--

DROP TABLE IF EXISTS `vendimet`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vendimet` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nr_protokollit` varchar(70) DEFAULT NULL,
  `emri_lendes` varchar(500) DEFAULT NULL,
  `vlera_parashikuar` decimal(18,2) DEFAULT NULL,
  `lloji_kontrates` varchar(150) DEFAULT NULL,
  `autoriteti_kontraktues` varchar(255) DEFAULT NULL,
  `oe_ankues` varchar(255) DEFAULT NULL,
  `ankesa_kunder` varchar(255) DEFAULT NULL,
  `p_aprovohet_ankesa` int(1) DEFAULT NULL,
  `p_ap_anulohet_vendimi` int(1) DEFAULT NULL,
  `p_terheqje_ankeses` int(1) DEFAULT NULL,
  `p_rivleresim` int(1) DEFAULT NULL,
  `p_ritender` int(1) DEFAULT NULL,
  `p_permiresim_dosjes` int(1) DEFAULT NULL,
  `p_njoftim` int(1) DEFAULT NULL,
  `p_refuzohet_ankesa` int(1) DEFAULT NULL,
  `p_ankesa_pasafatshme` int(1) DEFAULT NULL,
  `p_ankesa_vertetohet_vendimi` int(1) DEFAULT NULL,
  `p_ap_vertetohet_vendimi` int(1) DEFAULT NULL,
  `p_urdherese` int(1) DEFAULT NULL,
  `vendimi` varchar(255) DEFAULT NULL,
  `p_terheqje_verejtje` int(1) DEFAULT NULL,
  `neni` varchar(50) DEFAULT NULL,
  `komente_paneli` text,
  `nr_vendimit` text,
  `vendimet_meparshme` text,
  `vendimi_jokonsistent` text,
  `konfiskim_tarifes` int(1) DEFAULT NULL,
  `kryetar` varchar(100) DEFAULT NULL,
  `referent` varchar(100) DEFAULT NULL,
  `anetari_1` varchar(100) DEFAULT NULL,
  `anetari_2` varchar(100) DEFAULT NULL,
  `anetari_3` varchar(100) DEFAULT NULL,
  `nr_panelisteve` int(11) DEFAULT NULL,
  `rekomandimi_ekspertit` varchar(255) DEFAULT NULL,
  `e_aprovohet_ankesa` int(1) DEFAULT NULL,
  `e_ap_anulohet_vendimi` int(1) DEFAULT NULL,
  `e_rivleresim` int(1) DEFAULT NULL,
  `e_ritender` int(1) DEFAULT NULL,
  `e_permiresim_dosjes` int(1) DEFAULT NULL,
  `e_refuzohet_ankesa` int(1) DEFAULT NULL,
  `e_ankesa_pasafatshme` int(1) DEFAULT NULL,
  `e_ankesa_vertetohet_vendimi` int(1) DEFAULT NULL,
  `e_ap_vertetohet_vendimi` int(1) DEFAULT NULL,
  `komente_eksperti` text,
  `eksperti_shqyrtues` varchar(255) DEFAULT NULL,
  `eksperti_teknik` varchar(255) DEFAULT NULL,
  `shqyrtues_jashtem` varchar(255) DEFAULT NULL,
  `shqyrtues_brendshem` varchar(255) DEFAULT NULL,
  `teknik` varchar(255) DEFAULT NULL,
  `seanca` int(1) DEFAULT NULL,
  `data_ankeses` date DEFAULT NULL,
  `data_vendimit` date DEFAULT NULL,
  `data_publikimit` date DEFAULT NULL,
  `komente_gjenerale` text,
  `created_by` int(11) DEFAULT NULL,
  `created_on` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_on` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vendimet`
--

LOCK TABLES `vendimet` WRITE;
/*!40000 ALTER TABLE `vendimet` DISABLE KEYS */;
/*!40000 ALTER TABLE `vendimet` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vendimet_tags`
--

DROP TABLE IF EXISTS `vendimet_tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vendimet_tags` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tag_id` int(11) DEFAULT NULL,
  `vendimi_id` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_on` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_on` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vendimet_tags`
--

LOCK TABLES `vendimet_tags` WRITE;
/*!40000 ALTER TABLE `vendimet_tags` DISABLE KEYS */;
/*!40000 ALTER TABLE `vendimet_tags` ENABLE KEYS */;
UNLOCK TABLES;
