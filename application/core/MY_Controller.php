<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

/* load the MX_Controller class */
require APPPATH."third_party/MX/Controller.php";

class MY_Controller extends MX_Controller {
	public function __construct() 
	{
		// load helpers and libraries
        $this->load->helper('url');
        $this->load->library(array('ion_auth'));
		$this->load->model('Navigation_model');
	}

	protected function view($view_url, $data = NULL){
		// GET THE TEMPLATE STRUCURE , CHECK THE NAVIGATION AUTHORIZATION
		$url_string = $this->uri->uri_string();
		$segs = $this->uri->segment_array();
		$user_groups = $this->ion_auth->get_users_groups()->result();
		$i = 0;
		if(!empty($segs)){
			$abort = TRUE;
			foreach ($segs as $key => $seg) {
				if($i==0){
					$url = $this->uri->segment($i+$key);
				}else{
					$url .= '/'.$seg;
				}
				$template = $this->db->select('n.*')->from('navigation n')->where('url', $url)->where('n.active', 1)->get()->row();
				if(isset($template->id)){
					$header['template'] = $template;
					if($template->authorization){
						$template_groups = $this->db->select('gn.group_id')->from('navigation n')->join('groups_navigation gn', 'gn.navigation_id = n.id')->where('url', $url)->get()->result();
						foreach ($template_groups as $template_group) {
							foreach ($user_groups as $user_group) {
								if($template_group->group_id == $user_group->id){
									$abort = FALSE;
									break;
								}
							}
						}
					}else{
						$abort = FALSE;
					}
				}
				$i++;
			}
			if($abort){
				redirect('/', 'refresh');
			}			
		}else{
			if($url_string == '' || empty($url_string)){
				$this->db->where('id', 1);
			}else{
				$this->db->where('url', $url_string);
			}
			$header['template'] = $this->db->select('n.*')->from('navigation n')->where('n.active', 1)->get()->row();
		}

		// CHECK THE NAVIGATION AUTHORIZATION
		$header['navigation'] = $this->_get_navigation();
		$footer = null;
		$data['legjenda'] = $this->config->item('legjenda');
		$result = $this->load->view('template/header', $header);
		$result .= $this->load->view($view_url, $data);
		$result .= $this->load->view('template/footer', $footer);
		return $result;
	}

	private function _get_navigation(){
		$groups = array();
		$user_groups = $this->ion_auth->get_users_groups()->result();
		$navigation = $this->db->select('n.*')->from('navigation n')->where('n.parent', null)->where('n.active', 1)->where('n.hidden', 0)->order_by('n.index', 'ASC')->get()->result();
		foreach($navigation as $key => $parent){
			if($parent->authorization){
				$navigation[$key]->groups = $this->db->select('gn.group_id')->from('groups_navigation gn')->where('gn.navigation_id', $parent->id)->get()->result();
				$authorized = FALSE;
				foreach ($navigation[$key]->groups as $navigation_group) {
					foreach ($user_groups as $user_group) {
						if($navigation_group->group_id == $user_group->id){
							$authorized = TRUE;
							break;
						}
					}
				}
				if(!$authorized){
					unset($navigation[$key]);
					continue;
				}						
			}
			$navigation[$key]->children = $this->db->select('n.*')->from('navigation n')->where('n.parent', $parent->id)->where('n.active', 1)->where('n.hidden', 0)->order_by('n.index', 'ASC')->get()->result();
			foreach($navigation[$key]->children as $key2 => $child){
				if($child->authorization){
					$navigation[$key]->children[$key2]->groups = $this->db->select('gn.group_id')->from('groups_navigation gn')->where('gn.navigation_id', $child->id)->get()->result();
					$authorized = FALSE;
					foreach ($navigation[$key]->children[$key2]->groups as $child_group) {
						foreach ($user_groups as $user_group) {
							if($child_group->group_id == $user_group->id){
								$authorized = TRUE;
								break;
							}
						}
					}
					if(!$authorized){
						unset($navigation[$key]->children[$key2]);
						continue;
					}						
				}				
			}
		}
		return $navigation;
	}
}