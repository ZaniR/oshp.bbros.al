<?php
$lang['language'] = 'english';
//General
$lang['company_name_uppercase'] = 'DEMOCRACY PLUS';
$lang['company_name'] = 'Democracy Plus';

//Header
$lang['per_ne'] = 'About us';
$lang['si_teperdoret'] = 'Instructions';

//Footer
$lang['footer_text'] = 'Democracy Plus (D+) is an independent and non-profit organization founded by a group of activists who strongly believe in democratic values and in a Kosovo with genuine democracy. The main goal of D+ is to promote democratic values and practices as well as policies that build an active society.';
$lang['perkrahur_nga'] = 'Supported by';

//About
$lang['rreth_projektit'] = 'About the project';
$lang['rreth_projektit_tekst'] = 'Democracy Plus has established a database of PRB Decisions, under the Monitoring of the Procurement Review Body Project, supported by USAID. The purpose of this database is to help any interested party in the decisions and work of the Procurement Review Body (PRB) to easily and simply find and analyze the decisions of the PRB, applying various filters, which help users find the information.';
$lang['tedhenat_nedb'] = "Information in the database";
$lang['tedhenat_nedb_tekst1'] = 'The data in this database are taken from documents published on the PRB website. The data is fed manually to the database, checking for mismatches with dates, company names and other information. Decisions found in the database date from January 1, 2017 to this date.';
$lang['tedhenat_nedb_paragraf'] = 'Database information is accessible to users in a readable format, allowing them to analyze and use different types of filters for practical purposes.
There are 30 parameters on the basis of which decisions can be filtered, in order to be found more easily.
The database also uses "tags" as an additional tool to easily find decisions related to the same issue.
Data can be exported to common formats (PDF, Excel, CSV) for further research.
The database will be updated with all information at all times.';
$lang['kufizimet'] = 'Data limitations';
$lang['kufizimet_tekst'] = 'The data in this database are taken from documents published on the PRB website.
The database does not include decisions that the PRB has not published on the website.
D+ will identify such decisions and request the PRB to access them.
There are also restrictions on the names of experts, for closed sessions in cases where experts failed to participate, or where the decisions of the PRB did not include all the information.
D+ will be committed to continuously provide all expert names.';
$lang['rreth_donatorit'] = 'About the donor';
$lang['rreth_donatorit_tekst'] = 'The database of PRB decisions is supported by USAID through the Transparent, Effective and Accountable Municipalities (USAID TEAM) activity.';
$lang['rreth_donatorit_tekstother'] =  'USAID established the five-year activity titled Transparent, Effective and Accountable Municipalities (USAID TEAM) to support the Government of Kosovo in implementing anti-corruption reforms and improve the transparency and accountability of the procurement process in 38 Kosovo municipalities.
USAID’s Transparent, Effective and Accountable Municipalities in Kosovo activity strives to strengthen accountability and effectiveness in public procurement, as a tool to improve public services and increase citizens’ trust, especially at the municipal level.';
$lang['sqarim'] = 'Clarification';
$lang['sqarim_tekst'] = 'This database was developed by Democracy Plus and is supported by USAID through the Transparent, Effective and Accountable Municipalities (USAID TEAM) activity.
Information in this database are not subject to the control or consent of the donor and therefore does not necessarily reflect the views of the United States Agency for International Development (USAID) or the Government of the United States of America.';
$lang['pronesi'] = 'The portal is owned and managed by Democracy Plus';
$lang['rruga'] = 'Street: Tirana Block C/4/1, Entrance C, 8th floor, No. 83/84';


//Vendimet
$lang['on_delete'] = 'Data were successfully deleted.';
$lang['filtro'] = 'Filter according to parameters';
$lang['dataPublikimit'] = 'Publication date';
$lang['vleraParashikuar'] = 'Estimated value';
$lang['autoritetiKontraktues'] = 'Contracting Authority';
$lang['OEAnkues'] = 'Complainant EO';
$lang['paneli'] = 'Panel';
$lang['vendimetPanelit'] = 'Decisions of the panel';
$lang['rekomandimet'] = 'Experts’ recommendations';
$lang['nenet'] = 'Articles';
$lang['neni'] = 'Article';
$lang['keywords'] = 'Keywords';
$lang['noKeyWords'] = 'There are no keywords';
$lang['ngaData'] = 'From date';
$lang['deriData'] = 'Until date';
$lang['vleraMinimale'] = 'Minimum value';
$lang['vleraMaximale'] = 'Maximum value';
$lang['panelistat'] = 'Panelists';
$lang['perditsimi'] = 'Last update';
$lang['nrProtokollit'] = 'No. of protocol';
$lang['emriLendes'] = 'Case name';
$lang['llojiKontrates'] = 'Type of contract';
$lang['ankesaKunder'] = 'Complaint against';
$lang['vendimi'] = 'Decision';
$lang['vendimet'] = 'Decisions';
$lang['terheqjeVerejtjes'] = 'Reprimand (Panel)';
$lang['komentePaneli'] = 'Comments regarding the panel’s decision';
$lang['nrVendimit'] = 'No. of Decision';
$lang['vendimetOld'] = 'Previous decisions';
$lang['joKonsistent'] = 'The decision is inconsistent with the decision';
$lang['konfiskimTarif'] = 'Confiscation of tariff';
$lang['kryetarPanel'] = 'Chair of the Panel';
$lang['referentPanel'] = 'Panel clerk';
$lang['anetari_I'] = 'First member of the panel';
$lang['anetari_II'] = 'Second member of the panel';
$lang['anetari_III'] = 'Third member of the panel';
$lang['nrPanelist'] = 'Number of panelists';
$lang['komentEksperti'] = 'Comment (Expert)';
$lang['ekspertiShqyrtues'] = 'Review expert';
$lang['ekspertiTeknik'] = 'Technical expert';
$lang['seancaHM'] = 'Open / Closed Session';
$lang['dataAnkeses'] = 'Date of complaint';
$lang['dataVendimit'] = 'Date of decision'; 
$lang['kohaAV'] = 'Complaint/Decision Duration';
$lang['kohaVP'] = 'Decision/Publish Duration';
$lang['kohaAP'] = 'Complaint/Publish Duration';
$lang['komentePergjithshem'] = 'General comments';
$lang['kerkimAvancuar'] = 'Advanced search';
$lang['shtoVendim'] = 'Add decision';
$lang['zgjedhje'] = 'Selection';
$lang['pastroKerkimin'] = 'Clear search';
$lang['vendimetKritere'] = 'PRB decisions based on selected parameters';
$lang['zgjedhKolonat'] = 'Select columns';
$lang['zgjedhTeGjitha'] = 'Select all / none';
$lang['kolonatFillestare'] = 'Restore initial columns';
$lang['shfaqRreshta'] = 'Display %d rows';
$lang['gjithRreshtat'] = 'All rows';
$lang['startEnd'] = 'Display _START_ until _END_ from _TOTAL_ data';
$lang['prit'] = 'Please wait...';
$lang['empty'] = 'No data';
$lang['faqjaPrevious'] = 'Previous page';
$lang['faqjaTjeter'] = 'Next page';
$lang['faqjaPare'] = 'First page';
$lang['faqjaFundit'] = 'Last page';
$lang['search'] = 'Search';
$lang['teGjitha'] = 'All';
$lang['rezultatetKriter'] = 'Results according to parameters';
$lang['datat'] = 'Dates';
$lang['nga'] = 'from';
$lang['deri'] = 'until';
$lang['aktivitetiProkurimit'] = 'Procurement activity';
$lang['ankesa'] = 'Appeal';
$lang['afatet'] = 'Deadlines';
$lang['kerkesaOE'] = 'Claim against EO';
$lang['pretendimet'] = 'CA claims';
$lang['diskualifikimi'] = 'Disqualification / Duration';
$lang['listazezeKritere'] = 'PRB blacklist based on selected parameters';
$lang['totalMAX'] = '(from total _MAX_ of data)';
$lang['kerkesa'] = 'Request';
$lang['vendimiListzeze'] = 'Decision to blacklist';
$lang['eksperti'] = 'Expert';
$lang['dataKerkeses'] = 'Date of Request';
$lang['operatoriEkonomik'] = 'Economic Operator';
$lang['gjobeAK'] = 'Fine against CA';
$lang['shuma'] = 'Amount';
$lang['heqjeLicense'] = 'Request for license revocation';
$lang['gjobaKriter'] = 'PRB fines based on selected parameters';
$lang['rekomanduarOE'] = 'Recommended EO';
$lang['pezullimetKriter'] = 'PRB suspensions based on selected parameters';
$lang['aprovimRefuzim'] = 'Approval/Refusal to lift the suspension';
$lang['lista_e_zeze'] = 'Blacklist';
$lang['gjobe'] = 'Fine';
$lang['heqje_pezullimi'] = 'Removal of Suspension';
$lang['kerkoMeVite'] = 'Search according to years';
$lang['Vendimet për ankesat e OE'] = 'Decisions on EO complaints';
$lang['Databaza e vendimeve të OSHP-së'] = 'Database of PRB decisions';
$lang['Vendimet për futje në listë të zezë'] = 'Decisions to blacklist';
$lang['Vendimet për gjobë ndaj AK'] = 'Decisions on fines against the CA';
$lang['Vendimet për heqje pezullimi'] = 'Decisions to lift the suspension';
$lang['Është tejkaluar afati ligjor prej 34 ditëve për marrje të vendimit.'] = 'The legal deadline of 34 days for the decision has passed.';
$lang['Ka vendime të mëparshme.'] = 'There are previous decisions.';
$lang['Për Ne'] = 'About us';
$lang['Si të përdoret?'] = 'Instructions';
$lang['siTePerdoret'] = 'How to use the database for PRB decisions';
$lang['katerKategori'] = 'The PRB decisions are divided into four categories';
$lang['ankesatInfo'] = 'Decisions on EO complaints are decisions that of the PRB regarding complaints of EO against the CA for breach of PPL.';
$lang['kolonatStandarte'] = 'Standard columns that appear are:';
$lang['kolonat_stand_img'] = 'http://oshp.dplus-ks.org/assets/images/kolonat_stand.png';
$lang['varesisht_nga_butoni'] = 'Depending on the need, other columns may appear by clicking the Select Columns button';
$lang['kolonat_extra_img'] = 'http://oshp.dplus-ks.org/assets/images/kolonat_extra.png';
$lang['total_qe_shfaqen'] = 'In total, there are 26 columns that can be displayed.';
$lang['databaza_jep_mundsi'] = 'The database provides the opportunity to search decisions in three ways. Firstly, the advanced search divided by categories as in the picture below:';
$lang['kerkimi_avancuar_img'] = 'http://oshp.dplus-ks.org/assets/images/kerkimi_avancuar.png';
$lang['vendimet_nje_muaji'] = 'If we want to display the decisions of a month, for example February 2019, then we have to click on the box “from date”, where the window then appears for the selection of the dates. The same applies with the “until date” box.';
$lang['filtrimi_teDhenave'] = 'Data filtration can also be done through keywords. D+ has analyzed all decisions by placing keywords to ensure that decisions can be found more easily.';
$lang['fjalet_kyqe_img'] = 'http://oshp.dplus-ks.org/assets/images/fjalet_kyqe.png';
$lang['kriteret_zgjedhje'] = 'In the picture above the keyword chosen is the abnormally low price and the date from February 1 until February 28.
The database displays all decisions based on these two parameters.
All parameters can be applied together.
A decision can have one or more keywords.';
$lang['gjendje_fillestare'] = 'To return to the initial state, use the Clear Search button, where all decisions are then listed.';
$lang['menyre_tjeter_kerkimi'] = 'Another way to search is through the Search button.
Anything can be written in this field and the database will display the decisions based on the written word.
For example, if you want to find decisions regarding the Gjilan motorway, then the search terms can be “banulle”, “bresalc”, etc.';
$lang['kerko_img'] = 'http://oshp.dplus-ks.org/assets/images/kerko.png';
$lang['kerkimi_behet'] = 'The search is done directly without having to press Enter or any other button.
Search results appear as soon as you start typing in the search field.
The searched term is highlighted in yellow, as in the picture above.';
$lang['shiko_teDhenat'] = 'To look at the data of a decision click on the blue arrow by each decision.
In the picture above, to view the decision 499/17 info, the data are presented in this form:';
$lang['vendimi_detaje_img'] = 'http://oshp.dplus-ks.org/assets/images/vendimi_detaje.png';
$lang['lexo_vendimin'] = 'To read the decision, click on the link 499/17, and the decision will be opened in PDF format directly from the PRB website.';
$lang['shembull_tjeter'] = 'In another example, if you want to see the decisions for the Municipality of Prishtina only, in the search field type MA Prishtina and all the decisions for this municipality are displayed.';
$lang['kerkimi_1_img'] = 'http://oshp.dplus-ks.org/assets/images/kerkimi_1.png';
$lang['opsioni_shto_kolona'] = 'Using the option to add columns, the database can also find decisions for certain experts.
In an example in the picture below, all the decisions of expert Basri Fazliu are displayed';
$lang['kerkimi_2_img'] = 'http://oshp.dplus-ks.org/assets/images/kerkimi_2.png';
$lang['menyre_tjeter_kerkimi'] = 'Another way of searching is for one column below only';
$lang['kerkimi_3_img'] = 'http://oshp.dplus-ks.org/assets/images/kerkimi_3.png';
$lang['vetem_vendimet'] = 'Again, if we only want the decisions of the Municipality of Prishtina, then in the field “CA” we input MA Prishtina:';
$lang['kerkimi_4_img'] = 'http://oshp.dplus-ks.org/assets/images/kerkimi_4.png';
$lang['eksportimi_titull'] = 'Exporting search results';
$lang['mundesi_eksportimi'] = 'The database allows for the search results to be exported in Excel, PDF and CSV formats, or print them';
$lang['eksporti_img'] = 'http://oshp.dplus-ks.org/assets/images/eksporti.png';
$lang['foto_me_larte'] = 'The photo above shows the exported file to PDF with all decisions for the Municipality of Prishtina.';
$lang['liste_e_zeze_info'] = 'Blacklist decisions are CA decisions seeking disqualification of an EO.
For this section no advanced serach is available, as the number of decisions is small and they can be found with ease.
There are 22 total columns that can be displayed.';
$lang['vendimet_gjoba'] = 'Decisions on fines.';
$lang['gjoba_info'] = 'The PRB takes decisions to fine any CA when they commit continuous violations or when they do not comply with the decisions and orders of the PRB. There are 19 total columns that can be displayed. There is no advanced search for this section either, as there are not many decisions.
';
$lang['heqje_pezullimi_info'] = 'A procurement activity is automatically suspended if there is a complaint filed to the PRB. Contracting authorities request for the suspension to be lifted if they consider that the public interest is greater than that of the complaining EO. The PRB takes a decision to approve or reject the request for suspension.
A total of 17 columns can appear.
';







