<?php
$lang['language'] = 'serbian';
//General
$lang['company_name_uppercase'] = 'DEMOKRATIJA PLUS';
$lang['company_name'] = 'Demokratija Plus';

//Header
$lang['per_ne'] = 'O nama';
$lang['si_teperdoret'] = 'Kako se koristi?';

//Footer
$lang['footer_text'] = 'Demokratija Plus (D+) je nezavisna i neprofitna organizacija koju je osnovala grupa aktivista koji snažno veruju u demokratske vrednosti i Kosovo sa istinskom demokratijom. Glavni je cilj D+ da podstakne demokratske vrednosti i prakse, kao i politike koje grade aktivno društvo.';
$lang['perkrahur_nga'] = 'Podržao';

//About
$lang['rreth_projektit'] = 'O projektu';
$lang['rreth_projektit_tekst'] = 'Demokratija Plus je u sklopu projekta za praćenje rada Tela za razmatranje nabavki, kreirala bazu podataka odluka TRN-a, uz podršku USAID-a. Svrha je ove baze podataka da posluži svima onima zainteresovanima za odluke i rad Tela za razmatranje nabavki (TRN,) kako bi lakše i jednostavnije pronašli i analizirali odluke TRN-a na osnovu različitih filtera, koji pomažu korisnicima da pronađu informacije.';
$lang['tedhenat_nedb'] = "Podaci u bazi podataka";
$lang['tedhenat_nedb_tekst1'] = 'Podaci sadržani u ovoj bazi podataka su uzeti iz dokumenata objavljenih na internet stranici TRN-a. Podaci se ubacuju u bazu podataka ručno, takođe proveravajući podatke tamo gde postoje nepoklapanja u datumima, imenima kompanija i drugim podacima. Odluke koje se nalaze u bazi podataka datiraju od 1. januara 2017. do danas.';
$lang['tedhenat_nedb_paragraf'] = 'Podaci u bazi podataka su dostupni korisnicima u čitljivom formatu što im omogućava analizu i upotrebu različitih vrsta filtera u praktične svrhe. Postoji 30 kriterijuma na osnovu kojih se mogu filtrirati odluke, kako bi se lakše pronašle. Baza podataka takođe koristi oznake  „tags“ kao dodatnu alatku za lakše pronalaženje odluka vezanih za isto pitanje. Podaci se mogu izvoziti u uobičajene formate (PDF, Excel, CSV) radi daljih istraživanja. Baza podataka će se sve vreme ažurirati novim podacima.';
$lang['kufizimet'] = 'Ograničenja podataka';
$lang['kufizimet_tekst'] = 'Podaci u ovoj bazi podataka su uzeti iz dokumenata objavljenih na internet stranici TRN-a. Baza podataka ne sadrži odluke koje TRN nije objavio na internet stranici. D + će identifikovati ove odluke i tražiće od TRN-a pristup istima. Postoje i ograničenja u vezi sa imenima eksperata, posebno u zatvorenim sesijama,  u slučajevima u kojima nisu učestvovali eksperti ili gde odluke TRN-a nisu uključivale sve informacije. D + će se založiti u kontinuitetu da obezbedi imena eksperata.';
$lang['rreth_donatorit'] = 'O donatoru';
$lang['rreth_donatorit_tekst'] = 'Baza podataka odluka TRN-a podržana je od strane USAID-a, u sklopu aktivnosti za transparentne, efektivne i odgovorne opštine (USAID TEAM).';
$lang['rreth_donatorit_tekstother'] =  'USAID je osmislio petogodišnju aktivnost za transparentne, efektivne i odgovorne opštine (USAID TEAM) kako bi podržao Vladu Kosova u sprovođenju antikorupcijskih reformi i poboljšao transparentnost i odgovornost u procesu nabavki u 38 kosovskih opština. Aktivnost USAID-a za transparentne, efektivne i odgovorne opštine na Kosovu radi na jačanju snošenja odgovornosti i efektivnosti u javnim nabavkama, kao sredstvo za poboljšanje javnih usluga i povećanje poverenja građana, posebno na opštinskom nivou.';
$lang['sqarim'] = 'Pojašnjenje';
$lang['sqarim_tekst'] = 'This database was developed by Democracy Plus and is supported by USAID through the Transparent, Effective and Accountable Municipalities (USAID TEAM) activity.
Information in this database are not subject to the control or consent of the donor and therefore does not necessarily reflect the views of the United States Agency for International Development (USAID) or the Government of the United States of America.';
$lang['pronesi'] = 'Portal je u vlasništvu i njime upravlja Demokratija Plus';
$lang['rruga'] = 'Ulica: Tirana Blok C/4/1, Ulaz C, VIII sprat, br. 83/84';


//Vendimet
$lang['on_delete'] = 'Podaci su uspešno izbrisani.';
$lang['filtro'] = 'Filtriraj na osnovu kriterijuma';
$lang['dataPublikimit'] = 'Datum objavljivanja';
$lang['vleraParashikuar'] = 'Predviđena vrednost';
$lang['autoritetiKontraktues'] = 'Ugovorni organ';
$lang['OEAnkues'] = 'PO koji podnosi žalbu';
$lang['paneli'] = 'Panel';
$lang['vendimetPanelit'] = 'Odluke panela';
$lang['rekomandimet'] = 'Preporuke eksperata';
$lang['nenet'] = 'Članovi';
$lang['neni'] = 'Article';
$lang['keywords'] = 'Ključna reč';
$lang['noKeyWords'] = 'Nema ključne reči';
$lang['ngaData'] = 'Od dana';
$lang['deriData'] = 'Do dana';
$lang['vleraMinimale'] = 'Minimalna vrednost';
$lang['vleraMaximale'] = 'Maksimalna vrednost';
$lang['panelistat'] = 'Panelisti';
$lang['perditsimi'] = 'Ažurirano poslednji put';
$lang['nrProtokollit'] = 'Br. protokola';
$lang['emriLendes'] = 'Naziv predmeta';
$lang['llojiKontrates'] = 'Vrsta ugovora';
$lang['ankesaKunder'] = 'Žalba protiv';
$lang['vendimi'] = 'Odluka';
$lang['vendimet'] = 'Odluke';
$lang['terheqjeVerejtjes'] = 'Opomena (Panel)';
$lang['komentePaneli'] = 'Komentari u vezi sa odlukom panela';
$lang['nrVendimit'] = 'Br. odluke';
$lang['vendimetOld'] = 'Prethodne odluke';
$lang['joKonsistent'] = 'Odluka nedosledna sa odlukom';
$lang['konfiskimTarif'] = 'Konfiskovanje tarife';
$lang['kryetarPanel'] = 'Predsednik panela';
$lang['referentPanel'] = 'Referent panela';
$lang['anetari_I'] = 'I član panela';
$lang['anetari_II'] = 'II član panelal';
$lang['anetari_III'] = 'III član panela';
$lang['nrPanelist'] = 'Br. članova panela';
$lang['komentEksperti'] = 'Komentar (ekspert)';
$lang['ekspertiShqyrtues'] = 'Ekspert za razmatranje';
$lang['ekspertiTeknik'] = 'Tehnički ekspert';
$lang['seancaHM'] = 'Otvorena/zatvorena sesija';
$lang['dataAnkeses'] = 'Datum žalbe';
$lang['dataVendimit'] = 'Datum odluke'; 
$lang['kohaAV'] = 'Dužina trajanja žalbe/odluke';
$lang['kohaVP'] = 'Dužina trajanja odluke/objavljivanje';
$lang['kohaAP'] = 'Dužina trajanja žalbe/objavljivanje';
$lang['komentePergjithshem'] = 'Opšti komentari';
$lang['kerkimAvancuar'] = 'Napredno pretraživanje';
$lang['shtoVendim'] = 'Dodaj odluku';
$lang['zgjedhje'] = 'Izbor';
$lang['pastroKerkimin'] = 'Izbriši pretragu';
$lang['vendimetKritere'] = 'Odluke TRN-a shodno odabranim kriterijumima';
$lang['zgjedhKolonat'] = 'Izaberi kolone';
$lang['zgjedhTeGjitha'] = 'Izaberi sve/nijednu';
$lang['kolonatFillestare'] = 'Vrati početne kolone';
$lang['shfaqRreshta'] = 'Prikaži %d redova';
$lang['gjithRreshtat'] = 'Svi redovi';
$lang['startEnd'] = 'Prikazano _START_ do _END_ od _TOTAL_ podataka';
$lang['prit'] = 'Molimo vas sačekajte...';
$lang['empty'] = 'Nema podataka';
$lang['faqjaPrevious'] = 'Prethodna stranica';
$lang['faqjaTjeter'] = 'Naredna stranica';
$lang['faqjaPare'] = 'Prva stranica';
$lang['faqjaFundit'] = 'Poslednja stranica';
$lang['search'] = 'Pretraživanje';
$lang['teGjitha'] = 'Sve';
$lang['rezultatetKriter'] = 'Rezultati na osnovu kriterijuma';
$lang['datat'] = 'Datumi';
$lang['nga'] = 'od';
$lang['deri'] = 'do';
$lang['aktivitetiProkurimit'] = 'Aktivnost nabavke';
$lang['ankesa'] = 'Žalba';
$lang['afatet'] = 'Rokovi';
$lang['kerkesaOE'] = 'Zahtev protiv PO';
$lang['pretendimet'] = 'Navodi UO';
$lang['diskualifikimi'] = 'Diskvalifikacija/Dužina trajanja';
$lang['listazezeKritere'] = 'Crna lista TRN-a na osnovu izabranih kriterijuma';
$lang['totalMAX'] = '(od ukupno _MAX_ podataka)';
$lang['kerkesa'] = 'Zahtev';
$lang['vendimiListzeze'] = 'Odluka o crnoj listi';
$lang['eksperti'] = 'Ekspert';
$lang['dataKerkeses'] = 'Datum podnošenja zahteva';
$lang['operatoriEkonomik'] = 'Privredni operater';
$lang['gjobeAK'] = 'Novčana kazna protiv UO';
$lang['shuma'] = 'Iznos';
$lang['heqjeLicense'] = 'Zahtev za oduzimanje licence';
$lang['gjobaKriter'] = 'Novčane kazne TRN-a shodno izabranim kriterijumima';
$lang['rekomanduarOE'] = 'Preporučeni PO';
$lang['pezullimetKriter'] = 'Obustave TRN-a na osnovu izabranih kriterijuma';
$lang['aprovimRefuzim'] = 'Usvajanje/odbijanje ukidanja obustave';
$lang['lista_e_zeze'] = 'Crna lista';
$lang['gjobe'] = 'Novčana kazna';
$lang['heqje_pezullimi'] = 'Ukidanje obustave';
$lang['kerkoMeVite'] = 'Pretraživanje po godinama';
$lang['Vendimet për ankesat e OE'] = 'Odluke na žalbe PO';
$lang['Databaza e vendimeve të OSHP-së'] = 'Baza podataka odluka TRN-a';
$lang['Vendimet për futje në listë të zezë'] = 'Odluke o uvršćivanju na crnu listu';
$lang['Vendimet për gjobë ndaj AK'] = 'Odluke o novčanoj kazni UO';
$lang['Vendimet për heqje pezullimi'] = 'Odluke o ukidanju obustave';
$lang['Është tejkaluar afati ligjor prej 34 ditëve për marrje të vendimit.'] = 'Prekoračen zakonski rok od 34 dana za donošenje odluke.';
$lang['Ka vendime të mëparshme.'] = 'Postoje prethodne odluke.';
$lang['Për Ne'] = 'O nama';
$lang['Si të përdoret?'] = 'Kako se koristi?';
$lang['siTePerdoret'] = 'Kako se koristi baza podataka odluka TRN-a';
$lang['katerKategori'] = 'Odluke TRN-a su podeljene u četiri kategorije';
$lang['ankesatInfo'] = 'Odluke o žalbama PO predstavljaju odluke koje TRN donosi na žalbe PO protiv UO, zbog povrede ZIP.';
$lang['kolonatStandarte'] = 'Standardne kolone koje se pojavljuju su:';
$lang['kolonat_stand_img'] = 'http://oshp.dplus-ks.org/assets/images/kolonat_stand.png';
$lang['varesisht_nga_butoni'] = 'Po potrebi se mogu prikazati i druge kolone, klikom na taster Odaberi kolone';
$lang['kolonat_extra_img'] = 'http://oshp.dplus-ks.org/assets/images/kolonat_extra.png';
$lang['total_qe_shfaqen'] = 'Ukupno se prikazuje 26 kolona.';
$lang['databaza_jep_mundsi'] = 'Baza podataka pruža mogućnost pretraživanja odluka na tri načina. Prvo, napredno pretraživanje podeljeno po kategorijama, kao na slici ispod:';
$lang['kerkimi_avancuar_img'] = 'http://oshp.dplus-ks.org/assets/images/kerkimi_avancuar.png';
$lang['vendimet_nje_muaji'] = 'Ako želimo da prikažemo odluke iz jednog meseca, na primer iz februara 2019. godine, onda moramo da kliknemo na rubriku  „od dana“, nakon čega će se pojaviti prozor za odabir datuma. Isto bi trebalo uraditi i u rubrici „do dana“';
$lang['filtrimi_teDhenave'] = 'Filtriranje podataka se takođe može izvršiti pomoću ključnih reči. D + je analizirao sve odluke unošenjem ključnih reči, kako bi se odluke mogle lakše pronaći.';
$lang['fjalet_kyqe_img'] = 'http://oshp.dplus-ks.org/assets/images/fjalet_kyqe.png';
$lang['kriteret_zgjedhje'] = 'Na slici gore, kao ključna reč izabrana je Neuobičajeno niska cena i datum od 1. do 28. februara. Baza podataka prikazuje sve odluke na osnovu ova dva kriterijuma. Svi kriterijumi se mogu koristiti zajedno. Ista odluka može imati jednu ili više ključnih reči.';
$lang['gjendje_fillestare'] = 'Da biste se vratili na početak, pritisnite taster Obriši pretragu, gde se nakon toga, navode sve odluke.';
$lang['menyre_tjeter_kerkimi'] = 'Drugi način pretraživanja je taster Pretraživanje. U ovo polje se može uneti bilo šta a baza podataka će prikazati odluke na osnovu napisane reči. Na primer, ako želite da pronađete odluke za autoput ka Gnjilanu, onda termin kojim možete vršiti pretraživanje može biti „banula“, „bresaljce“ itd.';
$lang['kerko_img'] = 'http://oshp.dplus-ks.org/assets/images/kerko.png';
$lang['kerkimi_behet'] = 'Pretraživanje se vrši direktno bez potrebe da pritisnete bilo koji taster ili Enter. Rezultati pretraživanja se pojavljuju čim počnete da kucate u polju za pretraživanje. Traženi pojam je označen žutom bojom, kao na gornjoj slici.';
$lang['shiko_teDhenat'] = 'Da biste videli  podatke odluke, kliknite na plavu strelicu koja se nalazi pored svake odluke. Na slici gore, da biste videli podatke odluke 499/17, podaci su predstavljeni u ovom obliku:';
$lang['vendimi_detaje_img'] = 'http://oshp.dplus-ks.org/assets/images/vendimi_detaje.png';
$lang['lexo_vendimin'] = 'Da pročitate odluku, kliknite na link 499/17, gde se odluka otvara u PDF formatu direktno sa internet stranice TRN-a.';
$lang['shembull_tjeter'] = 'Drugi primer je ako želite da vidite odluke samo za Opštinu Priština, u polju za pretraživanje upišite SO Priština i prikazaće vam se sve odluke za ovu opštinu.';
$lang['kerkimi_1_img'] = 'http://oshp.dplus-ks.org/assets/images/kerkimi_1.png';
$lang['opsioni_shto_kolona'] = 'Korišćenjem opcije za dodavanje kolona, u bazi podataka možete naći i odluke po konkretnim ekspertima. Primer na slici ispod su sve odluke u kojima je Basri Fazliu bio ekspert.';
$lang['kerkimi_2_img'] = 'http://oshp.dplus-ks.org/assets/images/kerkimi_2.png';
$lang['menyre_tjeter_kerkimi'] = 'Drugi način pretraživanja je pretraga samo po jednoj koloni koja se nalazi ispod';
$lang['kerkimi_3_img'] = 'http://oshp.dplus-ks.org/assets/images/kerkimi_3.png';
$lang['vetem_vendimet'] = 'Ponovo, ako želimo samo da pronađemo odluke za Opštinu Priština, onda u polju „UO“ upišite SO Priština:';
$lang['kerkimi_4_img'] = 'http://oshp.dplus-ks.org/assets/images/kerkimi_4.png';
$lang['eksportimi_titull'] = 'Izvoženje rezultata pretraživanja';
$lang['mundesi_eksportimi'] = 'Baza podataka omogućuje izvoženje rezultata pretraživanja u Excel, PDF i CSV formate ili čak njihovo štampanje';
$lang['eksporti_img'] = 'http://oshp.dplus-ks.org/assets/images/eksporti.png';
$lang['foto_me_larte'] = 'Slika gore prikazuje datoteku izveženu u PDF formatu, sa svim odlukama za Opštinu Priština.';
$lang['liste_e_zeze_info'] = 'Odluke o ubacivanju na crnu listu su odluke u kojima UO traži da se neki PO diskvalifikuje. U ovom delu ne postoji mogućnost naprednog pretraživanja, jer je broj odluka mali i može se lako pronaći. Prikazuju se ukupno 22 kolone.';
$lang['vendimet_gjoba'] = 'Odluke o novčanim kaznama';
$lang['gjoba_info'] = 'TRN donosi odluku da novčano kazni neki UO kada on počini prekršaje u kontinuitetu ili kada ne ispoštuje odluke i naredbe TRN-a. Ima ukupno 19 kolona koje se mogu prikazati. I u ovom delu, ne postoji mogućnost naprednog pretraživanja, pošto nema puno odluka.';
$lang['heqje_pezullimi_info'] = 'Jedna aktivnost nabavke se automatski obustavlja ako se izjavi žalba TRN-u. Ugovorni organi podnose zahteve da se ukine obustava, kada smatraju da je javni interes veći od interesa PO koji podnosi žalbu. TRN odlučuje da odobri ili odbije zahtev za obustavu. Ukupno se prikazuje 17 kolona.';






